install the composer packages

```
composer install
```

clear all the caches

```
php artisan config:clear
```


```
php artisan view:clear
```


```
php artisan route:clear
```

also check if **pdo_pgsql** extension is enable in **php.ini** file.
