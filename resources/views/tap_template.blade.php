<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<style type="text/css">
		.text-center{
			text-align: center;
		}
		.m-0{
			margin: 0;
		}
		.mt-5{
			margin-top: 5px;
		}
		.p-0{
			padding: 0;
		}
		.pl-5{
			padding-left: 5px;
		}
		.pr-5{
			padding-right: 5px;
		}
		.pt-5{
			padding-top: 5px;
		}
		.pt-2{
			padding-top: 2px;
		}
		.pb-2{
			padding-bottom: 2px;
		}
		.b-1{
			border: 1px solid black;
		}
		.w-25{
			width: 25%;
		}
		.w-30{
			width: 30%;
		}
		.w-40{
			width: 40%;
		}
		.w-49{
			width: 49%;
		}
		.w-50{
			width: 50%;
		}
		.w-60{
			width: 60%;
		}
		.w-100{
			width: 100%;
		}
		.h-25{
			height: 25%;
		}
		.h-50{
			height: 50%;
		}
		.h-75{
			height: 75%;
		}
		.h-100{
			height: 100%;
		}
		.header{
			background-color: black;
			color: white;
		}
		.inline{
			float:left;
		}
		.clear{
			clear:both;
		}
		.bg-black{
			background-color: black;
		}
		.bg-cyan{
			background-color: #68bab5;
		}
		.bg-yellow{
			background-color: #dbd86b;
		}
		.white{
			color: white;
		}
		.cyan{

		}
		.bold{
			font-weight: bold;
		}
		.underline{
			text-decoration: underline;
		}
		.br-1{
			border-right: 1px solid black;
		}
		.bl-1{
			border-left: 1px solid black;
		}
		.bt-1{
			border-top: 1px solid black;
		}
		.bb-1{
			border-bottom: 1px solid black;
		}
		p{
			font-size: 10px;
			padding: 0;
			margin: 0;
		}
	</style>
</head>
<body>
	<div class="w-100 h-100">
		<h2 class="text-center">Work Product</h2>
		<div class="inline w-30">
			<p class="bold m-0">Student's Name: {{$student_name}}</p>
		</div>
		<div class="inline w-30">
			<p class="bold m-0">Class: {{$class_name}}</p>
		</div>
		<div class="inline w-40">
			<p class="bold m-0">Standard: {{$standard}}</p>
		</div>
		<div class="clear"></div>
		<p class="bold m-0">Student's Email: {{$email}}</p>
		<p class="bold m-0">Teachers's Name: {{$teacher_name}}</p>
		<div class="w-100 bg-black">
			<div class="inline w-25 pt-2 pb-2 m-0">
				<p class="text-center white p-0 m-0">Form 1A</p>
				<p class="text-center white p-0 m-0">(Initial Thinking)</p>
			</div>
			<div class="inline w-25 p-0 pt-2 pb-2 m-0">
				<p class="text-center white p-0 m-0">Form 1A</p>
				<p class="text-center white p-0 m-0">(After Thinking)</p>
			</div>
			<div class="inline w-50 p-0 pt-2 pb-2 m-0">
				<p class="text-center white p-0 m-0">Form 2</p>
				<p class="text-center white p-0 m-0">(Application of Learned Skill)</p>
			</div>
			<div class="clear"></div>
		</div>
		<div class="inline w-60 p-0 m-0 bg-cyan">
			<p class="bold pl-5 pt-2 pb-2">Form 1A/1B Article Title</p>
		</div>
		<div class="inline w-40 p-0 m-0 bg-yellow">
			<p class="bold pl-5 pt-2 pb-2">Form 2 Article Title</p>
		</div>
		<div class="clear"></div>
		<div class="w-100 h-100">
			<div class="inline w-60 h-25 p-0 m-0">
				<div class="w-100 h-100 bl-1 bb-1">
					<div class="pl-5 pt-5 bold underline w-100">
						@empty($form_1A['title'])
							{!! $form_1B['title'] !!}
						@else
							{!! $form_1A['title'] !!}
						@endempty
					</div>
					<div class="pl-5 pt-5 bold underline w-100 h-25">
						@empty($form_1A['question'])
							{!! $form_1B['question'] !!}
						@else
							{!! $form_1A['question'] !!}
						@endempty
					</div>
					<div class="w-100 bg-black">
						<div class="inline w-50 pt-2 pb-2 m-0">
							<p class="text-center white">INITIAL Answer And Thinking(1A)</p>
						</div>
						<div class="inline w-50 pt-2 pb-2 m-0">
							<p class="text-center white">AFTER Answer And Thinking(1A)</p>
						</div>
						<div class="clear"></div>
					</div>
					<div class="inline w-49 h-25 p-0 m-0 br-1">
						<p class="pl-5 pt-5 m-0 underline">Form 1A Answer:</p>
						<div class="pl-5 pt-5 m-0">{!! $form_1A['answer'] !!}</div>
					</div>
					<div class="inline w-50 h-25 p-0 m-0">
						<p class="pl-5 pt-5 m-0 underline">Form 2A Answer:</p>
						<div class="pl-5 pt-5 m-0">{!! $form_1B['answer'] !!}</div>
					</div>
					<div class="clear"></div>
					<div class="w-100 h-100 bt-1">
						<div class="inline w-49 h-100 p-0 m-0 br-1">
							<p class="pl-5 pt-5 m-0 underline">Form 1A Explain Your Thinking:</p>
							<div class="pl-5 pt-5 m-0">{!! $form_1A['thinking'] !!}</div>
						</div>
						<div class="inline w-50 h-100 p-0 m-0">
							<p class="pl-5 pt-5 m-0 underline">Form 2A Explain Your Thinking:</p>
							<div class="pl-5 pt-5 m-0">{!! $form_1B['thinking'] !!}</div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
			<div class="inline w-40 h-100 p-0 m-0">
				<div class="w-100 h-100 b-1">
					<div class="h-25 pl-5 pt-5 pr-5">
						@empty($form_2['title'])
							
						@else
							{!! $form_2['title'] !!}
						@endempty
						
					</div>
					<div class="h-25 pl-5 pt-5 pr-5">
						@empty($form_2['question'])
							
						@else
							{!! $form_2['question'] !!}
						@endempty
						
					</div>
					<div class="h-25 pl-5 pt-5 pr-5">
						<p class="bold underline">Form 2 Answer</p>
						@empty($form_2['answer'])
							
						@else
							{!! $form_2['answer'] !!}
						@endempty
						
					</div>
					<div class="pl-5 pt-5 pr-5">
						<p class="bold underline">Form 2 Explain Your Thinking</p>
						
						@empty($form_2['thinking'])
							
						@else
							{!! $form_2['thinking'] !!}
						@endempty
						
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
