<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post("auth/login", 'LogInController@login');
Route::group(['middleware' => 'auth:api'], function () {
    Route::apiResource('accounts', 'API\AccountsController');
    Route::apiResource('schools', 'API\SchoolController');
    Route::apiResource('levels', 'API\LevelsController');
    Route::apiResource('standard', 'API\StandardController');
    Route::apiResource('groups', 'API\GroupsController');
    Route::apiResource('protocols', 'API\ProtocolsController');
    Route::apiResource('profile', 'API\ProfileController');
    Route::apiResource("questions", 'API\QuestionsController');
    Route::apiResource("question_type", 'API\QuestionTypeController');
    Route::apiResource("roles", 'API\RolesController');
    Route::apiResource("account-roles", 'API\AccountRolesController');
    Route::apiResource("account-types", 'API\AccountTypesController')->only(['index']);
    Route::apiResource("actions", 'API\ActionsController');
    Route::apiResource("policy", 'API\PolicyController');
    Route::apiResource("resources", 'API\ResourcesController');
    Route::apiResource("resource-policy", 'API\ResourcePoliciesController');
    Route::apiResource("periods", 'API\PeriodsController');
    Route::apiResource("question/subjects", 'API\QuestionSubjectsController');
    Route::apiResource("question/cycles", 'API\QuestionCyclesController');
    Route::apiResource("question/grades", 'API\QuestionGradesController');
    Route::apiResource("invitation/groups", 'API\InvitationGroupsController');
    Route::apiResource("censorship/levels", 'API\CensorshipLevelsController');
    Route::apiResource("censorship/word/lists", 'API\CensorshipWordListsController');
    Route::apiResource("assessment/periods", 'API\AssessmentPeriodsController');
    Route::apiResource("invitation/group/accounts", 'API\InvitationGroupAccountsController')->except(['index','show','update']);
    Route::apiResource("files", "API\FilesController")->except(["show", "update"]);
    Route::apiResource("announcements", "API\AnnouncementsController")->except(["update", "show"]);

    Route::apiResource('course-grade-categories', 'API\CourseGradeCategoryController');
    Route::apiResource('assessments', 'API\AssessmentController');
    Route::apiResource('requested/classes', 'API\RequestedClassesController');
    Route::get('assessments-questions', 'API\AssessmentController@questionList');
    Route::get('assessments-questions/{id}', 'API\AssessmentController@getQuestion');
    Route::get('assessments-unfinished', 'API\AssessmentController@getUnfinished');
    Route::delete('assessments-questions/{id}', 'API\AssessmentController@deleteAssessmentQuestion');
    Route::apiResource('assessment-templates', 'API\AssessmentTemplateController');
    Route::apiResource('topics', 'API\TopicsController')->except(['show']);

    Route::get("draft/questions", "API\DraftQuestionsController@index");
    Route::post("status-update/questions/{id}", "API\QuestionStatusController@updateStatus");

    Route::get("level/courses", "API\CourseListLevelWiseController@index");
    Route::get("student/course/work/details/{courseId}/{courseWorkId}/{userId?}","API\StudentClassWorkController@submissionDetails");
    Route::post("student/course/work/submit/{id}/{courseId}/{courseWorkId}","API\StudentClassWorkController@submitCourseWork");
    Route::get("courses", 'API\CoursesController@index');
    Route::post("courses", 'API\CoursesController@create');
    Route::post("courses/{id}/save/google/meet/link", 'API\CoursesController@saveGoogleMeetLink');
    Route::get("courses/{id}", 'API\CoursesController@get');
    Route::put("courses/{id}", 'API\CoursesController@update');
    Route::patch("courses/{id}", 'API\CoursesController@patch');
    Route::get('sync-courses', 'API\CoursesController@syncCourses');
    Route::post("course/student-remove/{courseId}/{studentId}", "API\RemoveStudentFromCourseController@removeStudent");

    Route::apiResource("course/teachers", 'API\TeachersController');
    Route::apiResource("course/students", 'API\StudentsController');
    Route::apiResource("grade/settings","API\GradeSettingsController")->except(["index"]);

    Route::get("invitations/{courseId}", 'API\InvitationsController@courseInvitations');
    Route::get("pending/invitations", 'API\InvitationsController@userInvitations');
    Route::post("invitations/student", 'API\InvitationsController@createStudent');
    Route::post("invitations/accept/{id}", 'API\InvitationsController@acceptInvitation');
    Route::post("invitations/delete/{id}", 'API\InvitationsController@deleteInvitation');
    Route::post("invitations/teacher", 'API\InvitationsController@createTeacher');
    Route::post("invitations/{studentId}/gurdian", 'API\InvitationsController@createGurdian');
    Route::post("sync/course/invitations/{courseId}", 'API\InvitationsController@syncCourseInvitations');
    Route::post("invite/student/groups/{id}","API\InvitationsController@sendGroupInvitationStudents");
    Route::get("question/topics","API\QuestionTopicsController@topics");


    Route::prefix('courses/{courseId}')->group(function() {
        Route::get('course-works', 'API\CourseWorkController@index');
        Route::get('sync-course-works', 'API\CourseWorkController@syncCourseWorks');
        Route::post('course-works', 'API\CourseWorkController@create');
        Route::put('course-work/{courseWorkId}', 'API\CourseWorkController@update');
        Route::delete('course-work/{courseWorkId}', 'API\CourseWorkController@delete');

        Route::get('course-work-materials', 'API\CourseWorkController@index');
        Route::post('course-work-materials', 'API\CourseWorkController@create');

        Route::get('course-work/{courseWorkId}/student-submissions', 'API\StudentClassWorkController@index');
        Route::get('course-work/{courseWorkId}/student-submissions/{id}/{user_id}', 'API\StudentClassWorkController@get');
        Route::post('course-work/{courseWorkId}/student-submissions/{id}', 'API\StudentClassWorkController@studentSubmission');
        Route::post('course-work/{courseWorkId}/grade-submissions/{id}', 'API\StudentClassWorkController@gradeSubmission');
        Route::post("course-work/{courseWorkId}/return-submission/{id}","API\CourseWorkReturnStudentSubmissionController@returnToStudent");
    });

    Route::prefix('dashboard')->group(function (){
        Route::get("question-bank","API\Dashboard\QuestionBankController@index");
        Route::post("question-bank/search/questions","API\Dashboard\QuestionBankController@questions");
        Route::post("question-bank/search/accounts","API\Dashboard\QuestionBankController@accounts");
        Route::post("question-bank/search/subjects","API\Dashboard\QuestionBankController@searchSubject");
        Route::post("question-bank/search/grades","API\Dashboard\QuestionBankController@searchGrade");
        Route::post("question-bank/search/schools","API\Dashboard\QuestionBankController@searchSchool");
        //grade book dashboard
        Route::get("grade-book","API\Dashboard\GradeBookController@index");
        Route::get("grade-book/reset/grades","API\Dashboard\GradeBookController@resetGrades");
        Route::post("grade-book/filter","API\Dashboard\GradeBookController@filter");
        //attendance dashboard
        Route::get("attendance","API\Dashboard\AttendanceDashboardController@index");
        Route::get("attendance/student-details/{student}","API\Dashboard\AttendanceDashboardController@studentDetails");

        Route::get('student-enrollment', "API\Dashboard\DashboardController@studentEnrollment");
        Route::get('student-assignment', "API\Dashboard\DashboardController@studentAssignment");
        Route::get('live-instructions', "API\Dashboard\DashboardController@liveInstructions");

        Route::get('account-list', "API\AutoSuggestController@accountList");
        Route::get('student-list', "API\AutoSuggestController@studentList");
        Route::get('school-list', "API\AutoSuggestController@schoolList");
        Route::get('teacher-list', "API\AutoSuggestController@teacherList");
        Route::get('grade-list', "API\AutoSuggestController@gradeList");
        Route::get('class-list', "API\AutoSuggestController@classList");
        Route::get('topic-list', "API\AutoSuggestController@topicList");
        Route::get('protocol-list', "API\AutoSuggestController@protocolList");
        Route::get('standard-list', "API\AutoSuggestController@standardList");
        Route::get('subject-list', "API\AutoSuggestController@subjectList");
        Route::get('group-list', "API\AutoSuggestController@groupList");

    });

    Route::get("course-work/questions/{assessment_id}", "API\CourseWorkQuestionsController@index");

    Route::post("question-review/{question_id}","API\QuestionReviewController@save");

    Route::get('reward-point', "API\RewardPointController@getRewardPoint");
    Route::post('reward-point/{type}', "API\RewardPointController@rewardCalculation");
    //import account api routes
    Route::get('import/account/download',"API\ImportAccountController@example");
    Route::post('import/account',"API\ImportAccountController@import");
    //import group account routes
    Route::post('import/group/account',"API\ImportInvitationGroupAccountsController@import");
    //book store route
    Route::get("book/store", "API\BookStoreController@index");    
    Route::get("student/work/product/generate/individual/{id}","API\StudentWorkProductController@generateIndividual");
    //question export 
    Route::post("export/question", "API\QuestionExportController@question");
    //file export
    Route::post("export/file", "API\FileExportController@export");
    //question import
    Route::post("import/questions","API\QuestionsImportController@import");
    //class wise accounts
    Route::get('class-wise-account-list/{class_id}', "API\AutoSuggestController@classWiseAccountList");
    //assessment archive
    Route::post("archive/assessment/{id}", "API\AssessmentArchiveController@archiveAssessment");

    //Live learning Routes
    Route::get("live/learning/assessment/get/{id}", "API\LiveLearning\AssessmentController@get");
    Route::post("live/learning/assessment/start", "API\LiveLearning\AssessmentController@start");
    Route::get("live/learning/assessment/studentlogin/{assessment_id}", "API\LiveLearning\AssessmentController@studentlogin");
    Route::post("live/learning/assessment/status", "API\LiveLearning\AssessmentController@status");
    Route::get("live/learning/assessment/close/{id}", "API\LiveLearning\AssessmentController@close");
    Route::get("live/learning/assessment/summary/{id?}", "API\LiveLearning\AssessmentController@summary");
    Route::get("live/learning/assessment/student/responded/{id}", "API\LiveLearning\AssessmentController@studentResponded");
    Route::get("live/learning/assessment/getstudentRespond/{assessment_id}/{student_id}", "API\LiveLearning\AssessmentController@getstudentRespond");
    Route::get("live/learning/assessment/live/feed/{id}", "API\LiveLearning\AssessmentController@liveFeed");
    Route::get("live/learning/assessment/comment/logs/{id}", "API\LiveLearning\AssessmentController@commentLogs");
    Route::post("posts/comments/add", "API\LiveLearning\CommentsController@add");
    Route::get("posts/comments/get/{id}", "API\LiveLearning\CommentsController@get");
    Route::post("posts/comment/replies/add", "API\LiveLearning\RepliesController@add");
    Route::get("posts/comment/replies/get/{id}", "API\LiveLearning\RepliesController@get");
    Route::post("posts/add", "API\LiveLearning\PostController@add");
    Route::get("posts/get/{id}", "API\LiveLearning\PostController@get");
    Route::get("posts/current/form/{assessment_id}", "API\LiveLearning\PostController@currentFormPosts");
    Route::get("posts/reactions/{assessment_id}/{post_id?}", "API\LiveLearning\PostController@postReactions");
    Route::get("forms/wise/post/reactions/{assessment_id}", "API\LiveLearning\PostController@formWisePostReactions");
    Route::post("posts/reaction/submit", "API\LiveLearning\PostReactionsController@submitReaction");
    Route::get("student/active/assessment", "API\LiveLearning\StudentAssessmentController@activeAssessment");
    Route::post("live/learning/assessment/adjust/time", "API\LiveLearning\AssessmentStatusController@adjustTime");
    Route::get("live/learning/assessment/settings/{assessment_id}", "API\LiveLearning\AssessmentSettingsController@get");
    Route::post("live/learning/assessment/settings/update", "API\LiveLearning\AssessmentSettingsController@update");
    Route::get("live/learning/teachers/snapshot/{assessment_id}","API\LiveLearning\SnapShotController@teachersSnapShot");
    Route::get("live/learning/running/assessment/{assessment_id}","API\LiveLearning\AssessmentController@runningAssessment");
    Route::get("live/learning/running/elapsedtime/{assessment_id}","API\LiveLearning\AssessmentStatusController@elapsedtime");
});

