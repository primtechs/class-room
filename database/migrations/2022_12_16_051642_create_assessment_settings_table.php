<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssessmentSettingsTable extends Migration
{

    public function up()
    {
        Schema::create('assessment_settings', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("assessment_id");
            $table->boolean("can_comment");
        });
    }

    public function down()
    {
        Schema::dropIfExists('assessment_settings');
    }
}
