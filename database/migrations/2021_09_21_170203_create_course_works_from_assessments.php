<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentsFromAssessments extends Migration
{

    public function up()
    {
        Schema::create('course_works_from_assessments', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("course_work_id");
            $table->bigInteger("assessment_id");
        });
    }


    public function down()
    {
        Schema::dropIfExists('assignments_from_assessments');
    }
}
