<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentWorkProductLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_work_product_logs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("assessment_id");
            $table->bigInteger("student_id");
            $table->json("google_drive_response")->default(NULL);
            $table->json("student_submission_response")->default(NULL);
            $table->json("turn_in_response")->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_work_product_logs');
    }
}
