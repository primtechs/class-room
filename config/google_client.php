<?php

return [
    'application' => 'Quickstart',
    'API_KEY'=> env('GOOGLE_API_KEY'),
    'key' => [
       'web' => [
           'client_id' => env('GOOGLE_CLIENT_ID'),
           'project_id' => env('GOOGLE_PROJECT_ID'),
           'auth_uri' => env('GOOGLE_AUTH_URI'),
           'token_uri' => env('GOOGLE_TOKEN_URI'),
           'auth_provider_x509_cert_url' => env('GOOGLE_AUTH_PROVIDER_CERT_URL'),
           'client_secret' => env('GOOGLE_CLIENT_SECRET'),
           'redirect_uris' => explode(',', env('WEB_REDIRECT_URIS')),
           'javascript_origins' => explode(',', env('WEB_JAVASCRIPT_ORIGINS')),
           'scopes'=> [
                "https://www.googleapis.com/auth/classroom.courses",
                "https://www.googleapis.com/auth/classroom.rosters",
                "https://www.googleapis.com/auth/classroom.rosters.readonly",
                "https://www.googleapis.com/auth/classroom.profile.photos",
                "https://www.googleapis.com/auth/classroom.profile.emails",
                "https://www.googleapis.com/auth/classroom.announcements",
                "https://www.googleapis.com/auth/classroom.announcements.readonly",
                "https://www.googleapis.com/auth/classroom.coursework.students.readonly",
                "https://www.googleapis.com/auth/classroom.coursework.me.readonly",
                "https://www.googleapis.com/auth/classroom.coursework.students",
                "https://www.googleapis.com/auth/classroom.coursework.me",
                "https://www.googleapis.com/auth/classroom.topics",
                "https://www.googleapis.com/auth/classroom.topics.readonly",
                "https://www.googleapis.com/auth/classroom.guardianlinks.students.readonly",
                "https://www.googleapis.com/auth/classroom.guardianlinks.students",
                "https://www.googleapis.com/auth/drive",
                "https://www.googleapis.com/auth/drive.file",
                "https://www.googleapis.com/auth/drive.appdata",
                "https://www.googleapis.com/auth/classroom.courseworkmaterials",
                "https://www.googleapis.com/auth/classroom.courseworkmaterials.readonly"
           ],
       ]
    ]
];
