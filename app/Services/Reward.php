<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\ClassLog;
use App\Models\RewardPoint;
use Carbon\Carbon;

class Reward
{
    protected $type;
    protected $data;
    protected $account;

    public static $attendance = 'attendance';

    public function setData($type, $data, $account)
    {
        $this->type = $type;
        $this->data = $data;
        $this->account = $account;
    }

    public function rewardPointCalculation()
    {
        $point = $this->calculate();
        $this->insertRewardPoint($point);
    }

    public function insertRewardPoint($point)
    {
        if($this->type === 'attendance' && !$this->alreadyGotPoint()) {
            RewardPoint::create([
                "value" => $point,
                "type" => $this->type,
                "details" => json_encode($this->data),
                "account_id" => $this->account->id
            ]);
        } else if($this->type != 'attendance'){
            RewardPoint::create([
                "value" => $point,
                "type" => $this->type,
                "details" => json_encode($this->data),
                "account_id" => $this->account->id
            ]);
        }
    }

    public function calculate()
    {
        switch ($this->type)
        {
            case 'attendance':
                return $this->attendancePointCalculation();
            case 'assignment':
                return $this->assignmentPointCalculation();
            case 'reaction':
                return $this->reactionPointCalculation();
            case 'wordType':
                return $this->wordTypePointCalculation();
            case 'assessmentResponse':
                return $this->assessmentResponseRewardPointCalculation();
            case 'itemCreate':
                return $this->itemCreatedRewardPointCalculation();
            case 'createItemResponse':
                return $this->createdItemResponseRewardCalculation();
            default:
                return 0;
        }
    }

    public function attendancePointCalculation()
    {
        // get last 3 log
        $day = Carbon::today();

        $day1 = ClassLog::where('type', 'attendance')
            ->where('created_at', '>', $day)
            ->where('account_id', '=', $this->account->id)
            ->orderBy('id', 'asc')->first();
        $isToday = $day1 ? $day1->created_at->isToday() : false;

        $day2 = ClassLog::where('type', 'attendance')
            ->where('created_at', '>', $day->subDays(1))
            ->where('account_id', '=', $this->account->id)
            ->orderBy('id', 'asc')
            ->first();
        $isYesterday = $day2 ? $day2->created_at->isYesterday() : false;

        $day3 = ClassLog::where('type', 'attendance')
            ->where('created_at', '>', $day->subDays(2))
            ->where('account_id', '=', $this->account->id)
            ->orderBy('id', 'asc')
            ->first();;
        $isDayBeforeYesterday = $day3 ?  $day3->created_at === $day->subDays(2) : false;

        if ($isToday && $isYesterday && $isDayBeforeYesterday)
        {
            if ($day1->hour < 12 && $day2->hour < 12 && $day3 < 12) {
                return 120;
            } else {
                return 50;
            }
        } elseif ($isToday) {
            if ($day1->hour < 12) {
                return 100;
            } else {
                return 50;
            }
        }
        return 0;
    }

    public function assignmentPointCalculation()
    {
        switch ($this->data['reason'])
        {
            case 'assignment_completed_with_revision':
                return 100;
            case 'assignment_completed':
            case 'assignment_completed_on_time':
            case 'assignment_completed_unsuccessfully':
                return 200;
            case 'assignment_completed_successfully':
                return 400;
        }
        return 0;
    }

    public function reactionPointCalculation()
    {
        switch ($this->data['reason'])
        {
            case 'like_issued':
            case 'likes_received':
                return 20;
            case 'teacher_likes_all_item':
            case 'comments_issued':
            case 'comments_received':
                return 50;
        }
        return 0;
    }

    public function wordTypePointCalculation()
    {
        return 10;
    }

    public function assessmentResponseRewardPointCalculation()
    {
        switch ($this->data['reason'])
        {
            case 'no_response_mc':
            case 'no_response_open_ended':
                return 0;
            case 'incorrect_response_mc':
            case 'incorrect_response_open_ended':
                return 100;
            case 'correct_response_first_attempt_mc':
            case 'correct_response_first_attempt_open_ended':
            case 'correct_response_with_revision_mc':
            case 'correct_response_with_revision_open_ended':
                return 200;
            case 'correct_response_mc':
            case 'correct_response_open_ended':
                return 500;
        }
        return 0;
    }

    public function itemCreatedRewardPointCalculation()
    {
        switch ($this->data['reason'])
        {
            case 'items_created':
                return 200;
            case 'items_reused':
                //TODO resolve reward
                // return 200 + $this->data->reused
                return 200;
            case 'teacher_likes_all_item':
            case 'comments_received_item_created':
                return 50;
        }
        return 0;
    }

    public function createdItemResponseRewardCalculation()
    {
        switch ($this->data['reason'])
        {
            case 'total_response':
            case 'correct_responses':
            case 'incorrect_responses':
            case 'correct_response_with_revision':
            case 'late_response':
                return 10;
        }
        return 0;
    }

    public function alreadyGotPoint()
    {
        $reward = RewardPoint::where('type', self::$attendance)
            ->where('created_at', '<', Carbon::now())
            ->where('account_id', '=', $this->account->id)
            ->get();
        if(count($reward) > 0) {
            return true;
        }

        return false;
    }
}
