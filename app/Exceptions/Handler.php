<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\Debug\Exception\FlattenException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Database\QueryException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Throwable
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $e)
    {
       return parent::render($request, $e);
        $exceptionClass = (new \ReflectionClass($e))->getShortName();
        $message = "Something went wrong on server";
        $debugMode = env('APP_DEBUG', config('app.debug', false));

        switch($exceptionClass){
            case 'HttpResponseException':
                $status = Response::HTTP_INTERNAL_SERVER_ERROR;
                break;

            case 'MethodNotAllowedHttpException':
                $status = Response::HTTP_METHOD_NOT_ALLOWED;
                $message = "Route has been used with wrong method";
                break;
            case 'ModelNotFoundException':
                $status = Response::HTTP_METHOD_NOT_ALLOWED;
                $message = $debugMode ? $e->getMessage() : "No result found";
                break;
            case 'NotFoundHttpException':
                $status = Response::HTTP_NOT_FOUND;
                $message = "Requested route has not found";
                break;

            case 'AuthorizationException':
                $status = Response::HTTP_FORBIDDEN;
                $message = $e->getMessage() ?? "Wrong or invalid paramter provided";
                break;

            case 'ValidationException':
                $status = Response::HTTP_BAD_REQUEST;
                $message = $e->errors() ?? "Wrong or invalid paramter provided";
                break;

            case 'InvalidArgumentException':
            case 'InvalidFileException':
            case 'InvalidException':
                $status = Response::HTTP_BAD_REQUEST;
                $message = $e->getMessage();
                break;

            case 'AuthException':
            case 'ExpiredException':
                $status = Response::HTTP_UNAUTHORIZED;
                $message = $e->getMessage();
                break;

            case 'BadRequest400Exception':
                $status = Response::HTTP_BAD_REQUEST;
                $message = "Invalid search input provided";
                break;

            case 'ValidatorException':
                $status = Response::HTTP_BAD_REQUEST;
                $message = array_values(array_map(function($val){return app('translator')->trans($val[0]);}, $e->getMessage())
                );
                break;

            case 'QueryException':
            default:
                $status = Response::HTTP_INTERNAL_SERVER_ERROR;
                $message = $debugMode ? $e->getMessage() : $message;
                break;
        }

        return response()->json([
            'error' => [
                'status' => $status,
                'type' => $exceptionClass,
                'messages' => (array) $message
            ]],
            $status
        );
    }
}
