<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ImportQuestionFormatValidator implements Rule
{
    private $message = "";
    private $questions;
    private $resources;
    private $responses;
    private $openResponses;

    private $questionColumns = [
        "id",
        "type",
        "title",
        "description",
        "topic",
        "standard",
        "question_subject",
        "question_grade",
        "status",
        "published_state"
    ];

    private $resourceColumns = [
        "question_id",
        "url"
    ];

    private $responseColumns = [
        "question_id",
        "description",
        "is_valid",
        "sub_grade",
        "title",
        "match_label"
    ];

    private $openResponseColumns = [
        "question_id",
        "criterion_name",
        "criterion_description",
        "point",
        "level",
        "description"
    ];

    public function __construct($sheets)
    {
        $this->questions = $sheets['Questions'] ?? [];
        $this->resources = $sheets['Resources'] ?? [];
        $this->responses = $sheets['Responses'] ?? [];
        $this->openResponses = $sheets['Open Responses'] ?? [];
    }


    public function passes($attribute, $value)
    {
        if(count($this->questions) === 0 ){
            $this->message = "No Question Found";
            return false;
        }

        $missingQuestionKeys = [];
        $questionKeys = array_keys($this->questions[0]);
        foreach($this->questionColumns as $column){
            if(!in_array($column, $questionKeys)){
                $missingQuestionKeys[] = $column;
            }
        }
        if(count($missingQuestionKeys) > 0){
            $this->message = "Question columns ". join(",", $missingQuestionKeys) ." are not found";
            return false;
        }

        if (count($this->resources)) {
            $missingResourceKeys = [];
            $resourceKeys = array_keys($this->resources[0]);
            foreach($this->resourceColumns as $column){
                if(!in_array($column, $resourceKeys)){
                    $missingResourceKeys[] = $column;
                }
            }
            if(count($missingResourceKeys) > 0){
                $this->message = "Resource columns ". join(",", $missingResourceKeys) ." are not found";
                return false;
            }
        }

        if(count($this->responses)){
            $missingResponseKeys = [];
            $responseKeys = array_keys($this->responses[0]);
            foreach($this->responseColumns as $column){
                if(!in_array($column, $responseKeys)){
                    $missingResponseKeys[] = $column;
                }
            }
            if(count($missingResponseKeys) > 0){
                $this->message = "Resource columns ". join(",", $missingResponseKeys) ." are not found";
                return false;
            }
        }

        if (count($this->openResponses)) {
            $missingOpenResponseKeys = [];
            $openResponseKeys = array_keys($this->openResponses[0]);
            foreach($this->openResponseColumns as $column){
                if(!in_array($column, $openResponseKeys)){
                    $missingOpenResponseKeys[] = $column;
                }
            }
            if(count($missingOpenResponseKeys) > 0){
                $this->message = "Open  Resource columns ". join(",", $missingOpenResponseKeys) ." are not found";
                return false;
            }
        }

        return true;
    }

    public function message()
    {
        return $this->message;
    }
}
