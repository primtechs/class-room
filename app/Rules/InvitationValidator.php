<?php

namespace App\Rules;

use App\Models\Accounts\Account;
use App\Models\Invitations;
use Illuminate\Contracts\Validation\Rule;

class InvitationValidator implements Rule
{
    private $userId;
    private $courseId;
    private $accountTypeId;
    private $message;

    public function __construct($accountTypeId = "")
    {
        $this->userId = request()->get("userId");
        $this->courseId = request()->get("courseId");
        $this->accountTypeId = $accountTypeId;
        $this->message = "";
    }

    public function passes($attribute, $value)
    {
        //check if the account exists or  not
        $isAccountAvailable = Account::where("email", $this->userId)->count();
        if ($isAccountAvailable === 0) {
            $this->message = "No Account Found.";
            return false;
        }
        //check if the role is correct
        $account = Account::where("email", $this->userId)->first();
        if($this->accountTypeId === Account::$TEACHER){
            if(!($account->account_type_id === Account::$TEACHER || $account->account_type_id === Account::$ADMIN)){
                $this->message = "Invalid Role for this email";
                return false;
            }

        }else if($account->account_type_id !== $this->accountTypeId){
            $this->message = "Invalid Role for this email";
            return false;
        }
        //check if already invited or not
        $isAlreadyInvited = Invitations::where([
            'external_user_id' => $this->userId,
            'external_course_id' => $this->courseId
        ])->count();
        if ($isAlreadyInvited > 0) {
            $this->message = "Invitation Already Sent";
            return false;
        }
        return true;
    }


    public function message()
    {
        return $this->message;
    }
}
