<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ImportAccountFormatValidator implements Rule
{


    private $cells = [
        "FirstName",
        "LastName",
        "Status",
        "PhoneNumber",
        "Email",
        "AccountType",
        "Role",
        "School",
    ];

    private $rows;

    private $message = "";

    public function __construct(array $lines)
    {
        $this->rows = [];
        if (count($lines) > 0) {
            $this->rows = $lines[0];
        } else{
            $this->message = "File is Empty Nothing to import";
        }
    }

    public function passes($attribute, $value)
    {
        $keys = array_keys($this->rows);
        foreach($this->cells as $cell){
            if(!in_array($cell, $keys)){
                $this->message = "Invalid File format download again";
                return false;
            }
        }
        return true;
    }

    public function message()
    {
        return $this->message;
    }
}
