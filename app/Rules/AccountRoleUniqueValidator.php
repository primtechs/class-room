<?php

namespace App\Rules;

use App\Models\AccountRole;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Route;

class AccountRoleUniqueValidator implements Rule
{

    private $role;
    private $account;
    private $route;
    private $message;
    private $currentAccountRole;

    public function __construct(AccountRole $currentAccountRole = null)
    {
        $this->currentAccountRole = $currentAccountRole;
        $this->account = request()->get("account_id", 0);
        $this->role = request()->get("role_id", 0);
        $this->route = Route::currentRouteName();
    }


    public function passes($attribute, $value)
    {
        $accountRole = AccountRole::where(['role_id' => $this->role, "account_id" => $this->account])->first();
        if ($this->route === "account-roles.store") {
            if ($accountRole !== null) {
                $this->message = "This Account Already has this role";
                return false;
            }
        } else if ($this->route === "account-roles.update") {
            if ($accountRole != $this->currentAccountRole) {
                $this->message = "This Account Already has this role";
                return false;
            }
        }
        return true;
    }

    public function message()
    {
        return $this->message;
    }
}
