<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DueDateValidator implements Rule
{
    private $message = "";
    private $dueDate;
    private $dueTime;

    public function __construct()
    {
        $this->dueDate = request()->get("dueDate", []);
        $this->dueTime = request()->get("dueTime", []);
    }

    public function passes($attribute, $value)
    {
        if (count($this->dueDate) > 0) {
            if (count($this->dueTime) == 0) {
                $this->message = "Due Time is required";
                return false;
            }
            $formatted_date = "{$this->dueDate["year"]}-{$this->dueDate["month"]}-{$this->dueDate["day"]}";
            $formatted_time = "{$this->dueTime["hours"]}:{$this->dueTime["minutes"]}:00";
            if (strtotime("$formatted_date $formatted_time") < time()) {
                $this->message = "Due Date & Time must be in future";
                return false;
            }
        }

        return true;
    }


    public function message()
    {
        return $this->message;
    }

}
