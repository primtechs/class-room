<?php

namespace App\Rules;

use App\Models\Policy;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Route;

class PolicyUniqueValidator implements Rule
{

    private $name;
    private $action;
    private $role;
    private $route;
    private $message;
    private $currentPolicy;

    public function __construct($currentPolicy = null)
    {
        $this->currentPolicy = $currentPolicy;
        $this->name = request()->get("name", "");
        $this->action = request()->get("action_id", 0);
        $this->role = request()->get("role_id", 0);
        $this->route = Route::currentRouteName();
    }

    public function passes($attribute, $value)
    {
        $policy = Policy::where([
            'name' => $this->name,
            'action_id' => $this->action,
            'role_id' => $this->role
        ])->first();
        if ($this->route === 'policy.store') {
            if ($policy !== null) {
                $this->message = "This policy already exists with same action and role";
                return false;
            }
        } else if ($this->route === 'policy.update') {
            if ($policy != $this->currentPolicy) {
                $this->message = "This policy already exists with same action and role";
                return false;
            }
        }
        return true;
    }


    public function message()
    {
        return $this->message;
    }
}
