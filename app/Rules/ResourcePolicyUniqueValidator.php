<?php

namespace App\Rules;

use App\Models\ResourcePolicy;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Route;

class ResourcePolicyUniqueValidator implements Rule
{
    private $currentResourcePolicy;
    private  $resource;
    private  $policy;
    private  $route;
    private  $message;

    public function __construct(ResourcePolicy $currentResourcePolicy = null)
    {
        $this->currentResourcePolicy = $currentResourcePolicy;
        $this->resource = request()->get("resource_id",0);
        $this->policy = request()->get("policy_id",0);
        $this->route = Route::currentRouteName();
    }

    public function passes($attribute, $value)
    {
        $resourcePolicy = ResourcePolicy::where(["resource_id"=>$this->resource,"policy_id"=>$this->policy])->first();
        if ($this->route === 'resource-policy.store') {
            if ($resourcePolicy != null){
                $this->message = "This Resource Policy Already Exists";
                return false;
            }
        }else if ($this->route=== 'resource-policy.update'){
            if ($this->currentResourcePolicy != $resourcePolicy){
                $this->message = "This Resource Policy Already Exists";
                return false;
            }
        }
        return true;
    }

    public function message()
    {
        return $this->message;
    }
}
