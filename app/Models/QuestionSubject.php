<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionSubject extends Model
{
    protected $table = "question_subjects";
    protected $primaryKey = "question_subject_id";
    public $timestamps = false;
    protected $fillable =[
        'id',
        'name',
        'description',
    ];
}
