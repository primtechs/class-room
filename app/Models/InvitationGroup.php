<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Accounts\Account;

class InvitationGroup extends Model
{
    protected $table = "invitation_groups";
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

	public function scopeNameIgnoreCase($query, $name){
		$nameToLower = strtolower($name);
        return $query->whereRaw("LOWER(name) = '$nameToLower'");
	}

    public function accounts(){
        return $this->hasManyThrough(Account::class, InvitationGroupAccount::class, "invitation_group_id", "id", "id", "account_id");
    }

}
