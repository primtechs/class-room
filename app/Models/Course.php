<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = "classes";
    public $timestamps = false;

    protected $fillable = [
        'school_id',
        'class_name',
        'name',
        'level_id',
        'external_course_id',
        'external_course_metadata',
        'external_course_alias_metadata',
        'tag',
        'subject',
        'external_user_id',
        'google_meet_link'
    ];

    public function teacherClass()
    {
        return $this->hasMany(TeacherClass::class,"class_id","id");
    }

    public function studentClass()
    {
        return $this->hasMany(StudentClass::class,"class_id","id");
    }

    public function announcements()
    {
        return $this->hasMany(Announcement::class,"class_id","id");
    }

    public function assessments(){
        return $this->hasMany(Assessment::class, "class_id", "id");
    }

}
