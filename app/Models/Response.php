<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $table = "responses";
    public $timestamps = false;

    protected $fillable =[
        'question_id',
        'description',
        'is_valid',
        'rubric',
        'sub_grade',
        'title',
        'match_label'
    ];
}
