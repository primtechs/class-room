<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmentSchedule extends Model
{
    protected $table = "assessment_schedule";
    public $timestamps = false;

    protected $fillable = [
        'assessment_id',
        'class_id',
        'start_time',
        'end_time',
        'status',
        'account_id',
        'period_id',
    ];

    protected $casts = [
        'start_time' => 'datetime:Y-m-d H:i:s',
        'end_time' => 'datetime:Y-m-d H:i:s'
    ];

    public function scopeDate($query, $date)
    {
        return $query->whereRaw("DATE(start_time) = '$date'");
    }

    public function assessment()
    {
        return $this->belongsTo(Assessment::class, "assessment_id", "id");
    }
}
