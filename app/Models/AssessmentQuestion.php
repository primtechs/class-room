<?php

namespace App\Models;

use App\Models\Dashboard\DashboardQuestion;
use Illuminate\Database\Eloquent\Model;

class AssessmentQuestion extends Model
{
    protected $table = "assessment_questions";
    public $timestamps = false;

    protected $fillable =[
        'assessment_id',
        'type_id',
        'title',
        'description',
        'grade',
        'answered',
        'ord_id',
        'open_response',
        'open_response_grade',
        'rubrics',
        'response_rubric',
        'question_id'
    ];

    protected $visible = [
        'id',
        'assessment_id',
        'question_type_id',
        'type_id',
        'title',
        'description',
        'grade',
        'answered',
        'ord_id',
        'open_response',
        'open_response_grade',
        'rubrics',
        'response_rubric',
        'question_id',
        'overall_grade',
        'responses',
        'resources',
        'google_drive_link'
    ];

    protected $appends=[
        'title',
        'question_type_id',
        'overall_grade',
        'responses',
        'resources',
        'google_drive_link'
    ];

    protected $casts = [
        'open_response' => 'array',
        'rubrics' => 'array',
        'response_rubric' => 'array'
    ];

    public function assessment()
    {
        return $this->belongsTo(Assessment::class,"assessment_id","id");
    }

    public function assessment_responses()
    {
        return $this->hasMany(AssessmentResponse::class,"assessment_question_id","id");
    }

    public function posts()
    {
        return $this->hasMany(Post::class,"assessment_question_id","id");
    }

    public function assessment_resources()
    {
        return $this->hasMany(AssessmentResource::class,"question_id","id");
    }

    public function dashboard_question()
    {
        return $this->belongsTo(DashboardQuestion::class,"question_id","id");
    }

    public function assessment_question_attempts(){
        return $this->hasMany(AssessmentQuestionAttempt::class,  "assessment_question_id", "id");
    }

    public function question()
    {
        return $this->belongsTo(Question::class,"question_id","id");
    }

    public function getQuestionTypeIdAttribute()
    {
        return $this->type_id;
    }

    public function getOverallGradeAttribute()
    {
        $responses = $this->assessment_responses;
        $overall_grade = 0;
        foreach ($responses as $response){
            $overall_grade += $response->sub_grade;
        }
        return $overall_grade;
    }

    public function getResponsesAttribute()
    {
        return $this->assessment_responses;
    }

    public function getTitleAttribute(){
        return $this->question->title ?? $this->description;
    }

    public function getGoogleDriveLinkAttribute()
    {
        $google_drive_link = [];
        foreach ($this->assessment_resources as $assessment_resource){
            $google_drive_link[] = $assessment_resource->resource;
        }
        return $google_drive_link;
    }

    public function getResourcesAttribute()
    {
        $google_drive_link = [];
        foreach ($this->assessment_resources as $assessment_resource){
            $google_drive_link[] = $assessment_resource->resource;
        }
        return $google_drive_link;
    }

}
