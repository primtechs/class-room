<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostReaction extends Model
{
    protected $table = "posts_reaction";
    public $timestamps = false;
    protected $fillable = [
        'account_id',
        'post_id',
        'reaction_type',
        'last_update',
    ];

    public function post(){
        return $this->belongsTo(Post::class, "post_id", "id");
    }
}
