<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentProfile extends Model
{
    protected $table = "profile";
	public $timestamps = false;

	protected $fillable =[
	    'id',
		'account_id',
		'address',
		'city',
		'state',
		'zip',
		'phone',
		'picture',
		'dob',

	];
}
