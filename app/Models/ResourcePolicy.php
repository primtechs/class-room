<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResourcePolicy extends Model
{
    protected $table = "resources_policy";
    public $timestamps = false;

    protected $fillable =[
        'resource_id',
        'policy_id',
        'is_active'
    ];

    public function resource()
    {
        return $this->belongsTo(Resource::class,"resource_id","id");
    }

    public function policy()
    {
        return $this->belongsTo(Policy::class,"policy_id","id");
    }

}
