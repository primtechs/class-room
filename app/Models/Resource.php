<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    protected $table = "resources";
	public $timestamps = false;

	protected $fillable =[
		'name',
		'description',
		'res_url',
	];

    public function resourcePolicy()
    {
        return $this->hasMany(ResourcePolicy::class,"resource_id","id");
	}

}
