<?php

namespace App\Models;

use App\Models\Accounts\Account;
use App\Models\Dashboard\DashboardQuestion;
use Illuminate\Database\Eloquent\Model;

class TeacherQuestionBank extends Model
{
    protected $table = "teacher_question_bank";
    public $timestamps = false;
    protected $primaryKey = "id";

    protected $fillable = [
        'account_id',
        'question_id',
    ];

    public function dashboard_question()
    {
        return $this->belongsTo(DashboardQuestion::class,"question_id","id");
    }

    public function question()
    {
        return $this->belongsTo(Question::class,"question_id","id");
    }

    public function account()
    {
        return $this->belongsTo(Account::class,"account_id","id");
    }

}
