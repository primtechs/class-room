<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Standard extends Model
{
    protected $table = "standard";
	public $timestamps = false;

	protected $fillable =[
		'name',
		'description',
		'level_id',
	];

    public function question()
    {
        return $this->hasMany(Question::class,"standard_id","id");
    }

	public function standard()
	{
		return $this->hasMany(Assessment::class,"standard_id","id");
	}

}
