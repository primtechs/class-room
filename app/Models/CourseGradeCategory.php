<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseGradeCategory extends Model
{
    protected $table = "course_grade_categories";
    public $timestamps = false;

    protected $fillable = [
        'category_id',
        'category',
        'amount',
        'course_id',
        'external_course_id',
        'show_overall_grade'
    ];
}
