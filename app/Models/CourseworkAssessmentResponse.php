<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseworkAssessmentResponse extends Model
{
    protected $table = "coursework_assessment_responses";
    public $timestamps = false;

    protected $fillable = [
        'course_work_id',
        'external_id',
        'response',
        'meta_data',
        'sub_grade',
        'user_id',
        'returned_to_student',
        'turned_in'
    ];

    protected $casts = [
        'meta_data' => 'array',
    ];

    public function scopeDraftGrade($query,$condition,$value)
    {
        return $query->whereRaw("meta_data ->> 'draftGrade' $condition $value");
    }

    public function scopeGraded($query)
    {
        return $query->whereRaw("meta_data ->> 'draftGrade' notnull");
    }

    public function scopeUnGraded($query)
    {
        return $query->whereRaw("meta_data ->> 'draftGrade' isnull");
    }

    public function scopeState($query,$state)
    {
        return $query->whereRaw("meta_data ->> 'state' = '$state'");
    }

    public function scopeLateSubmission($query,$late)
    {
        return $query->whereRaw("cast (meta_data ->>'late' as boolean) = $late");
    }

    public function getDraftGradeAttribute()
    {
        if ($this->meta_data === NULL) return 0;
        $meta= json_decode($this->meta_data,true);
        if ($meta['draftGrade'] === NULL) return 0;
        return $meta['draftGrade'];
    }
}
