<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Protocols extends Model
{
    protected $table = "protocols";
	public $timestamps = false;

	protected $fillable =[
	    'id',
		'name',

	];
}
