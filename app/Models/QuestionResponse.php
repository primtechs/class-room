<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionResponse extends Model
{
    protected $table = "responses";
    public $timestamps = false;

    protected $fillable = [
        "question_id",
        "description",
        "is_valid",
        "rubric",
        "sub_grade",
        "last_udate",
        "title",
        "match_label"
    ];

    protected $casts = [
        "rubric" => "array"
    ];

    public function question()
    {
        return $this->belongsTo(Question::class,"question_id","id");
    }
}
