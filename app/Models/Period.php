<?php

namespace App\Models;

use App\Models\Accounts\Account;
use Illuminate\Database\Eloquent\Model;

class Period extends Model
{

    protected $table = "marking_period";
    public $timestamps = false;
    protected $primaryKey = "period_id";
    protected $fillable =[
        'name',
        'start_date',
        'end_date',
        'active',
        'school_id',
        'account_id'
    ];

    public function school()
    {
        return $this->belongsTo(School::class,"school_id","id");
    }

    public function account()
    {
        return $this->belongsTo(Account::class,"account_id","id");
    }
}
