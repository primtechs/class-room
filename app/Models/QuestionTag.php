<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionTag extends Model
{
    protected $table = "tags";
    public $timestamps = false;

    protected $fillable = [
        'question_id',
        'tag_name'
    ];

    public function question()
    {
        return $this->belongsTo(Question::class,"question_id","id");
    }

}
