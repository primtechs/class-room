<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $table = "topic";
    public $timestamps = false;

    protected $fillable =[
        'name',
        'description',
        'external_course_id',
        'external_metadata'
    ];

    protected $visible = [
        'id',
        'name',
        'description',
        'external_course_id',
        'external_metadata',
        'topicId'
    ];

    protected $appends = [
        'topicId'
    ];

    protected $casts = [
        'external_metadata' => 'array'
    ];

    public function question()
    {
        return $this->hasMany(Question::class,"topic_id","id");
    }

    public function assessments(){
        return $this->hasMany(Assessment::class, "topic_id", "id");
    }

    public function getTopicIdAttribute(){
        return $this->external_metadata["topicId"] ?? "";
    }

}
