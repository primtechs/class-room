<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RewardPoint extends Model
{
    protected $table = 'reward_points';

    protected $fillable = [
        'type',
        'value',
        'details',
        'account_id'
    ];
}
