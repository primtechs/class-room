<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Accounts\Account;

class Coursework extends Model
{
    protected $table = "classwork";
    public $timestamps = false;

    protected $fillable = [
        'class_id',
        'external_course_id',
        'external_course_work_id',
        'title',
        'description',
        'type',
        'submission',
        'metadata'
    ];

    protected $visible = [
        'id',
        'class_id',
        'external_course_id',
        'external_course_work_id',
        'title',
        'description',
        'type',
        'submission',
        'metadata',
        'turned_in',
        'assessment_id'
    ];

    protected $appends = [
        'turned_in',
        'assessment_id',
    ];

    public function courseWorkFromAssessment(){
        return $this->hasOne(CourseWorkFromAssessment::class, "course_work_id", "id");
    }

    public function courseAssessmentResponses(){
        return $this->hasMany(CourseworkAssessmentResponse::class, "course_work_id", "id");
    }

    public function studentClasses(){
        return $this->hasMany(StudentClass::class, "class_id", "class_id");
    }

    public function students(){
        return $this->hasManyThrough(Account::class, StudentClass::class, "class_id", "id", "class_id", "account_id");
    }

    public function getAssessmentIdAttribute(){
        if($this->courseWorkFromAssessment !== NULL){
            return $this->courseWorkFromAssessment->assessment_id;
        }
        return NULL;
    }

    public function getTurnedInAttribute()
    {
        return $this->courseAssessmentResponses()->where("response", "!=", NULL)->count();
    }

}
