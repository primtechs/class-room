<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Accounts\Account;


class Post extends Model
{
    protected $table = "posts";
    public $timestamps = false;
    protected $fillable = [
        'account_id',
        'reference_id',
        'reference_type',
        'body',
        'assessment_question_id',
        'assessment_response_id',
        'last_update',
        'attempt_id'
    ];

    protected $visible = [
        'id',
        'account_id',
        'reference_id',
        'reference_type',
        'body',
        'assessment_question_id',
        'assessment_response_id',
        'last_update',
    ];

    protected $casts = [
        'body' => 'array'
    ];


    public function assessment(){
        return $this->belongsTo(Assessment::class, "reference_id", "id");
    }

    public function assessment_question()
    {
        return $this->belongsTo(AssessmentQuestion::class,"assessment_question_id","id");
    }

    public function comments()
    {
        return $this->hasMany(Comment::class,"post_id","id");
    }

    public function account(){
        return $this->hasOne(Account::class, "id", "account_id");
    }

    public function reactions()
    {
        return $this->hasMany(PostReaction::class, "post_id", "id");
    }

    public function assessmentResponse(){
        return $this->hasOne(AssessmentResponse::class, "id", "assessment_response_id");
    }

}
