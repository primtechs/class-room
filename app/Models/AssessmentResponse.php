<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmentResponse extends Model
{
    protected $table = "assessment_responses";
    public $timestamps = false;

    protected $fillable = [
        'assessment_question_id',
        'response',
        'sub_grade',
        'is_correct',
        'rubric',
        'title',
        'match_label',
        'last_update'
    ];

    protected $visible = [
        'id',
        'assessment_question_id',
        'response',
        'description',
        'sub_grade',
        'is_correct',
        'is_valid',
        'rubric',
        'title',
        'match_label'
    ];

    protected $casts = [
        'rubric' => 'array'
    ];

    protected $appends = [
        'description',
        'is_valid'
    ];

    public function question()
    {
        return $this->belongsTo(AssessmentQuestion::class, "assessment_question_id", "id");
    }

    public function getDescriptionAttribute()
    {
        return $this->response;
    }

    public function getIsValidAttribute()
    {
        return $this->is_correct == 1;
    }

}
