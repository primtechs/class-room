<?php

namespace App\Models;

use App\Models\Dashboard\DashboardQuestion;
use Illuminate\Database\Eloquent\Model;

class QuestionResource extends Model
{
    protected $table = "question_resources";
    public $timestamps = false;
    protected $fillable = [
        'name',
        'description',
        'res_url',
        'type',
        'question_id'
    ];
    protected $visible = [
        'id',
        'name',
        'description',
        'res_url',
        'type',
        'url',
        'question_id'
    ];

    protected $appends = [
        "url"
    ];

    public function question()
    {
        return $this->belongsTo(Question::class,"question_id","id");
    }

    public function dashboard_question()
    {
        return $this->belongsTo(DashboardQuestion::class,"question_id","id");
    }

    public function getUrlAttribute()
    {
        return $this->res_url;
    }

}
