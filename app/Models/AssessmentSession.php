<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmentSession extends Model
{
    protected $table = "assessment_session";
    public $timestamps = false;

    protected $fillable = [
        'account_id',
        'assessment_id',
        'logintime',
    ];


}
