<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Accounts\Account;

class InvitationGroupAccount extends Model
{
    
    protected $table = "invitation_group_accounts";
    public $timestamps = false;

    protected $fillable = [
        'invitation_group_id',
        'account_id',
    ];

    public function account(){
        return $this->belongsTo(Account::class, "account_id", "id");
    }

    public function invitationGroup(){
        return $this->belongsTo(InvitationGroup::class, "invitation_group_id", "id");
    }

}
