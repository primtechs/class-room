<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $table = "actions";
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function policies()
    {
        return $this->hasMany(Policy::class, "action_id", "id");
    }

}
