<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmentLog extends Model
{
    protected $table = "assessment_log";
    public $timestamps = false;

    protected $fillable = [
        'assessment_id',
        'account_id',
        'account_id_to',
        'class_id',
        'message',
        'last_update',
    ];
}
