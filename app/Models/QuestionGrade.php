<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class QuestionGrade extends  Model
{
    protected $table = "question_grades";
    protected $primaryKey = "question_grade_id";
    public $timestamps = false;
    protected $fillable =[
        'name',
        'description',
    ];
}
