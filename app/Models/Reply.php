<?php

namespace App\Models;

use App\Models\Accounts\Account;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $table = "replies";
    public $timestamps = false;

    protected $fillable = [
        'comment_id',
        'account_id',
        'reply',
        'last_update',
    ];

    protected $visible = [
        'id',
        'comment_id',
        'account_id',
        'reply',
        'last_update',
        'first_name',
        'last_name',
        'profile_picture'
    ];

    protected $appends = [
        'first_name',
        'last_name',
        'profile_picture'
    ];

    public function account(){
        return $this->belongsTo(Account::class, "account_id", "id");
    }

    public function comment(){
        return $this->belongsTo(Comment::class, "comment_id", "id");
    }

    public function getLastNameAttribute()
    {
        return $this->account->first_name ?? "";
    }

    public function getFirstNameAttribute()
    {
        return $this->account->last_name ?? "";
    }

    public function getProfilePictureAttribute(){
        if($this->account->external_user_profile){
            return $this->account->external_user_profile["picture"];
        }
        return "";
    }

}
