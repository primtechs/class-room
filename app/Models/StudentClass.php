<?php

namespace App\Models;

use App\Models\Accounts\Account;
use Illuminate\Database\Eloquent\Model;

class StudentClass extends Model
{
    protected $table = "student_class";
    public $timestamps = false;

    protected $fillable = [
        'class_id',
        'account_id',
        'status',
        'enrollment_code',
        'external_student_metadata',
        'invitation_metadata'
    ];

    protected $casts = [
        'invitation_metadata' => 'array'

    ];

    protected $visible = [
        'id',
        'class_name',
        'class_id',
        'account_id',
        'status',
        'external_user_metadata',
        'invitation_metadata'
    ];

    protected $appends = [
        'class_name',
        'external_user_metadata'
    ];

    public function course()
    {
        return $this->belongsTo(Course::class, "class_id", "id");
    }

    public function account()
    {
        return $this->belongsTo(Account::class,"account_id","id");
    }

    public function getExternalUserMetadataAttribute()
    {
        if ($this->external_student_metadata === NULL) {
            $account = $this->account;
            return [
                'name' => [
                    "fullName" => "{$account->first_name} {$account->last_name}"
                ],
                "emailAddress" => $account->email,
                "photoUrl" => "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/s72-fbw=1/photo.jpg"
            ];
        }
        return json_decode($this->external_student_metadata, true);
    }

    public function getClassNameAttribute()
    {
        return $this->course->class_name ?? "";
    }

}
