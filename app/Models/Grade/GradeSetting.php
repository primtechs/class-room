<?php

namespace App\Models\Grade;

use Illuminate\Database\Eloquent\Model;

class GradeSetting extends Model
{
    protected $table = "grade_setting";
    public $timestamps = false;
    protected $primaryKey="setting_id";

    protected $fillable =[
        'courseid',
        'calculation_type',
        'show_grade',
        'exclude_missing'
    ];

    protected $visible=[
        'setting_id',
        'courseid',
        'calculation_type',
        'show_grade',
        'exclude_missing'
    ];

    public function categories()
    {
        return $this->hasMany(GradeCategory::class,"setting_id","setting_id");
    }
}
