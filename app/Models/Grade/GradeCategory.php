<?php

namespace App\Models\Grade;

use Illuminate\Database\Eloquent\Model;

class GradeCategory extends Model
{
    protected $table = "grade_category";
    public $timestamps = false;
    protected $primaryKey="category_id";

    protected $fillable =[
        'setting_id',
        'category_name',
        'category_point'
    ];

    protected $visible=[
        'category_id',
        'setting_id',
        'category_name',
        'category_point'
    ];

    public function setting()
    {
        return $this->belongsTo(GradeSetting::class,"setting_id","setting_id","categories");
    }
}
