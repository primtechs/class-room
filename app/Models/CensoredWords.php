<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CensoredWords extends Model
{
    protected $table = "censored_words";
    public $timestamps = false;

    protected $fillable = [
        'description',
        'account_id',
    ];
}
