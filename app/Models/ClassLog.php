<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassLog extends Model
{
    protected $table = 'logs';

    protected $fillable = [
        'type',
        'response',
        'account_id'
    ];

}
