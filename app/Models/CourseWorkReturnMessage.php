<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseWorkReturnMessage extends Model
{
    protected $table = "course_work_return_messages";
    public $timestamps = false;

    protected $fillable = [
        'account_id',
        'course_work_id',
        'message',
        'created_at'
    ];
}
