<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = "groups";
	public $timestamps = false;

	protected $fillable =[
	    'id',
		'name',
		'is_active',
		'color',
		'style'

	];

	public function assessments(){
		return $this->hasMany(Assessment::class, "group_id", "id");
	}

}
