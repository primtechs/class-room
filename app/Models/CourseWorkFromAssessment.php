<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseWorkFromAssessment extends Model
{
    protected $table = "course_works_from_assessments";
    public $timestamps = false;

    protected $fillable = [
        'course_work_id',
        'assessment_id'
    ];

    public function courseWork(){
        return $this->belongsTo(Coursework::class, "course_work_id", "id");
    }

    public function assessment(){
        return $this->belongsTo(Assessment::class, "assessment_id", "id");
    }

}
