<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
    protected $table = "policy";
    public $timestamps = false;

    protected $fillable = [
        'role_id',
        'action_id',
        'name',
        'description',
    ];

    public function action()
    {
        return $this->belongsTo(Action::class, "action_id", "id");
    }

    public function role()
    {
        return $this->belongsTo(Role::class, "role_id", "id");
    }

    public function resourcePolicy()
    {
        return $this->hasMany(ResourcePolicy::class,"policy_id","id");
    }

}
