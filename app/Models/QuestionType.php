<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionType extends Model
{
    protected $table = "question_type";
	public $timestamps = false;

	protected $fillable =[
		'name',
		'description',
	];

    public function question()
    {
        return $this->hasMany(Question::class,"type_id","id");
    }
}
