<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmentPeriod extends Model
{
    protected $table = "period";
    public $timestamps = false;
    protected $fillable =[
        'name',
        'start_time',
        'end_time',
    ];

    public function assessment(){
        return $this->hasMany(Assessment::class, "period_id", "id");
    }
}
