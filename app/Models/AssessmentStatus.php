<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmentStatus extends Model
{
    protected $table = "assessment_status";
    public $timestamps = false;

    protected $fillable = [
        'assessment_id',
        'start_time',
        'end_time',
        'status',
        'last_update'
    ];
}
