<?php

namespace App\Models;

use App\Models\Accounts\Account;
use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $table = "announcements";
    public $timestamps = false;

    protected $fillable = [
        'class_id',
        'account_id',
        'announcement_metadata'
    ];

    protected $casts =[
        'announcement_metadata' => 'array'
    ];

    public function course()
    {
         return $this->belongsTo(Course::class,"class_id","id");
    }

    public function account()
    {
        return $this->belongsTo(Account::class,"account_id","id");
    }

    public function scopeExternalId($query,$condition,$value)
    {
        return $query->whereRaw("announcement_metadata ->> 'id' $condition '$value'");
    }

}
