<?php

namespace App\Models;

use App\Models\Accounts\Account;
use Illuminate\Database\Eloquent\Model;


class Question extends Model
{
    protected $table = "questions";
    public $timestamps = false;

    protected $fillable = [
        'type_id',
        'title',
        'description',
        'topic_id',
        'standard_id',
        'question_subject_id',
        'question_grade_id',
        'status',
        'account_id',
        'total_approval',
        'average_rating',
        'open_response_grade',
        'open_response',
        'rubrics',
        'response_rubric',
        'published_state',
        'date_added',
        'date_updated',
    ];

    protected $appends = [
        'question_id',
        'google_drive_link',
        'teacher_status',
        'type_name',
        'tag',
        'topic_name',
        'standard_name',
        'subject_name',
        'grade_name',
        'user_rating',
        'responses',
        'overall_grade',
        'files'
    ];

    protected $visible = [
        'id',
        'question_id',
        'type_id',
        'title',
        'description',
        'topic_id',
        'standard_id',
        'question_subject_id',
        'question_grade_id',
        'tag',
        'status',
        'teacher_status',
        'total_approval',
        'account_id',
        'google_drive_link',
        'type_name',
        'topic_name',
        'standard_name',
        'subject_name',
        'grade_name',
        'average_rating',
        'user_rating',
        'responses',
        'rubrics',
        'open_response_grade',
        'open_response',
        'overall_grade',
        'files',
        'published_state',
		'date_added',
        'date_updated',
    ];

    protected $casts = [
        'response_rubric' => 'array',
        'rubrics' => 'array',
        'open_response' => 'array'
    ];

    public function topic()
    {
        return $this->belongsTo(QuestionCycle::class, "topic_id", "id");
    }

    public function type()
    {
        return $this->belongsTo(QuestionType::class, "type_id", "id");
    }

    public function question_tag()
    {
        return $this->hasOne(QuestionTag::class, "question_id", "id");
    }

    public function standard()
    {
        return $this->belongsTo(Standard::class, "standard_id", "id");
    }

    public function subject()
    {
        return $this->belongsTo(QuestionSubject::class, "question_subject_id", "question_subject_id");
    }

    public function grade()
    {
        return $this->belongsTo(QuestionGrade::class, "question_grade_id", "question_grade_id");
    }

    public function account()
    {
        return $this->belongsTo(Account::class, "account_id", "id");
    }

    public function response()
    {
        return $this->hasMany(QuestionResponse::class, "question_id", "id");
    }

    public function resources()
    {
        return $this->hasMany(QuestionResource::class, "question_id", "id");
    }

    public function teacher_question_bank()
    {
        return $this->hasMany(TeacherQuestionBank::class, "question_id", "id");
    }

    public function assessment_questions()
    {
        return $this->hasMany(AssessmentQuestion::class, "question_id", "id");
    }

    public function getGoogleDriveLinkAttribute()
    {
        return $this->resources;
    }

    public function getOverallGradeAttribute()
    {
        $responses = $this->response;
        $overall_grade = 0;
        foreach ($responses as $response) {
            $overall_grade += $response->sub_grade;
        }
        return $overall_grade;
    }

    public function getTopicNameAttribute()
    {
        return $this->topic->name ?? "";
    }

    public function getTypeNameAttribute()
    {
        return $this->type->name ?? "";
    }

    public function getStandardNameAttribute()
    {
        return $this->standard->name ?? "";
    }

    public function getSubjectNameAttribute()
    {
        return $this->subject->name ?? "";
    }

    public function getGradeNameAttribute()
    {
        return $this->grade->name ?? "";
    }

    public function getTeacherStatusAttribute()
    {
        $user = request()->user();
        if($user === NULL){
            return 0;
        }
        $count = $this->teacher_question_bank()->where(['account_id' => $user->id])->count();
        return $count > 0 ? 1 : 0;
    }

    public function getUserRatingAttribute()
    {
        $user = request()->user();
        if($user === NULL){
            return 0.0;
        }
        if ($this->reviews != null) {
            $reviews = json_decode($this->reviews, true);
            foreach ($reviews as $review) {
                if ($review['account_id'] == $user->id) {
                    return $review['rating'];
                }
            }
        }
        return 0.0;
    }

    public function getTagAttribute()
    {
        $tag = $this->question_tag;
        if ($tag != null) {
            return $tag->tag_name;
        }
        return "";
    }

    public function getResponsesAttribute()
    {
        return $this->response;
    }

    public function getQuestionIdAttribute()
    {
        return $this->id;
    }

    public function getFilesAttribute()
    {
        return [];
    }

}
