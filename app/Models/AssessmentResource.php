<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmentResource extends Model
{
    protected $table = "assessment_resources";
    public $timestamps = false;

    protected $fillable =[
        'assessment_id',
        'resource_type',
        'url',
        'question_id',
        'resource'
    ];

    protected $visible = [
        'id',
        'name',
        'assessment_id',
        'resource_type',
        'url',
        'question_id',
        'resource'
    ];

    protected $appends = [
        'name'
    ];

    protected $casts = [
        'resource' => 'array'
    ];

    public function question()
    {
        return $this->belongsTo(AssessmentQuestion::class,"question_id","id");
    }

    public function getNameAttribute(){
        if($this->resource === NULL){
            return "";
        }
        return $this->resource['name'];
    }

}
