<?php

namespace App\Models;

use App\Models\Accounts\Account;
use Illuminate\Database\Eloquent\Model;

class Invitations extends Model
{
    protected $table = "invitations";
    public $timestamps = false;

    protected $fillable = [
        'id',
        'external_user_id',
        'external_course_id',
        'class_id',
        'role',
    ];

    public function account()
    {
        return $this->hasOne(Account::class, "email", "external_user_id");
    }
}
