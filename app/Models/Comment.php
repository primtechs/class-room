<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Accounts\Account;

class Comment extends Model
{
    protected $table = "comments";
    public $timestamps = false;

    protected $fillable = [
        'account_id',
        'post_id',
        'comment',
        'last_update'
    ];

    protected $visible = [
        'id',
        'account_id',
        'post_id',
        'comment',
        'last_update',
        'first_name',
        'last_name',
        'profile_picture'
    ];

    protected $appends = [
        'first_name',
        'last_name',
        'profile_picture'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class,"post_id","id");
    }

    public function account()
    {
        return $this->belongsTo(Account::class, "account_id", "id");
    }

    public function replies(){
        return $this->hasMany(Reply::class, "comment_id", "id");
    }

    public function getLastNameAttribute()
    {
        return $this->account->first_name ?? "";
    }

    public function getFirstNameAttribute()
    {
        return $this->account->last_name ?? "";
    }

    public function getProfilePictureAttribute(){
        if($this->account->external_user_profile){
            return $this->account->external_user_profile["picture"];
        }
        return "";
    }

}
