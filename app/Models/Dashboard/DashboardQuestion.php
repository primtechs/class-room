<?php

namespace App\Models\Dashboard;

use App\Models\AssessmentQuestion;
use App\Models\QuestionResource;
use App\Models\TeacherQuestionBank;
use Illuminate\Database\Eloquent\Model;

class DashboardQuestion extends Model
{
    protected $table = "questions";
    public $timestamps = false;

    protected $visible = [
        'description',
        'total_teacher_approval',
        'status',
        'total_reuse',
        'total_resources',
        'total_reviews',
        'total_comments',
        'total_likes',
        'average_rating',
    ];

    protected $appends = [
        'total_teacher_approval',
        'total_reuse',
        'total_resources',
        'total_reviews',
        'total_comments',
        'total_likes',
    ];

    public function teacher_question_banks()
    {
        return $this->hasMany(TeacherQuestionBank::class, "question_id", "id");
    }

    public function assessment_questions()
    {
        return $this->hasMany(AssessmentQuestion::class, "question_id", "id");
    }

    public function resources()
    {
        return $this->hasMany(QuestionResource::class, "question_id", "id");
    }

    public function getTotalTeacherApprovalAttribute()
    {
        return $this->teacher_question_banks()->count();
    }

    public function getTotalReuseAttribute()
    {
        return $this->assessment_questions()->count();
    }

    public function getTotalResourcesAttribute()
    {
        return $this->resources()->count();
    }

    public function getTotalReviewsAttribute()
    {
        $assessmentQuestion = AssessmentQuestion::find($this->question_id);
        if ($assessmentQuestion == null) return 0;
        return $assessmentQuestion->posts->comments()->count();
    }

    public function getTotalCommentsAttribute()
    {
        $comments = $this->reviews;
        if ($comments == null) return 0;
        return count(json_decode($this->reviews, true));
    }

    public function getTotalLikesAttribute()
    {
        return 0;
    }


}
