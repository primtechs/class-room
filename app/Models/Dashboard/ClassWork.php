<?php

namespace App\Models\Dashboard;

use Illuminate\Database\Eloquent\Model;

class ClassWork extends Model
{
    protected $table = "classwork";
    public $timestamps = false;

    public function scopeDueDate($query, $operation, $parameter)
    {
//        $time = "COALESCE(metadata -> 'dueTime' ->> 'hours','00'),':',COALESCE(metadata -> 'dueTime' ->> 'minutes','00'),':',COALESCE(metadata -> 'dueTime' ->> 'seconds','00')";
        $date  = "metadata -> 'dueDate' ->> 'year','-',metadata -> 'dueDate' ->> 'month','-',metadata -> 'dueDate' ->> 'day'";
        $dueDate ="concat($date)";
        $condition = "to_timestamp($dueDate,'YYYY-MM-DD')";
        $query->whereRaw("$condition $operation '$parameter'");//check if date is not empty
        $query->whereRaw("$dueDate != '--'");
        return $query;
    }

    public function scopeState($query,$status)
    {
        return $query->whereRaw("metadata ->> 'state' = '$status'");
    }

}
