<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentWorkProductLog extends Model
{
    protected $table = "student_work_product_logs";
    public $timestamps = false;

    protected $fillable = [
        'assessment_id',
        'student_id',
        'google_drive_response',
        'student_submission_response',
        'turn_in_response',
        'pending_answer',
    ];
    protected $casts = [
        'google_drive_response' => 'array',
        'student_submission_response' => 'array',
        'turn_in_response' => 'array'
    ];

}
