<?php

namespace App\Models;

use App\Models\Accounts\Account;
use Illuminate\Database\Eloquent\Model;

class AssessmentQuestionResult extends Model
{
    protected $table = "assessment_question_results";
    public $timestamps = false;
    protected $fillable = [
        'attempt_id',
        'response',
        'response_id',
        'responsers',
        'account_id'
    ];

    public function assessment_question_attempts()
    {
        return $this->hasMany(AssessmentQuestionAttempt::class, "id", "attempt_id");
    }

    public function account(){
        return $this->belongsTo(Account::class, "account_id", "id");
    }

}
