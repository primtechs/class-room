<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Accounts\Account;

class AccountAssessment extends Model
{
    protected $table = "account_assessment";
    public $timestamps = false;

    protected $fillable = [
        'assessment_question_id',
        'account_id',
        'response_id',
        'is_correct',
        'comments',
        'last_update',
        'assessment_id',
    ];

    protected $casts = [
        'last_update' => 'datetime:Y-m-d H:i:s'
    ];

    public function account(){
        return $this->belongsTo(Account::class, "account_id", "id");
    }
}
