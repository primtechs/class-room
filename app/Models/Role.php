<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = "roles";
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function scopeNameIgnoreCase($query, $name){
        $nameToLower = strtolower($name);
        return $query->whereRaw("LOWER(name) = '$nameToLower'");
    }

    public function policies()
    {
        return $this->hasMany(Policy::class, "role_id", "id");
    }

    public function accountRole()
    {
        return $this->hasOne(AccountRole::class,"role_id","id");
    }

}
