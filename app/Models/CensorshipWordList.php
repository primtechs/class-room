<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CensorshipWordList extends Model
{
    protected $table = "censorship_word_list";
    public $timestamps = false;

    protected $fillable = [
        'name',
        'censorship_id',
        'replace_with',
        'is_active',
    ];

    public function scopeNameIgnoreCase($query, $name)
    {
        return $query->whereRaw('LOWER(name) = LOWER(?)', [$name]);/*parameter bingind was wrong */

    }
}
