<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Level;
use App\Models\Accounts\Account;

class RequestedClass extends Model
{
    protected $table = "requested_classes";
    public $timestamps = false;

    protected $fillable = [
        'name',
        'section',
        'subject',
        'room',
        'account_id',
        'level_id',
        'created_at',
    ];

    protected $visible = [
        'id',
        'name',
        'section',
        'subject',
        'room',
        'account_id',
        'account_name',
        'level_id',
        'level_name',
        'created_at',
    ];


    protected $appends = [
        'account_name',
        'level_name',
    ];

    public function account(){
        return $this->belongsTo(Account::class, "account_id", "id");
    }


    public function level(){
        return $this->belongsTo(Level::class, "level_id", "id");
    }

    public function getAccountNameAttribute(){
        if($this->account){
            return $this->account->first_name . " ". $this->account->last_name;
        }
        return "";
    }

    public function getLevelNameAttribute(){
        if($this->level){
            return $this->level->name;
        }
    }

}
