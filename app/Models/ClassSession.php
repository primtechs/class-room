<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Accounts\Account;

class ClassSession extends Model
{
    protected $table = "class_session";
    public $timestamps = false;

    protected $fillable = [
        'account_id',
        'class_id',
        'assessment_id',
        'ip',
        'computer',
        'session_key',
        'start_time',
        'end_time',
        'session_status',
        'last_update',
    ];

    public function account()
    {
        return $this->belongsTo(Account::class, "account_id", "id");
    }

}
