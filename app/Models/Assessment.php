<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use App\Models\Accounts\Account;

class Assessment extends Model
{
    protected $table = 'assessment';
    public $timestamps = false;

    protected $fillable = [
        'topic_id',
        'level_id',
        'title',
        'description',
        'status',
        'group_id',
        'protocol_id',
        'account_id',
        'standard_id',
        'submitted',
        'last_update',
        'rubric',
        'class_id',
        'grade_value',
        'grade_id',
        'subject_id',
        'is_archived',
        'max_point'

    ];

    protected $visible = [
        'id',
        'topic_id',
        'level_id',
        'title',
        'description',
        'status',
        'group_id',
        'protocol_id',
        'account_id',
        'account_name',
        'standard_id',
        'submitted',
        'last_update',
        'rubric',
        'class_id',
        'grade_value',
        'grade_id',
        'subject_id',
        'questions',
        'attempts',
        'schedule_date',
        'period_id',
        'period_name',
        'created_at',
        'is_archived',
        'max_point',

    ];

    protected $casts = [
        'rubric' => 'array'
    ];

    protected $appends = [
        'account_name',
        'questions',
        'attempts',
        'schedule_date',
        'period_id',
        'period_name',
        'created_at'
    ];

    public function schedule()
    {
        return $this->hasOne(AssessmentSchedule::class, "assessment_id", "id");
    }

    public function course(){
        return $this->belongsTo(Course::class, "class_id", "id");
    }

    public function topic(){
        return $this->belongsTo(Topic::class, "topic_id", "id");
    }

    public function standard(){
        return $this->belongsTo(Standard::class, "standard_id", "id");
    }

    public function group(){
        return $this->belongsTo(Group::class, "group_id", "id");
    }

    public function courseWorkFromAssessment(){
        return $this->hasOne(CourseWorkFromAssessment::class, "assessment_id", "id");
    }

    public function courseWork()
    {
        return $this->hasOneThrough(Coursework::class, CourseWorkFromAssessment::class, "assessment_id", "id", "id", "course_work_id");
    }

    public function assessment_questions()
    {
        return $this->hasMany(AssessmentQuestion::class, "assessment_id", "id");
    }

    public function assessment_attempts()
    {
        return $this->hasMany(AssessmentQuestionAttempt::class, "assessment_id", "id");
    }

    public function assessment_period(){
        return $this->belongsTo(AssessmentPeriod::class, "period_id", "id");
    }

    public function account(){
        return $this->belongsTo(Account::class, "account_id", "id");
    }

    public function getQuestionsAttribute()
    {
        return $this->assessment_questions ?? [];
    }

    public function getAttemptsAttribute()
    {
        return $this->assessment_attempts ?? [];
    }

    public function getScheduleDateAttribute(){
        if($this->schedule === NULL){
            return "";
        }
        return date("Y-m-d", strtotime($this->schedule->start_time));
    }

    public function getPeriodIdAttribute(){
        if($this->schedule === NULL){
            return "";
        }
        return $this->schedule->period_id;
    }

    public function getPeriodNameAttribute(){
        if($this->period_id === "" || $this->assessment_period  === NULL){
            return "";
        }
        return $this->assessment_period->name;
    }

    public function getCreatedAtAttribute(){
        return date("d M, Y h:i A", strtotime($this->last_update));
    }

    public function getAccountNameAttribute(){
        $account = $this->account;
        if ($account === NULL) return "";
        return "{$account->first_name} {$account->last_name}";
    }

}
