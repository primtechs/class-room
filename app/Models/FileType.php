<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FileType extends Model
{
    protected $table = "file_types";
    public $timestamps = false;
    protected $fillable =[
        'extension',
        'kind_of_file',
        'file_type',
        'fclass',
    ];
}
