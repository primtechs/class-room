<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionCycle extends Model
{
    protected $table = "question_cycles";
    public $timestamps = false;
    protected $fillable =[
        'id',
        'name',
        'description',
    ];
}
