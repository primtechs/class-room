<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmentSetting extends Model
{
    protected $table = "assessment_settings";
    public $timestamps = false;

    protected $fillable = [
        'assessment_id',
        'can_comment'
    ];
}
