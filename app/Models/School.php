<?php

namespace App\Models;

use App\Models\Accounts\Account;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table = "schools";
	public $timestamps = false;

	protected $fillable =[
	    'id',
		'school_name',
		'address',
		'city',
		'state',
		'zip',
		'district',
		'phone',
		'contact_name',
	];

	public function scopeNameIgnoreCase($query, $name){
		$nameToLower = strtolower($name);
        return $query->whereRaw("LOWER(school_name) = '$nameToLower'");
	}

	public function accounts(){
	    return $this->belongsToMany(Account::class,"account_school");
    }

    public function periods()
    {
        return $this->hasMany(Period::class,"school_id","id");
    }

}
