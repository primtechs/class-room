<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class AccountType extends Model
{
    protected $table = "account_type";
    public $timestamps = false;
    protected $fillable = [
        'name'
    ];

    public function scopeNameIgnoreCase($query, $name)
    {
        $nameToLower = strtolower($name);
        return $query->whereRaw("LOWER(name) = '$nameToLower'");
    }


    public function account()
    {
        return $this->hasOne(Account::class, "account_type_id", "id");
    }
}
