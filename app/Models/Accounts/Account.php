<?php

namespace App\Models\Accounts;


use App\Models\AccountRole;
use App\Models\Announcement;
use App\Models\Invitations;
use App\Models\Period;
use App\Models\Question;
use App\Models\School;
use App\Models\Role;
use App\Models\Post;
use App\Models\InvitationGroupAccount;
use App\Models\StudentClass;
use App\Models\TeacherClass;
use App\Models\RequestedClass;
use App\Models\ClassSession;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User;

class Account extends User
{

    public static $STUDENT = 2;
    public static $TEACHER = 1;
    public static $ADMIN = 6;

    use Notifiable;

    protected $table = "accounts";
    public $timestamps = false;
    protected $fillable = [
        'first_name',
        'last_name',
        'status',
        'phone_number',
        'email',
        'password',
        'account_type_id',
        'social_id',
        'external_user_id',
        'external_user_profile',
        'token',
        'api_token'
    ];

    protected $visible = [
        'id',
        'first_name',
        'last_name',
        'profile_picture',
        'status',
        'phone_number',
        'email',
        'role_id',
        'role_name',
        'school_id',
        'external_user_id',
        'school_name',
        'account_type_name',
        'account_type_id'
    ];

    protected $appends = [
        'profile_picture',
        'role_name',
        'role_id',
        'school_id',
        'school_name',
        'account_type_name',
    ];

    protected $casts = [
        'external_user_profile' => 'array',
        '$token' => 'array',
    ];

    public function invitation()
    {
        return $this->belongsTo(Invitations::class, "email", "external_user_id");
    }

    public function accountType()
    {
        return $this->belongsTo(AccountType::class, "account_type_id", "id");
    }
    public function accountRole()
    {
        return $this->hasOne(AccountRole::class, "account_id", "id");
    }

    public function school()
    {
        return $this->hasOneThrough(School::class, AccountSchool::class, "account_id", "id", "id", "school_id");
    }

    public function role()
    {
        return $this->hasOneThrough(Role::class, AccountRole::class, "account_id", "id", "id", "role_id");
    }

    public function periods()
    {
        return $this->hasMany(Period::class, "account_id", "id");
    }

    public function questions()
    {
        return $this->hasMany(Question::class, "account_id", "id");
    }

    public function announcements()
    {
        return $this->hasMany(Announcement::class, "account_id", "id");
    }

    public function invitationGroupAccounts(){
        return $this->hasMany(InvitationGroupAccount::class, "invitation_group_id", "id");
    }

    public function teacherClasses()
    {
        return $this->hasMany(TeacherClass::class, "account_id", "id");
    }

    public function requestClasses()
    {
        return $this->hasMany(RequestedClass::class, "account_id", "id");
    }

    public function studentClasses()
    {
        return $this->hasMany(StudentClass::class, "account_id", "id");
    }

    public function posts(){
        return $this->hasMany(Post::class, "account_id", "id");
    }


    public function classSession(){
        return $this->hasOne(ClassSession::class, "account_id", "id");
    }

    public function getProfilePictureAttribute(){
        if($this->external_user_profile){
            return $this->external_user_profile["picture"] ?? "";
        }
        return "";
    }

    public function getRoleNameAttribute()
    {
        return $this->role->name ?? "";
    }

    public function getRoleIdAttribute()
    {
        return $this->role->id ?? "";
    }

    public function getAccountTypeNameAttribute()
    {
        return $this->accountType->name ?? "";
    }

    public function getSchoolNameAttribute()
    {
        return $this->school->school_name ?? "";
    }

    public function getSchoolIdAttribute()
    {
        return $this->school->id ?? "";
    }

    public function isAdmin()
    {
        return $this->account_type_id === self::$ADMIN;
    }

    public function isTeacher()
    {
        return $this->account_type_id === self::$TEACHER;
    }

    public function isStudent()
    {
        return $this->account_type_id === self::$STUDENT;
    }
}
