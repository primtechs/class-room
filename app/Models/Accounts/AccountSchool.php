<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class AccountSchool extends Model
{

    protected $table = "account_school";
    public $timestamps = false;
    protected $fillable = [
        'account_id',
        'school_id',
        'is_active'
    ];
}
