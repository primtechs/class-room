<?php

namespace App\Models;

use App\Models\Accounts\Account;
use Illuminate\Database\Eloquent\Model;

class AccountRole extends Model
{
    protected $table = "account_roles";
    public $timestamps = false;

    protected $fillable = [
        'account_id',
        'role_id',
        'is_active',
    ];

    public function account()
    {
        return $this->belongsTo(Account::class,"account_id","id");
    }

    public function role()
    {
        return $this->belongsTo(Role::class,"role_id","id");
    }

}
