<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AssessmentTemplate extends Model
{
    protected $table = 'assessment_template';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'rubric',
        'account_id',
        'status',
        'protocol'
    ];

    protected $casts = [
        'rubric' => 'array'
    ];

}
