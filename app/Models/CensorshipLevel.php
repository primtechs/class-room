<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CensorshipLevel extends Model
{
    protected $table = "censorship_levels";
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];
}
