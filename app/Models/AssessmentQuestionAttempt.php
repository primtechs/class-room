<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessmentQuestionAttempt extends Model
{
    protected $table = "assessment_question_attempt";
    public $timestamps = false;

    protected $fillable = [
        'assessment_id',
        'assessment_question_id',
        'last_update',
    ];

    protected $casts = [
        'last_update' => 'datetime:Y-m-d h:i:s A'
    ];

    protected $visible = [
        'id',
        'question_id',
        'last_update',
        'index'
    ];

    protected $appends = [
        'index',
        'question_id'
    ];

    public function assessment(){
        return $this->belongsTo(Assessment::class, "assessment_id", "id");
    }

    public function assessment_question(){
        return $this->belongsTo(AssessmentQuestion::class, "assessment_question_id", "id");
    }

    public function assessment_question_results(){
        return $this->belongsTo(AssessmentQuestionResult::class, "id", "attempt_id");
    }

    public function getIndexAttribute()
    {
        return $this->id;
    }

    public function getQuestionIdAttribute()
    {
        return $this->assessment_question_id;
    }

}
