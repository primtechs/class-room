<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profile";
    public $timestamps = false;

    protected $attributes = [
        'address' => NULL,
        'city' => NULL,
        'state' => NULL,
        'zip' => NULL,
        'phone' => NULL,
        'dob' => NULL
    ];

    protected $fillable = [
        'account_id',
        'address',
        'city',
        'state',
        'zip',
        'phone',
        'picture',
        'dob'
    ];
}
