<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\RequestedClass;

class Level extends Model
{
    protected $table = "levels";
	public $timestamps = false;

	protected $fillable =[
	    'id',
		'name',

	];

	public function requestClasses(){
		return $this->hasMany(RequestedClass::class, "level_id", "id");
	}

}
