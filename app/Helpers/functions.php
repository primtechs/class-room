<?php

use App\Models\Accounts\Account;
use App\Models\Accounts\AccountSchool;
use Illuminate\Auth\Access\AuthorizationException;
use App\Events\LogOutEvent;
use App\Models\AssessmentQuestionAttempt;
use App\Models\Post;

if (!function_exists('setGoogleClient')) {

    function setGoogleClientFromAuthCode($code){
        $client = new Google_Client();
        $client->setAuthConfig(config('google_client.key'));
        $client->addScope(config('google_client.scopes'));
        $client->setRedirectUri(env("WEB_REDIRECT_URIS"));
        $client->setAccessType('offline');
        $token = $client->fetchAccessTokenWithAuthCode($code);
        $client->setAccessToken($token);
        return $client;
    }

    function setGoogleClient($token = null)
    {
        $client = new Google_Client();
        $client->setAuthConfig(config('google_client.key'));
        $client->addScope(config('google_client.scopes'));
        $client->setRedirectUri(env("WEB_REDIRECT_URIS"));
        $client->setAccessType('offline');
        $client->setApprovalPrompt("force");
        $client->setIncludeGrantedScopes(true);
        $client->setAccessToken($token);
        if ($client->isAccessTokenExpired()) {
            $token = $client->fetchAccessTokenWithRefreshToken();
            if($token !== null){
                $client->setAccessToken($token);
            }else{
                $account = request()->user();
                if ($account !== NULL) {
                    $logOutEvent = new LogOutEvent([
                        "data" => "Logged in another device",
                        "api_token" => $account->api_token
                    ]);
                    event($logOutEvent);
                }
            }          
        }
        return $client;
    }

    if (!function_exists('getOAuthToken')) {

        function getOAuthToken()
        {
            $user = request()->user();
            return $user->token;
        }
    }

    if (!function_exists('setAnnouncement')) {

        function setAnnouncement($data)
        {
            $announcement = new Google_Service_Classroom_Announcement();

            if (isset($data['text']) && $data['text']){
                $announcement->setText($data['text']);
            }

            if (isset($data['alternateLink']) && $data['alternateLink']){
                $announcement->setAlternateLink($data['alternateLink']);
            }

            if (isset($data['assigneeMode']) && $data['assigneeMode']){
                $announcement->setAssigneeMode($data['assigneeMode']);

                if($data['assigneeMode'] === 'INDIVIDUAL_STUDENTS') {
                    if (isset($data['individualStudentsOptions']) && $data['individualStudentsOptions']){
                        $option = setIndividualOptions($data['individualStudentsOptions']);
                        $announcement->setIndividualStudentsOptions($option);
                    }
                }
            }

            if (isset($data['state']) && $data['state']){
                $announcement->setState($data['state']);
            }

            if (isset($data['materials']) && $data['materials']){
                $announcement->setMaterials($data['materials']);
            }


            if (isset($data['scheduledTime']) && $data['scheduledTime']){
                $announcement->setScheduledTime($data['scheduledTime']);
            }

            // TODO: set others

            return $announcement;
        }
    }


    if (!function_exists('setInvitationStudent')) {

        function setInvitationStudent($data)
        {
            $invitation = new Google_Service_Classroom_Invitation();

            if (isset($data['userId']) && $data['userId']){
                $invitation->setUserId($data['userId']);
            }
            if (isset($data['courseId']) && $data['courseId']){
                $invitation->setCourseId($data['courseId']);
            }
            if (isset($data['role']) && $data['role']){
                $invitation->setRole('STUDENT');
            }

            // TODO: set others

            return $invitation;
        }
    }

    if (!function_exists('setInvitationTeacher')) {

        function setInvitationTeacher($data)
        {
            $invitation = new Google_Service_Classroom_Invitation();

            if (isset($data['userId']) && $data['userId']){
                $invitation->setUserId($data['userId']);
            }
            if (isset($data['courseId']) && $data['courseId']){
                $invitation->setCourseId($data['courseId']);
            }
            if (isset($data['role']) && $data['role']){
                $invitation->setRole('TEACHER');
            }

            // TODO: set others

            return $invitation;
        }
    }

    if (!function_exists('setInvitationGurdian')) {

        function setInvitationGurdian($data)
        {
            $invitation = new Google_Service_Classroom_GuardianInvitation();

            if (isset($data['invitedEmailAddress']) && $data['invitedEmailAddress']){
                $invitation->setInvitedEmailAddress($data['invitedEmailAddress']);
            }
            if (isset($data['studentId']) && $data['studentId']){
                $invitation->setStudentId($data['studentId']);
            }
            if (isset($data['state']) && $data['state']){
                $invitation->setState($data['state']);
            }
            if (isset($data['creationTime']) && $data['creationTime']){
                $invitation->setCreationTime($data['creationTime']);
            }
            /*           if (isset($data['role']) && $data['role']){
                           $invitation->setRole('TEACHER');
                       }*/

            // TODO: set others

            return $invitation;
        }
    }

    if (!function_exists('setCourseWork')) {

        function setCourseWork($data)
        {
            $courseWork = new Google_Service_Classroom_CourseWork();
            if (isset($data['title']) && $data['title']){
                $courseWork->setTitle($data['title']);
            }

            if (isset($data['description']) && $data['description']){
                $courseWork->setDescription($data['description']);
            }

            if (isset($data['materials']) && $data['materials']){
                $materials = array();
                foreach ($data['materials'] as $index=>$material){
                    $gmaterial = new Google_Service_Classroom_Material();
                    if(isset($data['materials'][$index]['driveFile'])) {
                        $material = $data['materials'][$index]['driveFile'];
                        $shareDriveFile = new Google_Service_Classroom_SharedDriveFile();
                        $driveFile = new Google_Service_Classroom_DriveFile();
                        $driveFile->setId($material['id']);
                        $shareDriveFile->setDriveFile($driveFile);
                        $shareDriveFile->setShareMode($data['materials'][$index]['shareMode']);
                        $gmaterial->setDriveFile($shareDriveFile);
                        array_push($materials, $gmaterial);
                    }
                }
                $courseWork->setMaterials($materials);
            }

            if (isset($data['state']) && $data['state']){
                $courseWork->setState($data['state']);
            }

            if (isset($data['dueDate']) && $data['dueDate']){
                $dueDay = setDueDate($data['dueDate']);
                $courseWork->setDueDate($dueDay);
            }

            if (isset($data['dueTime']) && $data['dueTime']){
                $dueTime = setDueTime($data['dueTime']);
                $courseWork->setDueTime($dueTime);
            }

            if (isset($data['scheduledTime']) && $data['scheduledTime']){
                $courseWork->setScheduledTime($data['scheduledTime']);
            }

            if (isset($data['maxPoints']) && $data['maxPoints']){
                $courseWork->setMaxPoints($data['maxPoints']);
            }

            if (isset($data['workType']) && $data['workType']){
                $courseWork->setWorkType($data['workType']);

                if ($data['workType'] === 'ASSIGNMENT' && isset($data['assignment']) && $data['assignment']){
                    $assignment = setAssignment($data['assignment']);
                    $courseWork->setAssignment($assignment);
                }

                if ($data['workType'] === 'MULTIPLE_CHOICE_QUESTION' && isset($data['multipleChoiceQuestion']) && $data['multipleChoiceQuestion']){
                    $multipleChoiceQuestions = setMultipleChoiceQuestion($data['multipleChoiceQuestion']);
                    $courseWork->setMultipleChoiceQuestion($multipleChoiceQuestions);
                }
            }

            if (isset($data['assigneeMode']) && $data['assigneeMode']){
                $courseWork->setAssigneeMode($data['assigneeMode']);
                if ($data['assigneeMode'] === 'INDIVIDUAL_STUDENTS' && isset($data['individualStudentsOptions']) && $data['individualStudentsOptions']){
                    $individualOptions = setIndividualOptions($data['individualStudentsOptions']);
                    $courseWork->setIndividualStudentsOptions($individualOptions);
                }
            }

            if (isset($data['submissionModificationMode']) && $data['submissionModificationMode']){
                $courseWork->setSubmissionModificationMode($data['submissionModificationMode']);
            }

            if (isset($data['topicId']) && $data['topicId']){
                $courseWork->setTopicId($data['topicId']);
            }

            return $courseWork;
        }
    }

    if (!function_exists('setCourseWorkMaterial')) {

        function setCourseWorkMaterial($data)
        {
            $courseWorkMaterial = new Google_Service_Classroom_CourseWorkMaterial();

            if (isset($data['title']) && $data['title']){
                $courseWorkMaterial->setTitle($data['title']);
            }

            if (isset($data['description']) && $data['description']){
                $courseWorkMaterial->setDescription($data['description']);
            }

            if (isset($data['materials']) && $data['materials']){
                $courseWorkMaterial->setMaterials($data['materials']);
            }

            if (isset($data['state']) && $data['state']){
                $courseWorkMaterial->setState($data['state']);
            }


            if (isset($data['scheduledTime']) && $data['scheduledTime']){
                $courseWorkMaterial->setScheduledTime($data['scheduledTime']);
            }

            if (isset($data['assigneeMode']) && $data['assigneeMode']){
                $courseWorkMaterial->setAssigneeMode($data['assigneeMode']);
                if ($data['assigneeMode'] === 'INDIVIDUAL_STUDENTS' && isset($data['individualStudentsOptions']) && $data['individualStudentsOptions']){
                    $individualOptions = setIndividualOptions($data['individualStudentsOptions']);
                    $courseWorkMaterial->setIndividualStudentsOptions($individualOptions);
                }
            }

            if (isset($data['topicId']) && $data['topicId']){
                $courseWorkMaterial->setTopicId($data['topicId']);
            }

            return $courseWorkMaterial;
        }
    }

    if (!function_exists('setCourseTopic')) {

        function setCourseTopic($data)
        {
            $topic = new Google_Service_Classroom_Topic();

            if (isset($data['name']) && $data['name']){
                $topic->setName($data['name']);
            }
            if (isset($data['topicId']) && $data['topicId']){
                $topic->setTopicId($data['topicId']);
            }
            if (isset($data['courseId']) && $data['courseId']){
                $topic->setCourseId($data['courseId']);
            }
            if (isset($data['courseId']) && $data['courseId']){
                $topic->setCourseId($data['courseId']);
            }
            if (isset($data['updateTime']) && $data['updateTime']){
                $topic->setUpdateTime($data['updateTime']);
            }
            
            return $topic;
        }
    }

    if (!function_exists('ownerId')) {

        function ownerId()
        {
            $user = request()->user();
            return $user->external_user_id;
        }
    }

    if (!function_exists('setCourse')) {

        function setCourse($data)
        {
            $course = new Google_Service_Classroom_Course();

            if (isset($data['ownerId']) && $data['ownerId']) {
                $course->setOwnerId($data['ownerId']);
            } else {
                $course->setOwnerId(ownerId());
            }


            if (isset($data['name']) && $data['name']){
                $course->setName($data['name']);
            }
            if (isset($data['section']) && $data['section']){
                $course->setSection($data['section']);
            }
            if (isset($data['descriptionHeading']) && $data['descriptionHeading']){
                $course->setDescriptionHeading($data['descriptionHeading']);
            }
            if (isset($data['description']) && $data['description']){
                $course->setDescription($data['description']);
            }
            if (isset($data['room']) && $data['room']){
                $course->setRoom($data['room']);
            }

            if (isset($data['courseState']) && $data['courseState']){
                $course->setCourseState($data['courseState']);
            }

            return $course;
        }
    }

    if (!function_exists('setShortAnswerSubmission')) {

        function setShortAnswerSubmission($data)
        {
            $submission = new Google_Service_Classroom_StudentSubmission();
            $answer = new Google_Service_Classroom_ShortAnswerSubmission();
            $answer->setAnswer($data['answer']);

            $submission->setShortAnswerSubmission($answer);

            return $submission;
        }
    }

    if (!function_exists('setMultipleChoiceSubmission')) {

        function setMultipleChoiceSubmission($data)
        {
            $submission = new Google_Service_Classroom_StudentSubmission();
            $multipleChoice = new Google_Service_Classroom_MultipleChoiceSubmission();
            $multipleChoice->setAnswer($data['answer']);

            $submission->setMultipleChoiceSubmission($multipleChoice);

            return $submission;
        }
    }

    if (!function_exists('setAssignmentSubmission')) {

        function setAssignmentSubmission($data)
        {
            $modifyAttachmentRequest = new Google_Service_Classroom_ModifyAttachmentsRequest();      
            $attachment = new Google_Service_Classroom_Attachment();
            $driveFile = new Google_Service_Classroom_DriveFile();
            $driveFile->setId($data['id']);
            $attachment->setDriveFile($driveFile);
            $modifyAttachmentRequest->setAddAttachments([$attachment]);
            return $modifyAttachmentRequest;
        }
    }

    if (!function_exists('assignGrade')) {

        function assignGrade($data)
        {
            $submission = new Google_Service_Classroom_StudentSubmission();
            $submission->setAssignedGrade($data['assignGrade']);

            return $submission;
        }
    }

    if (!function_exists('draftGrade')) {

        function draftGrade($data)
        {
            $submission = new Google_Service_Classroom_StudentSubmission();
            $submission->setDraftGrade($data['draftGrade']);

            return $submission;
        }
    }

    if (!function_exists('successResponse')) {

        function successResponse($data, $message = null, $code = 200)
        {
            return response()->json([
                'status'=> 'Success',
                'message' => $message,
                'data' => $data
            ], $code);

        }
    }

    if (!function_exists('setDueDate')) {

        function setDueDate($data)
        {
            $date = new Google_Service_Classroom_Date();

            if(isset($data['day']) ) {
                $date->setDay($data['day']);
            }

            if(isset($data['month'])) {
                $date->setMonth($data['month']);
            }

            if(isset($data['year'])) {
                $date->setYear($data['year']);
            }

            return $date;
        }
    }

    if (!function_exists('setDueTime')) {

        function setDueTime($data)
        {

            $time = new Google_Service_Classroom_TimeOfDay();

            if (isset($data['hours'])) {
                $time->setHours($data['hours']);
            }

            if (isset($data['minutes'])) {
                $time->setMinutes($data['minutes']);
            }

            if (isset($data['seconds'])) {
                $time->setSeconds($data['seconds']);
            }

            if (isset($data['nanos']) ) {
                $time->setNanos($data['nanos']);
            }

            return $time;
        }
    }

    if (!function_exists('setIndividualOptions')) {

        function setIndividualOptions($data)
        {
            $options = new Google_Service_Classroom_IndividualStudentsOptions();

            if (isset($data['studentIds']) && $data['studentIds']) {
                $options->setStudentIds($data['studentIds']);
            }

            return $options;
        }
    }

    if (!function_exists('setAssignment')) {

        function setAssignment($data)
        {
            $assignment = new Google_Service_Classroom_Assignment();

            if (isset($data['studentWorkFolder']) && $data['studentWorkFolder']) {
                $folder = new Google_Service_Classroom_DriveFolder();
                $studentWorkFolder = $data['studentWorkFolder'];

                if(isset($studentWorkFolder['id']) && $studentWorkFolder['id']) {
                    $folder->setId($studentWorkFolder['id']);
                }
                if(isset($studentWorkFolder['title']) && $studentWorkFolder['title']) {
                    $folder->setTitle($studentWorkFolder['title']);
                }
                if(isset($studentWorkFolder['alternateLink']) && $studentWorkFolder['alternateLink']) {
                    $folder->setAlternateLink($studentWorkFolder['alternateLink']);
                }

                $assignment->setStudentWorkFolder($folder);
            }

            return $assignment;
        }
    }

    if (!function_exists('setMultipleChoiceQuestion')) {

        function setMultipleChoiceQuestion($data)
        {
            $question = new Google_Service_Classroom_MultipleChoiceQuestion();

            if (isset($data['choices']) && $data['choices']) {
                $question->setChoices($data['choices']);
            }

            return $question;
        }
    }

    if (!function_exists('getAccountDetails')) {
       function getAccountDetails() {
           $user = request()->user()->id;
           return Account::find($user);
       }
    }

    if (!function_exists('getSchool')) {
       function getSchool() {
           $user = request()->user()->id;
           $school = AccountSchool::where('account_id', $user)->first();
           return $school->school_id;
       }
    }

    if(!function_exists('isStudent')) {
        function isStudent(Google_Service_Classroom $course, $courseId)
        {
            return $course->courses_students->get($courseId, 'me');
        }
    }

    if(!function_exists('isTeacher')) {
        function isTeacher(Google_Service_Classroom $course, $courseId)
        {
            return $course->courses_teachers->get($courseId, 'me');
        }
    }

    if(!function_exists('checkInvitation')) {
        function checkInvitation($courseId)
        {
            $client = setGoogleClient(getOAuthToken());
            $classroom = new Google_Service_Classroom($client);
            $invitation = $classroom->invitations->listInvitations(['courseId' => $courseId]);
            dd($invitation);
        }
    }

    if (!function_exists("extractResourceIdFromUrl")) {
        function extractResourceIdFromUrl($url){
            $url = str_replace("https://drive.google.com/file/d/", "", $url);
            $url = str_replace("https://docs.google.com/document/d/", "", $url);
            $fragments = explode("/", $url);
            if (count($fragments)) {
                return $fragments[0];
            }
            return 0;
        }
    }

    if (!function_exists("convertDriveLinkToPreviewUrl")) {
        function convertDriveLinkToPreviewUrl($url){
            $url = str_replace("edit?usp=drive_web", "preview", $url);
            $url = str_replace("view?usp=drive_web", "preview", $url);
            return $url;
        }
    }

    if (!function_exists("isFromGoogleDocs")) {
        function isFromGoogleDocs($url){
            if (strpos($url, "https://docs.google.com/document/d/") === false) {
                return false;
            }
            return true;
        }
    }

    if (!function_exists('formName')) {
        function formName($post_id){
            $post  = Post::find($post_id);
            $attempts = []; 
            AssessmentQuestionAttempt::where("assessment_id", $post->reference_id)->orderBy("id", "ASC")->get()->each(function($attempt, $index) use(&$attempts){
                switch($index){
                    case 0:
                        $attempts[$attempt->id] = "form_1A";
                        break;
                    case 1: 
                        $attempts[$attempt->id] = "form_1B";
                        break;
                    case 2:
                        $attempts[$attempt->id] = "form_2";
                        break;
                }
            });
            return $attempts[$post->attempt_id];
        }
    }

}
