<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CommentAddedEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $post_id;
    private $assessment_id;
    private $account_id;
    private $role;
    private $comment_id;
    private $form;


    public function __construct($post_id, $account_id, $role, $comment_id, $form, $assessment_id)
    {
        $this->post_id = $post_id;
        $this->account_id = $account_id;
        $this->role = $role;
        $this->comment_id = $comment_id;
        $this->form = $form;
        $this->assessment_id = $assessment_id;
    }

    public function broadcastOn()
    {
        return ["channel-assessment"];
    }

    public function broadcastAs()
    {
        return 'assessment.comment.added';
    }

    public function broadcastWith()
    {
        return [
            'post_id' => $this->post_id,
            'account_id' => $this->account_id,
            'role' => $this->role,
            'comment_id' => $this->comment_id,
            'form' => $this->form,
            'assessment_id' => $this->assessment_id,
        ];
    }
}
