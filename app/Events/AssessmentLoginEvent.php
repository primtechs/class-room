<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AssessmentLoginEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


   private $data;

    public function __construct(array $data)
    {

        $this->data = $data;
    }
    public function broadcastOn()
    {
        return ["channel-assessment"];
    }

    public function broadcastAs()
    {
        return 'assessment.studentlogin';
    }

    public function broadcastWith()
    {
        return [
            'name' =>  $this->data['name'],
            'first_name' =>  $this->data['first_name'],
            'last_name' =>  $this->data['last_name'],
            'student_id' =>  $this->data['student_id'],
            'id' =>  $this->data['student_id'],
            'assessment_id' =>  $this->data['assessment_id'],
            'logintime' =>  $this->data['logintime'],
            'responders' =>  $this->data['responders'],
        ];
    }

}
