<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PostReactionAddedEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $id;
    private $post_id;
    private $assessment_id;
    private $post_account_id;
    private $account_id;
    private $role;
    private $form;
    private $reaction_type;


    public function __construct($id, $post_id, $account_id, $role, $form, $reaction_type, $assessment_id = 0, $post_account_id = 0)
    {
        $this->id = $id;
        $this->post_id = $post_id;
        $this->account_id = $account_id;
        $this->role = $role;
        $this->form = $form;
        $this->reaction_type = $reaction_type;
        $this->assessment_id = $assessment_id;
        $this->post_account_id = $post_account_id;
    }

    public function broadcastOn()
    {
        return ["channel-assessment"];
    }

    public function broadcastAs()
    {
        return 'assessment.post.reaction.added';
    }

    public function broadcastWith()
    {
        return [
            'id' => $this->id,
            'post_id' => $this->post_id,
            'post_account_id' => $this->post_account_id,
            'account_id' => $this->account_id,
            'role' => $this->role,
            'form' => $this->form,
            'reaction_type' => $this->reaction_type,
            'assessment_id' => $this->assessment_id,
        ];
    }
}
