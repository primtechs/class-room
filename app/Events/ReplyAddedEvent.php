<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\Models\Comment;
use App\Models\Reply;

class ReplyAddedEvent  implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    private $reply;
    private $comment;
    private $assessment_id;


    public function __construct($comment_id, $reply_id, $assessment_id)
    {
        $this->comment = Comment::find($comment_id);
        $this->reply = Reply::find($reply_id);
        $this->assessment_id = $assessment_id;
    }

    public function broadcastOn()
    {
        return ["channel-assessment"];
    }

    public function broadcastAs()
    {
        return 'assessment.reply.added';
    }

    public function broadcastWith()
    {
        return [
            'reply_id' => $this->reply->id,
            'account_id' => $this->reply->account_id,
            'post_id' => $this->comment->post_id,
            'comment_id' => $this->comment->id,
            'assessment_id' => $this->assessment_id,
        ];
    }
}
