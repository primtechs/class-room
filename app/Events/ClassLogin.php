<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Google_Service_Classroom;

class ClassLogin
{
    public $account;
    public $classRoom;

    public function __construct($account)
    {
        $this->account = $account;
//        $this->classRoom = new Google_Service_Classroom(setGoogleClient($account->token));
    }
}
