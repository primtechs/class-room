<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AssessmentTimeAdjustEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $assessment_id;
    private $time;
    private $type;

    public function __construct($assessment_id, $time, $type)
    {
        $this->assessment_id = $assessment_id;
        $this->time = $time;
        $this->type = $type;
    }

    public function broadcastOn()
    {
        return ["channel-assessment"];
    }

    public function broadcastAs()
    {
        return 'assessment.time.adjustment';
    }

    public function broadcastWith()
    {
        return [
            'assessment_id' => $this->assessment_id,
            'time' => $this->time,
            'type' => $this->type,
        ];
    }
}
