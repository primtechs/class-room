<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AssessmentFinishedEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

	private $assessment_id;
    public function __construct(int $id)
    {
        $this->assessment_id = $id;
    }

    public function broadcastOn()
    {
        return ["channel-assessment"];
    }

    public function broadcastAs()
    {
        return 'assessment.finished';
    }

    public function broadcastWith()
    {
        return [
            'assessment_id' => $this->assessment_id,
            'message' => 'No Active Assessment'
        ];
    }
    
}
