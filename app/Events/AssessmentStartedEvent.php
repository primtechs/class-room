<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\Models\AssessmentQuestionAttempt;



class AssessmentStartedEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $attempt;
    private $time;
    private $GenerateEventOnStudentSide;

    public function __construct(AssessmentQuestionAttempt $attempt, array $time,int $GenerateEventOnStudentSide=1 )
    {
        $this->attempt = $attempt;
        $this->time = $time;
        $this->GenerateEventOnStudentSide = $GenerateEventOnStudentSide;
    }

    public function broadcastOn()
    {
        return ["channel-assessment"];
    }

    public function broadcastAs()
    {
        return 'assessment.started';
    }

    public function broadcastWith()
    {
        return [
            'question_id' => $this->attempt->assessment_question_id,
            'assessment_id' => $this->attempt->assessment_id,
            'time' => $this->time,
            'GenerateEventOnStudentSide' => $this->GenerateEventOnStudentSide,
        ];
    }
}
