<?php

namespace App\Events;

use App\Models\Post;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PostAddedEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $post_id;
    private $assessment_id;
    private $account_id;

    public function __construct( $post_id, $assessment_id, $account_id)
    {
        $this->post_id = $post_id;
        $this->assessment_id = $assessment_id;
        $this->account_id = $account_id;
    }

    public function broadcastOn()
    {
        return ["channel-assessment"];
    }

    public function broadcastAs()
    {
        return 'assessment.post.added';
    }

    public function broadcastWith()
    {
        return [
            'assessment_id' => $this->assessment_id,
            'post_id' => $this->post_id,
            'account_id' => $this->account_id,
        ];
    }
}
