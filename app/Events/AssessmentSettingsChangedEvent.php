<?php

namespace App\Events;

use App\Models\AssessmentSetting;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AssessmentSettingsChangedEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $settings;

    public function __construct(AssessmentSetting $settings)
    {
        $this->settings = $settings;
    }

    public function broadcastOn()
    {
        return ["channel-assessment"];
    }

    public function broadcastAs()
    {
        return 'assessment.settings.changed';
    }

    public function broadcastWith()
    {
        return [
            'can_comment' => $this->settings->can_comment
        ];
    }
}
