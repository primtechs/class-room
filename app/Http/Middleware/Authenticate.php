<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        $token = $request->api_token;
        if (!$token)
            throw new \Exception('Api Token Required', 400);

        if (! $request->expectsJson()) {
            throw new \Exception('Provide Valid Credentials ', 401);
        }
    }
}
