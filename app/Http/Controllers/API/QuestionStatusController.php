<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Question;

class QuestionStatusController extends Controller
{
    public function updateStatus($id, Request $request){
        $status = $request->get("status");
        $question = Question::find($id);
        $question->status = $status;
        $question->save();
        return response()->json([
            'question' => $question->toArray(),
            'status' => 'success',
            'message' => 'Question Status Updated'
        ], 200);
    }
}
