<?php

namespace App\Http\Controllers\API\LiveLearning;

use App\Events\CommentAddedEvent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\Post;
use App\Models\CensorshipWordList;
use App\Models\CensoredWords;
use App\Models\AssessmentSchedule;
use Carbon\Carbon;


class CommentsController extends Controller
{
    public function add(Request $request)
    {
        $message = $request->get("comment");

        $post_id = $request->get("post_id");
        $user = request()->user();
       /* if Student you can not submit any comment when assessment time is over*/
       $post = Post::find($post_id);
        if($user->account_type_id==2){

          $schedule = AssessmentSchedule::where("assessment_id",$post->reference_id)->first();

            if( Carbon::now()->timestamp > strtotime($schedule->end_time)){
                return response()->json([
                    "status" => "error",
                    "comment" => null,
                    "message" => "You can't comment on this post, because the assessment  is over."
                ], 200);
            }
        }

        $account_id = $request->get("account_id");
        $role = $user->role->name;
        $form = formName($post_id);

        //sennsorship comment
        $messageParts = explode(" ", $message);
        $censoredParts = [];
        foreach($messageParts as $messagePart){
            $censorshipWord = CensorshipWordList::nameIgnoreCase($messagePart)->where("is_active", true)->first();
            if($censorshipWord === NULL){
                $censoredParts[] = $messagePart;
                continue;
            }
            $censoredParts[] = $censorshipWord->replace_with;
            CensoredWords::updateOrCreate(
                ['description' => $censorshipWord->name, 'account_id' => $account_id],
                ['description' => $censorshipWord->name, 'account_id' => $account_id, 'insert_time' => time()]
            );
        }

        $message = implode(" ", $censoredParts);

        if ($request->has('id')) {
            $id = $request->get('id');
            $comment = Comment::find($id);
            if($comment === NULL){
                return response()->json([
                    "status" => "error",
                    "comment" => null,
                    "message" => "Comment not found"
                ], 200);
            }
            $comment->comment = $message;
            $comment->post_id = $post_id;
            $comment->account_id = $account_id;
            $comment->last_update = date("Y-m-d H:i:s");
            $comment->save();
        }else{
            $comment = Comment::create([
                'account_id' => $account_id,
                'post_id' => $post_id,
                'comment' => $message,
                'last_update' => date("Y-m-d H:i:s")
            ]);
        }
        $comment->fresh();
        $commentArray = $comment->toArray();
        $commentArray["replies"] = [];
        $response = [
            'status' => 'success',
            'comment' => $commentArray
        ];
        event(new CommentAddedEvent($post_id, $account_id, $role, $comment->id, $form,$post->reference_id));
        return response()->json($response, 200);
    }

    public function get($id)
    {
        $comment = Comment::with("replies")->orderBy('id', 'desc')->find($id);
        $commentArray = $comment->toArray();
        $commentArray['replies'] = $comment->replies->toArray();
        $postArray = $comment->post->toArray();
        $postArray["first_name"] = $comment->post->account->first_name;
        $postArray["last_name"] = $comment->post->account->last_name;
        $commentArray["post"] = $postArray;
        if($comment){
            return response()->json(['status' => 'success', 'comment' => $commentArray, 'message' => 'Comment'], 200);
        }
        return response()->json(['status' => 'error', 'message' => 'Comment'], 200);
    }
}
