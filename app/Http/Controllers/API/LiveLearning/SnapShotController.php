<?php

namespace App\Http\Controllers\API\LiveLearning;

use App\Http\Controllers\Controller;
use App\Models\Assessment;
use App\Models\AssessmentQuestionAttempt;
use App\Models\Post;
use App\Models\Response;
use Illuminate\Support\Facades\DB;

class SnapShotController extends Controller
{
    public function teachersSnapShot($assessment_id){
        $assessment = Assessment::find($assessment_id);
        $attempts = AssessmentQuestionAttempt::where("assessment_id", $assessment_id)->orderBy("id", "ASC")->get();
        $data  = [
            "details" => [],
            "Form - 1A" => [
                "responses" => [],
                "results" => [],
            ],
            "Form - 1B" => [
                "responses" => [],
                "results" => [],
            ],
            "Form - 2" => [
                "responses" => [],
                "results" => [],
            ],
        ];
        $forms = ["Form - 1A", "Form - 1B", "Form - 2"];    

        $data["details"] = [
            "title" => $assessment->title,
            "class_name" => $assessment->course->class_name,
            "teacher_name" => $assessment->account_name,
            "grade" => $assessment->group->name,
            "topic" => $assessment->topic->name,
            "standard" => $assessment->standard->name,
            "description" => $assessment->description
        ];
        

        $attempts->each(function($attempt, $index) use(&$data, $forms){
            $form = $forms[$index];
            $posts = Post::with("assessmentResponse")
                         ->where(["attempt_id" => $attempt->id, "reference_id" => $attempt->assessment_id])
                         ->select(DB::raw("count(*) as students, assessment_response_id"))
                         ->groupBy("assessment_response_id")
                         ->get();
            $posts->each(function($post) use(&$data, &$form){
                $data[$form]["responses"][] = [
                    "response" => $post->assessment_response_id !== NULL ? $post->assessmentResponse->response : "No Answer Given",
                    "students" => $post->students,
                ];
            }); 

            $posts = Post::with("account")->where(["attempt_id" => $attempt->id, "reference_id" => $attempt->assessment_id])->get();
            $posts->each(function($post) use(&$data, &$form){
                $answer = "";

                $correctAnswer = Response::where("is_valid", true)->whereIn("question_id",function($query) use($post){
                    $query->select("question_id")->from("assessment_questions")->where("id", $post->assessment_question_id);
                })->first();

                if($correctAnswer !== NULL){
                    $answer = $correctAnswer->description;
                }

                $data[$form]["results"][] = [
                    "student_name" => "{$post->account->first_name} {$post->account->last_name}",
                    "response" => $post->body[0]["response"],
                    "correct" => $post->body[0]["response"] === $answer ? "YES": "NO",
                    "posted_on" => $post->last_update
                ];
            }); 

        });

        $response = [
            "snapshot" => $data,
            "status" => "success",
            "message" => 200
        ];

        return response()->json($response, 200);
    }
}
