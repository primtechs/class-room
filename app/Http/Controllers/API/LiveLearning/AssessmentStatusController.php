<?php

namespace App\Http\Controllers\API\LiveLearning;

use App\Events\AssessmentTimeAdjustEvent;
use App\Http\Controllers\Controller;
use App\Models\AssessmentStatus;
use App\Models\AssessmentQuestionAttempt;
use App\Models\AssessmentQuestion;
use Illuminate\Http\Request;
use Carbon\Carbon;

class AssessmentStatusController extends Controller
{
    public function adjustTime(Request $request){
        $assessment_id = $request->get("assessment_id");
        $time = $request->get("time");
        $type = $request->get("type");

        $assessmentStatus = AssessmentStatus::where("assessment_id", $assessment_id)->first();
        if($assessmentStatus){
            $endTime = Carbon::parse($assessmentStatus->end_time);
            if($type === 'increase'){
                $assessmentStatus->end_time = $endTime->addMinute($time)->format("Y-m-d H:i:s");
            }else{
                $assessmentStatus->end_time = $endTime->subMinute($time)->format("Y-m-d H:i:s");
            }
            $assessmentStatus->save();
            event(new AssessmentTimeAdjustEvent($assessment_id, $time, $type));
            return response()->json(['status' => 'success', 'time' => $time, 'type' => $type], 200);
        }
        return response()->json(['status' => 'error', 'message' => 'Status not found'], 200);
    }
    public function elapsedtime ($assessment_id){
        $assessmentStatus = AssessmentStatus::where('assessment_id', (int)$assessment_id)->where('end_time', '>=', date('Y-m-d H:i:s'))->first();

        if($assessmentStatus){
            $endTime = Carbon::parse($assessmentStatus->end_time);
            $now = Carbon::now();
            $minuites = $endTime->diffInMinutes($now);
            if($minuites<0) $minuites=0;
            $seconds = ($endTime->diffInSeconds($now))-($minuites*60);
            $attempt = AssessmentQuestionAttempt::where("assessment_id", (int)$assessment_id)->orderBy("id", "DESC")->first();
            if($attempt === NULL){
                return response()->json(['status' => 'success', 'question_id' =>0,'seconds' => 0, 'minuites' => 0,'end_time' => $assessmentStatus->end_time], 200);
            }
            $question = AssessmentQuestion::where("id", $attempt->assessment_question_id)->first();
            $question_id=$question->id;
            return response()->json(['status' => 'success', 'question_id' => $question_id,'seconds' => $seconds, 'minuites' => $minuites,'end_time' => $assessmentStatus->end_time], 200);
        }
        return response()->json(['status' => 'success', 'question_id' =>0,'seconds' =>0, 'minuites' => 0,'end_time' => ''], 200);
    }
}
