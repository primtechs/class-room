<?php

namespace App\Http\Controllers\API\LiveLearning;

use App\Events\AssessmentSettingsChangedEvent;
use App\Http\Controllers\Controller;
use App\Models\AssessmentSetting;
use Illuminate\Http\Request;

class AssessmentSettingsController extends Controller
{
    //
    public function get($assessment_id){
        $settings = AssessmentSetting::where("assessment_id", $assessment_id)->first();
        if($settings === NULL){
            $settings = AssessmentSetting::create([
                "assessment_id" => $assessment_id,
                "can_comment" => true
            ]);
        }
        $data = [
            "status" => "success",
            "settings" => $settings
        ];
        return response()->json($data, 200);
    }

    public function update(Request $request){
        $assessment_id = $request->get("assessment_id");
        $can_comment = $request->get("can_comment");
        $settings = AssessmentSetting::where("assessment_id", $assessment_id)->first();
        $settings->can_comment = $can_comment;
        $settings->save();
        $settings = $settings->fresh();
        event(new AssessmentSettingsChangedEvent($settings));
        $data = [
            "status" => "success",
            "settings" => $settings
        ];
        return response()->json($data, 200);
    }
}
