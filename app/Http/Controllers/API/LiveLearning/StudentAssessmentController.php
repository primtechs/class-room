<?php

namespace App\Http\Controllers\API\LiveLearning;

use App\Http\Controllers\Controller;
use App\Models\StudentClass;
use App\Models\AssessmentSchedule;
use App\Models\Assessment;
use App\Models\AssessmentQuestion;
use App\Models\AssessmentQuestionAttempt;
use App\Models\AssessmentStatus;
use App\Models\Post;
use Carbon\Carbon;

class StudentAssessmentController extends Controller
{

    public function activeAssessment(){
        $user = request()->user();

        $classes = StudentClass::where("account_id", $user->id)->pluck("class_id")->toArray();

        $schedule = AssessmentSchedule::whereIn("class_id", $classes)
                                      ->where("status", "Active")
                                      ->where("start_time", "<=", date("Y-m-d H:i:s"))
                                      ->where("end_time", ">=", date("Y-m-d H:i:s"))
                                      ->first();                     
        if($schedule){
            $assessment = Assessment::find($schedule->assessment_id);
            $attempt = AssessmentQuestionAttempt::where("assessment_id", $assessment->id)->orderBy("id", "DESC")->first();
            if($attempt === NULL){
                return response()->json(['status' => 'error', 'message' => 'No Active Assessment Available'], 200);
            }
            $question = AssessmentQuestion::where("id", $attempt->assessment_question_id)->first();
            $assessmentStatus = AssessmentStatus::where("assessment_id", $assessment->id)->first();
            $post = Post::where(["account_id" => $user->id, "attempt_id" => $attempt->id])->first();
            $postResponse = null;
            if($post){
                $postResponse['post'] = $post;
                $postResponse['comments'] = $post->comments->toArray();
                $postResponse['reactions'] = [
                    'total_likes' => $post->reactions()->where("reaction_type", "like")->pluck("account_id"),
                    'total_dislikes' => $post->reactions()->where("reaction_type", "dislike")->pluck("account_id")
                ];
            }

            $time = [
                'hour' => 0,
                'minute' => 0,
                'second' => 0
            ];

            if($assessmentStatus && $assessmentStatus->status !== "finished"){
                $endTime = Carbon::parse($assessmentStatus->end_time);
                $now = Carbon::now();
                if($endTime->gt($now)){
                    $difference = gmdate("H:i:s",$endTime->diffInSeconds($now));
                    $timeParts = explode(":", $difference);
                    $time['hour'] = $timeParts[0];
                    $time['minute'] = $timeParts[1];
                    $time['second'] = $timeParts[2];
                }
            }

            $data = [
                'status' => "success",
                'assessment' => $assessment,
                'schedule' => $schedule->toArray(),
                'attempt' => $attempt->toArray(),
                'question' => $question,
                'post_response' => $postResponse,
                'time' => $time,
                'message' => 'Active Assessment Found'
            ];
            return response()->json($data, 200);
        }
        return response()->json(['status' => 'error', 'message' => 'No Active Assessment Available'], 200);
    }

}
