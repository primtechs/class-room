<?php

namespace App\Http\Controllers\API\LiveLearning;

use App\Events\AssessmentFinishedEvent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Assessment;
use App\Models\AccountAssessment;
use App\Models\AssessmentQuestionResult;
use App\Models\AssessmentSchedule;
use App\Models\AssessmentQuestion;
use App\Models\AssessmentQuestionAttempt;
use App\Models\Post;
use App\Models\PostReaction;
use App\Models\Comment;
use App\Events\AssessmentStartedEvent;
use App\Events\AssessmentLoginEvent;
use App\Events\AssessmentCancelEvent;
use App\Models\AssessmentStatus;
use App\Models\AssessmentSession;
use App\Models\StudentClass;
use Carbon\Carbon;
use App\Models\Reply;

class AssessmentController extends Controller
{

    public function start(Request $request)
    {
        $id =$request->get("id");
        $time = $request->get("time");

        $assessment = Assessment::find($id);
        $user = request()->user();

        $schedule = AssessmentSchedule::where("assessment_id",$id)->first();

        if($schedule){
            if($assessment->account_id!=$user->id){
                return response()->json(['status' => 'error', 'message' => 'You are not authorize to start the assessment'], 200);
            }
			if(Carbon::now()->timestamp  >= strtotime($schedule->start_time) && Carbon::now()->timestamp < strtotime($schedule->end_time)){
			  if($schedule->status === "Active"){
                return response()->json(['status' => 'error', 'message' => 'This assessment is running'], 200);
				}

				$startTimeCount = AssessmentSchedule::where(["status" => "Active","class_id" => $assessment->class_id])->whereBetween("start_time",[
					$schedule->start_time, $schedule->end_time
				])->count();

				$endTimeCount = AssessmentSchedule::where(["status" => "Active","class_id" => $assessment->class_id])->whereBetween("end_time",[
					$schedule->start_time, $schedule->end_time
				])->count();

				if($startTimeCount > 0 || $endTimeCount > 0){
					return response()->json(['status' => 'error', 'message' => 'Already an assessment is running'], 200);
				}
			}
			else {
				  return response()->json(['status' => 'error', 'message' => 'Incorrect Schedule'], 200);
			}

        }

        $assessmentStatus = AssessmentStatus::where("assessment_id", $id)->first();

        if($assessmentStatus){
            $assessmentStatus->start_time = Carbon::now()->format("Y-m-d H:i:s");
            $assessmentStatus->end_time = Carbon::now()->addMinute($time)->format("Y-m-d H:i:s");
            $assessment->last_update = Carbon::now()->format("Y-m-d H:i:s");
            $assessmentStatus->status = "active";
            $assessmentStatus->save();
        }else{
            AssessmentStatus::create([
                "assessment_id" => $id,
                "start_time" => Carbon::now()->format("Y-m-d H:i:s"),
                "end_time" => Carbon::now()->addMinute($time)->format("Y-m-d H:i:s"),
                "status" => "Active",
                "last_update" => Carbon::now()->format("Y-m-d H:i:s")
            ]);
        }

        $questions = AssessmentQuestion::where("assessment_id", $id)->pluck("id")->toArray();
        $attempt = AssessmentQuestionAttempt::create([
            "assessment_id" => $id,
            "assessment_question_id" => $questions[0],
            "last_update" => date("Y-m-d H:i:s")
        ]);

        $assessment->status = "Active";
        $assessment->save();

        $schedule->status = "Active";
        $schedule->save();

        event(new AssessmentStartedEvent($attempt, ['hour' => 0, 'minute'=> $time, 'second' => 0]));
        return response()->json(['status' =>'success',  'message' => 'Assessment Activated', 'attempt'=> $attempt], 200);
    }

    public function status(Request $request){
        $id = $request->get("id");
        $type = $request->get("type");
        $time = $request->get("time");



        if($type === "finished"){
            $assessmentStatus = AssessmentStatus::where("assessment_id", $id)->first();
            $assessmentStatus->status= $type;
            $assessmentStatus->save();

            $assessment = Assessment::find($id);
            $schedule = AssessmentSchedule::where("assessment_id",$id)->first();

            $assessment->status = "finished";
            $assessment->save();

            $schedule->status = "close";
            $schedule->save();
            event(new AssessmentFinishedEvent($id));
            return response()->json(["status" => "finished", "message" => "Assessment Finished Successfully"], 200);
        }

        $assessment = Assessment::find($id);
        $assessmentStatus = AssessmentStatus::where("assessment_id", $id)->first();

        if($assessmentStatus){
            $assessmentStatus->start_time = Carbon::now()->format("Y-m-d H:i:s");
            $assessmentStatus->end_time = Carbon::now()->addMinute($time)->format("Y-m-d H:i:s");
            $assessment->last_update = Carbon::now()->format("Y-m-d H:i:s");
            $assessmentStatus->status = $type;
            $assessmentStatus->save();
        }else{
            AssessmentStatus::create([
                "assessment_id" => $id,
                "start_time" => Carbon::now()->format("Y-m-d H:i:s"),
                "end_time" => Carbon::now()->addMinute($time)->format("Y-m-d H:i:s"),
                "status" => $type,
                "last_update" => Carbon::now()->format("Y-m-d H:i:s")
            ]);
        }

        $questions = AssessmentQuestion::where("assessment_id", $id)->pluck("id")->toArray();
        $data = [
            "assessment_id" => $id,
            "assessment_question_id" => 0,
            "last_update" => date("Y-m-d H:i:s")
        ];

        switch($type){
            case "re-assign":
                $data["assessment_question_id"] = $questions[0];
                break;
            case "next":
                $data["assessment_question_id"] = $questions[1];
                break;
        }
        $GenerateEventOnStudentSide=1;

        $attempt = AssessmentQuestionAttempt::create($data);

        if($type === "re-assign"||$type === "next"){
            $schedule = AssessmentSchedule::where("assessment_id",  $id)
            ->where("start_time", "<=", date("Y-m-d H:i:s"))
            ->where("end_time", ">=", date("Y-m-d H:i:s"))->first();

            if(!$schedule) {
                $GenerateEventOnStudentSide=0;
            }

        }


        event(new AssessmentStartedEvent($attempt, ['hour' => 0, 'minute'=> $time, 'second' => 0],$GenerateEventOnStudentSide));
        $response = [
            "status" => "success",
            "attempt" => $attempt,
            "message" => "",
        ];
        return response()->json($response, 200);
    }

    public function close($id){

        Assessment::where("id", $id)->update(['submitted' => false, 'status' => 'Active']);
        AssessmentSchedule::where("assessment_id", $id)->update(['status' => 'close']);
        AssessmentQuestion::where("assessment_id", $id)->update(['answered' => false]);
        AssessmentQuestion::where("assessment_id", $id)->update(['answered' => false]);
        $posts = Post::where(['reference_id' => $id])->get()->pluck("id")->toArray();
        PostReaction::whereIn("post_id", $posts)->delete();

        $comments = Comment::whereIn("post_id", $posts)->get()->pluck("id")->toArray();
        Reply::whereIn("comment_id", $comments)->delete();
        Comment::whereIn("post_id", $posts)->delete();

        Post::whereIn("id", $posts)->delete();
        AccountAssessment::where("assessment_id", $id)->delete();

        $attempts = AssessmentQuestionAttempt::where("assessment_id", $id)->pluck("id")->all();
        AssessmentQuestionResult::whereIn("attempt_id", $attempts)->delete();
        AssessmentQuestionAttempt::where("assessment_id", $id)->delete();
        AssessmentSession::where("assessment_id", $id)->delete();
        AssessmentStatus::where("assessment_id", $id)->delete();
        event(new AssessmentCancelEvent($id));
        return response()->json(['status' => 'success', 'message' => 'Assessment Cleared']);

    }

    public function get($id){
        $assessment = Assessment::find($id);

        $assessmentStatus = AssessmentStatus::where("assessment_id", $assessment->id)->first();
        $schedule = AssessmentSchedule::where("assessment_id",$assessment->id)->first();

        $start_time = Carbon::parse($schedule->start_time);
        $end_time = Carbon::parse($schedule->end_time);
        $schedule_date = $start_time->format('j F Y');
        $schedule_time = [
            'start_time' => $start_time->format('H:i'),
            'end_time' => $end_time->format('H:i'),
            'schedule_date' => $schedule_date
        ];

        $time = [
            'hour' => 0,
            'minute' => 0,
            'second' => 0
        ];

        if($assessmentStatus && $assessmentStatus->status !== "finished"){
            $endTime = Carbon::parse($assessmentStatus->end_time);
            $now = Carbon::now();
            if($endTime->gt($now)){
                $difference = gmdate("H:i:s",$endTime->diffInSeconds($now));
                $timeParts = explode(":", $difference);
                $time['hour'] = $timeParts[0];
                $time['minute'] = $timeParts[1];
                $time['second'] = $timeParts[2];
            }
        }
        $totalStudent = StudentClass::leftjoin("accounts",'student_class.account_id', '=', 'accounts.id')->where("class_id", $assessment->class_id)->count();
        $Presentstudents = AssessmentSession::leftJoin('accounts', 'assessment_session.account_id', '=', 'accounts.id')-> where("assessment_session.assessment_id", $id)->orderBy("assessment_session.id","DESC")->get();
        $students=array();
        foreach($Presentstudents as $student){
            $name = "{$student->first_name} {$student->last_name}";
            $count = AssessmentQuestionResult::whereIn("attempt_id", function($query) use($id){
                $query->select("id")->from("assessment_question_attempt")->where("assessment_id", $id);
            })->where("account_id", $student->account_id)->count();
            $students[] = [
                "name" => $name,
                "responders" => $count,
                "id" => $student->id,
                "first_name" => $student->first_name,
                "last_name" => $student->last_name,
                "student_id" => $student->id,
                "logintime" => $student->logintime,

            ];
        }
        $absents=array();
        $AbsentStudents = StudentClass::leftjoin("accounts",'student_class.account_id', '=', 'accounts.id')->whereNotIn("student_class.account_id", function($query) use($id){
            $query->select("account_id")->from("assessment_session")->where("assessment_id", $id);
        })->where("class_id", $assessment->class_id)->get();
        foreach($AbsentStudents as $student){
            $name = "{$student->first_name} {$student->last_name}";
            $absents[] = [  "name" => $name,  "first_name" => $student->first_name, "last_name" => $student->last_name, "student_id" => $student->id,"id" => $student->id,"responders" => 0,"logintime" => ''];
        }
        $data = [
            'assessment' => $assessment->toArray(),
            'totalStudent' => $totalStudent,
            'Presentstudents' => $students,
            'AbsentStudents' => $absents,
            'time' => $time,
            'schedule_time' => $schedule_time,
            'status' => 'success',
            'message' => 'Assessment Details'
        ];
        return response()->json($data, 200);
    }

    public function studentResponded($id){
        $response = [
            'status' => 'success',
            'responses' => [],
            'students' => [],
            'message' => 'Students Responded'
        ];
        $attempts = AssessmentQuestionAttempt::where("assessment_id", $id)->pluck("id")->toArray();


        $posts = Post::with("account")->whereIn("attempt_id", function($query) use($id){
            $query->select("id")->from("assessment_question_attempt")->where("assessment_id", $id);
        })->orderBy("last_update", "DESC")->get();

        $posts->each(function($post) use(&$response, $id,$attempts){
            $name = "{$post->account->first_name} {$post->account->last_name}";

            $found = array_search($name, array_column($response['students'], 'name'));
            if($found === false){
                $count = AssessmentQuestionResult::whereIn("attempt_id", function($query) use($id){
                    $query->select("id")->from("assessment_question_attempt")->where("assessment_id", $id);
                })->where("account_id", $post->account_id)->count();
                $response['students'][] = [
                    "name" => $name,
                    "responders" => $count,
                    "logintime" => '',
                ];
            }
            $form="Form - 1A";
            if(isset($attempts[0]) && $post->attempt_id == $attempts[0] ){
                $post_attempt=1;
                $form="Form - 1A";
            }
            elseif(isset($attempts[1]) && $post->attempt_id == $attempts[1] ){
                $post_attempt=2;
                $form="Form - 1B";
            }
            elseif(isset($attempts[2])&& $post->attempt_id == $attempts[2] ){
                $post_attempt=3;
                $form="Form - 2";
            }

            $response['responses'][] = [
                'post_id' => $post->id,
                'name' => $name,
                'response' => $post->body[0]['response'],
                'title' => $post->body[0]['detail']['title'],
                'last_update' => $post->last_update,
                'attempt' => $post_attempt,
                'form' => $form,
            ];
        });

        // I do not understand your previous loop that is why I have added student query again. kindly remove it when you update the query with assessment_session table
        $assessment_id=$id;
        $response['students']=array();
        $Presentstudents = AssessmentSession::leftJoin('accounts', 'assessment_session.account_id', '=', 'accounts.id')-> where("assessment_session.assessment_id", $assessment_id)->orderBy("assessment_session.id","DESC")->get();
        foreach($Presentstudents as $student){
            $name = "{$student->first_name} {$student->last_name}";
            $count = AssessmentQuestionResult::whereIn("attempt_id", function($query) use($assessment_id){
                $query->select("id")->from("assessment_question_attempt")->where("assessment_id", $assessment_id);
            })->where("account_id", $student->account_id)->count();
            $response['students'][] = [
                "name" => $name,
                "id" => $student->id,
                "student_id" => $student->id,
                "first_name" => $student->first_name,
                "last_name" => $student->last_name,
                "responders" => $count,
                "logintime" => $student->logintime,

            ];
        }
        return response()->json($response, 200);
    }

    public function liveFeed($reference_id)
    {
        $data = [];
        $attempts = AssessmentQuestionAttempt::where("assessment_id", $reference_id)->orderBy("id","DESC")->get();
        $attemptCount= count($attempts);
        $labels = config("labels.$attemptCount");

        foreach($attempts as $index => $attempt){
             $data[$labels[$index]] = [];
            $posts = Post::with(["comments", "account", "assessment_question"])->where("attempt_id", $attempt->id)->get();
            $label = $labels[$index];
            foreach($posts as $post){
                $comments = $post->comments()->with("replies")->get();
                $postArray = $post->toArray();
                $postArray['account'] = $post->account->toArray();
                $postArray['comments'] = [];
                foreach($comments as $comment){
                    $commentArray = $comment->toArray();
                    $commentArray["replies"] = $comment->replies->toArray();
                    $postArray["comments"][] = $commentArray;
                }
                $postArray['question'] = $post->assessment_question->toArray();
                $postArray['total_likes'] = $post->reactions()->where("reaction_type", "like")->pluck("account_id")->toArray();
                $postArray['total_dislikes'] = $post->reactions()->where("reaction_type", "dislike")->pluck("account_id")->toArray();
                $data[$label][] = $postArray;
            }
        }

        return response()->json($data, 200);
    }
    public function studentlogin($assessment_id){
        $user = request()->user();
        $classes = StudentClass::where("account_id", $user->id)->pluck("class_id")->toArray();

        $schedule = AssessmentSchedule::leftJoin('assessment','assessment_schedule.assessment_id',  '=', 'assessment.id')
                                      -> whereIn("assessment_schedule.class_id", $classes)
                                      ->where("assessment.status", '!=' ,"finished")
                                      ->where("assessment_schedule.start_time", "<=", date("Y-m-d H:i:s"))
                                      ->where("assessment_schedule.end_time", ">=", date("Y-m-d H:i:s"))
                                      ->first();
        if($schedule){
            $assessment_id=$schedule->assessment_id;
            $assessment = Assessment::find($assessment_id);
            $assessement_session=AssessmentSession::insertOrIgnore(
                [
                    [ 'assessment_id'=>$assessment_id,
                    'account_id' => $user->id,
                    'logintime' =>  date("Y-m-d H:i:s"),
                    ]
                ]
            );

            if( $assessement_session) {
                $response=[];
                $response['name'] =  "{$user->first_name} {$user->last_name}";
                $response['id'] =  $user->id;
                $response['student_id'] =  $user->id;
                $response['first_name'] =  $user->first_name;
                $response['last_name'] =  $user->last_name;
                $response['assessment_id'] =$assessment_id;
                $response['logintime'] =date("Y-m-d H:i:s");
                $response['responders'] =0;
                event(new AssessmentLoginEvent($response));

            }

            $data = [
                'status' => 'success',
                'assessment_id' =>$assessment_id,
                'message' => 'Schedule Assessment Found'
            ];



            return response()->json($data, 200);
        }
        return response()->json(['status' => 'error',  'assessment_id' =>0, 'message' => 'No Schedule Assessment Available'], 200);


    }
    public function getstudentRespond($assessment_id,$student_id){
        $data=[];

        $Presentstudents = AssessmentSession::leftJoin('accounts', 'assessment_session.account_id', '=', 'accounts.id')-> where("assessment_session.account_id", $student_id)-> where("assessment_session.assessment_id", $assessment_id)->orderBy("assessment_session.id","DESC")->get();
        foreach($Presentstudents as $student){

            $name = "{$student->first_name} {$student->last_name}";
            $count = AssessmentQuestionResult::whereIn("attempt_id", function($query) use($assessment_id){
                $query->select("id")->from("assessment_question_attempt")->where("assessment_id", $assessment_id);
            })->where("account_id", $student->account_id)->count();
            $data = [
                "id" => $student->id,
                "student_id" => $student->id,
                "name" => $name,
                "first_name" => $student->first_name,
                "last_name" => $student->last_name,
                "assessment_id" => $assessment_id,
                "responders" => $count,
                "logintime" => $student->logintime,
            ];
        }

        return response()->json($data, 200);

    }

    public function summary($id = 0){

        if($id){
            $assessment = Assessment::find($id);
        }else{
            //this is specifically for student
            $user = auth()->user();
            $assessment = Assessment::whereIn("class_id", function($query) use($user){
                $query->select("class_id")->from("student_class")->where("account_id", $user->id);
            })->where("status", "finished")->orderBy("id", "DESC")->first();
        }

        if($assessment === NULL || $assessment->status !== "finished"){
            return response()->json(["status" => "error", "summary" => NULL], 200);
        }

        $forms = ["Form - 1A", "Form - 1B", "Form - 2"];

        $summary = [
            "Form - 1A" => [
                "total_likes" => 0,
                "total_comments" => 0
            ],
            "Form - 1B" => [
                "total_likes" => 0,
                "total_comments" => 0
            ],
            "Form - 2" => [
                "total_likes" => 0,
                "total_comments" => 0
            ]
        ];

        $attempts = AssessmentQuestionAttempt::where("assessment_id",$assessment->id)->orderBy("id", "ASC")->get();

        $attempts->each(function($attempt, $index) use (&$summary, $forms, $assessment){
            $form = $forms[$index];
            $summary[$form]["total_likes"] = PostReaction::whereIn("post_id", function($query) use($attempt, $assessment){
                $query->select("id")->from("posts")->where("reference_id", $assessment->id)->where("attempt_id", $attempt->id)->where("reaction_type", "like");
            })->count();
            $summary[$form]["total_comments"] = Comment::whereIn("post_id",function($query) use($attempt, $assessment){
                $query->select("id")->from("posts")->where("reference_id", $assessment->id)->where("attempt_id", $attempt->id);
            })->count();
        });

        return response()->json(["status" => "success", "summary" => $summary], 200);

    }

    public function runningAssessment($assessment_id)
    {
        $account = request()->user();
        $type = strtolower($account->accountType->name);
        $classes = [];
        $data['status']= "error";
        $data['message'] = "No Assessment is running";
        if($type === "teachers" || $type === "admin"){
            $schedule = AssessmentSchedule::where("status", "Active")
                                          ->where("start_time", "<=", date("Y-m-d H:i:s"))
                                          ->where("end_time", ">=", date("Y-m-d H:i:s"))
                                          ->whereIn("assessment_id", function($query) use($account){
                                            $query->select("id")->from("assessment")->where("account_id", $account->id);
                                          })->first();
        }else{
            $classes = StudentClass::where("account_id", $account->id)->pluck("class_id")->toArray();
            $schedule = AssessmentSchedule::whereIn("class_id", $classes)
                                          ->where("status", "Active")
                                          ->where("start_time", "<=", date("Y-m-d H:i:s"))
                                          ->where("end_time", ">=", date("Y-m-d H:i:s"))
                                          ->first();

        }
        if($schedule){
            if($assessment_id == $schedule->assessment_id){
                $data['status'] = "success";
                $data['message'] = "";
            }
        }
        return response()->json($data, 200);

    }

    public function commentLogs($id){

        $comments = Comment::with("post")->whereIn("post_id", function($query) use($id){
            $query->select("id")->from("posts")->whereIn("attempt_id", function($subQuery) use($id){
                $subQuery->select("id")->from("assessment_question_attempt")->where("assessment_id", $id);
            });
        })->orderBy("id", "DESC")->get();

        $response = [
            "comment_logs" => [],
            "status" => "success",
            "message" => ""
        ];
        foreach($comments as $comment){
            $post = $comment->post;
            $postedBy = $post->account;
            $commentArray = [
                'id' => $comment->id,
                'post_id' => $post->id,
                'posted_by' => "{$postedBy->first_name} {$postedBy->last_name}",
                'commented_by' => "{$comment->first_name} {$comment->last_name}",
                'response' => $post->body[0]['response'],
                'title' => $post->body[0]['detail']['title'],
                'comment' => $comment->comment
            ];
            $response["comment_logs"][] = $commentArray;
        }

        return response()->json($response, 200);

    }
}
