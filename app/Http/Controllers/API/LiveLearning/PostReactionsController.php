<?php

namespace App\Http\Controllers\API\LiveLearning;

use App\Events\PostReactionAddedEvent;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostReaction;
use Illuminate\Http\Request;

class PostReactionsController extends Controller
{
    public function submitReaction(Request $request)
    {
        $reaction_type = $request->get("reaction_type");
        $post_id = $request->get("post_id");
        $user = request()->user();
        $role = $user->role->name;
        $post = Post::find($post_id);
        $form = formName($post_id);
        $reaction  = PostReaction::where(["post_id" => $post_id, "account_id" => $user->id])->first();
        if($reaction){
            $reaction->reaction_type = $reaction_type;
            $reaction->last_update = date("Y-m-d H:i:s");
            $reaction->save();
        }else{
            $reaction = PostReaction::create([
                "post_id" => $post_id,
                "account_id" => $user->id,
                "reaction_type" => $reaction_type,
                "last_udate" => date("Y-m-d H:i:s")
            ]);
        }

        $total_likes = PostReaction::where("post_id", $post_id)->where("reaction_type", "like")->pluck("account_id")->toArray();
        $total_dislikes = PostReaction::where("post_id", $post_id)->where("reaction_type", "dislike")->pluck("account_id")->toArray();
        event(new PostReactionAddedEvent($reaction->id, $post_id, $user->id, $role, $form, $reaction_type, $post->reference_id, $post->account_id));
        return response()->json(["total_likes" => $total_likes, "total_dislikes" =>$total_dislikes], 200);
    }
}
