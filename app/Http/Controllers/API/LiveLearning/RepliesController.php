<?php

namespace App\Http\Controllers\API\LiveLearning;

use App\Events\ReplyAddedEvent;
use App\Http\Controllers\Controller;
use App\Models\CensoredWords;
use App\Models\CensorshipWordList;
use App\Models\Reply;
use Illuminate\Http\Request;
use App\Models\AssessmentSchedule;
use Carbon\Carbon;
use App\Models\Post;
use App\Models\Comment;
class RepliesController extends Controller
{
    public function add(Request $request)
    {
        $message = $request->get("reply");

        $comment_id = $request->get("comment_id");
        $user = request()->user();
        /* if Student you can not submit any reply when assessment time is over */
        $comment = Comment::find($comment_id);
        $post = Post::find($comment->post_id);
         if($user->account_type_id==2){


           $schedule = AssessmentSchedule::where("assessment_id",$post->reference_id)->first();

             if( Carbon::now()->timestamp > strtotime($schedule->end_time)){
                 return response()->json([
                     "status" => "error",
                     "comment" => null,
                     "message" => "You can't reply on this post, because the assessment  is over."
                 ], 200);
             }
         }
        $account_id = $request->get("account_id");

        //sennsorship comment
        $messageParts = explode(" ", $message);
        $censoredParts = [];
        foreach($messageParts as $messagePart){
            $censorshipWord = CensorshipWordList::nameIgnoreCase($messagePart)->where("is_active", true)->first();
            if($censorshipWord === NULL){
                $censoredParts[] = $messagePart;
                continue;
            }
            $censoredParts[] = $censorshipWord->replace_with;
            CensoredWords::updateOrCreate(
                ['description' => $censorshipWord->name, 'account_id' => $account_id],
                ['description' => $censorshipWord->name, 'account_id' => $account_id, 'insert_time' => time()]
            );
        }

        $message = implode(" ", $censoredParts);

        if ($request->has('id')) {
            $id = $request->get('id');
            $reply = Reply::find($id);
            if($reply === NULL){
                return response()->json([
                    "status" => "error",
                    "reply" => null,
                    "message" => "Reply not found"
                ], 200);
            }
            $reply->reply = $message;
            $reply->comment_id = $comment_id;
            $reply->account_id = $account_id;
            $reply->last_update = date("Y-m-d H:i:s");
            $reply->save();
        }else{
            $reply = Reply::create([
                'account_id' => $account_id,
                'comment_id' => $comment_id,
                'reply' => $message,
                'last_update' => date("Y-m-d H:i:s")
            ]);
        }
        $reply->fresh();
        $response = [
            'status' => 'success',
            'reply' => $reply->toArray()
        ];
        event(new ReplyAddedEvent($comment_id, $reply->id, $post->reference_id));
        return response()->json($response, 200);
    }

    public function get($id){
        $reply = Reply::find($id);
        if($reply){
            return response()->json(['status' => 'success', 'reply' => $reply, 'message' => 'Reply'], 200);
        }
        return response()->json(['status' => 'error', 'message' => 'Reply'], 200);
    }
}
