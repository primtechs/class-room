<?php

namespace App\Http\Controllers\API\LiveLearning;

use App\Events\PostAddedEvent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\AssessmentResponse;
use App\Models\AssessmentQuestionAttempt;
use App\Models\AssessmentQuestionResult;
use App\Models\Comment;
use App\Models\PostReaction;
use App\Models\TeacherClass;
use App\Models\CensorshipWordList;

class PostController extends Controller
{

    public function  add(Request $request){
        $user = request()->user();
        $reference_id = $request->get("assessment_id");
        $assessment_id = $request->get("assessment_id");
        $response_id = $request->get("response_id");
        $comment = $request->get("comment");
        $assessmentResponse = AssessmentResponse::find($response_id);
        $assessmentQuestionAttempt = AssessmentQuestionAttempt::where("assessment_id", $reference_id)->orderBy("id", "DESC")->first();

        if($assessmentQuestionAttempt == NULL){
            return response()->json(["status" => "error", "post" => NULL, "message" => "Unable to submit Post"], 200);
        }
        // check from post  same user same assessment same attempt
        $post = Post::where([
            'account_id' => $user->id,
            'reference_id' => $reference_id,
            'reference_type' => 'submit-assessment',
            'assessment_question_id' => $assessmentQuestionAttempt->assessment_question_id,

            'attempt_id' => $assessmentQuestionAttempt->id
        ])->first();

        if($post != NULL){
            return response()->json(["status" => "error", "post" => NULL, "message" => "Already submitted"], 200);
        }
        //sennsorship comment
        $messageParts = explode(" ", $comment);
        $censoredParts = [];
        foreach($messageParts as $messagePart){
            $censorshipWord = CensorshipWordList::nameIgnoreCase($messagePart)->where("is_active", true)->first();
            if($censorshipWord === NULL){
                $censoredParts[] = $messagePart;
                continue;
            }
            $censoredParts[] = $censorshipWord->replace_with;
        }
        $comment = implode(" ", $censoredParts);

        $body = [
            [
                "comment" => $comment,
                "response" => $assessmentResponse == NULL ? "" : $assessmentResponse->response,
                "detail" => [
                    "response_id" => $response_id,
                    "response" => $assessmentResponse == NULL ? "" : $assessmentResponse->response,
                    "title" => $assessmentResponse == NULL ? "" : $assessmentResponse->title,
                ],
                "title" => $comment,

            ]
        ];

        $post = Post::create([
            'account_id' => $user->id,
            'reference_id' => $reference_id,
            'reference_type' => 'submit-assessment',
            'body' => $body,
            'assessment_question_id' => $assessmentQuestionAttempt->assessment_question_id,
            'assessment_response_id' => $response_id,
            'last_update' => date("Y-m-d H:i:s"),
            'attempt_id' => $assessmentQuestionAttempt->id
        ]);
        AssessmentQuestionResult::create([
            'attempt_id' => $assessmentQuestionAttempt->id,
            'response' =>  $assessmentResponse == NULL ? "" : $assessmentResponse->response,
            'response_id' => $assessmentResponse == NULL ? 0 : $assessmentResponse->id,
            'responders' => 1,
            'account_id' => $user->id
        ]);
        $data = [
            "status" => "success",
            "post_response" => [
                'post' => $post->toArray(),
                'comments' => [],
                'reactions' => [
                    'total_likes' => [],
                    'total_dislikes' => []
                ]
            ],
            "message" => "Successfully submitted"
        ];
        event(new PostAddedEvent($post->id, $assessment_id, $user->id));
        return response()->json($data, 200);
    }

    public function get($id){
        $post = Post::find($id);
        if($post){
            $data['status'] = "success";
            $postResponse = $post->toArray();
            $postResponse['comments'] = [];
            $comments = $post->comments()->with("replies")->get();
            foreach($comments as $comment){
                $commentArray = $comment->toArray();
                $commentArray["replies"] = $comment->replies->toArray();
                $postResponse['comments'][] = $commentArray;
            }
            $postResponse['account'] = $post->account->toArray();
            $postResponse['question'] = $post->assessment_question->toArray();
            $postResponse['total_likes'] = $post->reactions()->where("reaction_type", "like")->pluck("account_id")->toArray();
            $postResponse['total_dislikes'] = $post->reactions()->where("reaction_type", "dislike")->pluck("account_id")->toArray();
            return response()->json(['status' => 'success', 'post' => $postResponse, 'message' => 'New post received'], 200);
        }
        return response()->json(['status' => 'error', 'message' => 'No Post Found'], 200);
    }

    public function formWisePostReactions($assessment_id){
        $student = auth()->user();

        $teacherIds = TeacherClass::whereIn("class_id", function($query) use($assessment_id){
            $query->select("class_id")->from("assessment")->where("id", $assessment_id);
        })->pluck("account_id")->toArray();


        $responses = [
            "form_1A" => [
                "likes" => [
                    "given"  => [],
                    "received"  => [],
                ],
                "comments" => [
                    "given"  => [],
                    "received"  => [],
                ]
            ],
            "form_1B" => [
                "likes" => [
                    "given"  => [],
                    "received"  => [],
                ],
                "comments" => [
                    "given"  => [],
                    "received"  => [],
                ]
            ],
            "form_2" => [
                "likes" => [
                    "given"  => [],
                    "received"  => [],
                ],
                "comments" => [
                    "given"  => [],
                    "received"  => [],
                ]
            ]
        ];

        $attempts = [];
        AssessmentQuestionAttempt::where("assessment_id", $assessment_id)->orderBy("id", "ASC")->get()->each(function($attempt, $index) use(&$attempts){
            switch($index){
                case 0:
                    $attempts["form_1A"] = $attempt->id;
                    break;
                case 1:
                    $attempts["form_1B"] = $attempt->id;
                    break;
                case 2:
                    $attempts["form_2"] = $attempt->id;
                    break;
            }
        });
        foreach($attempts as $form => $attempt){
            $post = Post::where([
                'account_id' => $student->id,
                'reference_id' => $assessment_id,
                'attempt_id' => $attempt
            ])->first();

            if($post){

                $responses[$form]["likes"]["given"] = PostReaction::where([
                    "post_id" => $post->id,
                    "account_id" => $student->id,
                    "reaction_type" => "like"
                ])->pluck("id");


                $responses[$form]["comments"]["given"] = Comment::where([
                    "post_id" => $post->id,
                    "account_id" => $student->id,
                ])->get();

                $responses[$form]["likes"]["received"] = PostReaction::where([
                    "post_id" => $post->id,
                    "reaction_type" => "like"
                ])->whereIn("account_id", $teacherIds)->pluck("id");

                $responses[$form]["comments"]["received"] = Comment::where([
                    "post_id" => $post->id,
                ])->whereIn("account_id", $teacherIds)->get();
            }
        }

        return response()->json($responses, 200);

    }

    public function postReactions($assessment_id,$post_id = NULL){
        $teachers = [
            "total_likes" => 0,
            "total_dislikes" => 0,
            "comments" => []
        ];
        $students = [
            "total_likes" => 0,
            "total_dislikes" => 0,
            "comments" => []
        ];

        //constructed for Student Work Submit Result
        if($post_id == NULL){
            $user = request()->user();
            $post = Post::where([
                'account_id' => $user->id,
                'reference_id' => $assessment_id
            ])->orderBy("id", "DESC")->first();
            if($post){
                $post_id = $post->id;
            }else{
                return response()->json(["status" => "error", "teachers" => $teachers, "students" => $students, "message" => "No Post Found"], 200);
            }

        }

        $teacherIds = TeacherClass::whereIn("class_id", function($query) use($assessment_id){
            $query->select("class_id")->from("assessment")->where("id", $assessment_id);
        })->pluck("account_id");

        $teachers["total_likes"] = PostReaction::where(["post_id" => $post_id, "reaction_type" => "like"])->whereIn("account_id", $teacherIds)->pluck("account_id");
        $teachers["total_dislikes"] = PostReaction::where(["post_id" => $post_id, "reaction_type" => "dislike"])->whereIn("account_id", $teacherIds)->pluck("account_id");
        $teachers["comments"] = Comment::where("post_id", $post_id)->whereIn("account_id", $teacherIds)->orderBy("last_update", "DESC")->get();

       $user = request()->user();

        $students["total_likes"] = PostReaction::where(["post_id" => $post_id, "reaction_type" => "like"])->where("account_id", $user->id)->pluck("account_id");
        $students["total_dislikes"] = PostReaction::where(["post_id" => $post_id, "reaction_type" => "dislike"])->where("account_id", $user->id)->pluck("account_id");
        $students["comments"] = Comment::where("post_id", $post_id)->where("account_id", $user->id)->orderBy("last_update", "DESC")->get();

        return response()->json(["status" => "success", "teachers" => $teachers, "students" => $students], 200);
    }

    public function currentFormPosts($assessment_id){
        $attempt = AssessmentQuestionAttempt::where("assessment_id", $assessment_id)->orderBy("id", "DESC")->first();
        $posts = Post::where("attempt_id", $attempt->id)->get();
        $data = [
            "status" => "success",
            "posts" => [],
            "message" => "Post List"
        ];
        foreach($posts as $post){
            $postResponse = $post->toArray();
            $postResponse['comments'] = [];
            $comments = $post->comments()->with("replies")->get();
            foreach($comments as $comment){
                $commentArray = $comment->toArray();
                $commentArray["replies"] = $comment->replies->toArray();
                $postResponse['comments'][] = $commentArray;
            }
            $postResponse['account'] = $post->account->toArray();
            $postResponse['question'] = $post->assessment_question->toArray();
            $postResponse['total_likes'] = $post->reactions()->where("reaction_type", "like")->pluck("account_id")->toArray();
            $postResponse['total_dislikes'] = $post->reactions()->where("reaction_type", "dislike")->pluck("account_id")->toArray();
            $data["posts"][] = $postResponse;
        }
        return response()->json($data, 200);
    }

}
