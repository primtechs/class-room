<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\RewardPoint;
use App\Services\Reward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class RewardPointController extends Controller
{
    protected $reward;

    public function __construct(Reward $reward)
    {
        $this->reward = $reward;
    }

    public function getRewardPoint()
    {
        $account = request()->user();
        $totalPoint = DB::table('reward_points')
            ->select('type', DB::raw('sum(value) as points'))
            ->where('account_id', $account->id)
            ->groupBy('type')
            ->get();

            

        return successResponse($totalPoint, 200);
    }

    public function rewardCalculation($type, Request $request)
    {
        $account = request()->user();
        $this->reward->setData($type, $request->all(), $account);
        $this->reward->rewardPointCalculation();

        return response()->json(['status' => 'success'], 200);
    }

}
