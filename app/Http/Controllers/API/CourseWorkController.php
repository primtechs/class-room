<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CourseWorkPatch;
use App\Models\Coursework;
use App\Models\StudentClass;
use App\Models\CourseworkAssessmentResponse;
use \App\Models\Coursework as LocalCourseWork;
use App\Repository\CourseworkRepository;
use App\Rules\DueDateValidator;
use Google_Service_Classroom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class CourseWorkController extends Controller
{
    protected $coursework;

    public function __construct(CourseworkRepository $coursework)
    {
        $this->coursework = $coursework;
    }

    public function index($courseId)
    {
        $limit = config("pagination.limit");
        if($courseId !== 'all'){
            $courseWorks = LocalCourseWork::with(['courseWorkFromAssessment','courseAssessmentResponses'])
                                          ->where('external_course_id', '=', $courseId)
                                          ->orderBy("id", "DESC")
                                          ->paginate($limit)
                                          ->toArray();
        }else{
            $studentClasses = StudentClass::where([
                "account_id" => request()->user()->id,
                "status" => true
            ])->get()->pluck("class_id")->toArray();
            $courseWorks = LocalCourseWork::with(['courseWorkFromAssessment','courseAssessmentResponses'])
                                          ->whereIn('class_id', $studentClasses)
                                          ->orderBy("id", "DESC")
                                          ->paginate($limit)
                                          ->toArray();
        }

        $response = [];
        collect($courseWorks['data'])->each(function ($courseWork, $index) use(&$response){
            $data = json_decode($courseWork['metadata'], 1);
            $data['turnedIn'] = $courseWork['turned_in'];
            $data['totalAssignee'] = $courseWork['submission'];
            $data['assessment_id'] = $courseWork['assessment_id'];
            $response['courseWork'][] = $data;
        });
        $response['pagination'] = [
            'from' => $courseWorks['from'],
            'to' => $courseWorks['to'],
            'total' => $courseWorks['total'],
            'per_page' => $courseWorks['per_page'],
            'current_page' => $courseWorks['current_page'],
            'last_page' => $courseWorks['last_page'],
        ];
        return successResponse($response, null, 200);
    }

    public function syncCourseWorks($courseId)
    {
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $courseWorks = $classroom->courses_courseWork->listCoursesCourseWork($courseId);
        //start of course work syncing
        $localCourseWorkIds = [];
        foreach ($courseWorks as $courseWork) {
            if ($courseWork->state !== 'DELETED') {
                $localCourseWork = LocalCourseWork::where("external_course_work_id", $courseWork["id"])->first();
                if ($localCourseWork != null) {
                    $this->coursework->updateLocal($courseWork);
                    $localCourseWorkIds[] = $localCourseWork->id;
                } else {
                    $newCourseWork = $this->coursework->syncLocal($courseId, $courseWork);
                    $localCourseWorkIds[] = $newCourseWork->id;
                }
                $data = json_decode($localCourseWork->metadata, 1);
                $data['turnedIn'] = $localCourseWork->turned_in;
                $data['totalAssignee'] = $localCourseWork->submission;
                $data['assessment_id'] = $localCourseWork->assessment_id;
            }

        }
        $courseWorksToBeDeleted = LocalCourseWork::whereNotIn("id", $localCourseWorkIds)->where("external_course_id", $courseId)->get()->pluck("id")->toArray();
        $this->coursework->deleteLocal($courseWorksToBeDeleted);
        //end of course work syncing
        //render course works with pagination
        $response = [];
        $limit = config("pagination.limit");
        $courseWorks = LocalCourseWork::with(['courseWorkFromAssessment','courseAssessmentResponses'])->where('external_course_id', '=', $courseId)->paginate($limit)->toArray();
        $response = [];
        collect($courseWorks['data'])->each(function ($courseWork, $index) use(&$response){
            $data = json_decode($courseWork['metadata'], 1);
            $data['turnedIn'] = $courseWork['turned_in'];
            $data['totalAssignee'] = $courseWork['submission'];
            $data['assessment_id'] = $courseWork['assessment_id'];
            $response['courseWork'][] = $data;
        });
        $response['pagination'] = [
            'from' => $courseWorks['from'],
            'to' => $courseWorks['to'],
            'total' => $courseWorks['total'],
            'per_page' => $courseWorks['per_page'],
            'current_page' => $courseWorks['current_page'],
            'last_page' => $courseWorks['last_page'],
        ];
        return successResponse($response, null, 200);
    }

    public function create(Request $request, $courseId)
    {
        $rules = [
            'title' => 'required',
            'dueDate' => [new DueDateValidator],
            'workType' => 'required|in:COURSE_WORK_TYPE_UNSPECIFIED,ASSIGNMENT,SHORT_ANSWER_QUESTION,MULTIPLE_CHOICE_QUESTION',
            'state' => 'sometimes|in:COURSE_WORK_STATE_UNSPECIFIED,PUBLISHED,DRAFT,DELETED',
            'assigneeMode' => 'sometimes|in:ASSIGNEE_MODE_UNSPECIFIED,ALL_STUDENTS,INDIVIDUAL_STUDENTS',
            'submissionModificationMode' => 'sometimes|in:SUBMISSION_MODIFICATION_MODE_UNSPECIFIED,MODIFIABLE_UNTIL_TURNED_IN,MODIFIABLE',
        ];
        $messages = [
            "title.required" => "Title is required",
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $response = [
                'status' => 'error',
                'message' => [
                    'title' => $validator->errors()->first("title"),
                    'dueDate' => $validator->errors()->first("dueDate"),
                    'workType' => $validator->errors()->first("workType"),
                    'state' => $validator->errors()->first("state"),
                    'assigneeMode' => $validator->errors()->first("assigneeMode"),
                    'submissionModificationMode' => $validator->errors()->first("submissionModificationMode")
                ],
                'data' => ''
            ];
            return response()->json($response, 201);
        }
        $data = $request->all();
        $data['courseId'] = $courseId;
        $response = $this->coursework->create($data);

        return successResponse($response, null, 201);
    }

    public function get()
    {

    }

    public function update(CourseWorkPatch $request,$courseId, $courseWorkId)
    {
        $classWork = Coursework::where('external_course_work_id', $courseWorkId)->first();
        if ($classWork === null) {
            return response()->json([
                'status' => 'Error',
                'message' => 'Course work does not exists'
            ], 404);
        }
        $data = $request->all();
        $data['courseId'] = $courseId;
        $data['courseWorkId'] = $courseWorkId;
        $response = $this->coursework->update($data, $classWork->id);

        return successResponse($response, null, 201);
    }

    public function delete($courseId,$courseWorkId)
    {
        $courseWork =$this->coursework->findBy("external_course_work_id",$courseWorkId);
        $response = [
            'status' => 'Success',
            'message' => 'Course Work Deleted Successfully',
            'data' => ''
        ];
        if ($courseWork !== null){
            try{
                $data = [];
                $data['id'] = $courseWork->id;
                $data['courseId'] = $courseId;
                $data['courseWorkId'] = $courseWorkId;
                $this->coursework->delete($data);
            }catch (\Exception $e){
                $response['status'] = "error";
                $response['message'] = "Something Went Wrong {$e->getMessage()}";
            }
        }
        return response()->json($response,200);
    }
}
