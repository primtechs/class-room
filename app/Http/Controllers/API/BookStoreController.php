<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class BookStoreController extends Controller
{
	public function index(Request $request)
	{
		$user = auth()->user();
		if($user === NULL){
			return "INVALID USER";
		}
		 $servername = request()->server('SERVER_NAME'); 

		if ($servername == '127.0.0.1'){
			$endpoint = 'http://localhost/class/index.php?route=account/class';
		}
		else {
			$endpoint = 'https://store.smartschools80.com/index.php?route=account/class';
		}
		

		$response = Http::asForm()->post($endpoint,[
			'account_id' => $user->id,
			'email' => $user->email,
			'first_name' => $user->first_name,
			'last_name' => $user->last_name,
			'phone_number' => $user->phone_number,
			'account_type_id' => $user->account_type_id,
		]);
		$url='';
		$server_error='';
		if($response->successful()){
			if($response->ok()){
				$url = $response->getBody();
				
			}else{
				$server_error= "SERVER ERROR ";
			}
		}else{
			$server_error= "SERVER ERROR";
		}
		if($server_error!=''){
				$returndata=array();
				$returndata=array(
				'account_id' => $user->id,
				'email' => $user->email,
				'first_name' => $user->first_name,
				'last_name' => $user->last_name,
				'phone_number' => $user->phone_number,
				'account_type_id' => $user->account_type_id
			);
			
			$postvalue= http_build_query($returndata); 
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL,$endpoint);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,
						$postvalue);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$url= $server_output = curl_exec($ch);

			curl_close ($ch);
		}
		
		$url=str_replace('amp;','',$url);
		if($url!='')
			return redirect()->to($url);
	}
}
