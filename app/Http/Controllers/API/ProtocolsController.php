<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Protocols;
use Illuminate\Support\Facades\Validator;

class ProtocolsController extends Controller
{

    public function index()
    {
        $data = [];
        $response = Protocols::all();
        $data['data'] = $response;
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => "required|unique:protocols,name"
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "name" => $validator->errors()->first("name")
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Data is saved successfully';
            $data['data'] = Protocols::create([
                'name' => $request->get("name")
            ]);
        }
        return response()->json($data, 201);

    }


    public function show($id)
    {
        $data = [];
        $protocols = Protocols::find($id);
        if ($protocols != NULL) {
            $data['status'] = 'success';
            $data['data'] = $protocols;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Protocols are Not Found';
        }
        return response()->json($data, 200);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => "required|unique:protocols,name,$id"
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "name" => $validator->errors()->first("name")
            ];
            $data['data'] = '';
        } else {
            $protocols = Protocols::find($id);
            if ($protocols != NULL) {
                $protocols->name = $request->get("name");
                $protocols->save();
                $data['status'] = 'success';
                $data['data'] = $protocols->fresh();
                $data['message'] = '';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Protocols Not Found';
            }
        }
        return response()->json($data, 200);
    }


    public function destroy(Request $request, $id)
    {
        $protocols = Protocols::find($id);
        if ($protocols != NULL) {
            try{
                $protocols->delete();
                $data['status'] = 'success';
                $data['data'] = '';
                $data['message'] = 'Protocols Deleted Successfully';
            }catch (\Exception $exception){
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Protocols Not Found';
        }
        return response()->json($data, 200);
    }
}
