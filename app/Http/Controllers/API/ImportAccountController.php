<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\School;
use App\Models\AccountRole;
use App\Models\Accounts\Account;
use App\Models\Accounts\AccountSchool;
use App\Models\Accounts\AccountType;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Rules\ImportAccountFormatValidator;
use Illuminate\Support\Facades\Validator;

class ImportAccountController extends Controller
{

    public function import(Request $request)
    {
        $file = $request->file("file");
        $lines = (new FastExcel())->import($file, function ($line) {
            return $line;
        });
        $rules = [
            'file' => [
                "mimetypes:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                new ImportAccountFormatValidator($lines->toArray())
            ],
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => [
                    'file' => $validator->errors()->first("file")
                ] 
            ], 200);
        }

        $data = [
            'status' => 'success',
            'accounts' => [],
            'errors' => []
        ];
        $importResponses = [];
        foreach($lines as $line){
            $importResponses[] = $this->importToDtabase($line);
        }

        foreach($importResponses as $response){
            if($response['status'] === "success"){
                $data['accounts'][] = $response['account'];
            }else{
                $data['errors'][] = $response['message'];
            }
        }

        return response()->json($data, 200);
    }

    public function example(){
        $filePath = public_path("account_import_demo.xlsx");
        $headers = ['Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
        $fileName = "account_import_".time().'.xlsx';
        return response()->download($filePath, $fileName, $headers);
    }

    private function importToDtabase(array $account)
    {
        $first_name = $account["FirstName"];
        $last_name = $account["LastName"];
        $status = strtolower($account["Status"]);
        $phone_number = $account["PhoneNumber"];
        $email = $account["Email"];

        $account_type_name = $account["AccountType"];
        $role_name = $account["Role"];
        $school_name = $account["School"];

        $account_type = AccountType::nameIgnoreCase($account_type_name)->first();
        $role = Role::nameIgnoreCase($role_name)->first();
        $school = School::nameIgnoreCase($school_name)->first();


        $statusList = ["active", "inactive"];

        $response = [];

        $emailValidator  = Validator::make(['email' => $email], [
            'email' => 'required|email'
        ]);

        if ($emailValidator->fails()) {
            return [
                'message' => "Invalid email $email",
                'status' => "error"
            ];
        }

        if (!in_array(strtolower($status), $statusList)) {
            return [
                'message' => "Invalid Status $status for $email",
                'status' => "error"
            ];
        }

        if ($account_type === NULL) {
            return [
                'message' => "Invalid Account Type $account_type_name for $email",
                'status' => "error"
            ];
        }

        if ($role === NULL) {
            return [
                'message' => "Invalid Role $role_name for $email",
                'status' => "error"
            ];
        }


        if ($school === NULL) {
            return [
                'message' => "Invalid School $school_name for $email",
                'status' => "error"
            ];
        }


        $accountExists = Account::where("email", $email)->count();

        if ($accountExists === 0) {
            $account = Account::create([
                'first_name' => $first_name,
                'last_name' => $last_name,
                'status' => strtolower($status) == "active",
                'phone_number' => $phone_number,
                'email' => $email,
                'account_type_id' => $account_type->id,
            ]);
            AccountRole::updateOrCreate([
                'account_id' => $account->id,
                'role_id' => $role->id,
            ], [
                'account_id' => $account->id,
                'role_id' => $role->id,
                'status' => strtolower($status) == "active"
            ]);
            AccountSchool::updateOrCreate([
                'account_id' => $account->id,
                'school_id' => $school->id
            ], [
                'account_id' => $account->id,
                'school_id' => $school->id,
                'is_active' => strtolower($status) == "active"
            ]);

            $response = [
                'message' => "$email imported successfully!",
                'status' => "success",
                'account' => $account->fresh()->toArray()
            ];
        }else{
             $account = Account::where("email", $email)->first();
            $response = [
                'message' => 'Account Already Exists',
                'status' => 'success',
                'account' => $account->toArray()
            ];
        }

        return $response;
    }
}
