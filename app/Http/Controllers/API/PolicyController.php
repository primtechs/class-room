<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Action;
use App\Models\Policy;
use App\Models\Role;
use App\Rules\PolicyUniqueValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PolicyController extends Controller
{

    public function index()
    {
        $data = [];
        $response = Policy::all();
        $data['status'] = 'success';
        $data['data'] = $response;
        $data['roles'] = Role::all();
        $data['actions'] = Action::all();
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'role_id' => 'required',
            'action_id' => 'required',
            'name' => ["required", "min:2", new PolicyUniqueValidator]
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'role_id' => $validator->errors()->first("role_id"),
                'action_id' => $validator->errors()->first("action_id"),
                'name' => $validator->errors()->first("name"),
                'description' => $validator->errors()->first("description"),
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Policy is saved successfully';
            $data['data'] = Policy::create([
                'role_id' => $request->get("role_id"),
                'action_id' => $request->get("action_id"),
                'name' => $request->get("name"),
                'description' => $request->get("description"),
            ]);
        }
        return response()->json($data, 201);

    }


    public function show($id)
    {
        $data = [];
        $policy = Policy::find($id);
        if ($policy != NULL) {
            $data['status'] = 'success';
            $data['data'] = $policy;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Not Found';
        }
        return response()->json($data, 200);
    }

    public function update(Request $request, Policy $policy)
    {
        $rules = [
            'role_id' => 'required',
            'action_id' => 'required',
            'name' => ["required", "min:2", new PolicyUniqueValidator($policy)]
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'role_id' => $validator->errors()->first("role_id"),
                'action_id' => $validator->errors()->first("action_id"),
                'name' => $validator->errors()->first("name"),
                'description' => $validator->errors()->first("description"),
            ];
            $data['data'] = '';
        } else {
            if ($policy != NULL) {
                $policy->role_id = $request->get("role_id");
                $policy->action_id = $request->get("action_id");
                $policy->name = $request->get("name");
                $policy->description = $request->get("description");
                $policy->save();
                $data['status'] = 'success';
                $data['data'] = $policy->fresh();
                $data['message'] = 'Policy Updated Successfully';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Policy is Not Found';
            }
        }
        return response()->json($data, 200);
    }

    public function destroy(Policy $policy)
    {
        if ($policy != NULL) {
            try {
                $policy->delete();
                $data['status'] = 'success';
                $data['data'] = '';
                $data['message'] = 'Policy Deleted Successfully';
            } catch (\Exception $exception) {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Not Found';
        }
        return response()->json($data, 200);
    }
}
