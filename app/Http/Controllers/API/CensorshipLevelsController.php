<?php

namespace App\Http\Controllers\API;

use App\Models\CensorshipLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class CensorshipLevelsController extends Controller
{
    public function index()
    {
        $data = [];
        $response = CensorshipLevel::all();
        $data['data'] = $response;
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|min:2|unique:censorship_levels,name'
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name")
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Censorship Level is saved successfully';
            $data['data'] = CensorshipLevel::create([
                'name' => $request->get("name")
            ]);
        }
        return response()->json($data, 201);
    }


    public function show($id)
    {
        $data = [];
        $censorshipLevel = CensorshipLevel::find($id);
        if ($censorshipLevel != NULL) {
            $data['status'] = 'success';
            $data['data'] = $censorshipLevel;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Censorship Level Not Found';
        }
        return response()->json($data, 200);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => "required|min:2|unique:censorship_levels,name,$id"
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name")
            ];
            $data['data'] = '';
        } else {
            $censorshipLevel = CensorshipLevel::find($id);
            if ($censorshipLevel != NULL) {
                $censorshipLevel->name = $request->get("name");
                $censorshipLevel->save();
                $data['status'] = 'success';
                $data['data'] = $censorshipLevel->fresh();
                $data['message'] = '';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Censorship Level is Not Found';
            }
        }
        return response()->json($data, 200);
    }


    public function destroy($id)
    {
        $censorshipLevels = CensorshipLevel::find($id);
        if ($censorshipLevels != NULL) {
            try {
                $censorshipLevels->delete();
                $data['status'] = 'success';
                $data['data'] = '';
                $data['message'] = 'Censorship Level Deleted Successfully';
            } catch (\Exception $exception) {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Censorship Level Not Found';
        }
        return response()->json($data, 200);
    }
}
