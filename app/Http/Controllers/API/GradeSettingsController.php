<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\GradeSettingRequest;
use App\Models\Grade\GradeCategory;
use App\Models\Grade\GradeSetting;

class GradeSettingsController extends Controller
{

    public function store(GradeSettingRequest $request)
    {
        $data = $request->data();
        $setting = GradeSetting::updateOrCreate(['courseid' => $data['courseid']], $data);
        $categories = $data['categories'];
        foreach ($categories as $category) {
            GradeCategory::create([
                'setting_id' => $setting->setting_id,
                'category_name' => $category['category_name'],
                'category_point' => $category['category_point'],
            ]);
        }
        $data = [];
        if ($setting != NULL) {
            $data = $setting->toArray();
            $data['categories'] = $setting->categories->toArray();
        }
        $response = [];
        $response['status'] = 'success';
        $response['setting'] = $data;
        $response['message'] = 'Grade Setting created successfully';
        return response()->json($response, 200);
    }

    public function show($courseid)
    {
        $gradeSetting = GradeSetting::where('courseid', $courseid)->first();
        $data = [
            "setting_id"=> "",
            "courseid"=> $courseid,
            "calculation_type"=> "",
            "show_grade" => true,
            "categories"=> [],
            "exclude_missing" => false,
        ];
        if ($gradeSetting != NULL) {
            $data = $gradeSetting->toArray();
            $data['categories'] = $gradeSetting->categories->toArray();
        }
        $response = [];
        $response['status'] = 'success';
        $response['setting'] = $data;
        $response['message'] = '';
        return response()->json($response, 200);
    }

    public function update(GradeSettingRequest $request, $setting_id)
    {
        $gradeSetting = GradeSetting::find($setting_id);
        $data = $request->data();
        $gradeSetting->update($data);
        $gradeSetting->refresh();
        $categories = $data['categories'];
        GradeCategory::where("setting_id", $setting_id)->delete();
        foreach ($categories as $category) {
            GradeCategory::create([
                'setting_id' => $setting_id,
                'category_name' => $category['category_name'],
                'category_point' => $category['category_point'],
            ]);
        }
        $data = [];
        if ($gradeSetting != NULL) {
            $data = $gradeSetting->toArray();
            $data['categories'] = $gradeSetting->categories->toArray();
        }
        $response = [];
        $response['status'] = 'success';
        $response['setting'] = $data;
        $response['message'] = 'Grade Setting updated successfully';
        return response()->json($response, 200);
    }

    public function destroy(GradeSetting $gradeSetting)
    {
        GradeCategory::where($gradeSetting->setting_id)->delete();
        $gradeSetting->delete();
        $response = [];
        $response['status'] = 'success';
        $response['setting'] = NULL;
        $response['message'] = 'Grade Setting deleted successfully';
        return response()->json($response, 200);
    }
}
