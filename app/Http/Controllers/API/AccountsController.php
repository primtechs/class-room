<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\AccountRole;
use App\Models\Accounts\Account;
use App\Models\Accounts\AccountSchool;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AccountsController extends Controller
{

    public function index()
    {
        $query = Account::with(["role", "school", "accountType"])->orderBy("id","DESC");
        $data = [];
        //start of filtering 
        request()->whenFilled('first_name', function($input) use(&$query){
            $query->where("first_name", "ILIKE", "%$input%");
        });
        request()->whenFilled('last_name', function($input) use(&$query){
            $query->where("last_name", "ILIKE", "%$input%");
        });
        request()->whenFilled('email', function($input) use(&$query){
            $query->where("email", $input);
        });
        request()->whenFilled('account_type_id', function($input) use(&$query){
            $query->where("account_type_id", $input);
        });
        request()->whenFilled('account_type_id', function($input) use(&$query){
            $query->where("account_type_id", $input);
        });
        request()->whenFilled('school_id', function($input) use(&$query){
            $accountIds = AccountSchool::where("school_id",$input)->get()->pluck('account_id')->toArray();
            $query->whereIn("id", $accountIds);
        });
        request()->whenFilled('role_id', function($input) use(&$query){
            $accountIds = AccountRole::where("role_id",$input)->get()->pluck('account_id')->toArray();
            $query->whereIn("id", $accountIds);
        });
        //end of filtering
        $limit = config("pagination.limit");
        $accounts = $query->paginate($limit)->toArray();

        $data['status'] = 'success';
        $data['message'] = '';
        $data['pagination'] = [
            'from' => $accounts['from'],
            'to' => $accounts['to'],
            'total' => $accounts['total'],
            'per_page' => $accounts['per_page'],
            'current_page' => $accounts['current_page'],
            'last_page' => $accounts['last_page'],
        ];
        $data['accounts'] = $accounts['data'];
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'status' => 'required',
            'phone_number' => ['required', "unique:accounts,phone_number"],
            'email' => ['required', "unique:accounts,email"],
            'account_type_id' => 'required',
            'role_id' => 'required',
            'school_id' => 'required'
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "first_name" => $validator->errors()->first("first_name"),
                "last_name" => $validator->errors()->first("last_name"),
                "status" => $validator->errors()->first("status"),
                "phone_number" => $validator->errors()->first("phone_number"),
                "email" => $validator->errors()->first("email"),
                "account_type_id" => $validator->errors()->first("account_type_id"),
                "role_id" => $validator->errors()->first("role_id"),
                "school_id" => $validator->errors()->first("school_id")
            ];
            $data['account'] = '';
            return response()->json($data, 200);
        }
        $account = Account::create([
            'first_name' => $request->get("first_name"),
            'last_name' => $request->get("last_name"),
            'status' => $request->get("status"),
            'phone_number' => $request->get("phone_number"),
            'email' => $request->get("email"),
            'account_type_id' => $request->get("account_type_id"),
        ]);
        AccountRole::updateOrCreate([
            'account_id' => $account->id,
            'role_id' => $request->get("role_id"),
        ], [
            'account_id' => $account->id,
            'role_id' => $request->get("role_id"),
            'status' => $request->get("status")
        ]);
        AccountSchool::updateOrCreate([
            'account_id' => $account->id,
            'school_id' => $request->get("school_id")
        ],[
            'account_id' => $account->id,
            'school_id' => $request->get("school_id"),
            'is_active' => $request->get("status")
        ]);
        $account->fresh();
        $data['status'] = 'success';
        $data['message'] = 'Account Created Successfully';
        $data['account'] = $account->toArray();
        return response()->json($data, 200);
    }

    public function show(Account $account)
    {
        $data['status'] = 'success';
        $data['message'] = '';
        $data['account'] = $account->toArray();
        return response()->json($data, 200);
    }

    public function update(Request $request, Account $account)
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'status' => 'required',
            'phone_number' => ['required', "unique:accounts,phone_number,{$account->id}"],
            'email' => ['required', "unique:accounts,email,{$account->id}"],
            'account_type_id' => 'required',
            'role_id' => 'required',
            'school_id' => 'required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "first_name" => $validator->errors()->first("first_name"),
                "last_name" => $validator->errors()->first("last_name"),
                "status" => $validator->errors()->first("status"),
                "phone_number" => $validator->errors()->first("phone_number"),
                "email" => $validator->errors()->first("email"),
                "account_type_id" => $validator->errors()->first("account_type_id"),
                "role_id" => $validator->errors()->first("role_id"),
                "school_id" => $validator->errors()->first("school_id")
            ];
            $data['account'] = '';
            return response()->json($data, 200);
        }
        $account->first_name = $request->get("first_name");
        $account->last_name = $request->get("last_name");
        $account->status = $request->get("status");
        $account->phone_number = $request->get("phone_number");
        $account->email = $request->get("email");
        $account->account_type_id = $request->get("account_type_id");
        $account->save();
        AccountRole::updateOrCreate([
            'account_id' => $account->id,
            'role_id' => $request->get("role_id"),
        ], [
            'account_id' => $account->id,
            'role_id' => $request->get("role_id"),
            'status' => $request->get("status")
        ]);
        AccountSchool::updateOrCreate([
            'account_id' => $account->id,
            'school_id' => $request->get("school_id")
        ],[
            'account_id' => $account->id,
            'school_id' => $request->get("school_id"),
            'is_active' => $request->get("status")
        ]);
        $account->fresh();
        $data['status'] = 'success';
        $data['message'] = 'Account Updated Successfully';
        $data['account'] = $account->toArray();
        return response()->json($data, 200);
    }

    public function destroy(Account $account)
    {
        if ($account != NULL) {
            DB::beginTransaction();
            try {
                AccountRole::where("account_id",$account->id)->delete();
                AccountSchool::where("account_id",$account->id)->delete();
                $account->delete();
                DB::commit();
                $data['status'] = 'success';
                $data['account'] = '';
                $data['message'] = 'Account Deleted Successfully';
            }catch (\Exception $e){
                DB::rollBack();
                $data['status'] = 'error';
                $data['account'] = '';
                $data['message'] = 'Account Not Found';
            }
        } else {
            $data['status'] = 'error';
            $data['account'] = '';
            $data['message'] = 'Account Not Found';
        }
        return response()->json($data, 200);
    }
}
