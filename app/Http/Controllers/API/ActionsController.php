<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Action;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ActionsController extends Controller
{

    public function index()
    {
        $data = [];
        $response = Action::all();
        $data['data'] = $response;
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|min:2|unique:actions,name'
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name")
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Data is saved successfully';
            $data['data'] = Action::create([
                'name' => $request->get("name")
            ]);
        }
        return response()->json($data, 201);
    }


    public function show($id)
    {
        $data = [];
        $action = Action::find($id);
        if ($action != NULL) {
            $data['status'] = 'success';
            $data['data'] = $action;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Action are Not Found';
        }
        return response()->json($data, 200);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => "required|min:2|unique:actions,name,$id"
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name")
            ];
            $data['data'] = '';
        } else {
            $action = Action::find($id);
            if ($action != NULL) {
                $action->name = $request->get("name");
                $action->save();
                $data['status'] = 'success';
                $data['data'] = $action->fresh();
                $data['message'] = '';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Data is Not Found';
            }
        }
        return response()->json($data, 200);
    }


    public function destroy($id)
    {
        $actions = Action::find($id);
        if ($actions != NULL) {
            try {
                $actions->delete();
                $data['status'] = 'success';
                $data['data'] = '';
                $data['message'] = 'Data Deleted Successfully';
            } catch (\Exception $exception) {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Not Found';
        }
        return response()->json($data, 200);
    }
}
