<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Accounts\Account;
use App\Models\Group;
use App\Models\Invitations;
use App\Models\Protocols;
use App\Models\QuestionSubject;
use App\Models\School;
use App\Models\Standard;
use App\Models\Topic;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    public function studentEnrollment(Request $request)
    {

        $account = getAccountDetails();

        $student = $request->get("student","");
        $class = $request->get("class","");
        $teacher = $request->get("teacher","");
        $grade = $request->get('grade', '');

        // acl control via account type
        //find account type

        $totalStudentQuery = DB::table('accounts as students')
            ->select('students.id as student_id', 'students.account_type_id as account_type',
                'students.external_user_id as student_external_id', 'students.email as student_email', 'classes.id as class_id', 'teachers.id as teachers_id', 'si.role as role')
            ->leftJoin('invitations as si', 'si.external_user_id', '=', 'students.email')
            ->leftJoin('classes', 'si.external_course_id', '=', 'classes.external_course_id')
            ->leftJoin('accounts as teachers', 'teachers.external_user_id', 'classes.external_user_id')
            ->leftJoin('profile', 'students.id', '=', 'profile.account_id')
            ->leftJoin('groups', 'groups.id', '=', 'profile.group_id')
        ;

        $totalCourseWorkQuery = DB::table('classwork')
            ->leftJoin('classes', 'classwork.external_course_id', '=', 'classes.external_course_id')
            ->leftJoin('accounts as teachers', 'teachers.external_user_id', 'classes.external_user_id')
            ->leftJoin('invitations as si', function($join) {
                $join->on('si.role', '=', DB::raw("'STUDENT'"));
                $join->on('classes.external_course_id', '=', 'si.external_course_id');
            })
            ->leftJoin('accounts as students', 'si.external_user_id', '=', 'students.email')
            ->leftJoin('profile', 'students.id', '=', 'profile.account_id')
            ->leftJoin('groups', 'groups.id', '=', 'profile.group_id')
        ;

        $totalCourseQuery = DB::table('classes')
            ->leftJoin('accounts as teachers', 'teachers.external_user_id', 'classes.external_user_id')
            ->leftJoin('invitations as si', function($join) {
                $join->on('si.role', '=', DB::raw("'STUDENT'"));
                $join->on('classes.external_course_id', '=', 'si.external_course_id');
            })
            ->leftJoin('accounts as students', 'si.external_user_id', '=', 'students.email')
            ->leftJoin('profile', 'students.id', '=', 'profile.account_id')
            ->leftJoin('groups', 'groups.id', '=', 'profile.group_id')
        ;

        $totalEnrollmentQuery = DB::table('invitations as si')
            ->leftJoin('accounts as students', 'si.external_user_id', '=', 'students.email')
            ->leftJoin('classes', 'si.external_course_id', '=', 'classes.external_course_id')
            ->leftJoin('accounts as teachers', 'teachers.external_user_id', 'classes.external_user_id')
            ->leftJoin('profile', 'students.id', '=', 'profile.account_id')
            ->leftJoin('groups', 'groups.id', '=', 'profile.group_id')
            ->where('si.role', 'STUDENT')
            ->distinct('si.external_course_id')
        ;

        if (!empty($class)) {
            $totalStudentQuery->where('classes.name', 'ILIKE', "%$class%");
            $totalCourseWorkQuery->where('classes.name', 'ILIKE', "%$class%");
            $totalCourseQuery->where('classes.name', 'ILIKE', "%$class%");
            $totalEnrollmentQuery->where('classes.name', 'ILIKE', "%$class%");
        }

        if (!empty($student)) {

            $totalStudentQuery->where('students.first_name', 'ILIKE', '%'.$student.'%');
            $totalStudentQuery->orWhere('students.last_name', 'ILIKE', '%'.$student.'%');
            $totalStudentQuery->orWhere('students.email', 'ILIKE', '%'.$student.'%');

            $totalCourseWorkQuery->where('students.first_name', 'ILIKE', '%'.$student.'%');
            $totalCourseWorkQuery->orWhere('students.last_name', 'ILIKE', '%'.$student.'%');
            $totalCourseWorkQuery->orWhere('students.email', 'ILIKE', '%'.$student.'%');

            $totalCourseQuery->where('students.first_name', 'ILIKE', '%'.$student.'%');
            $totalCourseQuery->orWhere('students.last_name', 'ILIKE', '%'.$student.'%');
            $totalCourseQuery->orWhere('students.email', 'ILIKE', '%'.$student.'%');

            $totalEnrollmentQuery->where('students.first_name', 'ILIKE', '%'.$student.'%');
            $totalEnrollmentQuery->orWhere('students.last_name', 'ILIKE', '%'.$student.'%');
            $totalEnrollmentQuery->orWhere('students.email', 'ILIKE', '%'.$student.'%');
        }

        if (!empty($teacher)) {
            $totalStudentQuery->where('teachers.first_name', 'ILIKE', '%'.$teacher.'%');
            $totalStudentQuery->orWhere('teachers.last_name', 'ILIKE', '%'.$teacher.'%');
            $totalStudentQuery->orWhere('teachers.email', 'ILIKE', '%'.$teacher.'%');

            $totalCourseWorkQuery->where('teachers.first_name', 'ILIKE', '%'.$teacher.'%');
            $totalCourseWorkQuery->orWhere('teachers.last_name', 'ILIKE', '%'.$teacher.'%');
            $totalCourseWorkQuery->orWhere('teachers.email', 'ILIKE', '%'.$teacher.'%');

            $totalEnrollmentQuery->where('teachers.first_name', 'ILIKE', '%'.$teacher.'%');
            $totalEnrollmentQuery->orWhere('teachers.last_name', 'ILIKE', '%'.$teacher.'%');
            $totalEnrollmentQuery->orWhere('teachers.email', 'ILIKE', '%'.$teacher.'%');

            $totalCourseQuery->where('teachers.first_name', 'ILIKE', '%'.$teacher.'%');
            $totalCourseQuery->orWhere('teachers.last_name', 'ILIKE', '%'.$teacher.'%');
            $totalCourseQuery->orWhere('teachers.email', 'ILIKE', '%'.$teacher.'%');
        }

        if (!empty($grade)) {
            $totalStudentQuery->where('groups.name', 'ILIKE', '%'.$grade.'%');
            $totalCourseWorkQuery->where('groups.name', 'ILIKE', '%'.$grade.'%');
            $totalEnrollmentQuery->where('groups.name', 'ILIKE', '%'.$grade.'%');
            $totalCourseQuery->where('groups.name', 'ILIKE', '%'.$grade.'%');
        }


        if ($account->isTeacher()) {
            $totalStudentQuery->where('teachers.id', '=', $account->id);
            $totalCourseWorkQuery->where('teachers.id', '=', $account->id);
            $totalCourseQuery->where('teachers.id', '=', $account->id);
            $totalEnrollmentQuery->where('teachers.id', '=', $account->id);
        }

        if ($account->isStudent()) {
            $totalStudentQuery->where('students.id', '=', $account->id);
            $totalCourseWorkQuery->where('students.id', '=', $account->id);
            $totalCourseQuery->where('students.id', '=', $account->id);
            $totalEnrollmentQuery->where('students.id', '=', $account->id);
        }

        $totalStudent = $totalStudentQuery
//            ->where('si.role', 'STUDENT')
            ->where('students.account_type_id', 28)
//            ->whereNotNull('students.id')
            ->distinct('students.id')
            ->count();
        $totalCourseWork = $totalCourseWorkQuery
            ->count();
        $totalCourse = $totalCourseQuery
            ->distinct('classes.id')
            ->count();
        $totalEnrollment = $totalEnrollmentQuery
            ->count();

//        $query = DB::table('invitations')
//            ->select(
//                'students.first_name as student_first_name',
////                DB::raw('(select count(classes.id) from classes where classes.external_course_id = invitations.external_course_id  group by external_course_id limit 1) total'),
//                'students.last_name as student_last_name',
//                'students.email as student_email',
//                'schools.school_name',
//                'students.external_user_id as student_external_id',
////                'classes.name as class_name',
////                'teachers.first_name as teacher_first_name',
////                'teachers.last_name as teacher_last_name',
////                'teachers.email as teacher_email',
//                'groups.name as group_name'
//            )
//            ->where('role', 'STUDENT');
//
//
//
//        $query->leftJoin('accounts as students', 'invitations.external_user_id', '=', 'students.email');
//        $query->leftJoin('account_school', 'account_school.account_id', '=', 'students.id');
//        $query->leftJoin('schools', 'schools.id', '=', 'account_school.school_id');
////        $query->leftJoin('classes', 'classes.external_course_id', '=', 'invitations.external_course_id');
////        $query->leftJoin('accounts as teachers', 'teachers.external_user_id', 'classes.external_user_id');
//        $query->leftJoin('profile', 'students.id', '=', 'profile.account_id');
//        $query->leftJoin('groups', 'groups.id', '=', 'profile.group_id');
//        $query->where('students.email', '!=', null);
//
//        if ($account->isStudent()) {
//            $query->where('students.id', '=', $account->id);
//        }
//
//        if (!empty($student)) {
//            $query->where('students.first_name', 'ILIKE', '%'.$student.'%');
//            $query->orWhere('students.last_name', 'ILIKE', '%'.$student.'%');
//            $query->orWhere('students.email', 'ILIKE', '%'.$student.'%');
//        }
//
//
//        if (!empty($grade)) {
//            $query->where('groups.name', 'ILIKE', '%'.$grade.'%');
//        }
//        $query->distinct('students.id');
//        $students = $query->get();
//
//        $validList = [];
//        foreach($students as $key => $s) {
//            $classes = DB::table('invitations')
//                ->leftJoin('classes', 'classes.external_course_id', '=', 'invitations.external_course_id')
//                ->where('invitations.external_user_id', $s->student_email)
//                ->where('name', '!=', null)
//                ->pluck('name')->toArray()
//            ;
//
////            $classes = DB::table('classes')
////                ->leftJoin('invitations', 'classes.external_course_id', '=', 'invitations.external_course_id')
////                ->where('invitations.external_user_id', $s->student_email)
////                ->where('name', '!=', null)
////                ->pluck('name')->toArray()
////            ;
//
//            $enrollment = DB::table('invitations')
//                ->leftJoin('classes', 'classes.external_course_id', '=', 'invitations.external_course_id')
//                ->where('invitations.external_user_id', $s->student_email)
//                ->where('name', '!=', null)
//                ->count()
//            ;
//
//            $teachersQuery = DB::table('invitations')
//                ->select('teachers.first_name', 'teachers.last_name', 'teachers.email')
//                ->leftJoin('classes', 'classes.external_course_id', '=', 'invitations.external_course_id')
//                ->leftJoin('accounts as teachers', 'teachers.external_user_id', 'classes.external_user_id')
//                ->where('invitations.external_user_id', $s->student_email)
//                ->where('name', '!=', null)
//            ;
//
////            if ($account->isTeacher()) {
////                $teachersQuery->where('teachers.id', '=', $account->id);
////            }
//
//            $teachers= $teachersQuery->get();
//
//            $ftQuery = DB::table('invitations')
//                ->select(DB::raw("CONCAT(teachers.first_name, ' ', teachers.last_name, ' ', teachers.email) AS name"))
//                ->leftJoin('classes', 'classes.external_course_id', '=', 'invitations.external_course_id')
//                ->leftJoin('accounts as teachers', 'teachers.external_user_id', 'classes.external_user_id')
//                ->where('invitations.external_user_id', $s->student_email)
//                ->where('name', '!=', null)
//            ;
//
////            if ($account->isTeacher()) {
////                $ftQuery->where('teachers.id', '=', $account->id);
////            }
//
//            $ft = $ftQuery->pluck('name')->toArray();
//
//            $s->classes = $classes;
//            $s->teachers = $teachers;
//            $s->total_enrollment = $enrollment;
//
//            if (!empty($class)) {
//                if (preg_match("/$class/i", implode(' ', $classes))){
//                    array_push($validList, $key);
//                }
//            }
//
//            if (!empty($teacher)) {
//                if (preg_match("/$teacher/i", implode(' ', $ft))){
//                    array_push($validList, $key);
//                }
//            }
//
//        }

        $stats = [
            'total_student' => $totalStudent,
            'total_enrollment' => $totalEnrollment,
            'total_coursework' => $totalCourseWork,
            'total_course' => $totalCourse
        ];

//        $list = [];
//        $keys = array_unique($validList);
//
//        if (!empty($class) || !empty($teacher)) {
//            foreach ($keys as $k) {
//                array_push($list, $students[$k]);
//            }
//        } else {
//            $list = $students;
//        }

        $data = [
            'stats' => $stats,
//            'students' => $list
        ];

        return successResponse($data, null, 200);
    }

    public function studentAssignment(Request $request)
    {

//        DB::connection()->enableQueryLog();
        $account = getAccountDetails();
        $now = Carbon::now();
        $totalCourseWorkQuery = DB::table('classwork');

        $student = $request->get("student","");
        $class = $request->get("class","");
        $teacher = $request->get("teacher","");
        $grade = $request->get('grade', '');
        $from = $request->get('from', '');
        $to = $request->get('to', '');

        $query = DB::table('coursework_assessment_responses as csr')
            ->select(
                DB::raw("count(*) AS total"),
                DB::raw("sum(case when csr.response is not null then 1 else 0 end) AS total_completed"),
//                DB::raw("sum(case when cs.id is not null then 1 else 0 end) AS total_coursework"),
                DB::raw("sum(case when csr.response is null then 1 else 0 end) AS total_due"),
                DB::raw("sum(case when
                csr.response is not null
                and (
                    cast( cs.metadata -> 'dueDate' ->> 'year' as integer) > cast(extract(year from csr.last_update) as integer)
                    or (
                    cast( cs.metadata -> 'dueDate' ->> 'year' as integer) = cast(extract(year from csr.last_update) as integer)
                    and cast( cs.metadata -> 'dueDate' ->> 'month' as integer) > cast(extract(month from csr.last_update) as integer)
                    )
                    or (
                    cast( cs.metadata -> 'dueDate' ->> 'year' as integer) = cast(extract(year from csr.last_update) as integer)
                    and cast( cs.metadata -> 'dueDate' ->> 'month' as integer) = cast(extract(month from csr.last_update) as integer)
                    and cast( cs.metadata -> 'dueDate' ->> 'day' as integer) >= cast(extract(day from csr.last_update) as integer)
                    )
                )
                then 1 else 0 end) as total_completed_on_time"),

                DB::raw("sum(case when
                csr.response is null
                and (
                    cast( cs.metadata -> 'dueDate' ->> 'year' as integer) < $now->year
                    or (
                    cast( cs.metadata -> 'dueDate' ->> 'year' as integer) = $now->year
                    and cast( cs.metadata -> 'dueDate' ->> 'month' as integer) < $now->month
                    )
                    or (
                    cast( cs.metadata -> 'dueDate' ->> 'year' as integer) = $now->year
                    and cast( cs.metadata -> 'dueDate' ->> 'month' as integer) = $now->month
                    and cast( cs.metadata -> 'dueDate' ->> 'day' as integer) < $now->day
                    )
                )
                then 1 else 0 end) as total_over_due"
                )

//                'csr.response as rr',
//                'csr.course_work_id as cwi',
//                'cs.title as classwork_name',
//                'students.first_name as student_first_name',
////                DB::raw('(select count(classes.id) from classes where classes.external_course_id = invitations.external_course_id  group by external_course_id limit 1) total'),
//                'students.last_name as student_last_name',
//                'students.email as student_email',
////                'schools.school_name',
//                'students.external_user_id as student_external_id',
//                'classes.name as class_name',
//                'teachers.first_name as teacher_first_name',
//                'teachers.last_name as teacher_last_name',
//                'teachers.email as teacher_email',
//                'groups.name as group_name',
//                DB::raw("cs.metaData-> 'dueDate'")

            )
            ->leftJoin('classwork as cs', 'cs.id', '=', 'csr.course_work_id')
            ->leftJoin('classes', 'cs.external_course_id', '=', 'classes.external_course_id')
            ->leftJoin('invitations', function($join) {
                $join->on('role', '=', DB::raw("'STUDENT'"));
                $join->on('classes.external_course_id', '=', 'invitations.external_course_id');
            })
            ->leftJoin('accounts as students', 'invitations.external_user_id', '=', 'students.email')
            ->leftJoin('accounts as teachers', 'teachers.external_user_id', 'classes.external_user_id')
            ->leftJoin('profile', 'students.id', '=', 'profile.account_id')
            ->leftJoin('groups', 'groups.id', '=', 'profile.group_id')
        ;

        $totalCourseWorkQuery->leftJoin('classes', 'classwork.external_course_id', '=', 'classes.external_course_id');

        if ($account->isStudent()) {
            $query->where('students.id', '=', $account->id);

            $totalCourseWorkQuery->leftJoin('invitations as si', function($join) {
                $join->on('si.role', '=', DB::raw("'STUDENT'"));
                $join->on('classes.external_course_id', '=', 'si.external_course_id');
            })
                ->leftJoin('accounts as students', 'si.external_user_id', '=', 'students.email')
            ;

        }

        if ($account->isTeacher()) {
            $query->where('teachers.id', '=', $account->id);

            $totalCourseWorkQuery->leftJoin('accounts as teachers', 'teachers.external_user_id', 'classes.external_user_id');
        }

        if (!empty($student)) {
            $query->where('students.first_name', 'ILIKE', '%'.$student.'%');
            $query->orWhere('students.last_name', 'ILIKE', '%'.$student.'%');
            $query->orWhere('students.email', 'ILIKE', '%'.$student.'%');

            if (!$account->isStudent()) {
                $totalCourseWorkQuery->leftJoin('invitations as si', function($join) {
                    $join->on('si.role', '=', DB::raw("'STUDENT'"));
                    $join->on('classes.external_course_id', '=', 'si.external_course_id');
                })
                    ->leftJoin('accounts as students', 'si.external_user_id', '=', 'students.email')
                ;
            }

            $totalCourseWorkQuery->where('students.first_name', 'ILIKE', '%'.$student.'%');
            $totalCourseWorkQuery->orWhere('students.last_name', 'ILIKE', '%'.$student.'%');
            $totalCourseWorkQuery->orWhere('students.email', 'ILIKE', '%'.$student.'%');
        }

        if (!empty($teacher)) {
            $query->where('teachers.first_name', 'ILIKE', '%'.$teacher.'%');
            $query->orWhere('teachers.last_name', 'ILIKE', '%'.$teacher.'%');
            $query->orWhere('teachers.email', 'ILIKE', '%'.$teacher.'%');

            if (!$account->isTeacher()) {
                $totalCourseWorkQuery->leftJoin('accounts as teachers', 'teachers.external_user_id', 'classes.external_user_id');
            }

            $totalCourseWorkQuery->where('teachers.first_name', 'ILIKE', '%'.$teacher.'%');
            $totalCourseWorkQuery->orWhere('teachers.last_name', 'ILIKE', '%'.$teacher.'%');
            $totalCourseWorkQuery->orWhere('teachers.email', 'ILIKE', '%'.$teacher.'%');
        }

        if (!empty($grade)) {
            $query->where('groups.name', 'ILIKE', '%'.$grade.'%');

            if (!$account->isStudent()) {
                $totalCourseWorkQuery->leftJoin('invitations as gi', function($join) {
                    $join->on('gi.role', '=', DB::raw("'STUDENT'"));
                    $join->on('classes.external_course_id', '=', 'gi.external_course_id');
                })
                    ->leftJoin('accounts as gst', 'gi.external_user_id', '=', 'gst.email')
                ;
                $totalCourseWorkQuery->leftJoin('profile', 'gst.id', '=', 'profile.account_id');
            } else {
                $totalCourseWorkQuery->leftJoin('profile', 'students.id', '=', 'profile.account_id');
            }

            $totalCourseWorkQuery->leftJoin('groups', 'groups.id', '=', 'profile.group_id');
            $totalCourseWorkQuery->where('groups.name', 'ILIKE', '%'.$grade.'%');

        }

        if (!empty($class)) {
            $query->where('classes.name', 'ILIKE', "%$class%")->get();

            $totalCourseWorkQuery->where('classes.name', 'ILIKE', "%$class%");
        }

        if(!empty($from)) {
            $dateFrom = Carbon::parse($from);

            $query->where(function($q) use($dateFrom) {
                $q->where(DB::raw("cast( cs.metadata -> 'dueDate' ->> 'year' as integer)"), '>', $dateFrom->year);
            });
            $query->orWhere(function($q) use($dateFrom) {
                $q->where(DB::raw("cast( cs.metadata -> 'dueDate' ->> 'year' as integer)"), '=', $dateFrom->year);
                $q->where(function($q) use($dateFrom) {
                    $q->where(DB::raw("cast( cs.metadata -> 'dueDate' ->> 'month' as integer)"), '>', $dateFrom->month);
                });
                $q->orWhere(function($q) use($dateFrom) {
                    $q->where(DB::raw("cast( cs.metadata -> 'dueDate' ->> 'month' as integer)"), '=', $dateFrom->month);
                    $q->where(DB::raw("cast( cs.metadata -> 'dueDate' ->> 'day' as integer)"), '>=', $dateFrom->month);
                });
            });

        }

        if(!empty($to)) {
            $dateTo = Carbon::parse($to);

            $query->where(function($q) use($dateTo) {
                $q->where(DB::raw("cast( cs.metadata -> 'dueDate' ->> 'year' as integer)"), '<', $dateTo->year);
            });
            $query->orWhere(function($q) use($dateTo) {
                $q->where(DB::raw("cast( cs.metadata -> 'dueDate' ->> 'year' as integer)"), '=', $dateTo->year);
                $q->where(function($q) use($dateTo) {
                    $q->where(DB::raw("cast( cs.metadata -> 'dueDate' ->> 'month' as integer)"), '<', $dateTo->month);
                });
                $q->orWhere(function($q) use($dateTo) {
                    $q->where(DB::raw("cast( cs.metadata -> 'dueDate' ->> 'month' as integer)"), '=', $dateTo->month);
                    $q->where(DB::raw("cast( cs.metadata -> 'dueDate' ->> 'day' as integer)"), '<=', $dateTo->month);
                });
            });
        }

        $stats = $query->first();
        $totalCourseWork = $totalCourseWorkQuery->count();

        if ($account->isTeacher()) {
            $totalCourseWorkQuery->where('teachers.id', '=', $account->id);
        }

        if ($account->isStudent()) {
            $totalCourseWorkQuery->where('students.id', '=', $account->id);
        }
//        dd(DB::getQueryLog());
        $stats->total_course_work = $totalCourseWork;
        return successResponse($stats, 'Success', 200);
    }



    public function liveInstructions(Request $request) {
        $account = getAccountDetails();
        $subject = $request->get("subject",0);
        $standard = $request->get("standard",0);
        $topic = $request->get('topic', 0);
        $grade = $request->get('grade', 0);
        $protocol = $request->get('protocol', 0);
        $from = $request->get('from', 0);
        $to = $request->get('to', 0);

        $totalSessionQuery = DB::table('assessment_schedule')
            ->leftJoin('assessment', 'assessment.id', '=', 'assessment_schedule.assessment_id');


        $totalMcqQuery = DB::table('assessment_questions as aq')
            ->leftJoin('assessment as asm', 'asm.id', '=', 'aq.assessment_id')
            ->leftJoin('question_type as qt', function ($join) {
//                $join->on('qt.id', '=', DB::raw(11));
                $join->on('qt.id', '=', 'aq.type_id');
            })
            ->where('qt.id', '=', 11)
        ;

        $totalQuestionQuery = DB::table('assessment_questions as aq')
            ->leftJoin('assessment as asm', 'asm.id', '=', 'aq.assessment_id')
        ;

        $totalCorrectQuery = DB::table('assessment as asm')
            ->leftJoin('assessment_questions as asq', 'asq.assessment_id', '=', 'asm.id')
            ->leftJoin('posts as asp', 'asp.assessment_question_id', '=', 'asq.id')
            ->leftJoin('assessment_responses as asr', function($join) {
                $join->on('asr.assessment_question_id', '=', 'asq.id');
//                $join->on('asr.is_correct', '=', DB::raw('Yes'));
            })
//            ->where('asq.type_id', '=', 11)
            ->where('asr.is_correct', '=', 'Yes')
        ;

        $totalResponseQuery = DB::table('assessment as asm')
            ->leftJoin('assessment_questions as asq', 'asq.assessment_id', '=', 'asm.id')
            ->leftJoin('posts as asp', 'asp.assessment_question_id', '=', 'asq.id')
            ->leftJoin('assessment_responses as asr', function($join) {
                $join->on('asr.assessment_question_id', '=', 'asq.id');
//                $join->on('asr.is_correct', '=', DB::raw('Yes'));
            })
//            ->where('asq.type_id', '=', 11)
//            ->where('asr.is_correct', '=', 'Yes')
        ;

//        $totalResponseQuery = DB::table('posts')
//            ->leftJoin('assessment_questions as aq', 'aq.id', '=', 'posts.assessment_question_id')
//            ->leftJoin('assessment as asm', 'asm.id', '=', 'aq.assessment_id')
//        ;

        $totalCommentsGivenQuery = DB::table('comments')
            ->leftJoin('posts', 'comments.post_id', '=', 'posts.id')
            ->leftJoin('assessment_questions as aq', 'aq.id', '=', 'posts.assessment_question_id')
            ->leftJoin('assessment as asm', 'asm.id', '=', 'aq.assessment_id')
        ;

        $totalCommentsReceivedQuery = DB::table('comments')
            ->leftJoin('posts', 'comments.post_id', '=', 'posts.id')
            ->leftJoin('assessment_questions as aq', 'aq.id', '=', 'posts.assessment_question_id')
            ->leftJoin('assessment as asm', 'asm.id', '=', 'aq.assessment_id')
        ;

        $totalLikeGivenQuery = DB::table('posts_reaction')
            ->leftJoin('posts', 'posts.id', '=', 'posts_reaction.post_id')
            ->leftJoin('assessment_questions as aq', 'aq.id', '=', 'posts.assessment_question_id')
            ->leftJoin('assessment as asm', 'asm.id', '=', 'aq.assessment_id')
        ;

        $totalLikesReceivedQuery = DB::table('posts_reaction')
            ->leftJoin('posts', 'posts.id', '=', 'posts_reaction.post_id')
            ->leftJoin('assessment_questions as aq', 'aq.id', '=', 'posts.assessment_question_id')
            ->leftJoin('assessment as asm', 'asm.id', '=', 'aq.assessment_id')
        ;

        if ($account->isStudent()) {

            $totalMcqQuery->leftJoin('posts', 'posts.assessment_question_id', '=', 'aq.id')
                ->where('posts.account_id', $account->id)
            ;

            $totalQuestionQuery->leftJoin('posts', 'posts.assessment_question_id', '=', 'aq.id')
                ->where('posts.account_id', $account->id)
            ;

            $totalCorrectQuery->where('asp.account_id', $account->id);

            $totalResponseQuery->where('posts.account_id', $account->id);

            $totalCommentsGivenQuery->where('comments.account_id', $account->id);

            $totalCommentsReceivedQuery
                ->where('posts.account_id', $account->id)
                ->where('comments.account_id', '!=', $account->id)
            ;

            $totalLikeGivenQuery->where('posts_reaction.account_id', $account->id);

            $totalLikesReceivedQuery
                ->where('posts.account_id', '=', $account->id)
                ->where('posts_reaction.account_id', '!=', $account->id)
            ;
        }

        if ($account->isTeacher()) {
            // teacher er course -> assessment -> filter assessment by account id
            $totalMcqQuery->where('asm.account_id', $account->id);

            $totalQuestionQuery->where('asm.account_id', $account->id);

            $totalCorrectQuery->where('asm.account_id', $account->id);

            $totalResponseQuery
                ->where('asm.account_id', '=', $account->id)
            ;

            $totalCommentsGivenQuery->where('comments.account_id', $account->id);

            $totalCommentsReceivedQuery
                ->where('asm.account_id', '=', $account->id)
                ->where('comments.account_id', '!=', $account->id)
            ;

            $totalLikeGivenQuery->where('posts_reaction.account_id', '=', $account->id);

            $totalLikesReceivedQuery
                ->where('asm.account_id', '=', $account->id)
                ->where('posts_reaction.account_id', '!=', $account->id)
            ;

            $totalSessionQuery->where('assessment.account_id', '=', $account->id);
        }

        if(!empty($topic) && (int)$topic) {

            $topicId = (int) $topic;

            $totalSessionQuery->where('assessment.topic_id', '=', $topicId);

            $totalMcqQuery->where('asm.topic_id', '=', $topicId);

            $totalQuestionQuery->where('asm.topic_id', '=', $topicId);

            $totalCorrectQuery->where('asm.topic_id', '=', $topicId);

            $totalResponseQuery->where('asm.topic_id', '=', $topicId);

            $totalCommentsGivenQuery->where('asm.topic_id', '=', $topicId);

            $totalCommentsReceivedQuery->where('asm.topic_id', '=', $topicId);

            $totalLikeGivenQuery->where('asm.topic_id', '=', $topicId);

            $totalLikesReceivedQuery->where('asm.topic_id', '=', $topicId);

        }

        if(!empty($standard) && (int)$standard) {

            $standardId = (int) $standard;

            $totalSessionQuery->where('assessment.standard_id', '=', $standardId);

            $totalMcqQuery->where('asm.standard_id', '=', $standardId);

            $totalQuestionQuery->where('asm.standard_id', '=', $standardId);

            $totalCorrectQuery->where('asm.standard_id', '=', $standardId);

            $totalResponseQuery->where('asm.standard_id', '=', $standardId);

            $totalCommentsGivenQuery->where('asm.standard_id', '=', $standardId);

            $totalCommentsReceivedQuery->where('asm.standard_id', '=', $standardId);

            $totalLikeGivenQuery->where('asm.standard_id', '=', $standardId);

            $totalLikesReceivedQuery->where('asm.standard_id', '=', $standardId);

        }

        if(!empty($subject) && (int)$subject) {

            $subjectId = (int) $subject;

            $totalSessionQuery->where('assessment.subject_id', '=', $subjectId);

            $totalMcqQuery->where('asm.subject_id', '=', $subjectId);

            $totalQuestionQuery->where('asm.subject_id', '=', $subjectId);

            $totalCorrectQuery->where('asm.subject_id', '=', $subjectId);

            $totalResponseQuery->where('asm.subject_id', '=', $subjectId);

            $totalCommentsGivenQuery->where('asm.subject_id', '=', $subjectId);

            $totalCommentsReceivedQuery->where('asm.subject_id', '=', $subjectId);

            $totalLikeGivenQuery->where('asm.subject_id', '=', $subjectId);

            $totalLikesReceivedQuery->where('asm.subject_id', '=', $subjectId);

        }

        if(!empty($protocol) && (int)$protocol) {

            $protocolId = (int) $protocol;

            $totalSessionQuery->where('assessment.protocol_id', '=', $protocolId);

            $totalMcqQuery->where('asm.protocol_id', '=', $protocolId);

            $totalQuestionQuery->where('asm.protocol_id', '=', $protocolId);

            $totalCorrectQuery->where('asm.protocol_id', '=', $protocolId);

            $totalResponseQuery->where('asm.protocol_id', '=', $protocolId);

            $totalCommentsGivenQuery->where('asm.protocol_id', '=', $protocolId);

            $totalCommentsReceivedQuery->where('asm.protocol_id', '=', $protocolId);

            $totalLikeGivenQuery->where('asm.protocol_id', '=', $protocolId);

            $totalLikesReceivedQuery->where('asm.protocol_id', '=', $protocolId);

        }

        if(!empty($grade) && (int)$grade) {

            $gradeId = (int) $grade;

            $totalSessionQuery->where('assessment.grade_id', '=', $gradeId);

            $totalMcqQuery->where('asm.grade_id', '=', $gradeId);

            $totalQuestionQuery->where('asm.grade_id', '=', $gradeId);

            $totalCorrectQuery->where('asm.grade_id', '=', $gradeId);

            $totalResponseQuery->where('asm.grade_id', '=', $gradeId);

            $totalCommentsGivenQuery->where('asm.grade_id', '=', $gradeId);

            $totalCommentsReceivedQuery->where('asm.grade_id', '=', $gradeId);

            $totalLikeGivenQuery->where('asm.grade_id', '=', $gradeId);

            $totalLikesReceivedQuery->where('asm.grade_id', '=', $gradeId);

        }

        $totalMcq = $totalMcqQuery->count();
        $totalQuestion = $totalQuestionQuery->count();
        $totalOther = $totalQuestion - $totalMcq;
        $totalCorrect = $totalCorrectQuery->count();
        $totalResponse =$totalResponseQuery->count();
        $totalIncorrect = $totalResponse - $totalCorrect;
        $totalCommentsGiven = $totalCommentsGivenQuery->count();
        $totalCommentsReceived = $totalCommentsReceivedQuery->count();
        $totalLikesGiven = $totalLikeGivenQuery->count();
        $totalLikesReceived = $totalLikesReceivedQuery->count();

        if(!empty($from)) {
            $dateFrom = Carbon::parse($from);
            $totalSessionQuery->where('assessment_schedule.start_time', '>', $dateFrom);
        }

        if(!empty($to)) {
            $dateTo = Carbon::parse($to);
            $totalSessionQuery->where('assessment_schedule.end_time', '<=', $dateTo);
        }

        $totalSessions = $totalSessionQuery->count();

        $data = [
            'total_sessions' => $totalSessions,
            'total_mcq' => $totalMcq,
            'total_question' => $totalQuestion,
            'total_others' => $totalOther,
            'total_correct' => $totalCorrect,
            'total_incorrect' => $totalIncorrect,
            'total_comments_given' => $totalCommentsGiven,
            'total_comments_received' => $totalCommentsReceived,
            'total_likes_given' => $totalLikesGiven,
            'total_likes_received' => $totalLikesReceived
        ];

        return successResponse($data, 'Success', 200);
    }

}
