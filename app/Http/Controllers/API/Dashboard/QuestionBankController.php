<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Accounts\Account;
use App\Models\Accounts\AccountSchool;
use App\Models\Accounts\AccountType;
use App\Models\AssessmentQuestion;
use App\Models\Dashboard\DashboardQuestion;
use App\Models\QuestionGrade;
use App\Models\QuestionSubject;
use App\Models\School;
use App\Models\TeacherQuestionBank;
use Illuminate\Http\Request;

class QuestionBankController extends Controller
{
    public function index()
    {
        $user = request()->user();
        $questionQuery = DashboardQuestion::where([]);
        if (strtolower($user->accountType->name) == "student") {
            $questionQuery = $questionQuery->where("account_id", $user->account_id);
        } else if (strtolower($user->accountType->name) == "teachers") {
            $school = $user->school->id;
            $accounts = AccountSchool::where("school_id", $school)->get()->pluck("account_id")->toArray();
            $questionQuery = $questionQuery->whereIn("account_id", $accounts);
        }
        $questions = $questionQuery->get();
        $questionID = $questionQuery->pluck("id")->toArray();
        $dashboard = [];
        $dashboard['total_question'] = count($questionID);
        $dashboard['total_admin_approved'] = $questionQuery->where("status", 1)->count();
        $dashboard['total_teacher_approved'] = TeacherQuestionBank::whereIn("question_id", $questionID)->count();
        $dashboard['total_reused'] = AssessmentQuestion::whereIn("question_id", $questionID)->count();
        $accountTypes = AccountType::all();
        $data = [
            'dashboard' => $dashboard,
            'account_types' => $accountTypes,
            'status' => 'success'
        ];
        return response()->json($data, 200);
    }

    public function questions(Request $request)
    {
        $user = request()->user();
        $questionQuery = DashboardQuestion::where([]);
        if (strtolower($user->accountType->name) == "student") {
            $questionQuery = $questionQuery->where("account_id", $user->account_id);
        } else if (strtolower($user->accountType->name) == "teachers") {
            $school = $user->school->school_id;
            $accounts = AccountSchool::where("school_id", $school)->get()->pluck("account_id")->toArray();
            $questionQuery = $questionQuery->whereIn("account_id", $accounts);
        }
        $conditions = ["question_subject_id", "question_grade_id", "status"];
        $where = [];
        foreach ($conditions as $condition) {
            if ($request->has($condition) && !empty($request->get($condition, ""))) {
                $where[$condition] = $request->get($condition);
            }
        }
        $questions = $questionQuery->where($where);

        $school_id = $request->get("school_id", "");
        if (!empty($student_id)) {
            $accounts = AccountSchool::where("school_id", $school_id)->get()->pluck("account_id")->toArray();
            if (count($accounts) > 0) {
                $questions = $questions->whereIn("account_id", $accounts);
            }
        }

        $teacher_status = $request->get("teacher_status");
        if (!empty($teacher_status)) {
            if ($teacher_status == "0") {
                $questions = $questions->where("total_approval", 0);
            } else {
                $questions = $questions->where("total_approval", ">", 0);
            }
        }
        $accounts = [];

        $teacher_id = $request->get("teacher_id", "");
        if (!empty($teacher_id)) {
            $schools = AccountSchool::select("school_id")
                ->distinct()
                ->where("account_id",$teacher_id)
                ->get()
                ->pluck("school_id")
                ->toArray();

            $accounts = AccountSchool::select("account_id")
                ->whereIn("school_id",$schools)
                ->get()
                ->pluck("account_id")
                ->toArray();
        }

        $student_id = $request->get("student_id", "");
        if (!empty($student_id)) {
            $accounts[] = $student_id;
        }

        if (count($accounts)) {
            $questions = $questions->whereIn("account_id", $accounts);
        }

        $dashboardQuestions = $questions->get();
        $questionID = $dashboardQuestions->pluck("id")->toArray();
        $dashboard = [];
        $dashboard['total_question'] = DashboardQuestion::whereIn("id", $questionID)->count();
        $dashboard['total_admin_approved'] = DashboardQuestion::whereIn("id", $questionID)->where("status", 1)->count();
        $dashboard['total_teacher_approved'] = TeacherQuestionBank::whereIn("question_id", $questionID)->count();
        $dashboard['total_reused'] = AssessmentQuestion::whereIn("question_id", $questionID)->count();
        $data = [
            'dashboard' => $dashboard,
            'status' => "success"
        ];
        return response()->json($data, 200);
    }

    public function accounts(Request $request)
    {
        $accountTypeId = $request->get("account_type_id");
        $accounts = Account::where("account_type_id", $accountTypeId)->get()->toArray();
        $data = [
            'accounts' => $accounts,
            'status' => 'success'
        ];
        return response()->json($data, 200);
    }

    public function searchSubject(Request $request)
    {
        $name = $request->get("name");
        $subjects = QuestionSubject::where("name", "ILIKE", "%$name%")->get()->toArray();
        $data = [
            'subjects' => $subjects,
            'status' => 'success'
        ];
        return response()->json($data, 200);
    }

    public function searchGrade(Request $request)
    {
        $name = $request->get("name");
        $grades = QuestionGrade::where("name", "ILIKE", "%$name%")->get()->toArray();
        $data = [
            'grades' => $grades,
            'status' => 'success'
        ];
        return response()->json($data, 200);
    }

    public function searchSchool(Request $request)
    {
        $name = $request->get("name");
        $schools = School::where("school_name", "ILIKE", "%$name%")->get()->toArray();
        $data = [
            'schools' => $schools,
            'status' => 'success'
        ];
        return response()->json($data, 200);
    }
}
