<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Accounts\Account;
use App\Models\Accounts\AccountSchool;
use App\Models\Course;
use App\Models\CourseworkAssessmentResponse;
use App\Models\Dashboard\ClassWork;
use App\Models\Group;
use App\Models\Invitations;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class GradeBookController extends Controller
{
    public function index()
    {
        $startDate = Carbon::now()->startOfMonth()->format("Y-m-d");
        $endDate = Carbon::now()->endOfMonth()->format("Y-m-d");
        //build the query according to user role
        $user = request()->user();
        $userType = $user->accountType;
        $queryClassWorks = ClassWork::where([]);
        if (strtolower($userType->name) === 'teachers') {
            $schoolID = AccountSchool::where("account_id", $user->id)->get()->pluck("school_id")->toArray();
            $courses = Course::whereIn("school_id", $schoolID)->get()->pluck("id")->toArray();
            $queryClassWorks = $queryClassWorks->whereIn("class_id", $courses);
        } else if (strtolower($userType->name) === 'student') {
            $externalCourseID = Invitations::where("external_user_id", $user->email)->get()->pluck("external_course_id");
            $queryClassWorks = $queryClassWorks->whereIn("external_course_id", $externalCourseID);
        }
        $classWorks = $queryClassWorks->dueDate(">=", $startDate)->dueDate("<=", $endDate)->where("external_course_id", "!=", NULL)->get();
        $classWorkID = $classWorks->pluck("id")->toArray();
        $total_classwork = count($classWorkID);
        $total_overdue = CourseworkAssessmentResponse::whereIn("course_work_id", $classWorkID)->lateSubmission('true')->count();
        $total_complete = CourseworkAssessmentResponse::whereIn("course_work_id", $classWorkID)->lateSubmission('false')->graded()->count();
        $total_graded = CourseworkAssessmentResponse::whereIn("course_work_id", $classWorkID)->graded()->count();
        $total_ungraded = CourseworkAssessmentResponse::whereIn("course_work_id", $classWorkID)->unGraded()->count();
        $total_returned = CourseworkAssessmentResponse::whereIn("course_work_id", $classWorkID)->state("RETURNED")->count();
        $dashboard = [
            'stats' => [
                'total_classwork' => $total_classwork,
                'total_complete' => $total_complete,
                'total_overdue' => $total_overdue,
                'total_graded' => $total_graded,
                'total_ungraded' => $total_ungraded,
                'total_returned' => $total_returned,
            ]
        ];
        return response()->json(['dashboard' => $dashboard, 'status' => 'success'], 200);
    }

    public function filter(Request $request)
    {
        $startDate = $request->get("startDate", "");
        $endDate = $request->get("endDate", "");
        $markingStart = $request->get("markingStart", "");
        $markingEnd = $request->get("markingEnd", "");
        $student = $request->get("student", "");
        $teacher = $request->get("teacher", "");
        $school = $request->get("school", "");
        $class = $request->get("class", "");
        $grade = $request->get("grade", "");
        $queryClassWorks = ClassWork::where([]);
        //build the query according to user role
        $user = request()->user();
        $userType = $user->accountType;
        if (strtolower($userType->name) === 'teachers') {
            $schoolID = AccountSchool::where("account_id", $user->id)->get()->pluck("school_id")->toArray();
            $courses = Course::whereIn("school_id", $schoolID)->get()->pluck("id")->toArray();
            $queryClassWorks = $queryClassWorks->whereIn("class_id", $courses);
        } else if (strtolower($userType->name) === 'student') {
            $externalCourseID = Invitations::where("external_user_id", $user->email)->get()->pluck("external_course_id");
            $queryClassWorks = $queryClassWorks->whereIn("external_course_id", $externalCourseID);
        }
        if (!empty($school)) {
            $courses = Course::where("school_id", $school)->get()->pluck("id")->toArray();
            $queryClassWorks = $queryClassWorks->whereIn("class_id", $courses);
        }
        if (!empty($startDate)) {
            $queryClassWorks = $queryClassWorks->duedate(">=", $startDate);
        }
        if (!empty($endDate)) {
            $queryClassWorks = $queryClassWorks->duedate("<=", $endDate);
        }

        if (!empty($class)) {
            $queryClassWorks = $queryClassWorks->where("class_id", $class);
        }
        if (!empty($teacher)){
            $account = Account::find($teacher);
            $email = $account->email;
            $teacherCourses = Invitations::select("external_course_id")
                ->distinct()
                ->where("external_user_id",$email)
                ->get()
                ->pluck("external_course_id")
                ->toArray();
            $queryClassWorks->whereIn("external_course_id",$teacherCourses);
        }
        if (!empty($student)){
            $account = Account::find($teacher);
            $email = $account->email;
            $studentCourses = Invitations::select("external_course_id")
                ->distinct()
                ->where("external_user_id",$email)
                ->get()
                ->pluck("external_course_id")
                ->toArray();
            $queryClassWorks->whereIn("external_course_id",$studentCourses);
        }
        if (!empty($grade)) {
            $queryClassWorks = $queryClassWorks->whereIn("grade_category_id", $grade);
        }
        $queryClassWorks = $queryClassWorks->where("external_course_id", "!=", NULL);
        $classWorks = $queryClassWorks->get();
        $classWorkID = $classWorks->pluck("id")->toArray();
        $assessmentResponseQuery = CourseworkAssessmentResponse::where([]);
        if (count($classWorkID)){
            $assessmentResponseQuery = CourseworkAssessmentResponse::whereIn("course_work_id", $classWorkID);
        }
        if (!empty($markingStart)){
            $assessmentResponseQuery->graded()->draftGrade(">=",$markingStart);
        }
        if (!empty($markingEnd)){
            $assessmentResponseQuery->graded()->draftGrade("<=",$markingEnd);
        }
        $total_classwork = count($classWorkID);
        $total_overdue = $assessmentResponseQuery->lateSubmission('true')->count();
        $total_complete = CourseworkAssessmentResponse::lateSubmission('false')->graded()->count();
        $total_graded = CourseworkAssessmentResponse::graded()->count();
        $total_ungraded = CourseworkAssessmentResponse::unGraded()->count();
        $total_returned = CourseworkAssessmentResponse::state("RETURNED")->count();

        $dashboard = [
            'stats' => [
                'total_classwork' => $total_classwork,
                'total_complete' => $total_complete,
                'total_overdue' => $total_overdue,
                'total_graded' => $total_graded,
                'total_ungraded' => $total_ungraded,
                'total_returned' => $total_returned,
            ]
        ];
        return response()->json(['dashboard' => $dashboard, "status" => "success"], 200);
    }

}
