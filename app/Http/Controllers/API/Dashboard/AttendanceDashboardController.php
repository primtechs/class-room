<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Accounts\Account;
use App\Models\Accounts\AccountSchool;
use App\Models\Course;
use App\Models\CourseworkAssessmentResponse;
use App\Models\Dashboard\ClassWork;
use App\Models\Group;
use App\Models\Invitations;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AttendanceDashboardController extends Controller
{
    public function index(Request $request)
    {
        $startDate = $request->get("start_date", Carbon::now()->startOfMonth()->format("Y-m-d"));
        $endDate = $request->get("end_date", Carbon::now()->endOfMonth()->format("Y-m-d"));
        $class = $request->get("class", "");
        $grade = $request->get("grade", "");
        $user = request()->user();
        $userType = $user->accountType;
        $classWorkQuery = ClassWork::where([]);
        //build the query according to user role
        if (strtolower($userType->name) === 'teachers') {
            $schoolID = AccountSchool::where("account_id", $user->id)->get()->pluck("school_id")->toArray();
            $classID = Course::whereIn("school_id", $schoolID)->get()->pluck("id")->toArray();
            $classWorkQuery = $classWorkQuery->whereIn("class_id", $classID);
        } else if (strtolower($userType->name) === 'student') {
            $externalCourseID = Invitations::where("external_user_id", $user->email)->get()->pluck("external_course_id");
            $classWorkQuery = $classWorkQuery->whereIn("external_course_id", $externalCourseID);
        }
        //ad conditions according to filter results
        if (!empty($class)) {
            $classID = Course::where("name", "ILIKE", "%$class%")->get()->pluck("id")->toArray();
            $classWorkQuery = ClassWork::whereIn("class_id", $classID);
        }
        if (!empty($grade)) {
            $gradeID = Group::where("name", "ILIKE", "%$grade%")->get()->pluck("id")->toArray();
            $classWorkQuery = $classWorkQuery->whereIn("grade_category_id", $gradeID);
        }
        $classWorks = $classWorkQuery->get();
        $classWorkID = $classWorks->pluck("id")->toArray();
        $query = CourseworkAssessmentResponse::whereIn("course_work_id", $classWorkID)->where("last_update", ">=", $startDate)->where("last_update", "<=", $endDate);
        $total_submitted = $query->count();
        $total_overdue = $query->lateSubmission('true')->count();
        $total_complete = $query->lateSubmission('false')->graded()->count();
        $total_graded = $query->graded()->count();
        $total_ungraded = $query->unGraded()->count();
        $total_returned = $query->state("RETURNED")->count();
        $courses = $classWorkQuery->distinct("external_course_id")->where("external_course_id", "!=", null)->pluck("external_course_id")->toArray();
        $invitations = Invitations::whereIn("external_course_id", $courses)->where("role","STUDENT")->get()->pluck("external_user_id")->toArray();
        $accounts = Account::whereIn("email", $invitations)->get();
        $students = [];
        $total_session = 0;
        $total_period = 35;
        foreach ($accounts as $account) {
            $submitted_assessment = CourseworkAssessmentResponse::whereIn("course_work_id", $classWorkID)
                ->where("last_update", ">=", "$startDate 00:00:00")
                ->where("last_update", "<=", "$endDate 23:59:59")
                ->where("user_id", $account->id)
                ->count();
            $logged_in = $submitted_assessment;
            $engaged_in_session = Post::where("account_id", $account->id)
                ->where("last_update", ">=", "$startDate 00:00:00")
                ->where("last_update", "<=", "$endDate 23:59:59")
                ->count();
            $students[] = [
                "id"=>$account->id,
                "name" => "{$account->first_name} {$account->last_name}",
                "logged_in" => (int)($logged_in * $total_period) / 100,
                "submitted_assessment" => (int)($submitted_assessment * $total_period) / 100,
                "engaged_in_session" => (int)($engaged_in_session * $total_period) / 100,
            ];
            $total_session += (int)($logged_in * $total_period) / 100;
        }
        $total_average_session = count($accounts->toArray()) ==0 ? 0 : (int)($total_session / count($accounts->toArray()));
        $dashboard = [
            'stats' => [
                'total_average_session' => $total_average_session,
                'total_submitted' => $total_submitted,
                'total_overdue' => $total_overdue,
                'total_complete' => $total_complete,
                'total_graded' => $total_graded,
                'total_ungraded' => $total_ungraded,
                'total_returned' => $total_returned,
            ],
            'students' => $students
        ];
        return response()->json(['dashboard' => $dashboard, "status" => "success"], 200);
    }

    public function studentDetails(Request $request,$student)
    {
        $startDate = Carbon::parse($request->get("start_date", Carbon::now()->startOfMonth()->format("Y-m-d")));
        $endDate = Carbon::parse($request->get("end_date", Carbon::now()->endOfMonth()->format("Y-m-d")));
        $class = $request->get("class", "");
        $grade = $request->get("grade", "");
        $user = request()->user();
        $userType = $user->accountType;
        $classWorkQuery = ClassWork::where([]);
        //build the query according to user role
        if (strtolower($userType->name) === 'teachers') {
            $schoolID = AccountSchool::where("account_id", $user->id)->get()->pluck("school_id")->toArray();
            $classID = Course::whereIn("school_id", $schoolID)->get()->pluck("id")->toArray();
            $classWorkQuery = $classWorkQuery->whereIn("class_id", $classID);
        } else if (strtolower($userType->name) === 'student') {
            $externalCourseID = Invitations::where("external_user_id", $user->email)->get()->pluck("external_course_id");
            $classWorkQuery = $classWorkQuery->whereIn("external_course_id", $externalCourseID);
        }
        //ad conditions according to filter results
        if (!empty($class)) {
            $classID = Course::where("name", "ILIKE", "%$class%")->get()->pluck("id")->toArray();
            $classWorkQuery = ClassWork::whereIn("class_id", $classID);
        }
        if (!empty($grade)) {
            $gradeID = Group::where("name", "ILIKE", "%$grade%")->get()->pluck("id")->toArray();
            $classWorkQuery = $classWorkQuery->whereIn("grade_category_id", $gradeID);
        }
        $classWorkID = $classWorkQuery->get()->pluck("id")->toArray();
        $attendance_details = [];
        while ($startDate->isBefore($endDate)) {
            $date = $startDate->format("d/M, Y");
            $submitted_assessment = CourseworkAssessmentResponse::whereIn("course_work_id", $classWorkID)
                ->where("last_update", ">=", $startDate->format("Y-m-d 00:00:00"))
                ->where("last_update", "<=", $startDate->format("Y-m-d 23:59:59"))
                ->where("user_id", $student)
                ->count();
            $logged_in = $submitted_assessment;
            $engaged_in_session = Post::where("account_id", $student)
                ->where("last_update", ">=", $startDate->format("Y-m-d 00:00:00"))
                ->where("last_update", "<=", $startDate->format("Y-m-d 23:59:59"))
                ->count();
            $startDate->addDay();
            $attendance_details[] = [
                'date' => $date,
                'submitted_assessment' => $submitted_assessment,
                'logged_in' => $logged_in,
                'engaged_in_session' => $engaged_in_session,
            ];
        }
        return response()->json(['attendance_details' => $attendance_details, "status" => "success"], 200);
    }
}
