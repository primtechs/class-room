<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Question;

class DraftQuestionsController extends Controller
{
    public function index(){
        $user = request()->user();
        $questions = Question::where(['account_id' => $user->id, 'published_state' => 'DRAFT'])->get();
        return response()->json([
            'status' => 'success',
            'questions' => $questions,
            'message' => ''
        ], 200);
    }
}
