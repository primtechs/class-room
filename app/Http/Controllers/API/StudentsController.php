<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Accounts\Account;
use App\Models\StudentClass;
use Google_Service_Classroom;

class StudentsController extends Controller
{
    public function index()
    {
        $courseID = request()->get("course_id");
        $firstName = request()->get("first_name");
        $lastName = request()->get("last_name");
        $phoneNumber = request()->get("phone_number");
        $email = request()->get("email");
        $limit = config("pagination.limit");
        $studentQuery = NULL;
        //students for a specific course
        if ($courseID) {
            $course = StudentClass::where("invitation_metadata->courseId", $courseID)->first();
            $studentQuery = Account::whereIn("id", function ($query) use ($course) {
                $query->select("account_id")->from("student_class")->where("class_id", $course->class_id);
            });
        } else {
            $account = request()->user();
            $accountType = $account->accountType->name;
            if ($accountType == "Teachers" || $accountType == "Admin") {
                $classes = $account->teacherClasses()->where("status", true)->pluck("class_id")->toArray();
                $studentQuery = Account::whereIn("id", function ($query) use ($classes) {
                    $query->select("account_id")->from("student_class")->whereIn("class_id", $classes);
                });
            }
        }


        $students = [
            "data" => [],
            "form" => "0",
            "to" => "0",
            "total" => "0",
            "per_page" => "0",
            "last_page" => "0",
            "current_page" => "0",
        ];

        if($studentQuery){
            if($firstName){
                $studentQuery->where("first_name", "ILIKE", "%$firstName%");
            }
    
            if($lastName){
                $studentQuery->where("last_name", "ILIKE", "%$lastName%");
            }
    
            if($phoneNumber){
                $studentQuery->where("phone_number", "ILIKE", "%$phoneNumber%");
            }
    
            if($email){
                $studentQuery->where("email", "ILIKE", "%$email%");
            }

            $students = $studentQuery->paginate($limit)->toArray();
        
        }

        $data = [];
        $data["status"] = "success";
        $data["msg"] = "All Students List";
        $data["studentList"]["students"] = $students["data"];
        $data['pagination'] = [
            'from' => $students['from'],
            'to' => $students['to'],
            'total' => $students['total'],
            'per_page' => $students['per_page'],
            'current_page' => $students['current_page'],
            'last_page' => $students['last_page'],
        ];
        return response()->json($data, 200);
    }
}
