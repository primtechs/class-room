<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CourseGradeCategoryRequest;
use App\Models\CourseGradeCategory;

class CourseGradeCategoryController extends Controller
{
    public function index()
    {
        $categories = CourseGradeCategory::all();
        return successResponse($categories, null, 200);
    }

    public function get($id)
    {
        $category = CourseGradeCategory::findOrFail($id);
        return successResponse($category, null, 200);
    }

    public function create(CourseGradeCategoryRequest $request)
    {
        $data = $request->all();
        if ($data['category_type'] == 2) {
            $total = 0;
            foreach ($data['categories'] as $category) {
                $total += $category['amount'];
            }

            if ($total != 100) {
                return response()->json([
                    'status'=> 'Error',
                    'message' => 'Total point must be equal to 100',
                ], 400);
            }
        }
        foreach ($data['categories'] as $category) {
            CourseGradeCategory::insert([
                'category_type' => $data['category_type'],
                'category' => $category['category'],
                'amount' => $category['amount'],
                'course_id' => $data['course_id'],
                'external_course_id' => $data['external_course_id'],
                'show_overall_grade' => $data['show_overall_grade'],
            ]);
        }

        return successResponse(["message" => 'Course Grade Categories Created Successfully'], null, 201);
    }

    public function put(CourseGradeCategoryRequest $request)
    {
        $data = $request->all();
        if(empty($data['course_id'])) {
            return response()->json([
                'status'=> 'Error',
                'message' => 'Course id is required',
            ], 400);
        }

        CourseGradeCategory::where('course_id', $data['course_id'])->delete();

        if ($data['category_type'] == 2) {
            $total = 0;
            foreach ($data['categories'] as $category) {
                $total += $category['amount'];
            }

            if ($total != 100) {
                return response()->json([
                    'status'=> 'Error',
                    'message' => 'Total point must be equal to 100',
                ], 400);
            }
        }
        foreach ($data['categories'] as $category) {
            CourseGradeCategory::insert([
                'category_type' => $data['category_type'],
                'category' => $category['category'],
                'amount' => $category['amount'],
                'course_id' => $data['course_id'],
                'external_course_id' => $data['external_course_id'],
                'show_overall_grade' => $data['show_overall_grade'],
            ]);
        }

        return successResponse(["message" => 'Course Grade Categories Updated Successfully'], null, 201);
    }

    public function delete()
    {

    }
}
