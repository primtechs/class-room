<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Question;

class CourseWorkQuestionsController extends Controller
{
    public function index($assessment_id){
        $questions = Question::whereRaw("id IN (SELECT question_id FROM assessment_questions WHERE assessment_id = ?)", [$assessment_id])->get();
        return response()->json([
            'questions' => $questions ?? [],
            'status' => 'success',
            'message' => ''
        ]);
    }
}
