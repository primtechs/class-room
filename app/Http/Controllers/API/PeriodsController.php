<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Accounts\Account;
use App\Models\Period;
use App\Models\School;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PeriodsController extends Controller
{

    public function index()
    {
        $data = [];
        $data['accounts'] = Account::all();
        $data['schools'] = School::all();
        $data['data'] = Period::all();
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $account = request()->user();
        $rules = [
            'name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'active' => 'required',
            'school_id' => 'required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = $validator->errors();
            $data['data'] = [];
        } else {
            $schools = $request->get("school_id");
            $data['status'] = 'success';
            $data['message'] = 'Period is saved successfully';
            foreach ($schools as $school) {
                $period = Period::create([
                    'name' => $request->get("name"),
                    'start_date' => $request->get("start_date"),
                    'end_date' => $request->get("end_date"),
                    'active' => $request->get("active"),
                    'account_id' => $account->id,
                    'school_id' => $school,
                ]);
                $data['data'][] = $period;
            }
        }
        return response()->json($data, 201);
    }

    public function show($id)
    {
        $data = [];
        $period = Period::find($id);
        if ($period != NULL) {
            $data['status'] = 'success';
            $data['data'] = $period;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Period Not Found';
        }
        return response()->json($data, 200);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'active' => 'required',
            'school_id' => 'required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = $validator->errors();
            $data['data'] = [];
        } else {
            $period = Period::find($id);
            if ($period != NULL) {
                $schools = $request->get("school_id");
                foreach ($schools as $school) {
                    $periodData = [
                        'name' => $request->get("name"),
                        'start_date' => $request->get("start_date"),
                        'end_date' => $request->get("end_date"),
                        'active' => $request->get("active"),
                        'school_id' => $school,
                        'account_id' => request()->user()->id,
                    ];
                    $period = Period::updateOrCreate(['period_id' => $id, 'school_id' => $school], $periodData);
                    $data['data'][] = $period;
                }
                $data['status'] = 'success';
                $data['message'] = '';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Period Not Found';
            }
        }
        return response()->json($data, 200);
    }

    public function destroy($id)
    {
        $period = Period::find($id);
        if ($period != NULL) {
            try {
                $period->delete();
                $data['status'] = 'success';
                $data['data'] = '';
                $data['message'] = 'Period Deleted Successfully';
            } catch (\Exception $exception) {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Period Not Found';
        }
        return response()->json($data, 200);
    }
}
