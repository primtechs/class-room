<?php

namespace App\Http\Controllers\API;

use App\Models\Topic;
use App\Repository\TopicRepository;
use Google_Service_Classroom;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TopicsController extends Controller
{
    protected $topics;

    public function __construct(TopicRepository $topics)
    {
        $this->topics = $topics;
    }

    public function index()
    {
        if(request()->has("courseId")){
            $courseId  = request()->get("courseId", "");
            return $this->topics->topicsByCourse($courseId);
        }
        return $this->topics->topics();
    }

    public function store(Request $request) {
        $data = $request->all();
        $response = $this->topics->create($data);
        return successResponse($response, null,201);
    }

    public function update(Request $request, $id){
        $name = $request->get("name");
        $topic = Topic::find($id);
        $data = [
            'id' => $id,
            'name' => $name,
            'topicId' => $topic->external_metadata['topicId'],
            'courseId' => $topic->external_metadata['courseId'],
            'updateTime' => date("Y-m-d\TH:i:s\Z")
        ];
        $response = $this->topics->updateTopic($data, $topic);
        return successResponse($response, null, 201);
    }

    public function destroy($id){
        $data = [];
        $topic = Topic::find($id);
        try{
            $topic->delete();
            $this->topics->deleteTopic([
                'topicId' => $topic->external_metadata['topicId'],
                'courseId' => $topic->external_metadata['courseId'],
            ]);
            $data['status'] = 'success';
            $data['topic'] = '';
            $data['message'] = 'Topic Deleted Successfully';
        }catch(Exception $exception){
            $data['status'] = 'error';
            $data['topic'] = '';
            $data['message'] = 'Unable to Perform the Action Topic is in use';
        }
        return response()->json($data, 200);
    }
}
