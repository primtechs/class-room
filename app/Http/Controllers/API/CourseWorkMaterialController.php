<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CourseWorkMaterial;
use Google_Service_Classroom;

class CourseWorkMaterialController extends Controller
{

    public function index($courseId)
    {
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $courseWorkMaterials = $classroom->courses_courseWorkMaterials->listCoursesCourseWorkMaterials($courseId);
        return successResponse($courseWorkMaterials, null,200);
    }

    public function create(CourseWorkMaterial $request, $courseId) {
        $data = $request->all();
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $courseWork = setCourseWorkMaterial($data);
        // TODO: set to db
        $response = $classroom->courses_courseWorkMaterials->create($courseId, $courseWork);
        return successResponse($response, null,201);
    }

    public function get()
    {

    }

    public function update()
    {

    }

    public function delete()
    {

    }
}
