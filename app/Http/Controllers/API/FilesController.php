<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\File;
use App\Models\FileType;
use Storage;

class FilesController extends Controller
{

    public function index()
    {
        $limit = config("pagination.limit");
        $query = File::orderBy("id", "desc");
        //search queries
        request()->whenFilled('start_date', function ($input) use(&$query) {
            $query = $query->where("created_at", "<=", $input);
        });
        request()->whenFilled('end_date', function ($input) use(&$query) {
            $query = $query->where("created_at", ">=", $input);
        });
        request()->whenFilled('type', function ($input) use(&$query) {
            $query = $query->where("type", $input);
        });
        //end of search queries
        $files = $query->paginate($limit)->toArray();
        $data['pagination'] = [
            'from' => $files['from'],
            'to' => $files['to'],
            'total' => $files['total'],
            'per_page' => $files['per_page'],
            'current_page' => $files['current_page'],
            'last_page' => $files['last_page'],
        ];
        $data['files'] = $files['data'];
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }


    public function store(Request $request)
    {
        $uploadFiles = $request->file("files");
        $response = [
            'status' => 'success',
            'message' => '',
            'files' => []
        ];
        foreach($uploadFiles as $file){
            $mimeType = $file->getClientMimeType();
            $fileName = time()."_".$file->getClientOriginalName();

            $fileType = FileType::where('file_type', $mimeType)->first();
            if($fileType){
                $type = $fileType->fclass;
                $path = "documents";
                if ($fileType->fclass === "video" || $fileType->fclass === "audio") {
                    $path = "videos";
                }else if($fileType->fclass === "image"){
                    $path = "images";
                }
                $file->storeAs($path, $fileName, ['disk' => 's3']);
                $url = env('AWS_LOCATION')."/$path/$fileName";
                $response['files'][] =  File::create([
                    'name' => $file->getClientOriginalName(),
                    'type' => $type,
                    'url' => $url,
                    'created_at' => date("Y-m-d")
                ]);
            }else{
                Log::warning("File create warning $mimeType Not found");
                continue;
            }
        }
        return response()->json($response, 200);
    }


    public function destroy($id)
    {
        $file = File::find($id);
        $awsLocation = env("AWS_LOCATION");
        $awsFile = str_replace("{$awsLocation}/", "", $file->url);
        Storage::disk('s3')->delete($awsFile);
        $file->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'File deleted successfully!',
            'file' => null
        ], 200);
    }
}
