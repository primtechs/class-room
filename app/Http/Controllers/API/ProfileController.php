<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Profile;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function index()
    {
        $data = [];
        $response = Profile::all();
        $data['data'] = $response;
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'phone' => "required|unique:profile,phone",
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'phone' => $validator->errors()->first("phone")
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Profile is saved successfully';
            $data['data'] = Profile::create($request->all());
        }
        return response()->json($data, 201);
    }

    public function show($account_id)
    {
        $data = [];
        $profile = Profile::where("account_id",$account_id)->first();
        if ($profile != NULL) {
            $data['status'] = 'success';
            $data['profile'] = $profile;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['profile'] = '';
            $data['message'] = 'Not Found';
        }
        return response()->json($data, 200);
    }

    public function update(Request $request, $account_id)
    {
        $profile = Profile::where("account_id",$account_id)->first();
        $rules = [
            'phone' => "required|unique:profile,phone,{$profile->id}",
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'phone' => $validator->errors()->first("phone")
            ];
            $data['data'] = '';
        } else {
            if ($profile != NULL) {
				$profile->update($request->all());
                $data['status'] = 'success';
                $data['profile'] = $profile->fresh();
                $data['message'] = 'Profile has been updated successfully';
            } else {
                $data['status'] = 'error';
                $data['profile'] = '';
                $data['message'] = 'Not Found';
            }
        }
        return response()->json($data, 200);
    }

    public function destroy(Request $request, Profile $profile)
    {
        $profile = Profile::find($id);
        if ($profile != NULL) {
            $profile->delete();
            $data['status'] = 'success';
            $data['data'] = '';
            $data['message'] = 'Profile Deleted Successfully';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Not Found';
        }
    }
}
