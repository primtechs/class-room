<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\QuestionType;
use Illuminate\Support\Facades\Validator;

class QuestionTypeController extends Controller
{

    public function index()
    {
        $data = [];
        $response = QuestionType::all();
        $data['data'] = $response;
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => "required|unique:question_type,name",
            'description' => 'required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name"),
                'description' => $validator->errors()->first("description")
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['data'] = QuestionType::create([
                'name' => $request->get('name'),
                'description' => $request->get('description'),
            ]);
            $data['message'] = 'Question Type is saved successfully';
        }
        return response()->json($data, 201);
    }


    public function show($id)
    {
        $data = [];

        $school = QuestionType::find($id);
        if ($school != NULL) {
            $data['status'] = 'success';
            $data['data'] = $school;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Question Types are Not Found';
        }
        return response()->json($data, 200);
    }


    public function update(Request $request, $id)
    {
        $rules = [
            'name' => "required|unique:question_type,name,$id",
            'description' => 'required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name"),
                'description' => $validator->errors()->first("description")
            ];
            $data['data'] = '';
        } else {
            $question_type = QuestionType::find($id);
            if ($question_type != NULL) {
                $question_type->update($request->all());
                $data['status'] = 'success';
                $data['data'] = $question_type->fresh();
                $data['message'] = 'Question Type is updated Successfully';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Question Type is Not Found';
            }
        }
        return response()->json($data, 200);
    }


    public function destroy($id)
    {
        $question_type = QuestionType::find($id);
        if ($question_type != NULL) {
            try {
                $question_type->delete();
                $data['status'] = 'success';
                $data['data'] = '';
                $data['message'] = 'Question Type Deleted Successfully';
            } catch (\Exception $exception) {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Not Found';
        }
        return response()->json($data, 200);
    }
}
