<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\QuestionSubject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class QuestionSubjectsController extends Controller
{

    public function index()
    {
        $data = [];
        $response = QuestionSubject::all();
        $data['data'] = $response;
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }


    public function store(Request $request)
    {
        $rules = [
            'name' => "required|unique:question_subjects,name",
            'description' => 'required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name"),
                'description' => $validator->errors()->first("description")
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Subject is saved successfully';
            $data['data'] = QuestionSubject::create([
                'name' => $request->get("name"),
                'description' => $request->get("description"),
            ]);
        }
        return response()->json($data, 201);
    }


    public function show($id)
    {
        $data = [];
        $questionSubject = QuestionSubject::find($id);
        if ($questionSubject != NULL) {
            $data['status'] = 'success';
            $data['subject'] = $questionSubject;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['subject'] = '';
            $data['message'] = 'Subject is Not Found';
        }
        return response()->json($data, 200);
    }


    public function update(Request $request, $id)
    {
        $rules = [
            'name' => "required|unique:question_subjects,name,$id",
            'description' => 'required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name"),
                'description' => $validator->errors()->first("description")
            ];
            $data['subject'] = '';
        } else {
            $questionSubject = QuestionSubject::find($id);
            if ($questionSubject != NULL) {
                $name = $request->get("name");
                $description = $request->get("description");
                $questionSubject->name = $name;
                $questionSubject->description = $description;
                $questionSubject->save();
                $data['status'] = 'success';
                $data['data'] = $questionSubject->fresh();
                $data['message'] = 'Subject Updated Successfully';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Subject Not Found';
            }
        }
        return response()->json($data, 200);
    }


    public function destroy(Request $request, $id)
    {
        $questionSubject = QuestionSubject::find($id);
        if ($questionSubject != NULL) {
            try {
                $questionSubject->delete();
                $data['status'] = 'success';
                $data['subject'] = '';
                $data['message'] = 'Subject Deleted Successfully';
            }catch (\Exception $exception){
                $data['status'] = 'error';
                $data['subject'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['subject'] = '';
            $data['message'] = 'Subject Not Found';
        }
        return response()->json($data, 200);
    }
}
