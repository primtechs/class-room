<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Assessment;

class AssessmentArchiveController extends Controller
{

    public function archiveAssessment($id, Request $request){
        $assessment = Assessment::find($id);
        $assessment->is_archived = $request->get("is_archived", 0);
        $assessment->save();
        return response()->json([
            'status' => 'success',
            'message' => '',
            'assessment' => $assessment->fresh()
        ], 200);
    }

}
