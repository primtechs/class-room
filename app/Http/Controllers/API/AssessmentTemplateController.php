<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\AssessmentTemplateRequest;
use App\Models\AssessmentTemplate;
use Illuminate\Http\Request;

class AssessmentTemplateController extends Controller
{
    public function index()
    {
        $assessments = AssessmentTemplate::all();
        return successResponse($assessments, null, 200);
    }

    public function get($id)
    {
        $assessment = AssessmentTemplate::findOrFail($id);
        return successResponse($assessment, null, 200);
    }

    public function store(AssessmentTemplateRequest $request)
    {
        $data = $request->all();
        $assessmentTemplate = AssessmentTemplate::create($data);
        return successResponse($assessmentTemplate, null, 201);
    }

    public function put($id, Request $request)
    {
        $data = $request->all();
        $assessmentTemplate = AssessmentTemplate::findOrFail($id);
        $assessmentTemplate->update($data);
        return successResponse($assessmentTemplate, null, 200);
    }

    public function delete($id)
    {
        $assessmentTemplate = AssessmentTemplate::findOrFail($id);
        $assessmentTemplate->delete();
        return successResponse(null, 'Deleted Successfully', 200);
    }
}
