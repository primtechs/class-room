<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Level;
use Illuminate\Http\Request;
use App\Models\Standard;
use Illuminate\Support\Facades\Validator;

class StandardController extends Controller
{
    public function index()
    {
        $data = [];
        $response = Standard::all();
        $data['data'] = $response;
        $data['levels'] = Level::all();
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => "required|unique:standard,name",
            'level_id' => 'required',
            'description' => 'required'
        ];
        $messages = [
            'level_id.required' => 'Level is Required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name"),
                'level_id' => $validator->errors()->first("level_id"),
                'description' => $validator->errors()->first("level_id"),
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Standard is saved successfully';
            $data['data'] = Standard::create([
                'name' => $request->get("name"),
                'description' => $request->get("description"),
                'level_id' => $request->get("level_id"),
            ]);
        }
        return response()->json($data, 201);

    }

    public function show($id)
    {
        $data = [];
        $standard = Standard::find($id);
        if ($standard != NULL) {
            $data['status'] = 'success';
            $data['data'] = $standard;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Standards are Not Found';
        }
        return response()->json($data, 200);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => "required|unique:standard,name,$id",
            'level_id' => 'required',
            'description' => 'required'
        ];
        $messages = [
            'level_id.required' => 'Level is Required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name"),
                'level_id' => $validator->errors()->first("level_id"),
                'description' => $validator->errors()->first("level_id"),
            ];
            $data['data'] = '';
        } else {
            $standard = Standard::find($id);
            if ($standard != NULL) {
                $standard->name = $request->get("name");
                $standard->description = $request->get("description");
                $standard->level_id = $request->get("level_id");
                $standard->save();
                $data['status'] = 'success';
                $data['data'] = $standard->fresh();
                $data['message'] = 'Standard Updated Successfully';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Standard is Not Found';
            }
        }
        return response()->json($data, 200);
    }


    public function destroy(Request $request, $id)
    {
        $standard = Standard::find($id);
        if ($standard != NULL) {
            try {
                $standard->delete();
                $data['status'] = 'success';
                $data['data'] = '';
                $data['message'] = 'Standard Deleted Successfully';
            } catch (\Exception $exception) {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Standard Not Found';
        }
        return response()->json($data, 200);
    }
}
