<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\File;
use Rap2hpoutre\FastExcel\FastExcel;

class FileExportController extends Controller
{
    
    public function export(Request $request){
        $selected  = $request->get("selected", []);
        $files = File::whereIn("id", $selected)->get();
        $fileName =  "files_".date("Y_m_d_h_i_s").".xlsx";
        return (new FastExcel($files))->download($fileName, function($file){
            return [
                'name' => $file->name,
                'type' => $file->type,
                'url' => $file->url,
                'created_at' => $file->created_at
            ];
        });
    }

}
