<?php

namespace App\Http\Controllers\API;

use App\Events\CheckReward;
use App\Helpers\GoogleClient;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubmissionGrade;
use App\Models\CourseworkAssessmentResponse;
use App\Models\CourseWorkReturnMessage;
use App\Models\Coursework;
use App\Models\Accounts\Account;
use Google_Service_Classroom;
use Google_Service_Classroom_MultipleChoiceSubmission;
use Google_Service_Classroom_StudentSubmission;
use Google_Service_Classroom_TurnInStudentSubmissionRequest;
use Google_Service_Classroom_Resource_CoursesCourseWorkStudentSubmissions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class StudentClassWorkController extends Controller
{
    public function index($courseId, $courseWorkId)
    {
        $client = setGoogleClient(getOAuthToken());
        $class = new \Google_Service_Classroom($client);
        $submission = $class->courses_courseWork_studentSubmissions->listCoursesCourseWorkStudentSubmissions($courseId, $courseWorkId);

        return successResponse($submission, null, 200);
    }

    public function get($courseId, $courseWorkId, $id,$user_id)
    {
        $account = getAccountDetails();
        $client = setGoogleClient(getOAuthToken());
        $class = new \Google_Service_Classroom($client);
        $submission = $class->courses_courseWork_studentSubmissions->get($courseId, $courseWorkId, $id);
        if(strlen($user_id) > 10){
            $user_id = Account::where('external_user_id', $user_id)->first()->id;
        }
        $response = CourseworkAssessmentResponse::where([
            'external_id' => $id,
            'user_id' => $user_id,
        ])->first();
        if ($response === NULL) {
            $courseWork = Coursework::where('external_course_work_id', $courseWorkId)->first();
            $response = CourseworkAssessmentResponse::create([
                'course_work_id' => $courseWork->id,
                'external_id' => $id,
                'response' => NULL,
                'meta_data' => $submission,
                'sub_grade' => NULL,
                'user_id' => $user_id,
                'returned_to_student' => false,
                'turned_in' => false
            ]);
        }else{
            $response->meta_data = $submission;
            $response->save();
        }
        $messages = [];
        $courseWork = Coursework::where("external_course_work_id", $courseWorkId)->first();
        if ($courseWork !== NULL) {
            $messages = CourseWorkReturnMessage::where([
                'account_id' => $user_id,
                'course_work_id' => $courseWork->id
            ])->get();
        }
        return successResponse(
            [
                'external'=> $submission,
                'local' => $response->fresh(),
                'messages' => $messages
            ],
            null, 200);
    }

    public function studentSubmission(SubmissionGrade $request, $courseId, $courseWorkId, $id)
    {
        $client = setGoogleClient(getOAuthToken());
        $account = getAccountDetails();
        $data = $request->all();
        $class = new Google_Service_Classroom($client);
        $type = $data['type'];
        $submission = null;
        $assessmentResponse = null;

        try{
            isStudent($class, $courseId);
        } catch (\Google_Service_Exception $e){
            return response()->json([
                'status'=> 'Error',
                'message' => 'Invalid User Type',
            ], 200);
        }

        switch ($type) {
            case 'SHORT_ANSWER_SUBMISSION':
                $submission = setShortAnswerSubmission($data);
                $assessmentResponse = $data['answer'];
                break;
            case 'ASSIGNMENT_SUBMISSION':
                $submission = setAssignmentSubmission($data['attachment']);
                $assessmentResponse = $data['attachment']['url'];
                break;
            case 'MULTIPLE_CHOICE_SUBMISSION':
                $submission = setMultipleChoiceSubmission($data);
                $assessmentResponse = $data['answer'];
                break;
        }
        try {
            if ($type === 'ASSIGNMENT_SUBMISSION') {
                $clientSubmission = $class->courses_courseWork_studentSubmissions->modifyAttachments($courseId, $courseWorkId, $id, $submission);
            }else{
                $clientSubmission = $class->courses_courseWork_studentSubmissions->patch($courseId, $courseWorkId, $id, $submission);
            }
            //turn in the request
            $class->courses_courseWork_studentSubmissions->turnIn($courseId, $courseWorkId, $id, new Google_Service_Classroom_TurnInStudentSubmissionRequest());
        }catch (\Google_Service_Exception $e) {
            $error = json_decode($e->getMessage(), true);
            return response()->json([
                'status'=> 'error',
                'message' => $error['error']['message']
            ], 200);
        }
        // get submission or create
        $courseWork = Coursework::where([
            'external_course_work_id' => $courseWorkId
        ])->first();
        CourseworkAssessmentResponse::updateOrCreate(
            [
                'course_work_id' => $courseWork->id,
                'external_id' => $id,
                'user_id' => $account['id']
            ],
            [
                'meta_data' => json_encode($clientSubmission),
                'response' => $assessmentResponse,
                'returned_to_student' => false,
                'turned_in' => true
            ]
        );
        event(new CheckReward('assignment', ['reason' => 'assignment_completed'], request()->user()));
        return successResponse([
            'local' => $assessmentResponse,
            'external' => $clientSubmission
        ], null, 200);
    }

    public function gradeSubmission(SubmissionGrade $request, $courseId, $courseWorkId, $id)
    {
        $account = getAccountDetails();
        $data = $request->all();
        $client = setGoogleClient(getOAuthToken());
        $class = new \Google_Service_Classroom($client);
        $type = $data['type'];
        $submission = null;
        $query = [];
        $grade = null;

        try{
            isTeacher($class, $courseId);
        } catch (\Google_Service_Exception $e){
            return response()->json([
                'status'=> 'Error',
                'message' => 'Invalid User Type '.$e->getMessage()
            ], 400);
        }

        switch ($type) {
            case 'ASSIGN_GRADE':
                $submission = assignGrade($data);
                $query['updateMask'] = 'assignedGrade';
                $grade = $data['assignGrade'];
                break;
            case 'DRAFT_GRADE':
                $submission = draftGrade($data);
                $query['updateMask'] = 'draftGrade';
                break;
        }

        try {
            $external = $submission = $class->courses_courseWork_studentSubmissions->patch($courseId, $courseWorkId, $id, $submission, $query);
        }catch (\Google_Service_Exception $e) {
            return response()->json([
                'status'=> 'error',
                'message' => $e->getMessage()
            ], 400);
        }

        $courseWork = Coursework::where([
            'external_course_work_id' => $courseWorkId
        ])->first();

        $response = CourseworkAssessmentResponse::firstOrCreate(
            [
                'course_work_id' => $courseWork->id,
                'external_id' => $id,
                'user_id' => $account['id']
            ]
        );

        $response->meta_data = json_encode($external);
        $response->sub_grade = $grade;
        $response->save();

        return successResponse([
            'local' => $response,
            'external' => $external
        ], null, 200);
    }


    public function submissionDetails($courseId, $courseWorkId ,$user_id = "me")
    {
        $client = setGoogleClient(getOAuthToken());
        $class = new \Google_Service_Classroom($client);
        $submissions = $class->courses_courseWork_studentSubmissions->listCoursesCourseWorkStudentSubmissions($courseId, $courseWorkId);
        $data['message'] = "";
        $data['status'] = "success";
        $data['studentSubmission'] = $submissions->studentSubmissions[0];
        return response()->json($data, 200);
    }

    public function submitCourseWork($id, $courseId, $courseWorkId, Request $request)
    {
        $studentSubmission = $request->get("student_submission");
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $submission = new Google_Service_Classroom_StudentSubmission();
        if ($studentSubmission['courseWorkType'] === 'MULTIPLE_CHOICE_QUESTION') {
            $multipleChoiceAnswer = new Google_Service_Classroom_MultipleChoiceSubmission();
            $multipleChoiceAnswer->setAnswer($studentSubmission["multipleChoiceSubmission"]["answer"]);
            $submission->setMultipleChoiceSubmission($multipleChoiceAnswer);
        } elseif ($studentSubmission['courseWorkType'] === 'SHORT_ANSWER_QUESTION') {
            $shortAnswerSubmission = new \Google_Service_Classroom_ShortAnswerSubmission();
            $shortAnswerSubmission->setAnswer($studentSubmission["shortAnswerSubmission"]["answer"]);
            $submission->setShortAnswerSubmission($shortAnswerSubmission);
        }
        $submission->setAssignedGrade($studentSubmission["assignedGrade"]);
        $submission->setDraftGrade($studentSubmission["draftGrade"]);
        $submission->setState($studentSubmission['state']);
        try{
            $response = $classroom->courses_courseWork_studentSubmissions->patch($courseId, $courseWorkId, $id, $submission, ["updateMask" => "draftGrade,assignedGrade"]);
            return successResponse($response, null, 201);
        }catch (\Exception $e){
            Log::debug($e->getMessage());
        }
    }
}
