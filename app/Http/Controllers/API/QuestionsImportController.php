<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Rap2hpoutre\FastExcel\FastExcel;
use Rap2hpoutre\FastExcel\SheetCollection;
use App\Models\Question;
use App\Models\QuestionType;
use App\Models\QuestionResponse;
use App\Models\QuestionResource;
use App\Models\QuestionCycle;
use App\Models\QuestionSubject;
use App\Models\QuestionGrade;
use App\Models\Standard;
use App\Models\FileType;
use App\Models\TeacherQuestionBank;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use App\Rules\ImportQuestionFormatValidator;
use Illuminate\Support\Facades\Validator;
use Storage;

class QuestionsImportController extends Controller
{
    public function import(Request $request){
        $user = request()->user();

        $file = $request->file("file");
        $sheets = (new FastExcel)->withSheetsNames()->importSheets($file);

        $rules = [
            'file' => [
                "mimetypes:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                new ImportQuestionFormatValidator($sheets)
            ],
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => [
                    'file' => $validator->errors()->first("file")
                ] 
            ], 200);
        }

        $questions = $sheets['Questions'];
        $resources = $sheets['Resources'] ?? [];
        $responses = $sheets['Responses'] ?? [];
        $openResponses = $sheets['Open Responses'] ?? [];

        $importedQuestions = [];

        foreach($questions as $question){
            $type = QuestionType::where('name', $question['type'])->first();
            $topic = QuestionCycle::where('name', $question['topic'])->first();
            $standard = Standard::where('name', $question['standard'])->first();
            $subject = QuestionSubject::where('name', $question['question_subject'])->first();
            $grade = QuestionGrade::where('name', $question['question_grade'])->first();
            $formattedOpenResponses = [];
            $responseRubric = [];
            $questionRubrics = [];
            $criterions = collect($openResponses)->where("question_id", $question["id"])->groupBy('criterion_name');
            $criterions->each(function($criterion, $key) use(&$formattedOpenResponses){
                $rubrics = [];
                $criterion->each(function($item, $key) use(&$rubrics){
                    $rubrics[] = [
                        'level' => $item['level'],
                        'point' => $item['point'],
                        'description' => $item['description'],
                    ];
                });
                $formattedOpenResponses[] = [
                    "criterion_name" => $key,
                    "open_response_grade" => $criterion->max("point"),
                    "description" => $criterion[0]["criterion_description"],
                    "rubrics" => $rubrics
                ];
            });
            $data = [
                'type_id' => $type->id ?? NULL,
                'topic_id' => $topic->id ?? NULL,
                'title' => $question['title'],
                'description' => $question['description'] ?? "",
                'standard_id' => $standard->id ?? NULL,
                'question_subject_id' => $subject->question_subject_id ?? NULL,
                'question_grade_id' => $grade->question_grade_id ?? NULL,
                'status' =>  $question['status'],
                'total_approval' => 0,
                'account_id' => $user->id,
                'average_rating' => 0.0,
                'rubrics' => [],
                'response_rubric' => [],
                'open_response' => $formattedOpenResponses,
                'open_response_grade' => collect($formattedOpenResponses)->max('open_response_grade'),
                'published_state' => $question['published_state']
            ];
            $newQuestion = Question::create($data);
            collect($responses)->where("question_id", $question["id"])->each(function($response, $key) use(&$questionRubrics, &$responseRubric, $newQuestion){
                $responseData = [
                    'question_id' => $newQuestion->id,
                    "description" => $response["description"],
                    "is_valid" => $response["is_valid"],
                    "rubric" => [
                        [
                            'level' => $response['match_label'],
                            'point' => $response['sub_grade'],
                            'description' => $response['description'],
                        ]
                    ],
                    "sub_grade" => $response["sub_grade"],
                    "last_udate" => date("Y-m-d H:i:s"),
                    "title" => $response['title'],
                    "match_label" => $response['match_label'],
                ];

                $responseRubric[] = QuestionResponse::create($responseData)->fresh()->toArray();
                $questionRubrics[] = [
                    'level' => $response['match_label'],
                    'point' => $response['sub_grade'],
                    'description' => $response['description'],
                ];
            });
            collect($resources)->where("question_id", $question["id"])->each(function($resource, $index) use($newQuestion){
                $url = $resource["url"];
                $urlFragments = explode("/", $url);
                $fileName = end($urlFragments);
                $fileNameFragments = explode(".", $fileName);
                $extension = end($fileNameFragments);
                $fileType = FileType::where('extension', ".{$extension}")->first();
                if($fileType){
                    $type = $fileType->fclass;
                    $path = "documents";
                    if ($fileType->fclass === "video" || $fileType->fclass === "audio") {
                        $path = "videos";
                    }else if($fileType->fclass === "image"){
                        $path = "images";
                    }
                    Storage::disk('s3')->put("$path/{$newQuestion->id}_{$fileName}", file_get_contents($url));
                    $awsUrl = env('AWS_LOCATION')."/$path/{$newQuestion->id}_{$fileName}";
                    QuestionResource::create([
                        'name' => $fileName,
                        'res_url' => $awsUrl,
                        'description' => '',
                        'question_id' => $newQuestion->id,
                    ]);
                }else{
                    Log::warning("Question create warning {$fileType->extension} Not found");
                }
            });
            $newQuestion->rubrics = $questionRubrics;
            $newQuestion->response_rubric = $responseRubric;
            $newQuestion->save();
            TeacherQuestionBank::create([
                'account_id' => $user->id, 
                "question_id" => $newQuestion->id
            ]);
            $importedQuestions[] = $newQuestion->fresh()->toArray();
        }
        return response()->json([
            'status' => 'success',
            'questions' => $importedQuestions,
            'message' => 'Imported Question List'
        ], 200);
    }
}
