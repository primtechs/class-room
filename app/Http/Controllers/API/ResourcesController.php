<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ResourcesController extends Controller
{

    public function index()
    {
        $data = [];
        $response = Resource::all();
        $data['status'] = 'success';
        $data['data'] = $response;
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:resources,name',
            'res_url' => 'required'
        ];
        $messages = [
            'res_url.required' => "Resource URL is required",
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name"),
                'res_url' => $validator->errors()->first("res_url"),
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Resource is saved successfully';
            $data['data'] = Resource::create([
                'name' => $request->get("name"),
                'description' => $request->get("description"),
                'res_url' => $request->get("res_url")
            ]);
        }
        return response()->json($data, 200);
    }


    public function show($id)
    {
        $data = [];
        $resource = Resource::find($id);
        if ($resource != NULL) {
            $data['status'] = 'success';
            $data['data'] = $resource;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Resource are Not Found';
        }
        return response()->json($data, 200);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => "required|unique:resources,name,$id",
            'res_url' => 'required'
        ];
        $messages = [
            'res_url.required' => "Resource URL is required"
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name"),
                'res_url' => $validator->errors()->first("res_url"),
            ];
            $data['data'] = '';
        } else {
            $resource = Resource::find($id);
            if ($resource != NULL) {
				$resource->name = $request->get("name");
				$resource->description = $request->get("description");
				$resource->res_url = $request->get("res_url");
				$resource->save();
                $data['status'] = 'success';
                $data['data'] = $resource->fresh();
                $data['message'] = 'Resource Updated Successfully';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Resource is Not Found';
            }
        }
        return response()->json($data, 200);
    }


    public function destroy($id)
    {
        $resource = Resource::find($id);
        if ($resource != NULL) {
            try {
                $resource->delete();
                $data['status'] = 'success';
                $data['data'] = '';
                $data['message'] = 'Resource Deleted Successfully';
            }catch (\Exception $exception){

                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Not Found';
        }
        return response()->json($data, 200);
    }
}
