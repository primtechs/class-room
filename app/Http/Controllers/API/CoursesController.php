<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Invitations;
use App\Models\StudentClass;
use App\Models\TeacherClass;
use App\Repository\CourseRepository;
use Google_Service_Classroom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CoursesController extends Controller
{
    protected $courses;

    public function __construct(CourseRepository $courses)
    {
        $this->courses = $courses;
    }

    public function index(Request $request)
    {
        $type = $request->get('type') ?? null;
        $courses = $this->courses->getAllCourses($type);
        return successResponse($courses, null, 200);
    }

    public function syncCourses(Request $request)
    {
        $params = [];
        $type = strtoupper(request()->user()->account_type_name);
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);

        if ($type && $type === 'STUDENT') {
            $params['studentId'] = ownerId();
        }

        if ($type && $type === 'TEACHER') {
            $params['teacherId'] = ownerId();
        }

        $responses = $classroom->courses->listCourses($params);
        $courses = [];
        foreach ($responses as $response) {
            $local = Course::where('external_course_id', $response->id)->first();
            if ($local !== null) {
                $response->local = $local;
                $courses[] = $response;
                if ($type === 'STUDENT') {
                    $studentClass = StudentClass::where([
                        'class_id' => $local->id,
                        'account_id' => request()->user()->id,
                    ])->first();
                    if (!$studentClass) {
                        StudentClass::create([
                            'class_id' => $local->id,
                            'account_id' => request()->user()->id,
                            'status' => true,
                            'external_student_metadata' => request()->user()->external_user_profile
                        ]);
                    }else{
                        $studentClass->status = true;
                        $studentClass->save();
                    }
                } else {
                    $teacherClass = TeacherClass::where([
                            'class_id' => $local->id,
                            'account_id' => request()->user()->id]
                    )->first();
                    if (!$teacherClass) {
                        TeacherClass::create([
                            'class_id' => $local->id,
                            'account_id' => request()->user()->id,
                            'status' => true,
                            'external_teacher_metadata' => request()->user()->external_user_profile
                        ]);

                    }else{
                        $teacherClass->status = true;
                        $teacherClass->save();
                    }
                }

            } else {
                if ($type === 'ADMIN') {
                    $syncData = $this->courses->syncCourse($response);
                    $response->local = $syncData;
                    $courses[] = $response;
                }
            }
        }
        // fire an event and sync both local and google classroom data

        return successResponse($courses, null, 200);
    }

    public function saveGoogleMeetLink($id,Request $request){
        $googleMeetLink = $request->get("google_meet_link");
        $course =Course::where("external_course_id",$id)->first();
        if ($course === null){
            return response()->json([
                'status' => "error",
                'message' => "No Class found",
                'course' => null
            ],200);
        }
        $course->google_meet_link = $googleMeetLink;
        $course->save();
        $data = json_decode($course->external_course_metadata, true);
        $data['local'] = $course->fresh();
        return response()->json([
            'status' => "Success",
            'message' => "Google Meet Link Saved Successfully",
            'course' => $data
        ],200);
    }

    public function get($courseId)
    {
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);

        $response = $classroom->courses->get($courseId);
        $local = Course::where('external_course_id', $response->id)->first();
        $response->local = $local;

        return successResponse($response, null, 200);
    }

    public function create(Request $request)
    {
        $rules = [
            "name" => "required|unique:classes,name",
            "level_id" => "required",
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $messages = [
                'name' => $validator->errors()->first("name"),
                'level_id' => $validator->errors()->first("level_id"),
            ];
            return response()->json([
                'status' => 'error',
                'message' => $messages,
                'data' => ''
            ],200);
        }
        $data = $request->all();
        $courseData = $this->courses->create($data);
        $course = $courseData['course'];
        $account = request()->user();
        //create new teacher class relation
		if(is_array($account->external_user_profile)) $external_user_profile=json_encode($account->external_user_profile);
        else $external_user_profile=$account->external_user_profile;
		
        TeacherClass::create([
            'class_id' => $course->id,
            'account_id' => $account->id,
            'status' => true,
            'external_teacher_metadata' => $external_user_profile
        ]);
        return successResponse($courseData['response'], null, 201);
    }

    public function update($id, Request $request)
    {
        $data = $request->all();
        $rules = [
            "name" => "required|unique:classes,name,{$data["local"]["id"]}",
            "level_id" => "required"
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $messages = [
                'name' => $validator->errors()->first("name"),
                'level_id' => $validator->errors()->first("level_id")
            ];
            return response()->json([
                'status' => 'error',
                'message' => $messages,
                'data' => ''
            ],200);
        }
        $response = $this->courses->update($data, $id);
        return successResponse($response, null, 201);
    }

    public function patch($id, Request $request)
    {
        $data = $request->all();
        $rules = [
            "name" => "required|unique:classes,name,{$data["local"]["id"]}",
            "subject" => "required",
            "mask" => "required|in:name,section,description,Heading,room,courseState,ownerId",
            "courseState" => "sometimes|in:COURSE_STATE_UNSPECIFIED,ACTIVE,ARCHIVED,PROVISIONED,DECLINED,SUSPENDED"
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $messages = [
                'name' => $validator->errors()->first("name"),
                'subject' => $validator->errors()->first("subject"),
                'mask' => $validator->errors()->first("mask"),
                'courseState' => $validator->errors()->first("courseState"),
            ];
            return response()->json([
                'status' => 'error',
                'message' => $messages,
                'data' => ''
            ],200);
        }
        $mask = $request->mask;
        $query = [];

        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $course = setCourse($data);


        switch ($mask) {
            case 'name':
                $query['updateMask'] = 'name';
                break;
            case 'section':
                $query['updateMask'] = 'section';
                break;
            case 'description':
                $query['updateMask'] = 'description';
                break;
            case 'Heading':
                $query['updateMask'] = 'Heading';
                break;
            case 'room':
                $query['updateMask'] = 'room';
                break;
            case 'courseState':
                $query['updateMask'] = 'courseState';
                break;
            case 'ownerId':
                $query['updateMask'] = 'ownerId';
                break;

        }

        $response = $classroom->courses->patch($id, $course, $query);
        return successResponse($response, null, 200);
    }
}
