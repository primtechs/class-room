<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Accounts\AccountType;
class AccountTypesController extends Controller
{
    public function index(){
        return response()->json([
            'account_types' => AccountType::all(),
            'status' => 'success',
            'message' => ''
        ]);
    }
}
