<?php

namespace App\Http\Controllers\API;

use App\Events\CheckReward;
use App\Http\Controllers\Controller;
use App\Models\Accounts\AccountSchool;
use App\Models\AssessmentTemplate;
use App\Models\QuestionGrade;
use App\Models\QuestionResource;
use App\Models\QuestionResponse;
use App\Models\QuestionSubject;
use App\Models\QuestionTag;
use App\Models\QuestionType;
use App\Models\Standard;
use App\Models\TeacherQuestionBank;
use App\Models\Topic;
use App\Models\FileType;
use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Requests\QuestionFormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Google_Service_Drive;
use Storage;

class QuestionsController extends Controller
{

    public function index()
    {
        $data = [];
        $where = [];
        $params = ["type_id", "standard_id", "topic_id", "question_subject_id", "question_grade_id", "status"];
        $with = ["account","topic", "question_tag", "type", "standard", "subject", "grade", "response", "resources"];
        $query = Question::with($with)->withCount("assessment_questions")
        ->orderBy("date_updated", "desc") ->orderBy('id', 'desc');
        foreach ($params as $param) {
            if (request()->has($param)) {
                $where[$param] = request()->get($param);
            }
        }
        if (request()->has("tag")) {
            if (!empty(request()->get("tag"))) {
                $tags = explode(",", request()->get("tag"));
                $question_ids = QuestionTag::whereIn("tag_name", $tags)->get()->pluck("question_id");
                $query = $query->whereIn("id", $question_ids);
            }
        }
        if (request()->has("search_text")) {
            if($search_text = request()->get("search_text")){
                $query = $query->where("title", "ILIKE", "%$search_text%")->orWhere("description", "ILIKE", "%$search_text%");
            }
        }
        //check the user type
        $user = request()->user();
        $accountType = strtolower($user->accountType->name);
        if ($accountType == "student") {
            $where['account_id'] = $user->id;
        } else if ($accountType == "teachers") {
            $school_id = AccountSchool::where("account_id", $user->id)->first()->school_id;
            $accounts = AccountSchool::where("school_id", $school_id)->get()->pluck("account_id")->toArray();
            $query->whereIn("account_id", $accounts);
        } else if ($accountType == "admin") {
            $query = $query->whereIn("status", ["Public", "Send To Admin"]);
        }
        $limit = config("pagination.limit");
        $query->where("published_state", "PUBLISHED");
        // dd($query->toSql());
        $questions = $query->where($where)->paginate($limit);
        /*dd(collect($questions->items())->map(function($item){
            return [
                'first_name' => $item->account->first_name,
                'last_name' => $item->account->first_name,
            ];
        })->toArray());*/
        $paginatedQuestions = [];
        foreach($questions->items() as $index => $question){
            $questionArray = $question->toArray();
            if ($accountType == "admin" || $user->id ==$question['account_id'])
                $questionArray['hasEditPermission']	=1;
           else $questionArray['hasEditPermission']	=0;

           $questionArray['first_name'] = $question->account->first_name;
           $questionArray['last_name'] = $question->account->last_name;
           $questionArray['full_name'] = $question->account->first_name.' '.$question->account->last_name;
           $paginatedQuestions[] = $questionArray;
        }
		// condtions for updating question by user
		// foreach($questions['data'] as $index=>$question)
		// {
		// 	if ($accountType == "admin" || $user->id ==$question['account_id'])
		// 		 $questions['data'][$index]['hasEditPermission']	=1;
		// 	else $questions['data'][$index]['hasEditPermission']	=0;
		// }
		$data['pagination'] = [
            'from' => $questions->firstItem(),
            'to' => $questions->lastItem(),
            'total' => $questions->total(),
            'per_page' => $questions->perPage(),
            'current_page' => $questions->currentPage(),
            'last_page' => $questions->lastPage(),
        ];
        $data['data'] = $paginatedQuestions;
        $data['tags'] = QuestionTag::select("tag_name")->distinct()->get()->pluck("tag_name")->toArray();
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(QuestionFormRequest $request)
    {
        $client = setGoogleClient(getOAuthToken());

        $open_response = [];
        $request->whenHas('open_response', function($inputs) use(&$open_response){
            foreach($inputs as $input){
                $open_response[] = json_decode($input, true);
            }
        });
        $rubrics = [];
        $request->whenHas('rubrics', function($inputs) use(&$rubrics){
            foreach($inputs as $input){
                $rubrics[] = json_decode($input, true);
            }
        });
        $responses = [];
        $request->whenHas('responses', function($inputs) use(&$responses){
            foreach($inputs as $input){
                $responses[] = json_decode($input, true);
            }
        });
        $data = [];

        $user = request()->user();
        $accountType = strtolower($user->accountType->name);
        $data['status'] = 'success';
        $data['message'] = 'Data is saved successfully';
        $question = Question::create([
            'type_id' => $request->get("type_id"),
            'topic_id' => $request->get("topic_id"),
            'title' => $request->get("title"),
            'description' => $request->get("description"),
            'standard_id' => $request->get("standard_id"),
            'question_subject_id' => $request->get("question_subject_id"),
            'question_grade_id' => $request->get("question_grade_id"),
            'status' =>  $request->get("status", "Approve ?"),
            'total_approval' => 0,
            'account_id' => $user->id,
            'average_rating' => 0.0,
            'rubrics' => $rubrics,
            'response_rubric' => $responses,
            'open_response' => $open_response,
            'open_response_grade' => $request->get("open_response_grade", 0),
            'published_state' => $request->get("published_state", "PUBLISHED"),
            'date_added' => date("Y-m-d H:i:s"),
            'date_updted' => date("Y-m-d H:i:s"),
        ]);
        $tag = $request->get("tag", "");
        if (!empty($tag)) {
            QuestionTag::updateOrCreate(
                ['question_id' => $question->id, 'tag_name' => $tag],
                ['question_id' => $question->id, 'tag_name' => $tag]
            );
        }
        if ($accountType == "teachers" || $accountType == "admin") {
            TeacherQuestionBank::updateOrCreate(
                ['account_id' => $user->id, "question_id" => $question->id],
                ['account_id' => $user->id, "question_id" => $question->id]
            );
            $total_approval = TeacherQuestionBank::where("question_id", $question->id)->count();
            $question->total_approval = $total_approval;
            $question->save();
        }
        foreach ($responses as $response) {
            if (!empty($response['title'])) {
                QuestionResponse::create([
                    'question_id' => $question->id,
                    "description" => $response["description"],
                    "is_valid" => $response["is_valid"],
                    "rubric" => $response["rubric"],
                    "sub_grade" => $response["sub_grade"],
                    "last_udate" => date("Y-m-d H:i:s"),
                    "title" => $response['title'],
                    "match_label" => $response['match_label'],
                ]);
            }

        }
        $drive = new Google_Service_Drive($client);
        $files = $drive->files;
        $google_drive_links = $request->get("google_drive_link", []);
        foreach ($google_drive_links as $index => $linkText) {
            $link = json_decode($linkText, true);
            if($link['type'] !== 'aws'){
                $type = NULL;
                $awsUrl = NULL;
                $fileType = FileType::where('file_type', $link['mimeType'])->first();
                if($fileType){
                    $type = $fileType->fclass;
                }else{
                    Log::warning("Question create warning {$link['mimeType']} Not found");
                }
                if($fileType){
                    if(isFromGoogleDocs($link['url'])){
                        $file = $files->export($link['id'],"application/vnd.openxmlformats-officedocument.wordprocessingml.document" , ['alt' => 'media']);
                    }else if (isset($link['file'])) {
                        $file = $request->file("google_drive_link.$index.file");

                    }else{
                        $file = $files->get($link['id'], ['alt' => 'media']);
                    }
                    $path = "documents";
                    if ($fileType->fclass === "video" || $fileType->fclass === "audio") {
                        $path = "videos";
                    }else if($fileType->fclass === "image"){
                        $path = "images";
                    }
                    $name = explode(".", $link['name'])[0];
                    $fileName = "{$question->id}_{$name}{$fileType->extension}";
                    Storage::disk('s3')->put("$path/$fileName", $file->getBody()->getContents());
                    $awsUrl = env('AWS_LOCATION')."/$path/$fileName";
                }
                QuestionResource::create([
                    'name' => $link['name'],
                    'res_url' => $awsUrl,
                    'type' => $type,
                    'description' => $link['description'],
                    'question_id' => $question->id
                ]);
            }
        }
        $files = $request->file("files", []);
        foreach($files as $file){
            $mimeType = $file->getMimeType();
            $type = NULL;
            $awsUrl = NULL;
            $fileType = FileType::where('file_type', $mimeType)->first();
            if($fileType){
                $type = $fileType->fclass;
                $path = "documents";
                if ($fileType->fclass === "video" || $fileType->fclass === "audio") {
                    $path = "videos";
                }else if($fileType->fclass === "image"){
                    $path = "images";
                }
                $fileName = $file->store($path,['disk' => 's3']);
                $awsUrl = env('AWS_LOCATION')."/$fileName";
                QuestionResource::create([
                    'name' => $fileName,
                    'res_url' => $awsUrl,
                    'type' => $type,
                    'description' => NULL,
                    'question_id' => $question->id
                ]);
            }else{
                Log::warning("Question create warning {$link['mimeType']} Not found");
            }
        }
        $data['data'] = $question->fresh();
        event(new CheckReward('itemCreate', ['reason' => 'items_created'], request()->user()));
        return response()->json($data, 201);
    }

    public function show($id)
    {
        $data = [];

        $school = Question::find($id);
        if ($school != NULL) {
            $data['status'] = 'success';
            $data['data'] = $school;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Questions are Not Found';
        }
        return response()->json($data, 200);
    }

    public function update(QuestionFormRequest $request, $id)
    {
        $client = setGoogleClient(getOAuthToken());

        $open_response = [];
        $request->whenHas('open_response', function($inputs) use(&$open_response){
            foreach($inputs as $input){
                $open_response[] = json_decode($input, true);
            }
        });
        $rubrics = [];
        $request->whenHas('rubrics', function($inputs) use(&$rubrics){
            foreach($inputs as $input){
                $rubrics[] = json_decode($input, true);
            }
        });
        $responses = [];
        $request->whenHas('responses', function($inputs) use(&$responses){
            foreach($inputs as $input){
                $responses[] = json_decode($input, true);
            }
        });
        $data = [];

        $question = Question::find($id);
        $user = request()->user();
        $accountType = strtolower($user->accountType->name);
        if ($question != NULL) {
                $question->type_id = $request->get("type_id");
                $question->topic_id = $request->get("topic_id");
                $question->standard_id = $request->get("standard_id");
                $question->title = $request->get("title");
                $question->description = $request->get("description");
                $question->question_subject_id = $request->get("question_subject_id");
                $question->question_grade_id = $request->get("question_grade_id");
                $question->rubrics = $rubrics;
                $question->response_rubric = $responses;
                $question->open_response = $open_response;
                $question->open_response_grade = $request->get("open_response_grade", 0);
                $question->published_state = $request->get("published_state", "PUBLISHED");
                $question->date_updated = date("Y-m-d H:i:s");
                if ($accountType == "admin") {
                    $question->status = $request->get("status", "Approve ?");
                }
                $question->save();
                $tag = $request->get("tag", "");
                if (!empty($tag)) {
                    QuestionTag::where("question_id", $question->id)->delete();
                    QuestionTag::updateOrCreate(
                        ['question_id' => $question->id, 'tag_name' => $tag],
                        ['question_id' => $question->id, 'tag_name' => $tag]
                    );
                }
                if ($accountType == "teachers") {
                    if ($request->get("teacher_status") == 1) {
                        TeacherQuestionBank::updateOrCreate(
                            ['account_id' => $user->id, "question_id" => $question->id],
                            ['account_id' => $user->id, "question_id" => $question->id]
                        );
                        event(new CheckReward('itemCreate', ['reason' => 'teacher_likes_all_item'], request()->user()));
                    } else {
                        TeacherQuestionBank::where(['account_id' => $user->id, "question_id" => $question->id])->delete();
                    }
                    $question = $question->fresh();
                    $total_approval = TeacherQuestionBank::where("question_id", $question->id)->count();
                    $question->total_approval = $total_approval;
                    $question->save();
                }
                QuestionResponse::where("question_id", $question->id)->delete();
                foreach ($responses as $response) {
                    if(!empty($response['title'])){
                        QuestionResponse::create([
                            'question_id' => $question->id,
                            "description" => $response["description"],
                            "is_valid" => $response["is_valid"],
                            "rubric" => $response["rubric"],
                            "sub_grade" => $response["sub_grade"],
                            "last_udate" => date("Y-m-d H:i:s"),
                            "title" => $response['title'],
                            "match_label" => $response['match_label'],
                        ]);
                    }
                }
                $drive = new Google_Service_Drive($client);
                $files = $drive->files;
                //clear previous AWS files
                $resources = QuestionResource::where("question_id", $question->id)->get();
                foreach($resources as $resource){
                    $awsLocation = env("AWS_LOCATION");
                    $awsFile = str_replace("$awsLocation/", "", $resource['aws_url']);
                    Storage::disk('s3')->delete($awsFile);
                }
                QuestionResource::where("question_id", $question->id)->delete();
                $google_drive_links = $request->get("google_drive_link", []);
                foreach ($google_drive_links as $linkText) {
                    $link = json_decode($linkText, true);
                    if($link['type'] !== 'aws'){
                        $type = NULL;
                        $awsUrl = NULL;
                        if(!isset($link['mimeType'])){
                            $type = $link['type'];
                            $awsUrl = $link['res_url'];
                        }else if(isset($link['mimeType'])){
                            $fileType = FileType::where('file_type', $link['mimeType'])->first();
                            if($fileType){
                                $type = $fileType->fclass;
                            }else{
                                Log::warning("Question create warning {$link['mimeType']} Not found");
                            }
                            if($fileType){
                                if(isFromGoogleDocs($link['url'])){
                                    $file = $files->export($link['id'],"application/vnd.openxmlformats-officedocument.wordprocessingml.document" , ['alt' => 'media']);
                                }else{
                                    $file = $files->get($link['id'], ['alt' => 'media']);
                                }
                                $path = "documents";
                                if ($fileType->fclass === "video" || $fileType->fclass === "audio") {
                                    $path = "videos";
                                }else if($fileType->fclass === "image"){
                                    $path = "images";
                                }
                                $name = explode(".", $link['name'])[0];
                                $fileName = "{$question->id}_{$name}{$fileType->extension}";
                                Storage::disk('s3')->put("$path/$fileName", $file->getBody()->getContents());
                                $awsUrl = env('AWS_LOCATION')."/$path/$fileName";
                            }
                        }
                        QuestionResource::create([
                            'name' => $link['name'],
                            'res_url' => $awsUrl,
                            'type' => $type,
                            'description' => $link['description'],
                            'question_id' => $question->id
                        ]);
                    }
                }
                $files = $request->file("files", []);
                foreach($files as $file){
                    $mimeType = $file->getMimeType();
                    $type = NULL;
                    $awsUrl = NULL;
                    $fileType = FileType::where('file_type', $mimeType)->first();
                    if($fileType){
                        $type = $fileType->fclass;
                        $path = "documents";
                        if ($fileType->fclass === "video" || $fileType->fclass === "audio") {
                            $path = "videos";
                        }else if($fileType->fclass === "image"){
                            $path = "images";
                        }
                        $fileName = $file->store($path,['disk' => 's3']);
                        $awsUrl = env('AWS_LOCATION')."/$fileName";
                        QuestionResource::create([
                            'name' => $fileName,
                            'res_url' => $awsUrl,
                            'type' => $type,
                            'description' => NULL,
                            'question_id' => $question->id
                        ]);
                    }else{
                        Log::warning("Question create warning {$link['mimeType']} Not found");
                    }
                }
                $data['status'] = 'success';
                $data['data'] = $question->fresh();
                $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Data is Not Found';
        }
        return response()->json($data, 200);
    }

    public function destroy($id)
    {
        $question = Question::find($id);
        if ($question != NULL) {
            $resources = QuestionResource::where("question_id", $question->id)->get();
            foreach($resources as $resource){
                $awsLocation = env("AWS_LOCATION");
                $awsFile = str_replace("$awsLocation/", "", $resource['res_url']);
                Storage::disk('s3')->delete($awsFile);
            }
            QuestionTag::where('question_id', $question->id)->delete();
            TeacherQuestionBank::where('question_id', $question->id)->delete();
            QuestionResponse::where("question_id", $question->id)->delete();
            QuestionResource::where("question_id", $question->id)->delete();
            $question->delete();
            $data['status'] = 'success';
            $data['data'] = '';
            $data['message'] = 'Data Deleted Successfully';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Not Found';
        }
        return response()->json($data, 200);
    }

}
