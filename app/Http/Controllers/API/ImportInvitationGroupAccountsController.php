<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Accounts\Account;
use App\Models\InvitationGroup;
use App\Models\InvitationGroupAccount;
use App\Rules\ImportGroupAccountFormatValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Rap2hpoutre\FastExcel\FastExcel;

class ImportInvitationGroupAccountsController extends Controller
{
    public function import(Request $request){
        $invitation_group_id = $request->get("invitation_group_id");
        $file = $request->file("file");
        $lines = (new FastExcel())->import($file, function ($line) {
            return $line;
        });
        $rules = [
            'file' => [
                "mimetypes:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                new ImportGroupAccountFormatValidator($lines->toArray())
            ],
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => [
                    'file' => $validator->errors()->first("file")
                ]
            ], 200);
        }

        $data = [
            'status' => 'success',
            'accounts' => [],
            'errors' => [],
            'groups' => []
        ];

        $importResponses = [];
        foreach($lines as $line){
            $importResponses[] = $this->importToDtabase($line, $invitation_group_id);
        }

        foreach($importResponses as $response){
            if($response['status'] === "success"){
                $data['accounts'][] = $response['account'];
            }else{
                $data['errors'][] = $response['message'];
            }
            //list out the newly created groups
            if($response['group'] !== NULL ){
                $data['groups'][] = $response['group'];
            }
        }
        return response()->json($data, 200);
    }

    private function importToDtabase(array $account, $invitation_group_id)
    {
        $email = $account["Email"];
        $groupName = $account["GroupName"];
        $account = Account::where("email", $email)->first();
        $group = InvitationGroup::nameIgnoreCase($groupName)->first();

        $new_group = NULL;
        if($account === NULL){
            return [
                'status' => 'error',
                'message' => "Invalid account for $email"
            ];
        }
        if($group === NULL){
            $group = InvitationGroup::create([
                'name' => $groupName
            ]);
            $new_group = $group->toArray();
        }
        else  $new_group = $group->toArray();
        InvitationGroupAccount::updateOrCreate([
            'invitation_group_id' => $group->id,
            'account_id' => $account->id
        ], [
            'invitation_group_id' => $group->id,
            'account_id' => $account->id
        ]);

        return [
            'status' => 'success',
            'message' => "Imported Successfully $email",
            'account' => $account->toArray(),
            'group' => $new_group
        ];
    }

}
