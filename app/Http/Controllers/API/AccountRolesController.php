<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\AccountRole;
use App\Models\Accounts\Account;
use App\Models\Role;
use App\Rules\AccountRoleUniqueValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AccountRolesController extends Controller
{
    public function index()
    {
        $data = [];
        $response = AccountRole::all();
        $data['data'] = $response;
        $data['accounts'] = Account::all();
        $data['roles'] = Role::all();
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'account_id' => ['required',new AccountRoleUniqueValidator()],
            'role_id' => 'required'
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "account_id" => $validator->errors()->first("account_id"),
                "role_id" => $validator->errors()->first("role_id")
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Account Role is saved successfully';
            $account_id = $request->get("account_id");
            $role_id = $request->get("role_id");
            $is_active = $request->get("is_active");
            $data['data'] = AccountRole::updateOrCreate(
                ['account_id' => $account_id],
                ['account_id' => $account_id, 'role_id' => $role_id,'is_active'=>$is_active]
            );
        }
        return response()->json($data, 201);
    }

    public function show($id)
    {
        $data = [];
        $role = AccountRole::find($id);
        if ($role != NULL) {
            $data['status'] = 'success';
            $data['data'] = $role;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Account Role are Not Found';
        }
        return response()->json($data, 200);
    }

    public function update(Request $request, AccountRole $role)
    {
        $rules = [
            'account_id' => ['required',new AccountRoleUniqueValidator($role)],
            'role_id' => 'required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "account_id" => $validator->errors()->first("account_id"),
                "role_id" => $validator->errors()->first("role_id")
            ];
            $data['data'] = '';
        } else {
            if ($role != NULL) {
                $role->account_id = $request->get("account_id");
                $role->role_id = $request->get("role_id");
                $role->is_active = $request->get("is_active");
                $role->save();
                $data['status'] = 'success';
                $data['data'] = $role->fresh();
                $data['message'] = 'Account Role Updated Successfully';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Account Role is Not Found';
            }
        }
        return response()->json($data, 200);
    }

    public function destroy(AccountRole $role)
    {
        if ($role != NULL) {
            $role->delete();
            $data['status'] = 'success';
            $data['data'] = '';
            $data['message'] = 'Account Role Deleted Successfully';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Not Found';
        }
        return response()->json($data, 200);
    }
}
