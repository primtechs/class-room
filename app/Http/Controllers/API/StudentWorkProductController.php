<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Assessment;
use App\Models\StudentWorkProductLog;
use App\Models\AccountAssessment;
use App\Repository\StudentWorkProductRepository;


class StudentWorkProductController extends Controller
{

    public function generateIndividual($id, StudentWorkProductRepository $repository){
        $account = request()->user();

        if($account->account_type_id!=2){
            return response()->json([
                'status' => 'success',
                'message' => 'Not Student',
                'student_work_product' =>array()
            ], 200);
        }

        $assessment = Assessment::find($id);

        if( $assessment->status !='finished') {
            return response()->json([
                'status' => 'success',
                'message' => 'Assessment Not finished',
                'student_work_product' =>array()
            ], 200);
        }
        if (StudentWorkProductLog::where('assessment_id', $id)->where('student_id',  $account->id)->count() > 0 || $assessment->status !='finished') {
            return response()->json([
                'status' => 'success',
                'student_work_product' =>array()
            ], 200);
         }
        $AccountAssessment = AccountAssessment::where('account_id', '=', $account->id)
            ->where('assessment_id', '=', $assessment->id)
            ->get();
        $NumberAnswered = $AccountAssessment->count();
        $totalAnswerShould=3;
        $log= StudentWorkProductLog::firstOrCreate([
            'assessment_id' => $assessment->id,
            'student_id' => $account->id
        ]);
        $pending_answer = ($totalAnswerShould-$NumberAnswered);
        StudentWorkProductLog::where('student_id', $account->id)
            ->where('assessment_id', $assessment->id)
            ->update(['pending_answer' => $pending_answer]);

        try{
            if($log->google_drive_response === NULL){
                $hasUploaded = $repository->upload($assessment, $account, $log);
                if(!$hasUploaded){
                    StudentWorkProductLog::where('student_id', $account->id)
                    ->where('assessment_id', $assessment->id)->delete();
                    return response()->json([
                        'status' => 'success',
                        'message' => 'No Work Product Found',
                        'student_work_product' => NULL
                    ], 200);

                }else {

                    if($log->student_submission_response === NULL){
                        $repository->submitResponse($assessment, $account, $log);
                    }
                    if($log->turn_in_response === NULL){
                        $repository->turnIn($assessment, $account, $log);
                    }
                }
            }

        }catch(Exception $e){
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage(),
                'student_work_product' => NULL
            ], 200);
        }
        return response()->json([
            'status' => 'success',
            'student_work_product' => $log->fresh()->toArray()
        ], 200);
    }
}
