<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Policy;
use App\Models\Resource;
use App\Models\ResourcePolicy;
use App\Rules\ResourcePolicyUniqueValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ResourcePoliciesController extends Controller
{
    public function index()
    {
        $data = [];
        $response = ResourcePolicy::all();
        $data['data'] = $response;
        $data['resources'] = Resource::all();
        $data['policies'] = Policy::all();
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'resource_id' => ['required', new ResourcePolicyUniqueValidator],
            'policy_id' => 'required',
            'is_active' => 'required'
        ];
        $messages = [
            "resource_id.required" => "Resource is required",
            "policy_id.required" => "Policy is required",
            "is_active.required" => "Status is required"
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'resource_id' => $validator->errors()->first("resource_id"),
                'policy_id' => $validator->errors()->first("policy_id"),
                'is_active' => $validator->errors()->first("is_active")
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Resource Policy is saved successfully';
            $data['data'] = ResourcePolicy::create([
                'resource_id' => $request->get("resource_id"),
                'policy_id' => $request->get("policy_id"),
                'is_active' => $request->get("is_active"),
            ]);
        }
        return response()->json($data, 201);
    }

    public function show($id)
    {
        $data = [];
        $resourcePolicy = ResourcePolicy::find($id);
        if ($resourcePolicy != NULL) {
            $data['status'] = 'success';
            $data['resource_policy'] = $resourcePolicy;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['resource_policy'] = '';
            $data['message'] = 'Resource Policy is Not Found';
        }
        return response()->json($data, 200);
    }

    public function update(Request $request, ResourcePolicy $resourcePolicy)
    {
        $rules = [
            'resource_id' => ['required', new ResourcePolicyUniqueValidator($resourcePolicy)],
            'policy_id' => 'required',
            'is_active' => 'required'
        ];
        $messages = [
            "resource_id.required" => "Resource is required",
            "policy_id.required" => "Policy is required",
            "is_active.required" => "Status is required"
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'resource_id' => $validator->errors()->first("resource_id"),
                'policy_id' => $validator->errors()->first("policy_id"),
                'is_active' => $validator->errors()->first("is_active")
            ];
            $data['resource_policy'] = '';
        } else {
            if ($resourcePolicy != NULL) {
                $resourcePolicy->resource_id = $request->get("resource_id");
                $resourcePolicy->policy_id = $request->get("policy_id");
                $resourcePolicy->is_active = $request->get("is_active");
                $resourcePolicy->save();
                $data['status'] = 'success';
                $data['data'] = $resourcePolicy->fresh();
                $data['message'] = 'Resource Policy is Successfully Updated';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Resource Policy Not Found';
            }
        }
        return response()->json($data, 200);
    }

    public function destroy(Request $request, ResourcePolicy $resourcePolicy)
    {
        if ($resourcePolicy != NULL) {
            $resourcePolicy->delete();
            $data['status'] = 'success';
            $data['resource_policy'] = '';
            $data['message'] = 'Resource Policy Deleted Successfully';
        } else {
            $data['status'] = 'error';
            $data['resource_policy'] = '';
            $data['message'] = 'Resource Policy Not Found';
        }
        return response()->json($data, 200);
    }
}
