<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\QuestionGrade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class QuestionGradesController extends Controller
{

    public function index()
    {
        $data = [];
        $response = QuestionGrade::all();
        $data['data'] = $response;
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }


    public function store(Request $request)
    {
        $rules = [
            'name' => "required|unique:question_grades,name",
            'description' => 'required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name"),
                'description' => $validator->errors()->first("description")
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Grade is saved successfully';
            $data['data'] = QuestionGrade::create([
                'name' => $request->get("name"),
                'description' => $request->get("description"),
            ]);
        }
        return response()->json($data, 201);
    }


    public function show($id)
    {
        $data = [];
        $questionGrade = QuestionGrade::find($id);
        if ($questionGrade != NULL) {
            $data['status'] = 'success';
            $data['subject'] = $questionGrade;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['subject'] = '';
            $data['message'] = 'Grade is Not Found';
        }
        return response()->json($data, 200);
    }


    public function update(Request $request, $id)
    {
        $rules = [
            'name' => "required|unique:question_grades,name,$id,question_grade_id",
            'description' => 'required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name"),
                'description' => $validator->errors()->first("description")
            ];
        } else {
            $questionGrade = QuestionGrade::find($id);
            if ($questionGrade != NULL) {
                $name = $request->get("name");
                $description = $request->get("description");
                $questionGrade->name = $name;
                $questionGrade->description = $description;
                $questionGrade->save();
                $data['status'] = 'success';
                $data['data'] = $questionGrade->fresh();
                $data['message'] = 'Grade Updated Successfully';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Grade Not Found';
            }
        }
        return response()->json($data, 200);
    }


    public function destroy(Request $request, $id)
    {
        $questionGrade = QuestionGrade::find($id);
        if ($questionGrade != NULL) {
            try {
                $questionGrade->delete();
                $data['status'] = 'success';
                $data['subject'] = '';
                $data['message'] = 'Grade Deleted Successfully';
            }catch (\Exception $exception){
                $data['status'] = 'error';
                $data['subject'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['subject'] = '';
            $data['message'] = 'Grade Not Found';
        }
        return response()->json($data, 200);
    }
}
