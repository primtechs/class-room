<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StudentProfile;

class StudentProfileController extends Controller
{

    public function index()
    {
        $data = [];
        $response = StudentProfile::all();
        $data['data'] = $response;
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $rules = [
           // 'name' => 'required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = $validator->errors();
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Data is saved successfully';
            $data['data'] = StudentProfile::create($request->all());
        }
        return response()->json($data, 201);
    }

    public function show($id)
    {
        $data = [];

        $profile = StudentProfile::find($id);
        if ($profile != NULL) {
            $data['status'] = 'success';
            $data['data'] = $profile;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Not Found';
        }
        return response()->json($data, 200);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $rules = [
           // 'name' => 'required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = $validator->errors();
            $data['data'] = '';
        } else {
            $profile = StudentProfile::find($id);
            if ($profile != NULL) {
/*                 $standard->name = $request->get("name");
                $standard->save(); */
				$profile->update($request->all());
                $data['status'] = 'success';
                $data['data'] = $profile->fresh();
                $data['message'] = '';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Not Found';
            }
        }
        return response()->json($data, 200);
    }

    public function destroy(Request $request, StudentProfile $profile)
    {
        $profile = StudentProfile::find($id);
        if ($profile != NULL) {
            //$standard->name = $request->get("name");
            $profile->delete();
            $data['status'] = 'success';
            $data['data'] = '';
            $data['message'] = 'Data Deleted Successfully';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Not Found';
        }
    }
}
