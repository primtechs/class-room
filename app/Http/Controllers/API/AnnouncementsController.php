<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\AnnouncementRequest;
use App\Repository\AnnouncementRepository;
use Google_Service_Classroom;
use App\Models\StudentClass;
use App\Models\TeacherClass;
use App\Models\Announcement;
use App\Models\Course;

class AnnouncementsController extends Controller
{
    protected $announcement;

    public function __construct(AnnouncementRepository $announcement)
    {
        $this->announcement = $announcement;
    }

    public function index()
    {

        $user = request()->user();
        $type = $user->accountType->name;
        $classes = [];
        if (strtoupper($type) === 'STUDENT') {
            $classes = StudentClass::where(["account_id" => $user->id, 'status' => true])->get()->pluck("class_id");
        }else{
            $classes = TeacherClass::where(["account_id" => $user->id, 'status' => true])->get()->pluck("class_id");
        }

        $query = Announcement::whereIn("class_id",$classes)->orderBy("id", "DESC");

        request()->whenFilled('limit', function($input) use(&$query){
            $query = $query->limit($input);
        });

        $announcements = $query->get();
        $data = [];
        foreach($announcements as $announcement){
            $data[] = $announcement->announcement_metadata; 
        }
        return successResponse($data, null,201);
    }

    public function store(AnnouncementRequest $request) {
        $data = $request->all();
        $courses = $data['courses'];
        $announcements = [];
        foreach($courses as $course){
            $data['courseId'] = $course;
            $announcements[] = $this->announcement->create($data);
        }

        return successResponse($announcements, null,201);
    }

    public function destroy($id){
        $response = $this->announcement->delete($id);
        return response()->json($response, 200);
    }


}
