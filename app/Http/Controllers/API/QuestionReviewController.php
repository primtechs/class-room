<?php

namespace App\Http\Controllers\API;

use App\Events\CheckReward;
use App\Http\Controllers\Controller;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionReviewController extends Controller
{
    public function save($question_id, Request $request)
    {
        $question = Question::find($question_id);
        $user = auth()->user();
        $reviews = [];
        if ($question->reviews != null) {
            $reviews = json_decode($question->reviews,true);
        }
        if (count($reviews) == 0) {
            $reviews[] = [
                'account_id' => $user->id,
                'rating' => $request->get("user_rating")
            ];
        }
        $sum = 0;
        $found = 0;

        foreach ($reviews as $index => $review) {
            if ($review['account_id'] == $user->id) {
                $sum += floatval($request->get("user_rating"));
                $reviews[$index]['rating'] = $request->get("user_rating");
                $found = 1;
            } else {
                $sum += floatval($review['rating']);
            }
        }
        if ($found == 0) {
            $reviews[] = [
                'account_id' => $user->id,
                'rating' => $request->get("user_rating")
            ];
            $sum += floatval($request->get("user_rating"));
        }
        $question->average_rating = ($sum / count($reviews));
        $question->reviews = json_encode($reviews);
        $question->save();
        $data['status'] = 'success';
        $data['question'] = $question->fresh();
        $data['message'] = 'Review saved successfully';
        event(new CheckReward('itemCreate', ['reason' => 'comments_received_item_created'], request()->user()));
        return response()->json($data, 200);
    }
}
