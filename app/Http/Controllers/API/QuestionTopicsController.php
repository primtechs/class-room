<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Topic;

class QuestionTopicsController extends Controller
{
    public function topics()
    {
        $topics = Topic::all();
        return response()->json(['status' => 'success', 'topics' => $topics],200);
    }
}
