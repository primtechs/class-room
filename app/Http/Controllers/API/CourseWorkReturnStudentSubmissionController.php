<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repository\StudentWorkProductRepository;
use App\Models\CourseWorkReturnMessage;
use App\Models\Coursework;

class CourseWorkReturnStudentSubmissionController extends Controller
{
    public function returnToStudent($courseId, $courseWorkId, $id, StudentWorkProductRepository $repository){
        $account = request()->user();
        $message = request()->get("message");
        $courseWork = Coursework::where("external_course_work_id", $courseWorkId)->first();
        $returnMessage = CourseWorkReturnMessage::create([
            'account_id' => $account->id,
            'course_work_id' => $courseWork->id,
            'message' => $message,
            'created_at' => date("Y-m-d H:i:s")
        ]);

        $response = $repository->returnToStudent($courseId, $courseWorkId, $id);
        return response()->json([
            'status' => 'success',
            'message' => 'Returned Successfully',
            'response' => $response,
            'return_message' => $returnMessage
        ], 200);
    }
}
