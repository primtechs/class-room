<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Rap2hpoutre\FastExcel\FastExcel;
use Rap2hpoutre\FastExcel\SheetCollection;
use Illuminate\Http\Request;
use App\Models\Question;

class QuestionExportController extends Controller
{

    public function question(Request $request){
        $id = $request->get("id");
        $question = Question::find($id);
        $responses = $question->response;
        $resources = $question->resources;
        $open_responses = $question->open_response;
        $data = [
            'Question' => [],
            'Resources' => [],
            'Responses' => [],
            'Open Responses' => []
        ];

        $data['Question'][] = [
            'id' => $question->id,
            'type' => $question->type->name,
            'title' => $question->title,
            'description' => $question->description,
            'topic' => $question->topic->name,
            'standard' => $question->standard->name,
            'question_subject' => $question->subject->name,
            'question_grade' => $question->grade->name,
            'status' => $question->status,
            'published_state' => $question->published_state
        ];

        foreach($resources as $resource){
            $data['Resources'][] = [
                'question_id' => $question->id,
                'name' => $resource->name,
                'description' => $resource->description,
                'url' => $resource->url,
                'type' => $resource->type,
            ];
        }
        foreach($responses as $response){
            $data['Responses'][] = [
                "question_id" => $question->id,
                "description" => $response->description,
                "is_valid" => $response->is_valid,
                "sub_grade" => $response->sub_grade,
                "title" => $response->title,
                "match_label" => $response->match_label
            ];
        }

        foreach($open_responses as $open_response){
            $rubrics = $open_response['rubrics'] ?? [];
            foreach($rubrics as $rubric){
                $data['Open Responses'][] = [
                    "question_id" => $question->id,
                    "criterion_name" => $open_response["criterion_name"],
                    "criterion_description" => $open_response["criterion_description"] ?? "",
                    "point" => $rubric["point"],
                    "level" => $rubric["level"],
                    "description" => $rubric["description"] ?? ""
                ];
            }
        }

        $fileName = $question->id . "_file_".date("Ymdhis").".xlsx";
        return (new FastExcel(new SheetCollection($data)))->download($fileName);
    }

}
