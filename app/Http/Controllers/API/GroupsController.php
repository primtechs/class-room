<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Group;
use Illuminate\Support\Facades\Validator;

class GroupsController extends Controller
{

    public function index()
    {
        $data = [];
        $response = Group::all();
        $data['data'] = $response;
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => "required|unique:groups,name",
            'color' => "required",
            'style' => "required"
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "name" => $validator->errors()->first("name"),
                "color" => $validator->errors()->first("color"),
                "style" => $validator->errors()->first("style")
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Data is saved successfully';
            $data['data'] = Group::create([
                'name' => $request->get('name'),
                'is_active' => $request->get('is_active'),
                'color' => $request->get('color'),
                'style' => $request->get('style')
            ]);
        }
        return response()->json($data, 201);

    }


    public function show($id)
    {
        $data = [];
        $groups = Group::find($id);
        if ($groups != NULL) {
            $data['status'] = 'success';
            $data['data'] = $groups;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Group are Not Found';
        }
        return response()->json($data, 200);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => "required|unique:groups,name,$id",
            'color' => "required",
            'style' => "required"
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "name" => $validator->errors()->first("name"),
                "color" => $validator->errors()->first("color"),
                "style" => $validator->errors()->first("style")
            ];
            $data['data'] = '';
        } else {
            $groups = Group::find($id);
            if ($groups != NULL) {
                $groups->name = $request->get('name');
                $groups->is_active = $request->get('is_active');
                $groups->color = $request->get('color');
                $groups->style = $request->get('style');
                $groups->save();
                $data['status'] = 'success';
                $data['data'] = $groups->fresh();
                $data['message'] = '';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Group Not Found';
            }
        }
        return response()->json($data, 200);
    }


    public function destroy(Request $request, $id)
    {
        $groups = Group::find($id);
        if ($groups != NULL) {
            try {
                $groups->delete();
                $data['status'] = 'success';
                $data['data'] = '';
                $data['message'] = 'Group Deleted Successfully';
            } catch (\Exception $exception) {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Group Not Found';
        }
        return response()->json($data, 200);

    }
}
