<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\InvitationGroup;
use Illuminate\Support\Facades\Validator;


class InvitationGroupsController extends Controller
{

    public function index(){
        $data = [];
        $response = InvitationGroup::all();
        $data['invitation_groups'] = $response;
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }


    public function store(Request $request)
    {
        $rules = [
            'name' => "required|unique:invitation_groups,name",
        ];

        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "name" => $validator->errors()->first("name")
            ];
            $data['invitation_group'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Data is saved successfully';
            $data['invitation_group'] = InvitationGroup::create([
                'name' => $request->get("name")
            ]);
        }
        return response()->json($data, 201);
    }


    public function show($id)
    {
        $data = [];

        $invitationGroup = InvitationGroup::with('accounts')->find($id)->toArray();
        if ($invitationGroup != NULL) {
            $data['status'] = 'success';
            $data['invitation_group'] = $invitationGroup;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['invitation_group'] = '';
            $data['message'] = 'Invitation Group is Not Found';
        }
        return response()->json($data, 200);
    }


    public function update(Request $request, $id)
    {

        $rules = [
            'name' => "required|unique:invitation_groups,name,$id",
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "name" => $validator->errors()->first("name")
            ];
            $data['invitation_group'] = '';
        } else {
            $invitationGroup = InvitationGroup::find($id);
            if ($invitationGroup != NULL) {
                $name = $request->get("name");
                $invitationGroup->name = $name;
                $invitationGroup->save();
                $data['status'] = 'success';
                $data['invitation_group'] = $invitationGroup->fresh();
                $data['message'] = 'Invitation Group Updated successfully';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Invitation Group Not Found';
            }
        }
        return response()->json($data, 200);
    }


    public function destroy(Request $request, $id)
    {
        $invitationGroup = InvitationGroup::find($id);
        if ($invitationGroup != NULL) {
            try {
                $invitationGroup->delete();
                $data['status'] = 'success';
                $data['invitation_group'] = '';
                $data['message'] = 'Invitation Group Deleted Successfully';
            } catch (\Exception $exception) {
                $data['status'] = 'error';
                $data['invitation_group'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['invitation_group'] = '';
            $data['message'] = 'Invitation Group Not Found';
        }
        return response()->json($data, 200);
    }

}
