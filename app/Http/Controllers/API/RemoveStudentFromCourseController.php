<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\StudentClass;
use App\Models\Invitations;
use App\Models\Accounts\Account;
use Google_Service_Classroom;

class RemoveStudentFromCourseController extends Controller
{
    public function removeStudent($courseId, $studentId, Request $request){
        $course = Course::find($courseId);
        $account = Account::find($studentId);
        if ($course  == NULL || $account == NULL) {
            return response()->json(['status' => 'error', 'message' => 'Invalid Request'], 200);
        }
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $students = $classroom->courses_students;
        $data = [];
        try{
            $response = $students->delete($course->external_course_id, $account->external_user_id);
            $data = [
                'status' => 'success',
                'message' => "{$account->first_name} {$account->last_name} deletd successfully",
                'student' => $response
            ];
        }catch(Exception $e){
            $data = [
                'status' => 'success',
                'message' => "Something Went Wrong"
            ];
        }
        //clear the student data
        StudentClass::where(['class_id' => $courseId,'account_id' => $studentId])->delete();
        Invitations::where(['class_id' => $courseId,'external_user_id' => $account->email])->delete();
        return response()->json($data, 200);
    }
}
