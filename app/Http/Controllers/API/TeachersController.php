<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Google_Service_Classroom;

class TeachersController extends Controller
{
    public function index()
    {
        $courseID = request()->get("course_id");
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $response = $classroom->courses_teachers->listCoursesTeachers($courseID);
        $data = [];
        $data['status'] = "success";
        $data['msg'] = "Teachers List";
        $data['teacherList'] = $response->teachers ?? [];
        return response()->json($data, 200);
    }
}
