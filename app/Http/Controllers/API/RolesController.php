<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    public function index()
    {
        $data = [];
        $response = Role::all();
        $data['data'] = $response;
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|min:2|unique:roles,name'
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "name" => $validator->errors()->first("name")
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Data is saved successfully';
            $data['data'] = Role::create([
                'name' => $request->get("name")
            ]);
        }
        return response()->json($data, 201);
	}

    public function show($id)
    {
        $data = [];
        $role = Role::find($id);
        if ($role != NULL) {
            $data['status'] = 'success';
            $data['data'] = $role;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Role are Not Found';
        }
        return response()->json($data, 200);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => "required|min:2|unique:roles,name,$id"
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "name" => $validator->errors()->first("name")
            ];
            $data['data'] = '';
        } else {
            $role = Role::find($id);
            if ($role != NULL) {
                $role->name = $request->get("name");
                $role->save();
                $data['status'] = 'success';
                $data['data'] = $role->fresh();
                $data['message'] = '';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Data is Not Found';
            }
        }
        return response()->json($data, 200);
    }

    public function destroy($id)
    {
        $roles = Role::find($id);
        if ($roles != NULL) {
            try {
                $roles->delete();
                $data['status'] = 'success';
                $data['data'] = '';
                $data['message'] = 'Data Deleted Successfully';
            } catch (\Exception $exception) {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Not Found';
        }
        return response()->json($data, 200);
    }
}
