<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\GurdianInvitation;
use App\Models\Accounts\Account;
use App\Models\Course;
use App\Models\StudentClass;
use App\Models\TeacherClass;
use App\Models\InvitationGroup;
use App\Rules\InvitationValidator;
use Google_Service_Classroom;
use App\Models\Invitations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\StreamedResponse;


class InvitationsController extends Controller
{
    public function courseInvitations($course_id)
    {
        $course = Course::where("external_course_id", $course_id)->first();
        if ($course === NULL) {
            return response()->json([
                'status' => 'error',
                'students' => [],
                'teachers' => []
            ], 200);
        }
        $students = StudentClass::with("course")->where("class_id", $course->id)->get();
        $teachers = TeacherClass::with("course")->where("class_id", $course->id)->get();
        $data = [
            'status' => 'success',
            'students' => $students,
            'teachers' => $teachers
        ];
        return response()->json($data, 200);
    }

    public function userInvitations()
    {
        $account = request()->user();
        $type = $account->accountType->name;
        $data = [
            'status' => 'success',
            'invitations' => []
        ];
        if (strtoupper($type) === 'TEACHERS') {
            $data['invitations'] = TeacherClass::with("course")->where(["account_id" => $account->id, "status" => 0])->get();
        } else {
            $data['invitations'] = StudentClass::with("course")->where(["account_id" => $account->id, "status" => 0])->get();
        }
        return response()->json($data, 200);
    }

    public function createStudent(Request $request)
    {
        $rules = [
            'userId' => ["required", new InvitationValidator(Account::$STUDENT)],
        ];
        $response = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response['status'] = 'error';
            $response['message'] = [
                "userId" => $validator->errors()->first("userId")
            ];
            $response['data'] = '';
            return response()->json($response, 200);
        } else {
            $data = $request->all();
            $client = setGoogleClient(getOAuthToken());
            $classroom = new Google_Service_Classroom($client);
            $value['external_user_id'] = $request->get('userId');
            $value['external_course_id'] = $request->get('courseId');
            $value['role'] = 'STUDENT';
            //setup the invitation for class id for local database
            $course = Course::where('external_course_id', $request->get('courseId'))->first();
            if ($course !== null) {
                $value['class_id'] = $course->id;
            }
            //setup relation with student and class for local database
            $account = Account::where("email", $request->get('userId'))->first();
            if ($account !== null && $course !== null) {
                $newInvitation = Invitations::create($value);
                $lastInsertedId = $newInvitation->id;
                try {
                    $invitationMetaData = $classroom->invitations->create(setInvitationStudent($data));
                    if(is_array($account->external_user_profile)) $external_user_profile=json_encode($account->external_user_profile);
                    else $external_user_profile=$account->external_user_profile;
                    $invitation = StudentClass::updateOrCreate(
                        ['class_id' => $course->id, 'account_id' => $account->id],
                        [
                            'class_id' => $course->id,
                            'account_id' => $account->id,
                            'status' => false,
                            'enrollment_code' => '',
                            'external_student_metadata' =>  $external_user_profile,
                            'invitation_metadata' => $invitationMetaData
                        ]
                    );
                    $response['status'] = "success";
                    $response['message'] = "{$account->first_name} {$account->last_name} has been invited successfully";
                    $response['invitation'] = $invitation->fresh()->toArray();
                    return response()->json($response, 201);
                }catch (\Exception $e){
                    Invitations::destroy($lastInsertedId);
                    $errorMessage = $e->getMessage();
                    $errorData = json_decode($errorMessage, true);

                    if ($errorData && isset($errorData['error']['message'])) {
                        $specificErrorMessage = $errorData['error']['message'];
                        $response['status'] = 'error';
                        $response['message'] = [
                            "userId" => $specificErrorMessage,
                        ];
                        $response['data'] = '';

                        return response()->json($response, 200);
                    } else {

                        $response['status'] = 'error';
                        $response['message'] = [
                            "userId" => "An error occurred, but the message couldn't be extracted.",
                        ];
                        $response['data'] = '';

                        return response()->json($response, 200);
                    }

                }
            }

        }
        return response()->json([
            'status' => 'error',
            'message' => 'Something Went Wrong',
            'invitation' => ''
        ], 200);
    }

    public function createTeacher(Request $request)
    {
        $rules = [
            'userId' => ["required", new InvitationValidator(Account::$TEACHER)],
        ];
        $response = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response['status'] = 'error';
            $response['message'] = [
                "userId" => $validator->errors()->first("userId")
            ];
            $response['data'] = '';
            return response()->json($response, 200);
        } else {
            $data = $request->all();
            $client = setGoogleClient(getOAuthToken());
            $classroom = new Google_Service_Classroom($client);
            $value['external_user_id'] = $request->get('userId');
            $value['external_course_id'] = $request->get('courseId');
            $value['role'] = 'TEACHER';
            //setup the invitation for class id for local database
            $course = Course::where('external_course_id', $request->get('courseId'))->first();
            if ($course !== null) {
                $value['class_id'] = $course->id;
            }
            //setup relation with teacher and class for local database
            $account = Account::where("email", $request->get('userId'))->first();
            if ($account !== null && $course !== null) {
                $newInvitation = Invitations::create($value);
                $lastInsertedId = $newInvitation->id;
                try {
                    $invitationMetaData = $classroom->invitations->create(setInvitationTeacher($data));
                    if(is_array($account->external_user_profile)) $external_user_profile=json_encode($account->external_user_profile);
                    else $external_user_profile=$account->external_user_profile;
                    $invitation = TeacherClass::updateOrCreate(
                        ['class_id' => $course->id, 'account_id' => $account->id],
                        [
                            'class_id' => $course->id,
                            'account_id' => $account->id,
                            'status' => false,
                            'external_teacher_metadata' => $external_user_profile,
                            'invitation_metadata' => $invitationMetaData
                        ]
                    );
                    $response['status'] = "success";
                    $response['message'] = "{$account->first_name} {$account->last_name} has been invited successfully";
                    $response['invitation'] = $invitation->fresh()->toArray();
                    return response()->json($response, 201);
                }catch (\Exception $e){
                    Invitations::destroy($lastInsertedId);
                    $errorMessage = $e->getMessage();
                    $errorData = json_decode($errorMessage, true);
                    if ($errorData && isset($errorData['error']['message'])) {
                        $specificErrorMessage = $errorData['error']['message'];
                        $response['status'] = 'error';
                        $response['message'] = [
                            "userId" => $specificErrorMessage,
                        ];
                        $response['data'] = '';

                        return response()->json($response, 200);
                    } else {

                        $response['status'] = 'error';
                        $response['message'] = [
                            "userId" => "An error occurred, but the message couldn't be extracted.",
                        ];
                        $response['data'] = '';

                        return response()->json($response, 200);
                    }
                }


            }

        }
        return response()->json([
            'status' => 'error',
            'message' => 'Something Went Wrong',
            'invitation' => ''
        ], 200);
    }

    public function acceptInvitation($id)
    {
        $account = request()->user();
        $type = $account->accountType->name;
        if (strtoupper($type) === 'TEACHERS') {
            $invitedClass = TeacherClass::find($id);
        } else {
            $invitedClass = StudentClass::find($id);
        }
        $course = Course::find($invitedClass->class_id);
        $invitation = $invitedClass->invitation_metadata;
        if(!is_array($invitedClass->invitation_metadata)){
            $invitation = json_decode($invitedClass->invitation_metadata, true);
        }
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $classroom->invitations->accept($invitation["id"]);
        $response = json_decode($course->external_course_metadata, true);
        $invitedClass->status = true;
        $invitedClass->save();
        $response['local'] = $course;
        return response()->json(['status' => "success", 'course' => $response, 'message' => 'Invitation Accepted'], 200);
    }

    public function createGurdian(GurdianInvitation $request, $studentId)
    {
        $data = $request->all();
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $invitation = setInvitationGurdian($data);
        // TODO: set to db
        $invitations = $classroom->userProfiles_guardianInvitations->create($studentId, $invitation);
        return response()->json(['data' => $invitations], 201);
    }

    public function syncCourseInvitations($courseId)
    {
        $course = Course::where("external_course_id", $courseId)->first();
        if ($course === null) {
            $data = [
                'status' => 'error',
                'students' => [],
                'teachers' => [],
                'message' => 'Course Not Found'
            ];
            return response()->json($data, 200);
        }
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $params = [
            "courseId" => $courseId
        ];
        $response = $classroom->invitations->listInvitations($params);
        $invitations = [];
        $teacherPendingInvitations = [];
        $studentPendingInvitations = [];
        if (count($response->getInvitations())) {
            foreach ($response->getInvitations() as $invitation) {
                $invitations[] = $invitation;
                $account = Account::where("external_user_id", $invitation["userId"])->first();
                if ($account !== null) {
                    if(is_array($account->external_user_profile)) $external_user_profile=json_encode($account->external_user_profile);
                    else $external_user_profile=$account->external_user_profile;
                    if ($invitation["role"] === "TEACHER") {
                        $teacherClass = TeacherClass::updateOrCreate(
                            ['class_id' => $course->id, 'account_id' => $account->id],
                            [
                                'class_id' => $course->id,
                                'account_id' => $account->id,
                                'status' => false,
                                'external_teacher_metadata' => $external_user_profile,
                                'invitation_metadata' => json_encode($invitation)

                            ]
                        );
                        $teacherPendingInvitations [] = $teacherClass->id;
                    } else if ($invitation["role"] === "STUDENT") {
                        $studentClass = StudentClass::updateOrCreate(
                            ['class_id' => $course->id, 'account_id' => $account->id],
                            [
                                'class_id' => $course->id,
                                'account_id' => $account->id,
                                'status' => false,
                                'external_student_metadata' => $external_user_profile,
                                'invitation_metadata' => json_encode($invitation)

                            ]
                        );
                        $studentPendingInvitations [] = $studentClass->id;
                    }
                }
            }
        }

        //update the status of invitation
        $teacherClassQuery = TeacherClass::where("class_id", $course->id);
        $studentClassQuery = StudentClass::where("class_id", $course->id);
        if (count($teacherPendingInvitations)) {
            $teacherClassQuery->whereNotIn("id", $teacherPendingInvitations);
        }
        if (count($studentPendingInvitations)) {
            $studentClassQuery->whereNotIn("id", $studentPendingInvitations);
        }
        $teacherClassQuery->update(["status" => true]);
        $studentClassQuery->update(["status" => true]);
        //end of update status invitation

        $students = StudentClass::where("class_id", $course->id)->get();
        $teachers = TeacherClass::where("class_id", $course->id)->get();
        $data = [
            'status' => 'success',
            'students' => $students,
            'teachers' => $teachers,
            'message' => 'Course Invitation Synced Successfully'
        ];
        return response()->json($data, 200);
    }

    public function sendGroupInvitationStudents(Request $request, $id)
    {
        $group = InvitationGroup::where("id", $id)->first();
        $courseId = $request->get("courseId");
        //check if the group exist or not
        if ($group === NULL) {
            return response()->json(['status' => 'error', 'message' => 'No Group Found'], 200);
        }
        //check if course exists or not
        $course = Course::where('external_course_id', $courseId)->first();
        if ($course === NULL) {
            return response()->json(['status' => 'error', 'message' => 'No Course Found'], 200);
        }
        $accounts = $group->accounts;
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $role = "STUDENT";
        $invitations = [];
        $errors = [];
        foreach ($accounts as $account) {
            $validator = Validator::make(['email'=>$account->email],[
                'email' => 'required|email'
            ]);
            if (!$validator->fails()) {
                $value['external_user_id'] = $account->email;
                $value['external_course_id'] = $courseId;
                $value['class_id'] = $course->id;
                $value['role'] = $role;

                $data['userId'] = $account->email;
                $data['courseId'] = $courseId;
                $data['role'] = $role;

                $isInvited = Invitations::where(['external_user_id' => $account->email, 'external_course_id' => $courseId])->count();
                if ($isInvited === 0) {
                    $newInvitation = Invitations::create($value);
                    $lastInsertedId = $newInvitation->id;
                    try {
                        $invitationMetaData = $classroom->invitations->create(setInvitationStudent($data));

                        if(is_array($account->external_user_profile)) $external_user_profile=json_encode($account->external_user_profile);
                        else $external_user_profile=$account->external_user_profile;
                        $invitation = StudentClass::updateOrCreate(
                            ['class_id' => $course->id, 'account_id' => $account->id],
                            [
                                'class_id' => $course->id,
                                'account_id' => $account->id,
                                'status' => false,
                                'enrollment_code' => '',
                                'external_student_metadata' =>$external_user_profile,
                                'invitation_metadata' => $invitationMetaData
                            ]
                        );
                        $invitation = StudentClass::where(['class_id' => $course->id, 'account_id' => $account->id])->first();
                        if(isset($invitation))
                             $invitations[] = $invitation->fresh()->toArray();
                    }catch (\Exception $e){
                        Invitations::destroy($lastInsertedId);
                        $errors[] = $e->getMessage();

                    }

                } else {

                }

            }
            else{
                $errors[] = "Unable to invite {$account->first_name} {$account->last_name}";
            }
        }
        return response()->json([
            'status' => 'success',
            'invitations' => $invitations,
            'message' => 'Invited Accounts',
            'errors' => $errors
        ],200);
    }

    public function deleteInvitation(Request $request, $id){
        $type = $request->get("type");
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        if (strtoupper($type) === 'TEACHERS') {
            $invitedClass = TeacherClass::find($id);
        } else {
            $invitedClass = StudentClass::find($id);
        }
        if($invitedClass){
			if(is_array($invitedClass->external_user_metadata))
				$profile = $invitedClass->external_user_metadata;
			else $profile = json_decode($invitedClass->external_user_metadata);
			if(!isset( $profile['emailAddress']) &&isset($profile['email']))$profile['emailAddress']=$profile['email'];

			if(is_array($invitedClass->invitation_metadata))
				$invitationMetaData = $invitedClass->invitation_metadata;
			else $invitationMetaData = json_decode($invitedClass->invitation_metadata);



			if (is_object($invitationMetaData))
    			$invitationMetaData = json_decode(json_encode($invitationMetaData), true);
			$classroom->invitations->delete($invitationMetaData['id']);

            Invitations::where(['external_user_id' => $profile['emailAddress'],'external_course_id' => $invitationMetaData['courseId']])->delete();
            $invitedClass->delete();
            return response()->json([
                'status' => 'success',
                'invitation' => null,
                'message' => 'Successfully deleted Invitation'
            ],200);
        }
        return response()->json([
            'status' => 'error',
            'invitation' => null,
            'message' => 'Something Went Wrong'
        ],200);
    }

}
