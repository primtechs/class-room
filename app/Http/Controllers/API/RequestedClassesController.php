<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RequestedClass;
use Illuminate\Support\Facades\Validator;

class RequestedClassesController extends Controller
{
    public function index()
    {
        $data = [];
        $user = request()->user();
        $query = RequestedClass::orderBy("id", "DESC");
        if ($user->role_name === "TEACHERS") {
            $query = $query->where("account_id", $user->id);
        }
        $response = $query->get();
        $data['requested_classes'] = $response;
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }


    public function store(Request $request)
    {
        $rules = [
            'name' => "required",
            'section' => "required",
            'subject' => "required",
            'room' => "required",
            'level_id' => "required",
        ];

        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "name" => $validator->errors()->first("name"),
                "section" => $validator->errors()->first("section"),
                "subject" => $validator->errors()->first("subject"),
                "room" => $validator->errors()->first("room"),
                "level_id" => $validator->errors()->first("level_id"),
            ];
            $data['requested_class'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Data is saved successfully';
            $data['requested_class'] = RequestedClass::create([
                'name' => $request->get("name"),
                'section' => $request->get("section"),
                'subject' => $request->get("subject"),
                'room' => $request->get("room"),
                'level_id' => $request->get("level_id"),
                'account_id' => request()->user()->id,
                'created_at' => date("Y-m-d", time())
            ]);
        }
        return response()->json($data, 201);
    }


    public function show($id)
    {
        $data = [];

        $requestedClass = RequestedClass::find($id);
        if ($requestedClass != NULL) {
            $data['status'] = 'success';
            $data['requested_class'] = $requestedClass;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['requested_class'] = '';
            $data['message'] = 'Requested Class is Not Found';
        }
        return response()->json($data, 200);
    }


    public function update(Request $request, $id)
    {

        $rules = [
            'name' => "required",
            'section' => "required",
            'subject' => "required",
            'room' => "required",
            'level_id' => "required",
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "name" => $validator->errors()->first("name"),
                "section" => $validator->errors()->first("section"),
                "subject" => $validator->errors()->first("subject"),
                "room" => $validator->errors()->first("room"),
                "level_id" => $validator->errors()->first("level_id"),
            ];
            $data['requested_class'] = '';
        } else {
            $requestedClass = RequestedClass::find($id);
            if ($requestedClass != NULL) {
                $requestedClass->name = $request->get("name");
                $requestedClass->section = $request->get("section");
                $requestedClass->subject = $request->get("subject");
                $requestedClass->room = $request->get("room");
                $requestedClass->level_id = $request->get("level_id");
                $requestedClass->save();
                $data['status'] = 'success';
                $data['requested_class'] = $requestedClass->fresh();
                $data['message'] = '';
            } else {
                $data['status'] = 'error';
                $data['requested_class'] = '';
                $data['message'] = 'Requested Class Not Found';
            }
        }
        return response()->json($data, 200);
    }


    public function destroy(Request $request, $id)
    {
        $requestedClass = RequestedClass::find($id);
        $requestedClass->delete();
        $data['status'] = 'success';
        $data['requested_class'] = '';
        $data['message'] = 'Requested Class Deleted Successfully';
        return response()->json($data, 200);
    }
}
