<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Level;
use Illuminate\Support\Facades\Validator;

class LevelsController extends Controller
{

    public function index()
    {
        $data = [];
        $response = Level::all();
        $data['data'] = $response;
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }


    public function store(Request $request)
    {
        $rules = [
            'name' => "required|unique:levels,name",
        ];

        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "name" => $validator->errors()->first("name")
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Data is saved successfully';
            $data['data'] = Level::create([
                'name' => $request->get("name")
            ]);
        }
        return response()->json($data, 201);
    }


    public function show($id)
    {
        $data = [];

        $level = Level::find($id);
        if ($level != NULL) {
            $data['status'] = 'success';
            $data['level'] = $level;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['level'] = '';
            $data['message'] = 'Level is Not Found';
        }
        return response()->json($data, 200);
    }


    public function update(Request $request, $id)
    {

        $rules = [
            'name' => "required|unique:levels,name,$id",
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "name" => $validator->errors()->first("name")
            ];
            $data['level'] = '';
        } else {
            $level = Level::find($id);
            if ($level != NULL) {
                $name = $request->get("name");
                $level->name = $name;
                $level->save();
                $data['status'] = 'success';
                $data['data'] = $level->fresh();
                $data['message'] = '';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Level Not Found';
            }
        }
        return response()->json($data, 200);
    }


    public function destroy(Request $request, $id)
    {
        $level = Level::find($id);
        if ($level != NULL) {
            try {
                $level->delete();
                $data['status'] = 'success';
                $data['level'] = '';
                $data['message'] = 'Level Deleted Successfully';
            } catch (\Exception $exception) {
                $data['status'] = 'error';
                $data['level'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['level'] = '';
            $data['message'] = 'Level Not Found';
        }
        return response()->json($data, 200);
    }
}
