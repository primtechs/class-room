<?php

namespace App\Http\Controllers\API;

use App\Events\CheckReward;
use App\Http\Controllers\Controller;
use App\Http\Requests\AssessmentRequest;
use App\Models\AccountAssessment;
use App\Models\Assessment;
use App\Models\AssessmentLog;
use App\Models\AssessmentQuestion;
use App\Models\AssessmentResource;
use App\Models\AssessmentResponse;
use App\Models\AssessmentSchedule;
use App\Models\AssessmentPeriod;
use App\Models\AssessmentQuestionAttempt;
use App\Models\AssessmentQuestionResult;
use App\Models\ClassSession;
use App\Models\Course;
use App\Models\CourseWorkFromAssessment;
use App\Models\FileType;
use App\Models\QuestionResource;
use App\Models\StudentWorkProductLog;
use App\Repository\AssessmentQuestionRepository;
use App\Repository\AssessmentResourceRepository;
use App\Repository\AssessmentResponseRepository;
use App\Repository\CourseworkRepository;
use App\Repository\QuestionRepository;
use App\Repository\QuestionTypeRepository;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Repository\StudentWorkProductRepository;
use \App\Models\Coursework as LocalCourseWork;

class AssessmentController extends Controller
{
    protected $courseWork;
    protected $assessmentResource;
    protected $assessmentResponse;
    protected $assessmentQuestion;
    protected $questionType;
    protected $question;
    public static $courseWorkType = 'ASSIGNMENT';
    public static $courseWorkState = 'PUBLISHED';

    public function __construct(CourseworkRepository $courseWork,
                                AssessmentResponseRepository $assessmentResponse,
                                AssessmentQuestionRepository $assessmentQuestion,
                                AssessmentResourceRepository $assessmentResource,
                                QuestionTypeRepository $questionType,
                                QuestionRepository $question)
    {
        $this->courseWork = $courseWork;
        $this->assessmentResource = $assessmentResource;
        $this->assessmentResponse = $assessmentResponse;
        $this->assessmentQuestion = $assessmentQuestion;
        $this->questionType = $questionType;
        $this->question = $question;
    }

    public function index()
    {
        $limit = config("pagination.limit");
        $query = Assessment::orderBy("id", "DESC")->with(["assessment_questions", "assessment_period"]);
        request()->whenFilled('search_text', function ($input) use(&$query){
            $query = $query->where("title", "ILIKE", "%$input%");
        });
        request()->whenFilled('standard_id', function ($input) use(&$query){
            $query = $query->where("standard_id", $input);
        });
        request()->whenFilled('class_id', function ($input) use(&$query){
            $query = $query->where("class_id", $input);
        });
        request()->whenFilled('grade_id', function ($input) use(&$query){
            $query = $query->where("grade_id", $input);
        });
        request()->whenFilled('schedule_date', function ($input) use(&$query){
            $assessmentIds = AssessmentSchedule::date($input)->get()->pluck("assessment_id")->toArray();
            $query = $query->whereIn("id", $assessmentIds);
        });
        request()->whenFilled('is_archived' ,function($input) use(&$query){
            $query = $query->where("is_archived", $input);
        });
        $paginatedData = $query->paginate($limit)->toArray();
        $assessments = $paginatedData['data'];
		$user = request()->user();
        $accountType = strtolower($user->accountType->name);
        foreach ($assessments as $index => $assessment) {
            $questions = $assessments[$index]["questions"];
            foreach ($questions as $questionIndex => $question) {
                if ($question['open_response'] === null) {
                    $assessments[$index]['questions'][$questionIndex]['open_response'] = [];
                }
                if ($question['open_response_grade'] === null) {
                    $assessments[$index]['questions'][$questionIndex]['open_response_grade'] = 0;
                }
                if ($question['rubrics'] === null) {
                    $assessments[$index]['questions'][$questionIndex]['rubrics'] = [];
                }
                if ($question['response_rubric'] === null) {
                    $assessments[$index]['questions'][$questionIndex]['response_rubric'] = [];
                }
            }
			/*if (($accountType == "admin" || $user->id ==$assessment['account_id']) && $assessment['status']=='Active')
				$assessments[$index]['hasEditPermission']= 1;
			else
				$assessments[$index]['hasEditPermission']= 0;*/
        }
        $response = [
            'status' => 'Success',
            'message' => 'Assessments List',
            'pagination' => [
                'from' => $paginatedData['from'],
                'to' => $paginatedData['to'],
                'total' => $paginatedData['total'],
                'per_page' => $paginatedData['per_page'],
                'current_page' => $paginatedData['current_page'],
                'last_page' => $paginatedData['last_page'],
            ],
            'data' => $assessments
        ];
        return response()->json($response, 200);
    }

    public function show($id)
    {
        $assessment = Assessment::findOrFail($id);
        return successResponse($assessment, null, 200);
    }

    public function questionList()
    {
        ini_set('max_execution_time', 3600);
        ini_set('memory_limit', '2048M');

        $assessmentQuestions = AssessmentQuestion::all();

        foreach ($assessmentQuestions as $assessmentQuestion) {
            // resources
            $assessmentResources = AssessmentResource::where('question_id', $assessmentQuestion->id)->get();

            // responses
            $assessmentResponses = AssessmentResponse::where('assessment_question_id', $assessmentQuestion->id)->get();

            $assessmentQuestion->resources = $assessmentResources;
            $assessmentQuestion->responses = $assessmentQuestion->rubric_response;
        }

        return successResponse($assessmentQuestions, null, 200);
    }

    public function getQuestion($id)
    {
        $assessmentQuestion = AssessmentQuestion::findOrFail($id);

//        foreach ($assessmentQuestions as $assessmentQuestion) {
        $assessmentResources = AssessmentResource::where('question_id', $assessmentQuestion->id)->get();
        $assessmentResponses = AssessmentResponse::where('assessment_question_id', $assessmentQuestion->id)->get();

        $assessmentQuestion->resources = $assessmentResources;
        $assessmentQuestion->responses = $assessmentResponses;
//        }

        return successResponse($assessmentQuestion, null, 200);
    }

    public function store(AssessmentRequest $request)
    {
        $data = $request->getData();
        $courseData = [];
        $course = Course::findOrFail($data['class_id']);
        $courseData['courseId'] = $course['external_course_id'];
        $courseData['title'] = $data['title'];
        $courseData['description'] = $data['description'];
        $courseData['workType'] = self::$courseWorkType;
        $courseData['state'] = self::$courseWorkState;
        if(isset($data['max_point']) && (int)$data['max_point']>0 ){
            $courseData['maxPoints'] = $data['max_point'];
        }
        else  $courseData['maxPoints'] = 100;

        $courseData['response_rubric'] = isset($data['response_rubric']) ? $data['response_rubric'] : null;
        $courseWork = $this->courseWork->create($courseData);
        $assessment = Assessment::create($data);
        CourseWorkFromAssessment::create([
            'course_work_id' => $courseWork->local->id,
            'assessment_id' => $assessment->id
        ]);
        foreach ($data['questions'] as $question) {
            if (!empty($question['question_id']) || $question['question_id'] !== null) {
                event(new CheckReward('itemCreate', ['reason' => 'items_reused'], request()->user()));
            }
            $questionId = $question['question_id'];
            if($question['question_id'] === null){
                $questionFromAssessment = $this->question->createFromAssessment($data, $question);
                $questionId = $questionFromAssessment->id;
            }
            // store assessment question
            $assessmentQuestion['question_id'] = $questionId;
            $assessmentQuestion['assessment_id'] = $assessment->id;
            $assessmentQuestion['title'] = $question['title'];
            $assessmentQuestion['description'] = $question['description'];
            $assessmentQuestion['type_id'] = $question['type_id'];
            $assessmentQuestion['grade'] = $question['overall_grade'];
            $assessmentQuestion['open_response'] = $question['open_response'] ?? [];
            $assessmentQuestion['open_response_grade'] = $question['open_response_grade'];
            $assessmentQuestion['rubrics'] = $question['rubrics'] ?? [];
            $assessmentQuestion['response_rubric'] = $question['responses'] ?? [];

            if (isset($question['ord_id'])) {
                $assessmentQuestion['ord_id'] = $question['ord_id'];
            }

            if (isset($question['answered'])) {
                $assessmentQuestion['answered'] = $question['answered'];
            }
            $questionCreateResponse = AssessmentQuestion::create($assessmentQuestion);

            // store assessment responses
            foreach ($question['responses'] as $response) {
                if($response){
                    $assessmentResponse = [];
                    $assessmentResponse['assessment_question_id'] = $questionCreateResponse->id;
                    $assessmentResponse['response'] = $response['description'];
                    $assessmentResponse['title'] = $response['title'];
                    $assessmentResponse['sub_grade'] = $response['sub_grade'];
                    $assessmentResponse['is_correct'] = $response['is_valid'] === true ? 1 : 0;
                    $assessmentResponse['match_label'] = $response['match_label'];
                    $assessmentResponse['rubric'] = $response['rubric'];
                    $assessmentResponse['last_update'] = date("Y-m-d H:i:s");
                    $this->assessmentResponse->create($assessmentResponse);
                }
            }
            //creating question resources
            $questionResources = QuestionResource::where("question_id", $questionId)->get();
            foreach($questionResources as $resource){
                AssessmentResource::create([
                    'assessment_id' => $assessment->id,
                    'resource_type' => $resource->type,
                    'url' => $resource->res_url,
                    'question_id' => $questionCreateResponse->id,
                    'resource' => $resource->toArray()
                ]);
            }
        }

        $period = AssessmentPeriod::find($data['period_id']);
        $scheduleDate = $data['schedule_date'];
        //schedule the assessment
        AssessmentSchedule::create([
            'assessment_id' => $assessment->id,
            'class_id' => $course->id,
            'start_time' => date("$scheduleDate {$period->start_time}"),
            'end_time' => date("$scheduleDate {$period->end_time}"),
            'status' => 'close',
            'account_id' => request()->user()->id,
            'period_id' => $period->id
        ]);
        return successResponse($assessment->fresh(), null, 201);
    }

    public function update($id, AssessmentRequest $request)
    {
        $data = $request->getData();
        $assessment = Assessment::findOrFail($id);

        $assessment->update($data);

        $courseWorkFromAssessment = CourseWorkFromAssessment::where("assessment_id", $id)->first()->toArray();

        if(isset($courseWorkFromAssessment['course_work_id'])){
            $LocalCourseWork = LocalCourseWork::where("id", $courseWorkFromAssessment['course_work_id'])->first()->toArray();

            $coursworkDelete=array(
                'id' => $LocalCourseWork['id'],
                'courseId' => $LocalCourseWork['external_course_id'],
                'courseWorkId' => $LocalCourseWork['external_course_work_id'],
            );
          $this->courseWork->deleteFromAssessment($coursworkDelete);
          CourseWorkFromAssessment::where("assessment_id", $id)->delete();
          LocalCourseWork::where("id",$LocalCourseWork['id'])->delete();
        }

        $courseData = [];
        $course = Course::findOrFail($data['class_id']);
        $courseData['courseId'] = $course['external_course_id'];

        $courseData['title'] = $data['title'];
        $courseData['description'] = $data['description'];
        $courseData['workType'] = self::$courseWorkType;
        $courseData['state'] = self::$courseWorkState;
        if(isset($data['max_point']) && (int)$data['max_point']>0 ){
            $courseData['maxPoints'] = $data['max_point'];
        }
        else  $courseData['maxPoints'] = 100;

        $courseData['response_rubric'] = isset($data['response_rubric']) ? $data['response_rubric'] : null;

        $courseWork = $this->courseWork->create($courseData);
        CourseWorkFromAssessment::create([
            'course_work_id' => $courseWork->local->id,
            'assessment_id' => $id
        ]);




        $questionsNotToBeDeleted = [];
        foreach ($data['questions'] as $index => $question) {

            // update question
            $assessmentQuestion['title'] = $question['title'];
            $assessmentQuestion['description'] = $question['description'];
            $assessmentQuestion['type_id'] = $question['type_id'];
            $assessmentQuestion['assessment_id'] = $assessment->id;
            $assessmentQuestion['grade'] = $question['overall_grade'];
            $assessmentQuestion['open_response'] = $question['open_response'] ?? [];
            $assessmentQuestion['open_response_grade'] = $question['open_response_grade'];
            $assessmentQuestion['rubrics'] = $question['rubrics'] ?? [];
            $assessmentQuestion['response_rubric'] = $question['responses'] ?? [];

            if (isset($question['ord_id'])) {
                $assessmentQuestion['ord_id'] = $question['ord_id'];
            }

            if (isset($question['answered'])) {
                $assessmentQuestion['answered'] = $question['answered'];
            }
            //create question or update
            if (empty($question['question_id']) || $question['question_id'] === null) {
                $questionFromAssessment = $this->question->createFromAssessment($data, $question);
                $assessmentQuestion['question_id'] = $questionFromAssessment->id;
                $assessmentQuestion['response_rubric'] = $questionFromAssessment->response_rubric;
                $questionCreateResponse = $this->assessmentQuestion->create($assessmentQuestion);
            } else {
                $questionCreateResponse = AssessmentQuestion::where([
                    'question_id' => $question["question_id"],
                    'assessment_id' => $id
                ])->first();
                if ($questionCreateResponse === NULL) {
                    $assessmentQuestion['question_id'] = $question["question_id"];
                    $assessmentQuestion['response_rubric'] = $question["rubrics"];
                    $questionCreateResponse = AssessmentQuestion::create($assessmentQuestion);
                }else{

                    $questionCreateResponse->update($assessmentQuestion);
                }
            }

            $questionsNotToBeDeleted[] = $questionCreateResponse->id;

           //remove previous responses and store new responses
            AssessmentResponse::where("assessment_question_id", $questionCreateResponse->id)->delete();
            foreach ($question['responses'] as $response) {
                if($response){
                    $assessmentResponse = [];
                    $assessmentResponse['assessment_question_id'] = $questionCreateResponse->id;
                    $assessmentResponse['response'] = $response['description'];
                    $assessmentResponse['title'] = $response['title'];
                    $assessmentResponse['sub_grade'] = $response['sub_grade'];
                    $assessmentResponse['is_correct'] = $response['is_valid'] === true ? 1 : 0;
                    $assessmentResponse['rubric'] = $response['rubric'];
                    $assessmentResponse['match_label'] = $response['match_label'];
                    $this->assessmentResponse->create($assessmentResponse);
                }
            }
            //create question resources
            AssessmentResource::where("question_id", $questionCreateResponse->id)->where("assessment_id", $id)->delete();
            $questionResources = $this->assessmentResource->uploadResources($questionCreateResponse->id, $question);
            foreach($questionResources as $resource){
                AssessmentResource::create([
                    'assessment_id' => $assessment->id,
                    'resource_type' => $resource['type'],
                    'url' => $resource['res_url'],
                    'question_id' => $questionCreateResponse->id,
                    'resource' => $resource
                ]);
            }
        }
        //clear unnecessary questions
        $questionsToBeDeleted = AssessmentQuestion::whereNotIn("id", $questionsNotToBeDeleted)->where("assessment_id", $assessment->id)->get()->pluck("id")->toArray();
        AssessmentResponse::whereIn("assessment_question_id", $questionsToBeDeleted)->delete();
        AssessmentResource::whereIn("question_id", $questionsToBeDeleted)->delete();
        AssessmentQuestion::whereIn("id", $questionsToBeDeleted)->delete();
		$period = AssessmentPeriod::find($data['period_id']);
        $scheduleDate = $data['schedule_date'];
        //schedule the assessment
		AssessmentSchedule::where('assessment_id', $assessment->id)->update(
			['class_id' => $data['class_id'],
            'start_time' => date("$scheduleDate {$period->start_time}"),
            'end_time' => date("$scheduleDate {$period->end_time}"),
			'status' => 'close',
			'account_id' => request()->user()->id,
            'period_id' => $period->id

			]
			);

        return successResponse($assessment->fresh(), null, 201);
    }

    public function destroy($id)
    {
        $questions = AssessmentQuestion::where("assessment_id", $id)->get()->pluck("id")->toArray();
        $attempts = AssessmentQuestionAttempt::where("assessment_id", $id)->get()->pluck("id")->toArray();
        AssessmentLog::where("assessment_id", $id)->delete();
        AssessmentResponse::whereIn("assessment_question_id", $questions)->delete();
        AccountAssessment::where("assessment_id", $id)->delete();
        AssessmentQuestionResult::whereIn("attempt_id", $attempts)->delete();
        AssessmentQuestionAttempt::where("assessment_id", $id)->delete();
        AssessmentQuestion::where("assessment_id", $id)->delete();
        AssessmentSchedule::where("assessment_id", $id)->delete();
        AssessmentResource::where("assessment_id", $id)->delete();
        ClassSession::where("assessment_id", $id)->delete();
        CourseWorkFromAssessment::where("assessment_id", $id)->delete();
        StudentWorkProductLog::where("assessment_id", $id)->delete();
        $assessment = Assessment::findOrFail($id);
        $assessment->delete();
        return successResponse(null, 'Deleted Successfully', 200);
    }

    public function deleteAssessmentQuestion($id)
    {
        AssessmentResource::where('question_id', $id)->delete();
        AssessmentResponse::where('assessment_question_id', $id)->delete();
        AssessmentQuestion::where('assessment_id', $id)->delete();
    }
	public function getUnfinished(StudentWorkProductRepository $repository)
    {
		$user = request()->user();
		if($user->account_type_id==2){
		///  I need to undertand more clearly about Elequent model and add Having on Query\Builder
                   $sql="SELECT a.id as assessment_id,
                    a.title    FROM   assessment a
                    left join student_class st  on st.class_id = a.class_id
                    WHERE  NOT EXISTS (
                        SELECT 1
                        FROM   student_work_product_logs i
                        WHERE  a.id = i.assessment_id and  st.account_id=i.student_id and i.student_id='".$user->id."'
                    )  and a.status='finished' and st.account_id='".$user->id ."' GROUP BY(a.title , a.id)
                    order by a.id limit 5";
                    $AccountAssessments = DB::Select($sql);
                    foreach($AccountAssessments as $assessmentinfo){
                        $id=  $assessmentinfo->assessment_id;
                        $assessment = Assessment::find($id);
                            $account = request()->user();
                            $AccountAssessment = AccountAssessment::where('account_id', '=', $account->id)
                                ->where('assessment_id', '=', $assessment->id)
                                ->get();
                            $NumberAnswered = $AccountAssessment->count();
                            $totalAnswerShould=3;
                            $log = StudentWorkProductLog::firstOrCreate([
                                'assessment_id' => $assessment->id,
                                'student_id' => $account->id
                            ]);
                            $pending_answer = ($totalAnswerShould-$NumberAnswered);
                            StudentWorkProductLog::where('student_id', $account->id)
                                ->where('assessment_id', $assessment->id)
                                ->update(['pending_answer' => $pending_answer]);

                            try{
                                if($log->google_drive_response === NULL){
                                    $hasUploaded = $repository->upload($assessment, $account, $log);
                                    if(!$hasUploaded){
                                        StudentWorkProductLog::where('student_id', $account->id)
                                        ->where('assessment_id', $assessment->id)->delete();
                                        return response()->json([
                                            'status' => 'success',
                                            'message' => 'No Work Product Found',
                                            'student_work_product' => NULL
                                        ], 200);

                                    }else {

                                        if($log->student_submission_response === NULL){
                                            $repository->submitResponse($assessment, $account, $log);
                                        }
                                        if($log->turn_in_response === NULL){
                                            $repository->turnIn($assessment, $account, $log);
                                        }
                                    }
                                }

                            }catch(Exception $e){

                            }
                    }

        }
		else 	$AccountAssessments=array();
		$response = [
            'status' => 'Success',
            'message' => 'Unfinished Assessments List',

            'data' => $AccountAssessments
        ];
        return response()->json($response, 200);
    }
}
