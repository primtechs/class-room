<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\School;
use Illuminate\Support\Facades\Validator;

class SchoolController extends Controller
{

    public function index()
    {
        $data = [];
        $response = School::all();
        $data['data'] = $response;
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'school_name' => "required|unique:schools,school_name",
            'phone' => "required|unique:schools,phone",
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "school_name" => $validator->errors()->first("school_name"),
                "phone" => $validator->errors()->first("phone")
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'School is saved successfully';
            $data['data'] = School::create([
                'school_name' => $request->get('school_name'),
                'address' => $request->get('address'),
                'city' => $request->get('city'),
                'state' => $request->get('state'),
                'zip' => $request->get('zip'),
                'district' => $request->get('district'),
                'phone' => $request->get('district'),
                'contact_name' => $request->get('contact_name'),
                'domain' => $request->get('domain'),
            ]);
        }
        return response()->json($data, 201);
    }

    public function show($id)
    {
        $data = [];
        $school = School::find($id);
        if ($school != NULL) {
            $data['status'] = 'success';
            $data['data'] = $school;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'School are Not Found';
        }
        return response()->json($data, 200);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'school_name' => "required|unique:schools,school_name,$id",
            'phone' => "required|unique:schools,phone,$id",
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "school_name" => $validator->errors()->first("school_name"),
                "phone" => $validator->errors()->first("phone"),
            ];
            $data['data'] = '';
        } else {
            $school = School::find($id);
            if ($school != NULL) {
                $school->school_name = $request->get('school_name');
                $school->address = $request->get('address');
                $school->city = $request->get('city');
                $school->state = $request->get('state');
                $school->zip = $request->get('zip');
                $school->district = $request->get('district');
                $school->phone = $request->get('phone');
                $school->contact_name = $request->get('contact_name');
                $school->domain = $request->get('domain');
                $school->save();
                $data['status'] = 'success';
                $data['data'] = $school->fresh();
                $data['message'] = 'School Updated Successfully';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'School is Not Found';
            }
        }
        return response()->json($data, 200);
    }

    public function destroy(Request $request, $id)
    {
        $school = School::find($id);
        if ($school != NULL) {
            try {
                $school->delete();
                $data['status'] = 'success';
                $data['data'] = '';
                $data['message'] = 'School Deleted Successfully';
            } catch (\Exception $exception) {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = "School is associated with active Accounts can't Delete";
            }
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Not Found';
        }
        return response()->json($data, 200);
    }
}
