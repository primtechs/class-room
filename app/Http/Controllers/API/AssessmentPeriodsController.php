<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AssessmentSchedule;
use App\Models\AssessmentPeriod;
use Illuminate\Support\Facades\Validator;

class AssessmentPeriodsController extends Controller
{
    public function index()
    {
        $data = [];
        if(request()->has(['class_id', 'schedule_date'])){
            $class_id = request()->get("class_id");
            $schedule_date = request()->get("schedule_date");
            $query = AssessmentSchedule::where("class_id", $class_id)->where('status', "=",'Active');
			
            if($schedule_date){
                $query = $query->date($schedule_date);
            }
            if($period_id = request()->get("period_id")){
                $query = $query->where("period_id", "!=", $period_id);
            }
            $assessmentSchedule = $query->get()->pluck('period_id')->all();
			$data['data'] = AssessmentPeriod::whereNotIn('id', $assessmentSchedule)->orderBy('start_time', 'ASC')->get();
        }else{
            $data['data'] = AssessmentPeriod::all();
        }
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|min:2|unique:period,name',
            'start_time' => 'required',
            'end_time' => 'required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name"),
                'start_time' => $validator->errors()->first("start_time"),
                'end_time' => $validator->errors()->first("end_time"),
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Assessment Period is saved successfully';
            $data['data'] = AssessmentPeriod::create([
                'name' => $request->get("name"),
                'start_time' => $request->get("start_time"),
                'end_time' => $request->get("end_time"),
            ]);
        }
        return response()->json($data, 201);
    }


    public function show($id)
    {
        $data = [];
        $assessmentPeriod = AssessmentPeriod::find($id);
        if ($assessmentPeriod != NULL) {
            $data['status'] = 'success';
            $data['data'] = $assessmentPeriod;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'AssessmentPeriod are Not Found';
        }
        return response()->json($data, 200);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => "required|min:2|unique:period,name,$id",
            'start_time' => 'required',
            'end_time' => 'required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name"),
                'start_time' => $validator->errors()->first("start_time"),
                'end_time' => $validator->errors()->first("end_time"),
            ];
            $data['data'] = '';
        } else {
            $assessmentPeriod = AssessmentPeriod::find($id);
            if ($assessmentPeriod != NULL) {
                $assessmentPeriod->name = $request->get("name");
                $assessmentPeriod->start_time = $request->get("start_time");
                $assessmentPeriod->end_time = $request->get("end_time");
                $assessmentPeriod->save();
                $data['status'] = 'success';
                $data['data'] = $assessmentPeriod->fresh();
                $data['message'] = '';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Assessment Period is Not Found';
            }
        }
        return response()->json($data, 200);
    }


    public function destroy($id)
    {
        $assessmentPeriods = AssessmentPeriod::find($id);
        if ($assessmentPeriods != NULL) {
            try {
                $assessmentPeriods->delete();
                $data['status'] = 'success';
                $data['data'] = '';
                $data['message'] = 'Assessment Period Deleted Successfully';
            } catch (\Exception $exception) {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Not Found';
        }
        return response()->json($data, 200);
    }
}
