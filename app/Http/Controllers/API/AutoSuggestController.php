<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Protocols;
use App\Models\QuestionSubject;
use App\Models\School;
use App\Models\Standard;
use App\Models\Topic;
use App\Models\TeacherClass;
use Illuminate\Http\Request;
use App\Models\Accounts\Account;
use Illuminate\Support\Facades\DB;

class AutoSuggestController extends Controller
{
    public static $STUDENT = 28;
    public static $TEACHER = 27;


    public function classWiseAccountList($class_id){
        $accountIds = TeacherClass::where("class_id", $class_id)->get()->pluck("account_id")->all();
        $accounts = Account::whereIn("id", $accountIds)->where("account_type_id", 1)->get();
        return response()->json([
            'status' => 'success',
            'accounts' => $accounts
        ], 200);
    }

    public function accountList(Request $request){
        $account = $request->get("account");
        $accounts = Account::where("email","ILIKE","%$account%")->limit(5)->get();
        return successResponse($accounts, 'Success', 200);
    }

    public function studentList(Request $request) {
        $query = DB::table('accounts')->select('accounts.*', 'invitations.*', 'accounts.id as student_account_id');
        $query->leftJoin('invitations', 'accounts.email', '=', 'invitations.external_user_id');
//        $query->leftJoin('accounts', 'accounts.email', '=', 'invitations.external_user_id');
        $query->where('accounts.account_type_id', self::$STUDENT);

        $student =  $request->get("student","");
        if (!empty($student)) {
            $query->where('accounts.email', 'LIKE','%'.$student.'%')
                ->orWhere('accounts.first_name', 'ILIKE','%'.$student.'%')
                ->orWhere('accounts.last_name', 'ILIKE','%'.$student.'%')
            ;
        }
        $query->where('accounts.id', '!=', null);
        $students = $query->distinct('accounts.id')->get();
        return successResponse($students, 'Success', 200);
    }

    public function teacherList(Request $request) {
        $query = DB::table('accounts');
        $query->leftJoin('invitations', 'accounts.email', '=', 'invitations.external_user_id');
        $query->leftJoin('classes', 'classes.external_user_id', '=', 'accounts.external_user_id');
        $query->where('accounts.account_type_id', self::$TEACHER);
        $teacher =  $request->get("teacher","");
        if (!empty($teacher)) {
            $query->where('accounts.email', 'LIKE','%'.$teacher.'%')
                ->orWhere('accounts.first_name', 'LIKE','%'.$teacher.'%')
                ->orWhere('accounts.last_name', 'LIKE','%'.$teacher.'%')
            ;
        }
        $query->where('accounts.id', '!=', null);
        $teachers = $query->distinct('accounts.id')->get();
        return successResponse($teachers, 'Success', 200);
    }

    public function gradeList(Request $request) {
        $grade =  $request->get("grade","");
        if (!empty($grade)) {
            $group = Group::where('name', 'ILIKE', "%$grade%")->get();
        } else {
            $group = Group::all();
        }
        return successResponse($group, 'Success', 200);
    }

    public function classList(Request $request) {
        $account = getAccountDetails();
//        dd($account);
        $class =  $request->get("class","");

        $classQuery = DB::table('classes');

        if($account->isTeacher()) {
            $classQuery->leftJoin('accounts as teachers', 'teachers.external_user_id', '=', 'classes.external_user_id');
            $classQuery->where('teachers.id', '=', $account->id);
        }

        if($account->isStudent()) {
            $query = DB::table('invitations')->where('role', 'STUDENT');
            $query->leftJoin('accounts as students', 'students.email', '=', 'invitations.external_user_id');
            $query->leftJoin('classes', 'classes.external_course_id', '=', 'invitations.external_course_id');
            $query->where('students.id', '=', $account->id);
            if (!empty($class)) {
                $query->where('classes.name', 'ILIKE', "%$class%");
            }
        }

        if (!empty($class)) {
            $classQuery->where('classes.name', 'ILIKE', "%$class%");
        }

        if ($account->isStudent()) {
            $classes = $query->get();
        } else {
            $classes = $classQuery->get();
        }

        return successResponse($classes, 'Success', 200);
    }

    public function schoolList(Request $request) {
        $school =  $request->get("school","");
        if (!empty($school)) {
            $schools = School::where('school_name', 'ILIKE', "%$school%")->get();
        } else {
            $schools = School::all();
        }

        return successResponse($schools, 'Success', 200);
    }

    public function topicList(Request $request) {
        $topic =  $request->get("topic","");
        if (!empty($topic)) {
            $topics = Topic::where('name', 'ILIKE', "%$topic%")->get();
        } else {
            $topics = Topic::all();
        }

        return successResponse($topics, 'Success', 200);
    }

    public function protocolList(Request $request) {
        $protocol =  $request->get("protocol","");
        if (!empty($protocol)) {
            $protocols = Protocols::where('name', 'ILIKE', "%$protocol%")->get();
        } else {
            $protocols = Protocols::all();
        }

        return successResponse($protocols, 'Success', 200);
    }

    public function standardList(Request $request) {
        $standard =  $request->get("standard","");
        if (!empty($standard)) {
            $standards = Standard::where('name', 'ILIKE', "%$standard%")->get();
        } else {
            $standards = Standard::all();
        }

        return successResponse($standards, 'Success', 200);
    }

    public function groupList(Request $request) {
        $group =  $request->get("group","");
        if (!empty($subject)) {
            $groups = Group::where('name', 'ILIKE', "%$group%")->get();
        } else {
            $groups = Group::all();
        }

        return successResponse($groups, 'Success', 200);
    }

    public function subjectList(Request $request) {
        $subject =  $request->get("subject","");
        if (!empty($subject)) {
            $subjects = QuestionSubject::where('name', 'ILIKE', "%$subject%")->get();
        } else {
            $subjects = QuestionSubject::all();
        }

        return successResponse($subjects, 'Success', 200);
    }

}
