<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\InvitationGroupAccount;
use Illuminate\Support\Facades\Validator;

class InvitationGroupAccountsController extends Controller
{

    public function store(Request $request)
    {
        $rules = [
            'invitation_group_id' => "required",
            'account_id' => "required",
        ];

        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                "invitation_group_id" => $validator->errors()->first("invitation_group_id"),
                "account_id" => $validator->errors()->first("account_id")
            ];
            $data['invitation_group_accounts'] = [];
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Successfully saved!';
            $data['invitation_group_account'] = [];
            $accounts = $request->get("account_id", []);
            foreach($accounts as $account){
                $invitationGroupAccount = InvitationGroupAccount::updateOrCreate([
                    'invitation_group_id' => $request->get("invitation_group_id"),
                    'account_id' => $account
                ], [
                    'invitation_group_id' => $request->get("invitation_group_id"),
                    'account_id' => $account
                ]);
                $data['invitation_group_accounts'][] = $invitationGroupAccount->account;
            }
        }
        return response()->json($data, 201);
    }


    public function destroy(Request $request, $invitationGroupId)
    {
        $accountId = $request->get("account_id");
        $invitationGroupAccount = InvitationGroupAccount::where(['invitation_group_id' => $invitationGroupId, 'account_id' => $accountId])->first();
        if ($invitationGroupAccount != NULL) {
            try {
                $invitationGroupAccount->delete();
                $data['status'] = 'success';
                $data['invitation_group_account'] = '';
                $data['message'] = 'Deleted Successfully';
            } catch (\Exception $exception) {
                $data['status'] = 'error';
                $data['invitation_group_account'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['invitation_group_account'] = '';
            $data['message'] = 'Not Found';
        }
        return response()->json($data, 200);
    }

}
