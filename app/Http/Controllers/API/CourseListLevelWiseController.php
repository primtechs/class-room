<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\TeacherClass;
use Illuminate\Http\Request;


class CourseListLevelWiseController extends Controller
{

    public function index(Request $request)
    {
        $level_id = $request->get("level_id", NULL);
        $response = [
            'status' => 'success',
            'courses' => [],
            'message' => 'Course List'
        ];
        $user = request()->user();
        $classes = TeacherClass::where("account_id", $user->id)->get()->pluck("class_id")->toArray();
        if ($level_id !== NULL) {
            $courses = Course::where("level_id", $level_id)
                ->whereIn("id", $classes)
                ->where("external_course_metadata", "!=", NULL)
                ->get();
            foreach ($courses as $course) {
                $courseResponse = json_decode($course->external_course_metadata, 1);
                $courseResponse['local'] = $course;
                $response['courses'][] = $courseResponse;
            }
        }
        return response()->json($response, 200);
    }

}
