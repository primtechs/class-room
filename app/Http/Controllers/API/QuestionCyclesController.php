<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\QuestionCycle;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class QuestionCyclesController extends Controller
{
        public function index()
    {
        $data = [];
        $response = QuestionCycle::all();
        $data['data'] = $response;
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }


    public function store(Request $request)
    {
        $rules = [
            'name' => "required|unique:question_cycles,name",
            'description' => 'required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name"),
                'description' => $validator->errors()->first("description")
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Question Cycle is saved successfully';
            $data['data'] = QuestionCycle::create([
                'name' => $request->get("name"),
                'description' => $request->get("description"),
            ]);
        }
        return response()->json($data, 201);
    }


    public function show($id)
    {
        $data = [];
        $questionCycle = QuestionCycle::find($id);
        if ($questionCycle != NULL) {
            $data['status'] = 'success';
            $data['question_cycle'] = $questionCycle;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['question_cycle'] = '';
            $data['message'] = 'Question Cycle is Not Found';
        }
        return response()->json($data, 200);
    }


    public function update(Request $request, $id)
    {
        $rules = [
            'name' => "required|unique:question_cycles,name,$id",
            'description' => 'required',
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name"),
                'description' => $validator->errors()->first("description")
            ];
            $data['question_cycle'] = '';
        } else {
            $questionCycle = QuestionCycle::find($id);
            if ($questionCycle != NULL) {
                $name = $request->get("name");
                $description = $request->get("description");
                $questionCycle->name = $name;
                $questionCycle->description = $description;
                $questionCycle->save();
                $data['status'] = 'success';
                $data['data'] = $questionCycle->fresh();
                $data['message'] = 'Question Cycle Updated Successfully';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Question Cycle Not Found';
            }
        }
        return response()->json($data, 200);
    }


    public function destroy(Request $request, $id)
    {
        $questionCycle = QuestionCycle::find($id);
        if ($questionCycle != NULL) {
            try {
                $questionCycle->delete();
                $data['status'] = 'success';
                $data['question_cycle'] = '';
                $data['message'] = 'Question Cycle Deleted Successfully';
            }catch (\Exception $exception){
                $data['status'] = 'error';
                $data['question_cycle'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['question_cycle'] = '';
            $data['message'] = 'Question Cycle Not Found';
        }
        return response()->json($data, 200);
    }
}
