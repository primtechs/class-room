<?php

namespace App\Http\Controllers\API;

use App\Models\CensorshipWordList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class CensorshipWordListsController extends Controller
{
    public function index()
    {
        $data = [];
        $response = CensorshipWordList::all();
        $data['data'] = $response;
        $data['status'] = 'success';
        $data['message'] = '';
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|min:2|unique:censorship_word_list,name',
            'censorship_id' => 'required',
            'replace_with' => 'required',
            'is_active' => 'required'
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name"),
                'censorship_id' => $validator->errors()->first("censorship_id"),
                'replace_with' => $validator->errors()->first("replace_with"),
                'is_active' => $validator->errors()->first("is_active")
            ];
            $data['data'] = '';
        } else {
            $data['status'] = 'success';
            $data['message'] = 'Censorship Word List is saved successfully';
            $data['data'] = CensorshipWordList::create([
                'name' => $request->get("name"),
                'censorship_id' => $request->get("censorship_id"),
                'replace_with' => $request->get("replace_with"),
                'is_active' => $request->get("is_active")
            ]);
        }
        return response()->json($data, 201);
    }


    public function show($id)
    {
        $data = [];
        $censorshipWordList = CensorshipWordList::find($id);
        if ($censorshipWordList != NULL) {
            $data['status'] = 'success';
            $data['data'] = $censorshipWordList;
            $data['message'] = '';
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Censorship Word List Not Found';
        }
        return response()->json($data, 200);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => "required|min:2|unique:censorship_word_list,name,$id",
            'censorship_id' => 'required',
            'replace_with' => 'required',
            'is_active' => 'required'
        ];
        $data = [];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['status'] = 'error';
            $data['message'] = [
                'name' => $validator->errors()->first("name"),
                'censorship_id' => $validator->errors()->first("censorship_id"),
                'replace_with' => $validator->errors()->first("replace_with"),
                'is_active' => $validator->errors()->first("is_active")
            ];
            $data['data'] = '';
        } else {
            $censorshipWordList = CensorshipWordList::find($id);
            if ($censorshipWordList != NULL) {
                $censorshipWordList->name = $request->get("name");
                $censorshipWordList->censorship_id = $request->get("censorship_id");
                $censorshipWordList->replace_with = $request->get("replace_with");
                $censorshipWordList->is_active = $request->get("is_active");
                $censorshipWordList->save();
                $data['status'] = 'success';
                $data['data'] = $censorshipWordList->fresh();
                $data['message'] = '';
            } else {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Censorship Word List is Not Found';
            }
        }
        return response()->json($data, 200);
    }


    public function destroy($id)
    {
        $censorshipWordLists = CensorshipWordList::find($id);
        if ($censorshipWordLists != NULL) {
            try {
                $censorshipWordLists->delete();
                $data['status'] = 'success';
                $data['data'] = '';
                $data['message'] = 'Censorship Word List Deleted Successfully';
            } catch (\Exception $exception) {
                $data['status'] = 'error';
                $data['data'] = '';
                $data['message'] = 'Something Went Wrong';
            }
        } else {
            $data['status'] = 'error';
            $data['data'] = '';
            $data['message'] = 'Not Found';
        }
        return response()->json($data, 200);
    }
}
