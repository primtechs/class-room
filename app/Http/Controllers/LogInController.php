<?php

namespace App\Http\Controllers;

use App\Events\ClassLogin;
use App\Events\LogOutEvent;
use App\Models\Policy;
use App\Models\Resource;
use App\Models\ResourcePolicy;
use App\Repository\CourseRepository;
use App\Http\Requests\AuthUser;
use App\Models\Accounts\Account;
use App\Models\Assessment;
use App\Models\AssessmentQuestionAttempt;
use App\Models\AssessmentSchedule;
use App\Models\StudentClass;
use Google_Service_Oauth2;

class LogInController extends Controller
{

    public function login(AuthUser $request,CourseRepository $course)
    {
        $data = [];
        $code_token = $request->get("code_token");
        $client = setGoogleClientFromAuthCode($code_token['code']);
        $token = $client->getAccessToken();
        $oauth = new Google_Service_Oauth2($client);
        $profile = $oauth->userinfo->get();
        if ($profile) {
            $user = [
                "id" => $profile->id,
                "email" => $profile->email,
                "givenName" => $profile->givenName,
                "familyName" => $profile->familyName,
                "gender" => $profile->gender,
                "link" => $profile->link,
                "locale" => $profile->locale,
                "name" => $profile->name,
                "picture" => $profile->picture,
            ];
            $apiToken = hash('md5', time());
            $account = Account::where(["email" => $user['email'], 'status' => true])->first();
            //check if the account does not exist's then throw a failure,
            if ($account === null) {
                return response()->json(['status' => 'error', 'message' => 'Invalid Account'], 200);
            }
            //logout all users
            event(new LogOutEvent(["data"=>"Logged in another device","api_token"=>$account->api_token]));
            //account is available proceed with login
            $account->update([
                'first_name' => $user['givenName'],
                'last_name' => $user['familyName'],
                'status' => 1,
                'phone_number' => '',
                'email' => $user['email'],
                'password' => $user['id'],
                'social_id' => $user['id'],
                'external_user_id' => $user['id'],
                'external_user_profile' => $user,
                'token' => $token,
                'api_token' => $apiToken,
            ]);
            $account->fresh();
            $type = strtoupper($account->accountType->name);

            $user['account_id'] = $account->id;
            $user['user_type'] = $account->accountType->name;
            $user['has_active_assessment'] = $this->hasActiveAssessment($account);
            $user['token'] = $token;
            $user['menus'] = $this->accountResources($account);
            if($user['has_active_assessment']){
                if($type === "TEACHERS" || $type === "ADMIN"){
                    $schedule = AssessmentSchedule::where("status", "Active")
                                          ->where("start_time", "<=", date("Y-m-d H:i:s"))
                                          ->where("end_time", ">=", date("Y-m-d H:i:s"))
                                          ->whereIn("assessment_id", function($query) use($account){
                                            $query->select("id")->from("assessment")->where("account_id", $account->id);
                                          })->first(); 
                    if($schedule){
                        $assessment = Assessment::find($schedule->assessment_id);
                        $data['assessment'] = $assessment->toArray();
                    }
                }
            }


            $data['courses'] = $course->getAllCourses($type,$account);
            $data['status'] = "success";
            $data['message'] = "Welcome, {$account->first_name} {$account->last_name}";
            $data['user'] = $user;
            $data['api_token'] = $apiToken;
            event(new ClassLogin($account));
            return response()->json($data, 200);
        }
        return response()->json(['status' => 'error', 'message' => 'Invalid Account'], 200);
    }


    private function accountResources(Account $account)
    {
        $available_menus = [
            'dashboards' => [
                'question-dashboard',
                'student-dashboard',
                'grade-book-dashboard',
                'assignment-dashboard',
                'live-instructions-dashboard'
            ],
            'assessments' => ['assessments'],
            'admin_settings' => [
                'accounts',
                'levels',
                'schools',
                'protocols',
                'standard',
                'groups',
                'question-grades',
                'question-subjects',
                'question-cycles',
                'question-types',
                'assessment-periods',
                'periods',
                'invitation-groups',
                'topics',
            ],
            'censorship' => [
                'censorship-level',
                'censorship-word-list'
            ],
            'access_controls' => [
                'roles',
                'account-roles',
                'actions',
                'policy',
                'resources',
                'resource-policy',
            ],
            'question_bank' => ['files','all-questions', 'questions', 'create-question', 'draft-questions'],
            'teacher' => ['classes', 'students'],
            'student' => ['std-classes', 'std-classwork'],
            'invitations' => ['invitations'],
            'reward_point' => ['reward-point'],
            'notification' => ['notifications'],
            'archived_assessments' => ['archived-assessments'],
            'requested_classes' => ['requested-classes']
        ];
        $menus = [
            'dashboards' => [],
            'assessments' => [],
            'admin_settings' => [],
            'censorship' => [],
            'access_controls' => [],
            'question-bank' => [],
            'teacher' => [],
            'student' => [],
            'invitations' => [],
            'reward_point' => [],
            'notification' => [],
            'archived_assessments' => [],
            'requested_classes' => []
        ];
        if ($account->accountRole === null) {
            return $menus;
        }
        $policies = Policy::where("role_id", $account->accountRole->role_id)->get()->pluck("id")->toArray();
        $resourcePolicies = ResourcePolicy::whereIn("policy_id", $policies)->get()->pluck("resource_id")->toArray();
        $resources = Resource::whereIn("id", $resourcePolicies)->orderBy("id", "ASC")->get();
        foreach ($resources as $resource) {
            $keys = array_keys($available_menus);
            foreach ($keys as $key) {
                if (array_search($resource->res_url, $available_menus[$key]) !== FALSE) {
                    $menus[$key][] = $resource->toArray();
                }
            }
        }
        return $menus;
    }

    private function hasActiveAssessment(Account $account)
    {
        $type = strtolower($account->accountType->name);
        $classes = [];
        if($type === "teachers" || $type === "admin"){
            $schedule = AssessmentSchedule::where("status", "Active")
                                          ->where("start_time", "<=", date("Y-m-d H:i:s"))
                                          ->where("end_time", ">=", date("Y-m-d H:i:s"))
                                          ->whereIn("assessment_id", function($query) use($account){
                                            $query->select("id")->from("assessment")->where("account_id", $account->id);
                                          })->first(); 
        }else{
            $classes = StudentClass::where("account_id", $account->id)->pluck("class_id")->toArray();
            $schedule = AssessmentSchedule::whereIn("class_id", $classes)
                                          ->where("status", "Active")
                                          ->where("start_time", "<=", date("Y-m-d H:i:s"))
                                          ->where("end_time", ">=", date("Y-m-d H:i:s"))
                                          ->first(); 
        }
 
        if($schedule){
            $attempt = AssessmentQuestionAttempt::where("assessment_id", $schedule->assessment_id)->orderBy("id", "DESC")->first();
            if($attempt){
                return true;
            }
        } 
        return false;
    }

}
