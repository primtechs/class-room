<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GradeSettingRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'courseid' => 'required',
            'calculation_type' => 'required',
            'show_grade' => 'required',
            'exclude_missing' => 'required',
        ];
    }

    public function data()
    {
        return $this->only(['courseid','calculation_type','show_grade',"exclude_missing","categories"]);
    }
}
