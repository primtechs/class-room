<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseWorkMaterial extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
//            'maxPoints' => 'required',
            'state' => 'sometimes|in:COURSEWORK_MATERIAL_STATE_UNSPECIFIED,PUBLISHED,DRAFT,DELETED',
            'assigneeMode' => 'sometimes|in:ASSIGNEE_MODE_UNSPECIFIED,ALL_STUDENTS,INDIVIDUAL_STUDENTS',
        ];
    }
}
