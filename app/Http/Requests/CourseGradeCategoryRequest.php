<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseGradeCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "category_type" => "required",
            "categories" => "required|array|min:1",
            "categories.*.category" => "required",
            "categories.*.amount" => "required",
            "course_id" => "required",
            "external_course_id" => "required"
        ];
    }
}
