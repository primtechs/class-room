<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AssessmentRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'topic_id' => 'required',
            'level_id' => 'required',
            'title' => 'required',
            'status' => 'required',
            'group_id' => 'required',
            'protocol_id' => 'required',
            'account_id' => 'required',
            'standard_id' => 'required',
            'class_id' => 'required'
        ];
    }

    public function getData()
    {
        $data = $this->all();
        $data['last_update'] = date("Y-m-d H:i:s");
        $data['submitted'] = false;
        $data['status'] = "Active";
        return $data;
    }

}
