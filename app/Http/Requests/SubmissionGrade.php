<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubmissionGrade extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "type" => "required|in:ASSIGN_GRADE,DRAFT_GRADE,ASSIGNMENT_SUBMISSION,SHORT_ANSWER_SUBMISSION,MULTIPLE_CHOICE_SUBMISSION",
//            "state" => "sometimes|in:SUBMISSION_STATE_UNSPECIFIED,CREATED,TURNED_IN,RETURNED,RECLAIMED_BY_STUDENT",
        ];
    }
}
