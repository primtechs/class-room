<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class QuestionFormRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $published_state = $this->get('published_state', 'PUBLISHED');
        if ($published_state) {
            return [];
        }
        return [
            'title' => 'required'
        ];
    }

     public function failedValidation(Validator $validator) {
        $data = [
            'status' => 'error',
            'message' => [],
            'question'=> NULL
        ]; 
        $messages = $validator->errors()->messages();
        foreach ($messages as $key => $value){
            $data['message'][$key] = $value[0];
        }

        throw new HttpResponseException(response()->json($data, 200)); 
    }

}
