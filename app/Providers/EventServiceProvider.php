<?php

namespace App\Providers;

use App\Events\CheckReward;
use App\Events\ClassLogin;
use App\Listeners\CalculateReward;
use App\Listeners\MakeAttendance;
use App\Listeners\SyncGoogleClass;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ClassLogin::class => [
            MakeAttendance::class,
//            SyncGoogleClass::class
        ],
        CheckReward::class => [
            CalculateReward::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
