<?php

namespace App\Listeners;

use App\Events\LoggedInToClassroom;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SyncGoogleClass
{
    public function __construct()
    {
        //
    }

    public function handle($event)
    {
        $classroom = $event->classRoom;
        $responses = $classroom->courses->listCourses();
    }
}
