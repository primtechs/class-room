<?php

namespace App\Listeners;

use App\Models\ClassLog;
use App\Services\Reward;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CalculateReward
{
    protected $reward;

    public function __construct(Reward $reward)
    {
        $this->reward = $reward;
    }

    public function handle($event)
    {
        $account = $event->account;
        $type = $event->type;
        $data = $event->data;

        $this->reward->setData($type, $data, $account);
        $this->reward->rewardPointCalculation();
    }
}
