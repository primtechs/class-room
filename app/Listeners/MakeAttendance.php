<?php

namespace App\Listeners;

use App\Models\ClassLog;
use App\Services\Reward;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class MakeAttendance
{
    protected $reward;
    public static $type = 'attendance';

    public function __construct(Reward $reward)
    {
        $this->reward = $reward;
    }

    public function handle($event)
    {
        $account = $event->account;
        $type = static::$type;

        ClassLog::create([
            'type' => $type,
            'response' => json_encode($account),
            'account_id' => $account->id
        ]);

        $this->reward->setData($type, $event, $account);
        $this->reward->rewardPointCalculation();
    }
}
