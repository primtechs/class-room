<?php

namespace App\Repository;

use App\Models\Coursework;
use Google_Service_Classroom;
use App\Models\Course;
use App\Models\Assessment;
use App\Models\CourseWorkFromAssessment;
use App\Models\CourseworkAssessmentResponse;
use App\Models\AccountAssessment;
use App\Models\AssessmentLog;
use App\Models\AssessmentQuestion;
use App\Models\AssessmentResource;
use App\Models\AssessmentResponse;
use App\Models\AssessmentSchedule;
use App\Models\AssessmentPeriod;
use App\Models\AssessmentQuestionAttempt;
use App\Models\AssessmentQuestionResult;
use App\Models\ClassSession;
use App\Models\StudentWorkProductLog;
use Illuminate\Container\Container as App;

class CourseworkRepository extends Repository
{

    public static $type = [
        'ASSIGNMENT' => 1,
        'SHORT_ANSWER_QUESTION' => 2,
        'MULTIPLE_CHOICE_QUESTION' => 3
    ];

    public function model()
    {
        return 'App\Models\Coursework';
    }

    public function index($type)
    {

    }

    public function create(array $data)
    {
        $courseId = $data['courseId'];
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $courseWork = setCourseWork($data);
        $response = $classroom->courses_courseWork->create($courseId, $courseWork);

        $local = $this->createLocal($data, $response);
        $response->local = $local;
        return $response;
    }

    public function delete($data = [])
    {

        $courseId = $data['courseId'];
        $courseWorkId = $data['courseWorkId'];
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $courseWork = $classroom->courses_courseWork->get($courseId, $courseWorkId);
        //check if the course work is already deleted or not
        if ($courseWork->state !== 'DELETED') {
            $classroom->courses_courseWork->delete($courseId, $courseWorkId);
        }
        $this->deleteLocal([$data['id']]);
    }
    public function deleteFromAssessment($data = [])
    {

        $courseId = $data['courseId'];
        $courseWorkId = $data['courseWorkId'];
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $courseWork = $classroom->courses_courseWork->get($courseId, $courseWorkId);
        //check if the course work is already deleted or not
        if ($courseWork->state !== 'DELETED') {
            $classroom->courses_courseWork->delete($courseId, $courseWorkId);
            return true;
        }
        return false;
    }

    public function createLocal($data, $response)
    {
        $class = Course::where('external_course_id', $data['courseId'])->first();
        $courseWorkData['class_id'] = $class ? $class['id'] : 0;
        $courseWorkData['external_course_work_id'] = $response->id;
        $courseWorkData['external_course_id'] = $data['courseId'];
        $courseWorkData['title'] = $data['title'] ?? '';
        $courseWorkData['description'] = $data['description'] ?? '';
        $courseWorkData['type'] = self::$type[$data['workType']] ?? 0;
        $courseWorkData['grade_category_id'] = $data['grade_category_id'] ?? 0;
        $courseWorkData['point'] = $data['point'] ?? 100;
        $courseWorkData['submission'] = $data['submission'] ?? 0;

        $courseWorkData['metadata'] = json_encode($response);
        $courseWork = parent::create($courseWorkData);
        return $courseWork;
    }

    public function updateLocal($response)
    {
        $courseWork = Coursework::where('external_course_work_id',$response->id)->first();
        $courseWork->title = $response->title;
        $courseWork->description = $response->description;
        $courseWork->type = self::$type[$response->workType] ?? 0;
        $courseWork->grade_category_id = $data['grade_category_id'] ?? 0;
        $courseWork->point = $response->maxPoints ?? 100;
        $courseWork->metadata = json_encode($response);
        $courseWork->save();
        return $courseWork->fresh();
    }

    public function update(array $data, $id, $attribute = "id")
    {
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $courseWork = setCourseWork($data);
        $query = [];
        $updateData = [];
        $updateData['title'] = $data['title'];
        $updateData['description'] = $data['description'];
        $updateData['point'] = $data['point'] ?? 100;
        $updateData['grade_category_id'] = $data['grade_category_id'] ?? 0;
        $query['updateMask'] = 'title,description,topic_id,due_date,due_time,max_points,state';
        $response = $classroom->courses_courseWork->patch($data['courseId'], $data['courseWorkId'], $courseWork, $query);
        $updateData['metadata'] = json_encode($response);
        parent::update($updateData, $id, $attribute);
        return $response;
    }

    public function syncLocal($id, $response)
    {
        $class = Course::where('external_course_id', $id)->first();
        $courseWorkData['class_id'] = $class ? $class['id'] : 0;
        $courseWorkData['external_course_work_id'] = $response->id;
        $courseWorkData['external_course_id'] = $id;
        $courseWorkData['title'] = $response->title ?? '';
        $courseWorkData['description'] = $response->description ?? '';
        $courseWorkData['type'] = self::$type[$response->workType] ?? 0;
        $courseWorkData['grade_category_id'] = $response->grade_category_id ?? 0;
        $courseWorkData['point'] = $response->maxPoints ?? 100;
        $courseWorkData['submission'] = 0;

        $courseWorkData['metadata'] = json_encode($response);
        parent::create($courseWorkData);
    }

    public function deleteLocal(array $courseWorkIds){
        foreach($courseWorkIds as $id){
            $fromAssessment = CourseWorkFromAssessment::where("course_work_id", $id)->first();
            if ($fromAssessment) {
                $questions = AssessmentQuestion::where("assessment_id", $fromAssessment->assessment_id)->get()->pluck("id")->toArray();
                $attempts = AssessmentQuestionAttempt::where("assessment_id", $fromAssessment->assessment_id)->get()->pluck("id")->toArray();
                AssessmentLog::where("assessment_id", $fromAssessment->assessment_id)->delete();
                AssessmentResponse::whereIn("assessment_question_id", $questions)->delete();
                AccountAssessment::where("assessment_id", $fromAssessment->assessment_id)->delete();
                AssessmentQuestionResult::whereIn("attempt_id", $attempts)->delete();
                AssessmentQuestionAttempt::where("assessment_id", $fromAssessment->assessment_id)->delete();
                AssessmentQuestion::where("assessment_id", $fromAssessment->assessment_id)->delete();
                AssessmentSchedule::where("assessment_id", $fromAssessment->assessment_id)->delete();
                AssessmentResource::where("assessment_id", $fromAssessment->assessment_id)->delete();
                ClassSession::where("assessment_id", $fromAssessment->assessment_id)->delete();
                CourseWorkFromAssessment::where("assessment_id", $fromAssessment->assessment_id)->delete();
                StudentWorkProductLog::where("assessment_id", $fromAssessment->assessment_id)->delete();
                $assessment = Assessment::findOrFail($fromAssessment->assessment_id);


                $assessment->delete();
            }
            CourseworkAssessmentResponse::where("course_work_id", $id)->delete();
            Coursework::where("id", $id)->delete();
        }
    }
}
