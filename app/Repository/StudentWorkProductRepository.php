<?php
namespace App\Repository;
use App\Models\Assessment;
use App\Models\StudentWorkProductLog;
use App\Models\CourseworkAssessmentResponse;
use App\Models\Accounts\Account;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Google_Service_Drive;
use Google_Service_Drive_DriveFile;
use Google_Service_Classroom;
use Google_Service_Classroom_ModifyAttachmentsRequest;
use Google_Service_Classroom_Attachment;
use Google_Service_Classroom_DriveFile;
USE Google_Service_Classroom_TurnInStudentSubmissionRequest;
use Google_Service_Classroom_ReturnStudentSubmissionRequest;
use PDF;
class StudentWorkProductRepository{
	public function WorkProdcutInfomation($assessment_id=0,$account_id=0,Assessment $assessment, Account $account){
			$sql = "select distinct c.id, CONCAT(c.first_name,' ',c.last_name) as student_name,
              CONCAT(e.first_name,' ',e.last_name) as teacher_name,
              c.email,f.class_name, h.name as standard, b.assessment_id
            from assessment_question_results a
            inner join assessment_question_attempt b on b.id = a.attempt_id
            inner join accounts c on c.id = a.account_id
            inner join assessment d on d.id = b.assessment_id
            inner join accounts e on e.id = d.account_id
            inner join classes f on f.id = d.class_id
            inner join standard h on h.id  = d.standard_id
            where  b.assessment_id  = ".(int)$assessment_id." and  c.id = ".(int)$account_id."";
			$Header=DB::select($sql);
			$heading=Array();
			if($Header ){
				foreach($Header as $head){
					$sql = " select id, last_update from assessment_question_attempt  where assessment_id = ".(int)$assessment_id." order by id " ;
					$Attempts=DB::select($sql);
					$questions = array();
					 $i = 0;
					foreach($Attempts as $attempt){
						$sql="select a.attempt_id, c.title, c.description, b.response, b.assessment_question_id
                            from assessment_question_results a
                            inner join assessment_responses b on b.id  = a.response_id
                            inner join assessment_questions c on c.id = b.assessment_question_id
                            where a.attempt_id = ".(int)$attempt->id."
                              and a.account_id = ".(int)$head->id."
                            order by attempt_id";
							$Quest=DB::select($sql);
							foreach($Quest as $q){

								$sql="select * from posts where attempt_id = ".(int)$attempt->id." and account_id = ".(int)$account_id." order by last_update desc";
								$Responses=DB::select($sql);
								$posts =array();
								foreach($Responses as $post){

									$sql = " select * from comments where post_id = ".(int)$post->id." and account_id = ".(int)$account_id." order by last_update desc ";
									$Comm=DB::select($sql);
									$comments=array();
									foreach($Comm as $comment){
										$comments[]=array(
											'id'=>$comment->id,
											'account_id'=>$comment->account_id,
											'comment'=>$comment->comment,
											'last_update'=>$comment->last_update,
										);


									}
                                    $body=json_decode($post->body,true);

									$posts[]=array(
											'id'=>$post->id,
											'account_id'=>$post->account_id,
											'body'=>$body[0],
											'last_update'=>$post->last_update,
											'comments'=>$comments,
										);

								}
								if($i==0) $form="Form - 1A";
								if($i==1) $form="Form - 1B";
								if($i==2) $form="Form - 2";
								$questions[]=array(
											'formName'=>$form,
											'assessment_question_id'=>$q->assessment_question_id,
											'title'=>$q->title,
											'description'=>$q->description,
											'response'=>$q->response,
											'posts'=>$posts,

										);
							}
							$i++;
					}

					$heading = Array(
						"student_name"=> $head->student_name,
					   "teacher_name"=> $head->teacher_name,
					   "email"=> $head->email,
					   "class_name"=> $head->class_name,
					   "standard"=> $head->standard,
					   "details"=> $questions

						);
				}




			}
            else{
                $sql = "select distinct c.id,
                CONCAT(c.first_name,' ',c.last_name) as teacher_name,f.class_name, h.name as standard

                from  assessment a

                left join accounts c on c.id = a.account_id
                left join classes f on f.id = a.class_id
                left join standard h on h.id  = a.standard_id
                where  a.id  = ".(int)$assessment_id." ";
                $Header=DB::select($sql);
                $heading=Array();
                $questions = array();
                foreach($Header as $head){
                    $sql = " select id,assessment_question_id, last_update from assessment_question_attempt  where assessment_id = ".(int)$assessment_id." order by id " ;
					$Attempts=DB::select($sql);
					$questions = array();
					 $i = 0;
					foreach($Attempts as $attempt){

                        $sql="select  id, title, description
                            from  assessment_questions
                            where id = ".(int)$attempt->assessment_question_id."";
							$Quest=DB::select($sql);
							foreach($Quest as $q){
                                if($i==0) $form="Form - 1A";
								if($i==1) $form="Form - 1B";
								if($i==2) $form="Form - 2";
                                $questions[]=array(
                                    'formName'=>$form,
                                    'assessment_question_id'=>$q->id,
                                    'title'=>$q->title,
                                    'description'=>$q->description,
                                    'response'=>array(),
                                    'posts'=>array(),

                                );
                            }
                            $i++;
                    }
                }
                $heading = Array(
                    "student_name"=> $account->first_name. " ". $account->last_name,
                   "teacher_name"=> $head->teacher_name,
                   "email"=> $account->email,
                   "class_name"=> $head->class_name,
                   "standard"=> $head->standard,
                   "details"=> $questions

                    );
            }
           // print_r(); exit;
		return $heading;

	}
	public function upload(Assessment $assessment, Account $account, StudentWorkProductLog $log){

		if ($log->google_drive_response !== NULL) {
			return $log->google_drive_response;
		}
        $client = setGoogleClient(getOAuthToken());
        $service = new Google_Service_Drive($client);
        $workProduct=$this->WorkProdcutInfomation($assessment->id,$account->id,$assessment,$account);

      //  $url = "https://api.smartschools80.com/api/assessment/workproduct/student/attempts/537/389";
       // $username = "prometheus";
       // $password = "s@9876sPQf4567tuM4lD1T4M4Dr3-!";

            $data = [];
            $data['student_name'] = $workProduct['student_name'];
            $data['class_name'] = $workProduct['class_name'];
            $data['teacher_name'] = $workProduct['teacher_name'];
            $data['standard'] = $workProduct['standard'];
            $data['email'] = $workProduct['email'];
            $data['form_1A'] = [
                'question' => '',
                'answer' => '',
                'thinking' => ''
            ];
            $data['form_1B'] = [
                'question' => '',
                'answer' => '',
                'thinking' => ''
            ];
            $data['form_2'] = [
                'question' => '',
                'answer' => '',
                'thinking' => ''
            ];
            foreach ($workProduct['details'] as $detail) {
                if ($detail['formName'] === 'Form - 1A' ) {

                    $ans='';
                    $thinking='';

                    if(isset($detail['posts'][0]['body']["response"])) $ans=$detail['posts'][0]['body']["response"];
                    if(isset($detail['posts'][0]['body']["comment"])) $thinking=$detail['posts'][0]['body']["comment"];
                    $data['form_1A'] = [
                        'question' => $detail['description'],
                        'title' => $detail['title'],
                        'answer' => $ans,
                        'thinking' => $thinking
                    ];
                }
                if ($detail['formName'] === 'Form - 1B' ) {
                    $ans='';
                    $thinking='';
                    if(isset($detail['posts'][0]['body']["response"])) $ans=$detail['posts'][0]['body']["response"];
                    if(isset($detail['posts'][0]['body']["comment"])) $thinking=$detail['posts'][0]['body']["comment"];
                    $data['form_1B'] = [
                        'question' => $detail['description'],
                        'title' => $detail['title'],
                        'answer' => $ans,
                        'thinking' => $thinking
                    ];
                }
                if ($detail['formName'] === 'Form - 2') {
                    $ans='';
                    $thinking='';
                    if(isset($detail['posts'][0]['body']["response"])) $ans=$detail['posts'][0]['body']["response"];
                    if(isset($detail['posts'][0]['body']["comment"])) $thinking=$detail['posts'][0]['body']["comment"];
                    $data['form_2'] = [
                        'question' => $detail['description'],
                        'title' => $detail['title'],
                        'answer' => $ans,
                        'thinking' => $thinking
                    ];
                }
            }


            $pdf = PDF::loadView('tap_template', $data);
            $fileName="{$assessment->title}_student_work_product_{$assessment->id}{$account->id}";
			$fileName = str_replace(" ", "_", $fileName);
			$fileName=preg_replace('/[^A-Za-z0-9\-]/', '_', $fileName);
			$fileName = $fileName.".pdf";
            $path = public_path("assessments/work_products/$fileName");
			if(is_file($path)) unlink($path);
			$pdf->save($path);
            $file = new Google_Service_Drive_DriveFile(['name' => $fileName]);
            $content = file_get_contents($path);
            $mimeType = mime_content_type($path);
            $options = [
                'data' => $content,
                'mimeType' => $mimeType,
                'fields' => 'id'
            ];
            $file = $service->files->create($file, $options);
            $log->google_drive_response = [
            	'id' => $file->id
            ];
            $log->save();

            return $file;

        return false;
	}

	public function submitResponse(Assessment $assessment, Account $account, StudentWorkProductLog $log){
		$client = setGoogleClient(getOAuthToken());
		$classroom = new Google_Service_Classroom($client);
        $courseWork = $assessment->courseWork;
		$submissions = $classroom->courses_courseWork_studentSubmissions->listCoursesCourseWorkStudentSubmissions(
            $courseWork->external_course_id,
            $courseWork->external_course_work_id
        );
        $submission = $submissions[0];
        $modifyAttachmentRequest = new Google_Service_Classroom_ModifyAttachmentsRequest();
        $attachment = new Google_Service_Classroom_Attachment();
        $driveFile = new Google_Service_Classroom_DriveFile();
        $driveFile->setId($log->google_drive_response['id']);
        $attachment->setDriveFile($driveFile);
        $modifyAttachmentRequest->setAddAttachments([$attachment]);
        $classroom->courses_courseWork_studentSubmissions->modifyAttachments(
            $courseWork->external_course_id,
            $courseWork->external_course_work_id,
            $submission->id,
            $modifyAttachmentRequest
        );
        $response = $classroom->courses_courseWork_studentSubmissions->get(
            $courseWork->external_course_id,
            $courseWork->external_course_work_id,
            $submission->id,
        );
        $log->student_submission_response = $response;
        $log->save();
        //save to course work response
        $courseWorkAssessmentResponse = CourseworkAssessmentResponse::firstOrCreate(
            [
                'course_work_id' => $courseWork->id,
                'external_id' => $submission->id,
                'user_id' => $account->id
            ]
        );
        $courseWorkAssessmentResponse->response = $log->student_submission_response["assignmentSubmission"]["attachments"][0]['driveFile']['alternateLink'];
        $courseWorkAssessmentResponse->sub_grade = NULL;
        $courseWorkAssessmentResponse->meta_data = $log->student_submission_response;
        $courseWorkAssessmentResponse->save();
        return true;
	}

    public function turnIn(Assessment $assessment, Account $account, StudentWorkProductLog $log){
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $courseWork = $assessment->courseWork;
        $response = $classroom->courses_courseWork_studentSubmissions->turnIn(
            $courseWork->external_course_id,
            $courseWork->external_course_work_id,
            $log->student_submission_response["id"],
            new Google_Service_Classroom_TurnInStudentSubmissionRequest()
        );
        $log->turn_in_response = $response;
        $log->save();
        return true;
    }

    public function returnToStudent($courseId, $courseWorkId, $id){
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $response = $classroom->courses_courseWork_studentSubmissions->returnCoursesCourseWorkStudentSubmissions(
            $courseId,
            $courseWorkId,
            $id,
            new Google_Service_Classroom_ReturnStudentSubmissionRequest()
        );
        $courseWorkAssessmentResponse = CourseworkAssessmentResponse::where("external_id", $id)->first();
        if($courseWorkAssessmentResponse){
            $courseWorkAssessmentResponse->returned_to_student = true;
            $courseWorkAssessmentResponse->save();
        }
        return $response;
    }

}
