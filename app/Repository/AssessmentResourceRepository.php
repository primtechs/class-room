<?php

namespace App\Repository;

use Illuminate\Support\Facades\Log;
use App\Models\FileType;
use Google_Service_Drive;
use Storage;

class AssessmentResourceRepository extends Repository
{

    public function model()
    {
        return 'App\Models\AssessmentResource';
    }

    public function index($type)
    {

    }

    public function create(array $data)
    {
        return parent::create($data);
    }

    public function update(array $data, $id, $attribute = "id")
    {
        return parent::update($data, $id, $attribute);
    }

    public function uploadResources($id,$question){
        $resources = [];
        $google_drive_links = $question['google_drive_link'] ?? [];
        $client = setGoogleClient(getOAuthToken());
        $drive = new Google_Service_Drive($client);
        $files = $drive->files;
        foreach ($google_drive_links as $link){
            if ($link['type'] === 'aws') {
                $type = NULL;
                $awsUrl = NULL;
                $fileType = FileType::where('file_type', $link['mimeType'])->first();
                if($fileType){
                    $type = $fileType->fclass;
                    $path = "documents";
                    if ($fileType->fclass === "video" || $fileType->fclass === "audio") {
                        $path = "videos";
                    }else if($fileType->fclass === "image"){
                        $path = "images";
                    }
                    $file = $link['target_file'];
                    $fileName = "{$id}_{$link['name']}";
                    $file->storeAs($path, "{$id}_{$link['name']}", ['disk' => 's3']);
                    $awsUrl = env('AWS_LOCATION')."/$path/$fileName";
                    $resources[] = [
                        'name' => $fileName,
                        'type' => $type,
                        'description' => null,
                        'res_url' => $awsUrl,
                        'url' => $awsUrl
                    ];

                }else{
                    Log::warning("Question create warning {$link['mimeType']} Not found");
                }
            }else{
                $resources[] = $link;
            }
        }
        return $resources;
    }

}
