<?php

namespace App\Repository;
use App\Models\Course;
use App\Models\Announcement;
use Google_Service_Classroom;

class AnnouncementRepository extends Repository
{

    public function model()
    {
        return 'App\Models\Announcement';
    }

    public function index($type)
    {

    }

    public function create(array $data)
    {
        $account = getAccountDetails();
        $courseId = $data['courseId'];
        $course = Course::where("external_course_id", $courseId)->first();
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $announcement = setAnnouncement($data);
        $response = $classroom->courses_announcements->create($courseId, $announcement);
        if ($course !== null){
            $announcementData = [
              'class_id' => $course->id,
              'account_id' => $account->id,
              'announcement_metadata' => $response
            ];
            parent::create($announcementData);
        }
        return $response;
    }

    public function delete($id){
        $announcement = Announcement::externalId("=", $id)->first();
        if ($announcement) {
            $account = getAccountDetails();
            $client = setGoogleClient(getOAuthToken());
            $classroom = new Google_Service_Classroom($client);
            $metaData = $announcement->announcement_metadata;
            $response = $classroom->courses_announcements->delete($metaData['courseId'], $metaData['id']);
            $announcement->delete();
            return [
                'status' => 'success',
                'message' => 'Announcement Deleted Successfully',
                'announcement' => null
            ];
        }

        return [
            'status' => 'error',
            'message' => 'Announcement was not Found',
            'announcement' => null
        ];
    }
}
