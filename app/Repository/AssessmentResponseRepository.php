<?php

namespace App\Repository;

class AssessmentResponseRepository extends Repository
{

    public function model()
    {
        return 'App\Models\AssessmentResponse';
    }

    public function index($type)
    {

    }

    public function create(array $data)
    {
        return parent::create($data);
    }

    public function update(array $data, $id, $attribute = "id")
    {
        return parent::updateOrCreate($data, $id, $attribute);
    }
}
