<?php

namespace App\Repository;

use App\Models\QuestionResource;
use App\Models\QuestionResponse;
use App\Models\FileType;
use App\Models\TeacherQuestionBank;
use Illuminate\Support\Facades\Log;
use Google_Service_Drive;
use Storage;

class QuestionRepository extends Repository
{

    public function model()
    {
        return 'App\Models\Question';
    }

    public function index($type)
    {

    }

    public function createFromAssessment($data, $assessmentQuestion) {

        $user = request()->user();
        $accountType = strtolower($user->accountType->name);
        $newQuestion = parent::create([
            'type_id' => $assessmentQuestion["type_id"],
            'topic_id' => $assessmentQuestion["topic_id"],
            'title' => $assessmentQuestion["title"] ?? '',
            'description' => $assessmentQuestion["description"] ?? '',
            'standard_id' => $data["standard_id"],
            'question_subject_id' => $data["subject_id"],
            'question_grade_id' => $data["grade_id"],
            'status' => "Not Approved",
            'total_approval' => 0,
            'account_id' => $data['account_id'],
            'average_rating' => 0.0,
            'rubrics' => $assessmentQuestion["rubrics"] ?? [],
            'response_rubric' => $assessmentQuestion['responses'] ?? [],
            'open_response' => $assessmentQuestion["open_response"] ?? [],
            'open_response_grade' => $assessmentQuestion["open_response_grade"],
        ]);
        //create question responses
        $responses = $assessmentQuestion['responses'] ?? [];
        foreach ($responses as $response){
            if($response){
                QuestionResponse::create([
                    'question_id' => $newQuestion->id,
                    "description" => $response['description'],
                    "is_valid" => $response["is_valid"],
                    "rubric" => $response["rubric"],
                    "sub_grade" => $response["sub_grade"],
                    "last_udate" => date("Y-m-d H:i:s"),
                    "title" => $response['title'],
                    "match_label" => $response['match_label'],
                ]);
            }
        }
//        create question responses
        $google_drive_links = $assessmentQuestion['google_drive_link'] ?? [];
        $client = setGoogleClient(getOAuthToken());
        $drive = new Google_Service_Drive($client);
        $files = $drive->files;
        foreach ($google_drive_links as $link){
            if ($link) {
                $type = NULL;
                $awsUrl = NULL;
                $fileType = FileType::where('file_type', $link['mimeType'])->first();
                if($fileType){
                    $type = $fileType->fclass;
                }else{
                    Log::warning("Question create warning {$link['mimeType']} Not found");
                }
                if($fileType){
                    $path = "documents";
                    if ($fileType->fclass === "video" || $fileType->fclass === "audio") {
                        $path = "videos";
                    }else if($fileType->fclass === "image"){
                        $path = "images";
                    }
                    if($link['type'] === 'aws'){
                        $file = $link['target_file'];
                        $fileName = "{$newQuestion->id}_{$link['name']}";
                        $file->storeAs($path, "{$newQuestion->id}_{$link['name']}", ['disk' => 's3']);
                        $awsUrl = env('AWS_LOCATION')."/$path/$fileName";
                    }else{
                        if(isFromGoogleDocs($link['url'])){
                            $file = $files->export($link['id'],"application/vnd.openxmlformats-officedocument.wordprocessingml.document" , ['alt' => 'media']);
                        }else{
                            $file = $files->get($link['id'], ['alt' => 'media']);
                        }
                        $name = explode(".", $link['name'])[0];
                        $fileName = "{$newQuestion->id}_{$name}{$fileType->extension}";
                        Storage::disk('s3')->put("$path/$fileName", $file->getBody()->getContents());
                        $awsUrl = env('AWS_LOCATION')."/$path/$fileName";
                    }
                    QuestionResource::create([
                        'name' => $link['name'],
                        'res_url' => $awsUrl,
                        'description' => $link['description'] ?? '',
                        'question_id' => $newQuestion->id,
                    ]);
                }
            }
        }
        //Teacher question Bank
        if ($accountType == "teachers" || $accountType == "admin") {
            TeacherQuestionBank::create([
                'account_id' => $user->id, 
                "question_id" => $newQuestion->id
            ]);
        }
        //fresh and return newly created question
        return $newQuestion;
    }

    public function create(array $data)
    {
        return parent::create($data);
    }

    public function update(array $data, $id, $attribute = "id")
    {
        return parent::updateOrCreate($data, $id, $attribute);
    }
}
