<?php

namespace App\Repository;
use Google_Service_Classroom;
use Illuminate\Container\Container as App;
use App\Models\Topic;
use App\Models\TeacherClass;

class TopicRepository extends Repository
{

    public function model()
    {
        return 'App\Models\Topic';
    }

    public function topicsByCourse($courseId){
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $response = $classroom->courses_topics->listCoursesTopics($courseId);
        $topics = [];
        foreach ($response->topic as $topic){
            $local = Topic::where('external_course_id', '!=', NULL)
                            ->where(['external_course_id'=> $courseId, 'name'=>$topic->name])->first();
            if($local){
                $local->external_metadata = $topic;
                $local->save();
            }else{
                $local = Topic::create([
                    'name' => $topic->name,
                    'description' => '',
                    'external_course_id' => $topic->courseId,
                    'external_metadata' => $topic
                ]);
            }
            $topics[] = $local->fresh();
        }
        return response()->json([
            'status' => 'success',
            'topics' => $topics,
            'message' => 'Topics List'
        ], 200);
    }

    public function topics(){
        $account = request()->user();
        $teacherClasses = TeacherClass::with("course")->where(["account_id" => $account->id, "status" => true])->get();
        $courses = [];
        $teacherClasses->each(function ($teacherClass,$key) use(&$courses) {
            $courses[] = $teacherClass->course->external_course_id;
        });
        $topics = Topic::whereIn("external_course_id", $courses)->get();
        return response()->json([
            'status' => 'success',
            'topics' => $topics,
            'message' => 'Topics List'
        ], 200);
    }


    public function create(array $data)
    {
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $topic = setCourseTopic($data);
        $topic = $classroom->courses_topics->create($data['courseId'], $topic);
        $data['external_course_id'] = $data['courseId'];
        $data['external_metadata'] = $topic;
        $local = parent::create($data);
        return $local;
    }

    public function updateTopic(array $data, Topic $local){
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $topic = setCourseTopic($data);
        $topic = $classroom->courses_topics->patch($data['courseId'], $data['topicId'] , $topic, ['updateMask' => "name"]);
        //update local database;
        $local->external_metadata = $topic;
        $local->name = $data['name'];
        $local->save();
        return $local->fresh();
    }

    public function deleteTopic(array $data){
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $response =$classroom->courses_topics->delete($data['courseId'], $data['topicId']);
        return $response;
    }
}
