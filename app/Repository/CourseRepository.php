<?php

namespace App\Repository;
use App\Models\Course;
use App\Models\Topic;
use App\Models\TeacherClass;
use Illuminate\Support\Facades\DB;
use Google_Service_Classroom;
use Illuminate\Container\Container as App;

class CourseRepository extends Repository
{

    public function model()
    {
        return 'App\Models\Course';
    }

    public function getAllCourses($type , $user = NULL)
    {
        $courses = [];
        if ($user === NULL) {
            $user = request()->user();
        }
        $id = $user->id;
        if($type && $type === 'STUDENT') {
            $responses = DB::table('student_class')
                ->select('classes.*')
                ->join('classes', 'classes.id', '=', 'student_class.class_id')
                ->where('student_class.account_id', $id)
                ->where('student_class.status', true)
                ->distinct()
                ->get();
        } else {
            $responses = DB::table('teacher_class')
                ->select('classes.*')
                ->join('classes', 'classes.id', '=', 'teacher_class.class_id')
                ->where('teacher_class.account_id', $id)
                ->where('teacher_class.status', true)
                ->distinct()
                ->get();
        }
        foreach ($responses as $response) {
            $data = json_decode($response->external_course_metadata, 1);
            if ($data && count($data) > 0) {
                $data['local'] = $response;
                $courses[] = $data;
            }
        }
        return $courses;
    }

    public function create(array $data)
    {
        $client = setGoogleClient(getOAuthToken());
        $classroom = new Google_Service_Classroom($client);
        $course = setCourse($data);
        $response  = $classroom->courses->create($course);

        $courseData['school_id'] = getSchool();
        $courseData['name'] = $response->name;
        $courseData['class_name'] = $response->name;
        $courseData['level_id'] = $data['level_id'];
        $courseData['external_course_id'] = $response->id;
        $courseData['external_course_metadata'] = json_encode($response);
        $courseData['tag'] = $data['tag'] ?? null;
        $courseData['subject'] = $data['subject'] ?? null;
        $courseData['external_user_id'] = $response->ownerId;
        $courseData['external_course_alias_metadata'] = json_encode($response);
        $course = parent::create($courseData);
        $response->local = $course->toArray();
        //create default topic for class
        $topicData = setCourseTopic(['name' => "Assessment - {$response->name}"]);
        $topic = $classroom->courses_topics->create($courseData['external_course_id'], $topicData);
        Topic::create([
            'name' => "Assessment - {$response->name}",
            'description' => '',
            'external_course_id' =>  $courseData['external_course_id'],
            'external_metadata' => $topic
        ]);
        //end of default topic
        return [
            'response' => $response,
            'course' => $course
        ];
    }

    public function update(array $data, $id, $attribute = "id")
    {
        $client = setGoogleClient(getOAuthToken());
        $service = new Google_Service_Classroom($client);
        $course = setCourse($data);
        $response  = $service->courses->update($id,  $course);
        //local course update
        $localCourse = Course::find($data["local"]["id"]);
        $localCourse->name = $data['name'];
        $localCourse->level_id = $data['level_id'];
        $localCourse->class_name = $data['name'];
        $localCourse->subject = $data['subject'];
        $localCourse->external_course_metadata = json_encode($response);
        $localCourse->external_course_alias_metadata = json_encode($response);
        $localCourse->save();
        return $response;
    }

    public function syncCourse($response) {
        $courseData['school_id'] = getSchool();
        $courseData['name'] = $response->name;
        $courseData['class_name'] = $response->name;
        $courseData['external_course_id'] = $response->id;
        $courseData['external_course_metadata'] = json_encode($response);
        $courseData['tag'] = $data['tag'] ?? null;
        $courseData['subject'] = $data['subject'] ?? $response->name;
        $courseData['external_user_id'] = $response->ownerId;
        $courseData['external_course_alias_metadata'] = json_encode($response);

        $course = parent::create($courseData);
        $account = request()->user();
        TeacherClass::create([
            'class_id' => $course->id,
            'account_id' => $account->id,
            'status' => true,
            'external_teacher_metadata' => $account->external_user_profile
        ]);
        return $course;
    }
}
